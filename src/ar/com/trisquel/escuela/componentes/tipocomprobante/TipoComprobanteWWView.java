package ar.com.trisquel.escuela.componentes.tipocomprobante;

import ar.com.trisquel.escuela.data.contenedor.TipoComprobanteContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.TipoComprobanteRN;
import ar.com.trisquel.escuela.data.tablas.TipoComprobante;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class TipoComprobanteWWView extends VerticalLayout implements ClickListener,View {

	public static final String VIEW = "TipoComprobanteWW";
	private static final String LINK_VER = "LINK_VER";
	private static final String COLUMNA_TIPO = "COLUMNA_TIPO";
	private static final String COLUMNA_SALDO = "COLUMNA_SALDO";
	private static final String COLUMNA_OPERACION = "COLUMNA_OPERACION";
	private static final String COLUMNA_ACTIVO = "COLUMNA_ACTIVO";
	private ButtonIcon botonFiltros;
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonEliminar;
	private ButtonIcon botonRefrescar;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private TextField filtroNombre;
	private CheckBox filtroActivo;
	private Table table;
	private VerticalLayout layoutFiltros;
	private UI ui;

	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass(), ui);
		if (!control.isAcceso()) {
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		} else {
			onDraw();
		}
	}

	public void onDraw() {
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(EtiquetasLabel.getTipoComprobantes().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true, this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonFiltros = new ButtonIcon(ButtonIcon.BUSCAR, true, this);
		botonFiltros.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonFiltros);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		//botonAgregar.setEnabled(false);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		//botonModificar.setEnabled(false);
		layoutBotones.addComponent(botonModificar);
		botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true, this);
		botonEliminar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonEliminar);
		botonEliminar.setEnabled(false);
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true, this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonExcel);
		botonExcel.setEnabled(false);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true, this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonPDF);
		botonPDF.setEnabled(false);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		table = new Table();
		ArmaTabla();
		layout.addComponent(table);
		Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonFiltros) {
			layoutFiltros.setVisible(!layoutFiltros.isVisible());
		} else if (event.getButton() == botonRefrescar) {
			CreaDataSource();
		} else if (event.getButton() == botonModificar) {
			TipoComprobante tipoComprobante = (TipoComprobante) table.getValue();
			if (tipoComprobante != null) {
				Window windowABM = CreaWindowABM();
				TipoComprobanteABM tipoComprobanteABM = new TipoComprobanteABM(tipoComprobante,Dominios.AccesoADatos.UPDATE, ui, windowABM);
				windowABM.setContent(tipoComprobanteABM);
				windowABM.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						CreaDataSource();
					}
				});
				ui.addWindow(windowABM);
			}
		} else if (event.getButton() == botonAgregar) {
			Window windowABM = CreaWindowABM();
			TipoComprobanteABM tipoComprobanteABM = new TipoComprobanteABM(null,Dominios.AccesoADatos.INSERT, ui, windowABM);
			windowABM.setContent(tipoComprobanteABM);
			windowABM.addCloseListener(new CloseListener() {
				@Override
				public void windowClose(CloseEvent e) {
					CreaDataSource();
				}
			});
			ui.addWindow(windowABM);
		} else if (event.getButton() == botonEliminar
				|| event.getButton() == botonExcel
				|| event.getButton() == botonPDF) {
			/**
			 * No hace nada
			 */
		}

	}

	private void ArmaFiltros() {
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		layoutFiltros.setVisible(false);
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(6);
		//Nombre y Apellido
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label label = new Label("Nombre:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("60px");
		formFiltro.addComponent(label);
		VerticalLayout layout = new VerticalLayout();
		layout.setWidth("400px");
		filtroNombre = new TextField();
		filtroNombre.setNullRepresentation("");
		filtroNombre.setNullSettingAllowed(false);
		filtroNombre.addStyleName(R.Style.EDITABLE);
		filtroNombre.addStyleName(R.Style.LABEL_BOLD);
		filtroNombre.setWidth("300px");
		layout.addComponent(filtroNombre);
		formFiltro.addComponent(layout);
		//Activo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Activo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		filtroActivo = new CheckBox();
		filtroActivo.setValue(true);
		formFiltro.addComponent(filtroActivo);
		layoutFiltros.addComponent(formFiltro);

	}

	private void ArmaTabla() {
		table.setHeight("550px");
		table.setWidth("985px");
		table.setSelectable(true);
		table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final TipoComprobante tipoComprobante = (TipoComprobante) itemId;
				Button botonVer = new Button(tipoComprobante.getNombre());
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = CreaWindowABM();
						TipoComprobanteABM tipoComprobanteABM = new TipoComprobanteABM(tipoComprobante,Dominios.AccesoADatos.SELECT, ui, windowABM);
						windowABM.setContent(tipoComprobanteABM);
						ui.addWindow(windowABM);
					}
				});
				return botonVer;
			}
		});
		table.addGeneratedColumn(COLUMNA_OPERACION, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				TipoComprobante tipoComprobante = (TipoComprobante) itemId;
				ComboBox comboOperacion = Dominios.TipoComprobante.Operacion.CreaComboBox(false);
				comboOperacion.setWidth("100px");
				comboOperacion.setValue(tipoComprobante.getOperacion());
				comboOperacion.setReadOnly(true);
				return comboOperacion;
			}
		});
		table.addGeneratedColumn(COLUMNA_SALDO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				TipoComprobante tipoComprobante = (TipoComprobante) itemId;
				ComboBox comboSaldo = Dominios.TipoComprobante.Saldo.CreaComboBox(false);
				comboSaldo.setWidth("100px");
				comboSaldo.setValue(tipoComprobante.getSaldo());
				comboSaldo.setReadOnly(true);
				return comboSaldo;
			}
		});
		table.addGeneratedColumn(COLUMNA_TIPO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				TipoComprobante tipoComprobante = (TipoComprobante) itemId;
				ComboBox comboTipo = Dominios.TipoComprobante.Tipo.CreaComboBox(false);
				comboTipo.setWidth("150px");
				comboTipo.setValue(tipoComprobante.getTipo());
				comboTipo.setReadOnly(true);
				return comboTipo;
			}
		});
		table.addGeneratedColumn(COLUMNA_ACTIVO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				TipoComprobante tipoComprobante = (TipoComprobante) itemId;
				CheckBox checkActivo = new CheckBox();
				checkActivo.setValue(tipoComprobante.isActivo());
				checkActivo.setReadOnly(true);
				return checkActivo;
			}
		});
		CreaDataSource();
	}

	private void CreaDataSource() {
		table.setContainerDataSource(TipoComprobanteContenedor.LeeContainerXNombreXActivo(filtroNombre.getValue(), filtroActivo.getValue()));
		table.setVisibleColumns((Object[]) new String[] { LINK_VER, COLUMNA_TIPO,COLUMNA_SALDO ,COLUMNA_OPERACION ,COLUMNA_ACTIVO, "id" });
		table.setColumnHeaders("Tipo de Comprobante", "Tipo", "Saldo","Operación", "Activo", "Id");
	}

	public static Window CreaWindowABM() {
		Window windowABM = new Window(EtiquetasLabel.getTipoComprobantes());
		windowABM.setModal(true);
		windowABM.setResizable(false);
		windowABM.setDraggable(false);
		windowABM.setWidth("500px");
		windowABM.setHeight("250px");
		windowABM.center();
		windowABM.setCloseShortcut(KeyCode.ESCAPE, null);
		return windowABM;
	}
	
	public static void CreaTipoComprobantesDefecto(){
		TipoComprobanteRN.Add(TipoComprobanteRN.Inizializate("Factura de Venta", Dominios.TipoComprobante.Tipo.FACTURA, Dominios.TipoComprobante.Saldo.DEUDOR, Dominios.TipoComprobante.Operacion.VENTA));
		
		TipoComprobanteRN.Add(TipoComprobanteRN.Inizializate("Factura de Compra", Dominios.TipoComprobante.Tipo.FACTURA, Dominios.TipoComprobante.Saldo.ACREEDOR, Dominios.TipoComprobante.Operacion.COMPRA));
		TipoComprobanteRN.Add(TipoComprobanteRN.Inizializate("Nota de Debito Compra", Dominios.TipoComprobante.Tipo.NOTA_DE_DEBITO, Dominios.TipoComprobante.Saldo.ACREEDOR, Dominios.TipoComprobante.Operacion.COMPRA));
		TipoComprobanteRN.Add(TipoComprobanteRN.Inizializate("Nota de Credito Compra", Dominios.TipoComprobante.Tipo.NOTA_DE_CREDITO, Dominios.TipoComprobante.Saldo.DEUDOR, Dominios.TipoComprobante.Operacion.COMPRA));
		
		TipoComprobanteRN.Add(TipoComprobanteRN.Inizializate("Gastos", Dominios.TipoComprobante.Tipo.COMPROBANTE_CAJA, Dominios.TipoComprobante.Saldo.DEUDOR, Dominios.TipoComprobante.Operacion.CAJA));
		TipoComprobanteRN.Add(TipoComprobanteRN.Inizializate("Ingresos", Dominios.TipoComprobante.Tipo.COMPROBANTE_CAJA, Dominios.TipoComprobante.Saldo.ACREEDOR, Dominios.TipoComprobante.Operacion.CAJA));
	}

}
