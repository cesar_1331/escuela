package ar.com.trisquel.escuela.componentes.tipocomprobante;

import ar.com.trisquel.escuela.data.reglasdenegocio.TipoComprobanteRN;
import ar.com.trisquel.escuela.data.tablas.TipoComprobante;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class TipoComprobanteABM extends VerticalLayout implements ClickListener {

	private TextField id;
	private TextField nombre;
	private ComboBox tipo;
	private ComboBox saldo;
	private ComboBox operacion;
	private CheckBox activo;
	private ButtonIcon ButtonGuardar;
	private ButtonIcon ButtonDescartar;
	private UI ui;
	private String modo;
	private Window window;
	private TipoComprobante tipoComprobante;

	public TipoComprobanteABM(TipoComprobante tipoComprobante, String modo, UI ui, Window window) {
		this.ui = ui;
		this.modo = modo;
		this.window = window;
		this.tipoComprobante = tipoComprobante;
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setWidth("100%");
		layout.setColumns(3);
		addComponent(layout);
		//Id
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelId = new Label("Id:");
		labelId.addStyleName(R.Style.EDITABLE_LABEL);
		labelId.setWidth("60px");
		layout.addComponent(labelId);
		HorizontalLayout layoutId = new HorizontalLayout();
		layoutId.setWidth("400px");
		id = new TextField();
		id.addStyleName(R.Style.EDITABLE);
		id.addStyleName(R.Style.LABEL_BOLD);
		layoutId.addComponent(id);
		//Botones
		HorizontalLayout layoutBotones = new HorizontalLayout();
		ButtonGuardar = new ButtonIcon(ButtonIcon.GUARDAR, true, this);
		ButtonGuardar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonGuardar);
		ButtonDescartar = new ButtonIcon(ButtonIcon.DESCARTAR, true, this);
		ButtonDescartar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonDescartar);
		layoutId.addComponent(layoutBotones);
		layoutId.setComponentAlignment(layoutBotones, Alignment.TOP_RIGHT);
		layout.addComponent(layoutId);
		//Nombre
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Nombre:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		nombre = new TextField();
		nombre.addStyleName(R.Style.EDITABLE);
		nombre.addStyleName(R.Style.LABEL_BOLD);
		nombre.addStyleName(R.Style.TEXTO_MAYUSCULA);
		nombre.setWidth("300px");
		nombre.setMaxLength(25);
		layout.addComponent(nombre);
		//Tipo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Tipo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		tipo = Dominios.TipoComprobante.Tipo.CreaComboBox(false);
		tipo.setWidth("150px");
		layout.addComponent(tipo);
		//Saldo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Saldo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		saldo = Dominios.TipoComprobante.Saldo.CreaComboBox(false);
		saldo.setWidth("100px");
		layout.addComponent(saldo);
		//Operacion
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Operación:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		operacion = Dominios.TipoComprobante.Operacion.CreaComboBox(false);
		operacion.setWidth("100px");
		layout.addComponent(operacion);
		//Activo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Activo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		activo = new CheckBox();
		layout.addComponent(activo);		
		
		if (modo.equals(Dominios.AccesoADatos.INSERT)) {
			id.setReadOnly(true);
			activo.setValue(true);
			activo.setReadOnly(true);
			ButtonGuardar.setVisible(true);
			ButtonDescartar.setVisible(true);
		}
		else{
			id.setValue(String.valueOf(tipoComprobante.getId()));
			nombre.setValue(tipoComprobante.getNombre());
			tipo.setValue(tipoComprobante.getTipo());
			saldo.setValue(tipoComprobante.getSaldo());
			operacion.setValue(tipoComprobante.getOperacion());
			activo.setValue(tipoComprobante.isActivo());
			if (modo.equals(Dominios.AccesoADatos.SELECT)) {
				id.setReadOnly(true);
				nombre.setReadOnly(true);
				tipo.setReadOnly(true);
				saldo.setReadOnly(true);
				operacion.setReadOnly(true);
				activo.setReadOnly(true);
				ButtonGuardar.setVisible(false);
				ButtonDescartar.setVisible(false);
			} else if (modo.equals(Dominios.AccesoADatos.UPDATE)) {
				id.setReadOnly(true);
				tipo.setReadOnly(true);
				saldo.setReadOnly(true);
				operacion.setReadOnly(true);
				ButtonGuardar.setVisible(true);
				ButtonDescartar.setVisible(true);
			}
		}
		nombre.focus();
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == ButtonDescartar) {
			ui.removeWindow(window);
		} else if (event.getButton() == ButtonGuardar) {
			if (tipoComprobante == null){
				tipoComprobante = TipoComprobanteRN.Inizializate();
			}
			int saldoNro = 0;
			try{
				if (saldo.getValue() != null)
					saldoNro = Integer.parseInt(String.valueOf(saldo.getValue()));
			}catch(Exception e){}
			tipoComprobante.setNombre(nombre.getValue());
			tipoComprobante.setTipo(String.valueOf(tipo.getValue()));
			tipoComprobante.setSaldo(saldoNro);
			tipoComprobante.setOperacion(String.valueOf(operacion.getValue()));
			tipoComprobante.setActivo(activo.getValue());
			RespuestaEntidad respuesta = TipoComprobanteRN.Validate(tipoComprobante ,modo);
			if (respuesta.isError()) {
				MessageBox.showHTML(Icon.ERROR, "", respuesta.getMsgError(),ButtonId.OK);
			} else {
				MessageBox.showHTML(Icon.QUESTION, "",MensajeLabel.getConfirmaAccion(),
						new MessageBoxListener() {
					@Override
					public void buttonClicked(ButtonId buttonType) {
						if (buttonType == ButtonId.SAVE) {
							RespuestaEntidad respuesta = TipoComprobanteRN.Save(tipoComprobante);
							if (respuesta.isError()) {
								MessageBox.showHTML(Icon.ERROR, "",respuesta.getMsgError(),ButtonId.OK);
							} else {
								ui.removeWindow(window);
							}
						}
					}
				}, ButtonId.SAVE, ButtonId.CANCEL);
			}
		}
	}

}
