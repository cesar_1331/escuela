package ar.com.trisquel.escuela.componentes.parametro;

import ar.com.trisquel.escuela.data.reglasdenegocio.ParametroRN;
import ar.com.trisquel.escuela.data.tablas.Parametro;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class EscuelaView extends VerticalLayout implements ClickListener, View {
	public static final String VIEW = "EscuelaWW";
	private TextField nombre;
	private TextField domicilio;
	private TextField ciudad;
	private TextField provincia;
	private ComboBox cantidadPeriodos;
	private ButtonIcon ButtonModificar;
	private ButtonIcon ButtonGuardar;
	private ButtonIcon ButtonDescartar;
	private UI ui;

	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass(), ui);
		if (!control.isAcceso()) {
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		} else {
			onDraw();
		}
	}

	public void onDraw() {
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(EtiquetasLabel.getDatosEscuela().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		addComponent(Separator);
		
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		HorizontalLayout layoutForm = new HorizontalLayout();
		addComponent(layoutForm);
		GridLayout layout = new GridLayout();
		layout.setWidth("100%");
		layout.setColumns(3);
		layoutForm.addComponent(layout);
		//Nombre
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Nombre:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("90px");
		layout.addComponent(label);
		HorizontalLayout layoutId = new HorizontalLayout();
		layoutId.setWidth("800px");
		nombre = new TextField();
		nombre.addStyleName(R.Style.EDITABLE);
		nombre.setWidth("300px");
		nombre.addStyleName(R.Style.LABEL_BOLD);
		nombre.addStyleName(R.Style.TEXTO_MAYUSCULA);
		nombre.setMaxLength(100);
		layoutId.addComponent(nombre);
		layout.addComponent(layoutId);
		//Domicilio
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Domicilio:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		domicilio = new TextField();
		domicilio.addStyleName(R.Style.EDITABLE);
		domicilio.addStyleName(R.Style.LABEL_BOLD);
		domicilio.setWidth("400px");
		layout.addComponent(domicilio);
		//Ciudad
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Ciudad:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		ciudad = new TextField();
		ciudad.addStyleName(R.Style.EDITABLE);
		ciudad.addStyleName(R.Style.LABEL_BOLD);
		ciudad.addStyleName(R.Style.TEXTO_MAYUSCULA);
		ciudad.setWidth("300px");
		ciudad.setMaxLength(40);
		layout.addComponent(ciudad);
		//Provincia
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Domicilio:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		provincia = new TextField();
		provincia.addStyleName(R.Style.EDITABLE);
		provincia.addStyleName(R.Style.LABEL_BOLD);
		provincia.addStyleName(R.Style.TEXTO_MAYUSCULA);
		provincia.setWidth("300px");
		provincia.setMaxLength(40);
		layout.addComponent(provincia);	
		//Cantidad de Periodos
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Cantidad de Periodos:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		cantidadPeriodos  = Dominios.Periodos.CreaComboBox(false);
		cantidadPeriodos.addStyleName(R.Style.EDITABLE);
		cantidadPeriodos.addStyleName(R.Style.LABEL_BOLD);
		cantidadPeriodos.setWidth("150px");
		layout.addComponent(cantidadPeriodos);
		//Botones
		VerticalLayout layoutBotones = new VerticalLayout();
		ButtonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		ButtonModificar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonModificar);
		ButtonGuardar = new ButtonIcon(ButtonIcon.GUARDAR, true, this);
		ButtonGuardar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonGuardar);
		ButtonDescartar = new ButtonIcon(ButtonIcon.DESCARTAR, true, this);
		ButtonDescartar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonDescartar);
		layoutForm.addComponent(layoutBotones);
		
		Parametro parametro = ParametroRN.Read(ESCUELA_NOMBRE);
		if (parametro != null)
			nombre.setValue(parametro.getValor());
		parametro = ParametroRN.Read(ESCUELA_DOMICILIO);
		if (parametro != null)
			domicilio.setValue(parametro.getValor());
		parametro = ParametroRN.Read(ESCUELA_CIUDAD);
		if (parametro != null)
			ciudad.setValue(parametro.getValor());
		parametro = ParametroRN.Read(ESCUELA_PROVINCIA);
		if (parametro != null)
			provincia.setValue(parametro.getValor());
		parametro = ParametroRN.Read(ESCUELA_CANTIDADPERIODOS);
		if (parametro != null){
			try{
				cantidadPeriodos.setValue(Integer.parseInt(parametro.getValor()));
			}catch(Exception e){
				cantidadPeriodos.setValue(Dominios.Periodos.TRIMESTRE);
			}
		}
		nombre.setReadOnly(true);
		domicilio.setReadOnly(true);
		ciudad.setReadOnly(true);
		provincia.setReadOnly(true);
		cantidadPeriodos.setReadOnly(true);
		ButtonGuardar.setVisible(false);
		ButtonModificar.setVisible(true);
		ButtonDescartar.setVisible(false);
		
		VerticalLayout layoutVacio = new VerticalLayout();
		layoutVacio.setHeight("350px");
		addComponent(layoutVacio);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == ButtonModificar) {
			nombre.setReadOnly(false);
			domicilio.setReadOnly(false);
			ciudad.setReadOnly(false);
			provincia.setReadOnly(false);
			cantidadPeriodos.setReadOnly(false);
			ButtonGuardar.setVisible(true);
			ButtonModificar.setVisible(false);
			ButtonDescartar.setVisible(true);
		} else if (event.getButton() == ButtonDescartar) {
			onDraw(); 
		} else if (event.getButton() == ButtonGuardar) {
			ParametroRN.Save(ParametroRN.Inizializate(ESCUELA_NOMBRE, Dominios.TiposDeDatoParametro.TEXTO, nombre.getValue().toUpperCase()));
			ParametroRN.Save(ParametroRN.Inizializate(ESCUELA_DOMICILIO, Dominios.TiposDeDatoParametro.TEXTO, domicilio.getValue().toUpperCase()));
			ParametroRN.Save(ParametroRN.Inizializate(ESCUELA_CIUDAD, Dominios.TiposDeDatoParametro.TEXTO, ciudad.getValue().toUpperCase()));
			ParametroRN.Save(ParametroRN.Inizializate(ESCUELA_PROVINCIA, Dominios.TiposDeDatoParametro.TEXTO, provincia.getValue().toUpperCase()));
			ParametroRN.Save(ParametroRN.Inizializate(ESCUELA_CANTIDADPERIODOS, Dominios.TiposDeDatoParametro.NUMERO, String.valueOf(cantidadPeriodos.getValue())));
			onDraw();
		}
	}

	public static final String ESCUELA_NOMBRE = "ESCUELA.NOMBRE";
	public static final String ESCUELA_DOMICILIO = "ESCUELA.DOMICILIO";
	public static final String ESCUELA_PROVINCIA = "ESCUELA.PROVINCIA";
	public static final String ESCUELA_CIUDAD = "ESCUELA.CIUDAD";
	public static final String ESCUELA_CANTIDADPERIODOS = "ESCUELA.CANTIDADPERIODOS";
}
