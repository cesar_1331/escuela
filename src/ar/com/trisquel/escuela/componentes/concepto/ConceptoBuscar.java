package ar.com.trisquel.escuela.componentes.concepto;

import ar.com.trisquel.escuela.data.contenedor.ConceptoContenedor;
import ar.com.trisquel.escuela.data.tablas.Concepto;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class ConceptoBuscar extends VerticalLayout implements ClickListener{

	private static final String LINK_VER = "LINK_VER";
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonRefrescar;
	private TextField filtroNombre;
	private Table table;
	private VerticalLayout layoutFiltros;
	private UI ui;
	private Window window;
	private Concepto concepto;

	public ConceptoBuscar(UI ui ,Window window){
		this.ui = ui;
		this.window = window;
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true, this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonModificar);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		table = new Table();
		ArmaTabla();
		layout.addComponent(table);
		Label Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonRefrescar) {
			CreaDataSource();
		} else if (event.getButton() == botonModificar) {
			Concepto concepto = (Concepto) table.getValue();
			if (concepto != null) {
				Window windowABM = ConceptoWWView.CreaWindowABM();
				ConceptoABM conceptoABM = new ConceptoABM(concepto,Dominios.AccesoADatos.UPDATE, ui, windowABM);
				windowABM.setContent(conceptoABM);
				windowABM.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						CreaDataSource();
					}
				});
				ui.addWindow(windowABM);
			}
		} else if (event.getButton() == botonAgregar) {
			Window windowABM = ConceptoWWView.CreaWindowABM();
			ConceptoABM conceptoABM = new ConceptoABM(null,Dominios.AccesoADatos.INSERT, ui, windowABM);
			windowABM.setContent(conceptoABM);
			windowABM.addCloseListener(new CloseListener() {
				@Override
				public void windowClose(CloseEvent e) {
					CreaDataSource();
				}
			});
			ui.addWindow(windowABM);
		}

	}

	private void ArmaFiltros() {
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(6);
		//Nombre y Apellido
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label label = new Label("Nombre:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("50px");
		formFiltro.addComponent(label);
		filtroNombre = new TextField();
		filtroNombre.setNullRepresentation("");
		filtroNombre.setNullSettingAllowed(false);
		filtroNombre.addStyleName(R.Style.EDITABLE);
		filtroNombre.addStyleName(R.Style.LABEL_BOLD);
		filtroNombre.setWidth("300px");
		formFiltro.addComponent(filtroNombre);
		layoutFiltros.addComponent(formFiltro);

	}

	private void ArmaTabla() {
		table.setHeight("480px");
		table.setWidth("370px");
		table.setSelectable(true);
		table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Concepto concepto = (Concepto) itemId;
				Button botonVer = new Button(concepto.getNombre());
				botonVer.setWidth("300px");
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						setConcepto(concepto);
						ui.removeWindow(window);
					}
				});
				return botonVer;
			}
		});
		CreaDataSource();
	}

	private void CreaDataSource() {
		table.setContainerDataSource(ConceptoContenedor.LeeContainerXNombreXActivo(filtroNombre.getValue(), true));
		table.setVisibleColumns((Object[]) new String[] { LINK_VER,"id" });
		table.setColumnHeaders("Concepto","Id");
	}
	public Concepto getConcepto() {
		return concepto;
	}

	public void setConcepto(Concepto concepto) {
		this.concepto = concepto;
	}

}
