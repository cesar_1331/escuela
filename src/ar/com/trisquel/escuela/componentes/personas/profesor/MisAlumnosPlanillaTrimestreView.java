package ar.com.trisquel.escuela.componentes.personas.profesor;

import java.util.List;

import ar.com.trisquel.escuela.componentes.personas.alumno.AlumnoABM;
import ar.com.trisquel.escuela.componentes.personas.alumno.AlumnoWWView;
import ar.com.trisquel.escuela.data.reglasdenegocio.AlumnoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.PlanillaAlumnoRN;
import ar.com.trisquel.escuela.data.select.AsignaturaAlumnoSelect;
import ar.com.trisquel.escuela.data.select.PlanillaSelect;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.Asignatura;
import ar.com.trisquel.escuela.data.tablas.AsignaturaAlumno;
import ar.com.trisquel.escuela.data.tablas.Planilla;
import ar.com.trisquel.escuela.data.tablas.PlanillaAlumno;
import ar.com.trisquel.escuela.data.tablas.identificadores.PlanillaAlumnoId;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class MisAlumnosPlanillaTrimestreView extends VerticalLayout implements ClickListener {
	private ButtonIcon botonFiltros;
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonEliminar;
	private ButtonIcon botonRefrescar;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private Asignatura asignatura;
	private int periodo;
	private Table table;
	private UI ui;

	public MisAlumnosPlanillaTrimestreView(UI ui ,Asignatura asignatura ,int periodo) {
		this.ui = ui;
		this.asignatura = asignatura;
		this.periodo = periodo;
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);

		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(asignatura.toString().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true, this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonFiltros = new ButtonIcon(ButtonIcon.BUSCAR, true, this);
		botonFiltros.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonFiltros);
		botonFiltros.setEnabled(false);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		botonAgregar.setEnabled(false);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		botonModificar.setEnabled(false);
		layoutBotones.addComponent(botonModificar);
		botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true, this);
		botonEliminar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonEliminar);
		botonEliminar.setEnabled(false);
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true, this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonExcel);
		//botonExcel.setEnabled(false);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true, this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonPDF);
		//botonPDF.setEnabled(false);
		/*
		 * Tabla
		 */
		table = new Table();
		ArmaTabla();
		layout.addComponent(table);
		Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonRefrescar) {
			CreaDataSource();
		} else if (event.getButton() == botonExcel
				|| event.getButton() == botonPDF) {
		}
	}
	
	private void ArmaTabla() {
		table.setHeight("465px");
		table.setWidth("930px");
		table.setSelectable(true);
		CreaDataSource();
	}

	private void CreaDataSource() {
		table.addContainerProperty("Orden",             Label.class,  0);
		table.addContainerProperty("Apellido y Nombre" ,Button.class,  "");
		List<Planilla> listPlanilla = PlanillaSelect.SelectXAnioXMateriaXcursoIdXPeriodo(asignatura.getAnio() , asignatura.getMateriaId(), asignatura.getCursoId(),periodo, false);
		for (Planilla planilla : listPlanilla) {
			table.addContainerProperty(planilla.getDescripcion(),Label.class, null);
		}
		for (AsignaturaAlumno asignaturaAlumno : AsignaturaAlumnoSelect.LeeAlumnosXAsignatura(asignatura.getId() ,true)){
			Label orden = new Label(String.valueOf(asignaturaAlumno.getOrden()));
			orden.setWidth("40px");
			final Alumno alumno = AlumnoRN.Read(asignaturaAlumno.getId().getAlumnoId());
			Button botonVer = new Button(alumno.toString());
			botonVer.setPrimaryStyleName(R.Style.LINK);
			botonVer.addStyleName(R.Style.LABEL_BOLD);
			botonVer.setWidth("350px");
			botonVer.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					Window windowABM = AlumnoWWView.CreaWindowABM();
					AlumnoABM alumnoABM = new AlumnoABM(alumno ,Dominios.AccesoADatos.SELECT ,ui ,windowABM);
					windowABM.setContent(alumnoABM);
					ui.addWindow(windowABM);
				}
			});
			Object[] columnas = new Object[listPlanilla.size()+2];
			columnas[0] = orden;
			columnas[1] = botonVer;
			int i = 2;
			for (Planilla planilla : listPlanilla) {
				PlanillaAlumno planillaAlumno = PlanillaAlumnoRN.Read(new PlanillaAlumnoId(planilla.getId() , asignaturaAlumno.getId().getAlumnoId()));
				Label nota = new Label(String.valueOf(String.valueOf(planillaAlumno.getNota())));
				nota.setWidth("40px");
				columnas[i] = nota;
				i++;
			}
			table.addItem(columnas ,asignaturaAlumno.getId());
		}
	}
	
	public static Window CreaWindowABM(){
		Window windowABM = new Window();
		windowABM.setModal(true);
		windowABM.setResizable(false);
		windowABM.setDraggable(false);
		windowABM.setWidth("650px");
		windowABM.setHeight("600px");
		windowABM.center();
		windowABM.setCloseShortcut(KeyCode.ESCAPE, null);
		return windowABM;
	}
}
