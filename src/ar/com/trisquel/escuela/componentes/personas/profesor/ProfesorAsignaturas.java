package ar.com.trisquel.escuela.componentes.personas.profesor;

import java.util.Date;
import java.util.GregorianCalendar;

import ar.com.trisquel.escuela.componentes.materias.MateriaABM;
import ar.com.trisquel.escuela.componentes.materias.MateriaWWView;
import ar.com.trisquel.escuela.data.contenedor.AsignaturaContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.CursoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.MateriaRN;
import ar.com.trisquel.escuela.data.tablas.Asignatura;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.Materia;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class ProfesorAsignaturas extends VerticalLayout implements ClickListener {
	private static final String COLUMNA_MATERIA = "COLUMNA_MATERIA";
	private static final String COLUMNA_CURSO = "COLUMNA_CURSO";
	private static final String COLUMNA_TURNO = "COLUMNA_TURNO";
	private UI ui;
	private Table table; 
	private InlineDateField filtroAnio;
	private Profesor profesor;
	public ProfesorAsignaturas(Profesor profesor ,UI ui ){
		this.ui = ui;
		this.profesor = profesor;
		onDraw();
	}
	
	public void onDraw(){
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setColumns(6);
		addComponent(layout);
		//Nombre
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Profesor:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.addStyleName(R.Style.TEXTO_BIG);
		label.setWidth("80px");
		layout.addComponent(label);
		Label cursoLabel = new Label(profesor.toString());
		cursoLabel.addStyleName(R.Style.EDITABLE);
		cursoLabel.addStyleName(R.Style.LABEL_BOLD);
		cursoLabel.addStyleName(R.Style.TEXTO_BIG);
		cursoLabel.setWidth("450px");
		layout.addComponent(cursoLabel);
		VerticalLayout layoutAsignaturas = new VerticalLayout();
		addComponent(layoutAsignaturas);
		/*
		 * Tabla
		 */
		HorizontalLayout layoutTitulo = new HorizontalLayout();
		layoutTitulo.setPrimaryStyleName(R.Style.TEXTO_SUBTITULO_COLOR);
		layoutTitulo.setHeight("28px");
		layoutAsignaturas.addComponent(layoutTitulo);
		Label labelTitleMateria = new Label(EtiquetasLabel.getMaterias());
		labelTitleMateria.setWidth("130px");
		layoutTitulo.addComponent(labelTitleMateria);
		filtroAnio = new InlineDateField();
		filtroAnio.setValue(new Date(System.currentTimeMillis()));
		filtroAnio.setWidth("400px");
		filtroAnio.setResolution(Resolution.YEAR);
		filtroAnio.addStyleName(R.Style.LABEL_COLOR);
		filtroAnio.setImmediate(true);
		filtroAnio.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				CreaDataSource();
			}
		});
		layoutTitulo.addComponent(filtroAnio);
		/*
		 * Tabla
		 */
		table = new Table();
		ArmaTabla();
		addComponent(table);
		Label Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}
	
	private void ArmaTabla() {	
		table.setWidth("535px");
		table.setHeight("485px");
		table.setSelectable(true);
		table.addGeneratedColumn(COLUMNA_MATERIA, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Asignatura asignatura = (Asignatura)itemId;
				final Materia materia = MateriaRN.Read(asignatura.getMateriaId());
				Button botonVer = new Button(materia.getNombre());
				botonVer.setWidth("200px");
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = MateriaWWView.CreaWindowsABM();
						MateriaABM materiaABM = new MateriaABM(materia ,Dominios.AccesoADatos.SELECT ,ui ,windowABM);
						windowABM.setContent(materiaABM);
						ui.addWindow(windowABM);
						windowABM.focus();
					}
				});
				return botonVer;
			}
		});
		table.addGeneratedColumn(COLUMNA_TURNO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Asignatura asignatura = (Asignatura)itemId;
				final Curso curso = CursoRN.Read(asignatura.getCursoId());
				Label labelCurso = new Label(curso.getTurno());
				return labelCurso;
			}
		});
		table.addGeneratedColumn(COLUMNA_CURSO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Asignatura asignatura = (Asignatura)itemId;
				final Curso curso = CursoRN.Read(asignatura.getCursoId());
				Label labelCurso = new Label(curso.toString());
				return labelCurso;
			}
		});
		CreaDataSource();
	}
	
	private void CreaDataSource() {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(filtroAnio.getValue());
		table.setContainerDataSource(AsignaturaContenedor.LeeContainerXAnioXGradoXDivisionXProfesorXMateria(calendar.get(GregorianCalendar.YEAR), 0, profesor.getId(), 0));
		table.setVisibleColumns((Object[]) new String[] {COLUMNA_CURSO ,COLUMNA_MATERIA ,COLUMNA_TURNO});
		table.setColumnHeaders("Curso" ,"Materia" ,"Turno");
		table.setColumnWidth(COLUMNA_CURSO ,90);
		table.setColumnWidth(COLUMNA_MATERIA ,300);
		table.setColumnWidth(COLUMNA_TURNO ,90);
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
	
	}


}
