package ar.com.trisquel.escuela.componentes.personas.profesor;

import java.util.Date;
import java.util.List;

import ar.com.trisquel.escuela.data.reglasdenegocio.ProfesorCursoRN;
import ar.com.trisquel.escuela.data.select.CursoSelect;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.data.tablas.ProfesorCurso;
import ar.com.trisquel.escuela.data.tablas.identificadores.ProfesorCursoId;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class ProfesorCursos extends VerticalLayout implements ClickListener {

	private static final String COLUMNA_CURSO = "COLUMNA_CURSO";
	private static final String COLUMNA_ASOCIADO = "COLUMNA_ASOCIADO";
	private Table table; 
	private CheckBox filtroAsociado;
	private Profesor profesor;
	public ProfesorCursos(Profesor profesor ){
		this.profesor = profesor;
		onDraw();
	}
	
	public void onDraw(){
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setColumns(6);
		addComponent(layout);
		//Nombre
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Profesor:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.addStyleName(R.Style.TEXTO_BIG);
		label.setWidth("80px");
		layout.addComponent(label);
		Label cursoLabel = new Label(profesor.toString());
		cursoLabel.addStyleName(R.Style.EDITABLE);
		cursoLabel.addStyleName(R.Style.LABEL_BOLD);
		cursoLabel.addStyleName(R.Style.TEXTO_BIG);
		cursoLabel.setWidth("350px");
		layout.addComponent(cursoLabel);
		VerticalLayout layoutAsignaturas = new VerticalLayout();
		addComponent(layoutAsignaturas);
		/*
		 * Tabla
		 */
		HorizontalLayout layoutTitulo = new HorizontalLayout();
		layoutTitulo.setPrimaryStyleName(R.Style.TEXTO_SUBTITULO_COLOR);
		layoutTitulo.setHeight("28px");
		layoutAsignaturas.addComponent(layoutTitulo);
		Label labelTitleMateria = new Label(EtiquetasLabel.getMaterias());
		labelTitleMateria.setWidth("130px");
		layoutTitulo.addComponent(labelTitleMateria);
		filtroAsociado = new CheckBox("Mostrar asociados" ,true);
		filtroAsociado.setImmediate(true);
		filtroAsociado.setWidth("300px");
		filtroAsociado.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				CreaDataSource();
			}
		});
		layoutTitulo.addComponent(filtroAsociado);
		/*
		 * Tabla
		 */
		table = new Table();
		ArmaTabla();
		addComponent(table);
		Label Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}
	
	private void ArmaTabla() {	
		table.setWidth("430px");
		table.setHeight("485px");
		table.setSelectable(true);
		CreaDataSource();
	}
	
	private void CreaDataSource() {
		table.addContainerProperty(COLUMNA_CURSO,            Label.class,  0);
		table.addContainerProperty("turno"                  ,Label.class    ,  "");
		table.addContainerProperty(COLUMNA_ASOCIADO,       	 CheckBox.class, 0);
		table.removeAllItems();
		List<Curso> listCurso = CursoSelect.SelectXGradoXTurno(0, "", true);
		for (final Curso curso : listCurso){
			Label labelCurso = new Label(curso.toString());
			Label labelTurno = new Label(curso.getTurno());
			CheckBox checkAsociado = new CheckBox();
			checkAsociado.setImmediate(true);
			checkAsociado.addStyleName(R.Style.TABLE_CHECKBOX_EDITABLE);
			final ProfesorCurso profesorCurso = ProfesorCursoRN.Read(new ProfesorCursoId(profesor.getId(), curso.getId()));
			if (profesorCurso != null){
				checkAsociado.setValue(true);
			}
			else{
				checkAsociado.setValue(false);
			}
			checkAsociado.addValueChangeListener(new ValueChangeListener() {
				@Override
				public void valueChange(ValueChangeEvent event) {
					if (profesorCurso == null){
						Sesion sesion = (Sesion)UI.getCurrent().getSession().getAttribute(R.Session.SESSION);
						ProfesorCurso profesorCursoSave = ProfesorCursoRN.Inizializate(profesor.getId(), curso.getId()) ;
						profesorCursoSave.setFechaAsociacion(new Date(System.currentTimeMillis()));
						profesorCursoSave.setResponsable(sesion.getUsuarioId());
						ProfesorCursoRN.Save(profesorCursoSave);
						CreaDataSource();
					}
					else{
						ProfesorCursoRN.Delete(profesorCurso);
					}
				}
			});
			if (filtroAsociado.getValue() && profesorCurso != null){
				table.addItem(new Object[] {labelCurso, labelTurno,checkAsociado},curso);
			}
			else if (!filtroAsociado.getValue() && profesorCurso == null){
				table.addItem(new Object[] {labelCurso, labelTurno,checkAsociado},curso);
			}
		}
		table.setVisibleColumns((Object[]) new String[] {COLUMNA_CURSO ,"turno" ,COLUMNA_ASOCIADO});
		table.setColumnHeaders("Curso" ,"Turno" ,"Asociado");
		table.setColumnWidth(COLUMNA_CURSO ,120);
		table.setColumnWidth("turno" ,120);
		table.setColumnWidth(COLUMNA_ASOCIADO ,120);
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
	
	}


}
