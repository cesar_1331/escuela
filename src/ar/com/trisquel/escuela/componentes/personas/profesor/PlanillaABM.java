package ar.com.trisquel.escuela.componentes.personas.profesor;

import ar.com.trisquel.escuela.componentes.parametro.EscuelaView;
import ar.com.trisquel.escuela.componentes.personas.alumno.AlumnoABM;
import ar.com.trisquel.escuela.componentes.personas.alumno.AlumnoWWView;
import ar.com.trisquel.escuela.data.reglasdenegocio.AlumnoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ParametroRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.PlanillaAlumnoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.PlanillaRN;
import ar.com.trisquel.escuela.data.select.AsignaturaAlumnoSelect;
import ar.com.trisquel.escuela.data.select.PlanillaAlumnoSelect;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.Asignatura;
import ar.com.trisquel.escuela.data.tablas.AsignaturaAlumno;
import ar.com.trisquel.escuela.data.tablas.Parametro;
import ar.com.trisquel.escuela.data.tablas.Planilla;
import ar.com.trisquel.escuela.data.tablas.PlanillaAlumno;
import ar.com.trisquel.escuela.data.tablas.identificadores.AsignaturaAlumnoId;
import ar.com.trisquel.escuela.data.tablas.identificadores.PlanillaAlumnoId;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class PlanillaABM extends VerticalLayout implements ClickListener {
	private TextField asignaturaId;
	private TextField descripcion;
	private ComboBox periodo;
	private CheckBox esNotaFinal;
	private CheckBox mostrarAlumno;
	private ButtonIcon ButtonGuardar;
	private ButtonIcon ButtonDescartar;
	private Table table;
	private UI ui;
	private String modo;
	private Window window;
	private Planilla planilla;
	private Asignatura asignatura;

	public PlanillaABM(Planilla planilla,Asignatura asignatura ,String modo, UI ui, Window window) {
		this.ui = ui;
		this.modo = modo;
		this.window = window;
		this.asignatura = asignatura;
		this.planilla = planilla;
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setWidth("100%");
		layout.setColumns(3);
		addComponent(layout);
		//Asignatura
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Asignatura:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("65px");
		layout.addComponent(label);
		HorizontalLayout layoutId = new HorizontalLayout();
		layoutId.setWidth("500px");
		asignaturaId = new TextField();
		asignaturaId.addStyleName(R.Style.EDITABLE);
		asignaturaId.addStyleName(R.Style.LABEL_BOLD);
		asignaturaId.setValue(asignatura.toString());
		asignaturaId.setReadOnly(true);
		layoutId.addComponent(asignaturaId);
		//Botones
		HorizontalLayout layoutBotones = new HorizontalLayout();
		ButtonGuardar = new ButtonIcon(ButtonIcon.GUARDAR, true, this);
		ButtonGuardar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonGuardar);
		ButtonDescartar = new ButtonIcon(ButtonIcon.DESCARTAR, true, this);
		ButtonDescartar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonDescartar);
		layoutId.addComponent(layoutBotones);
		layoutId.setComponentAlignment(layoutBotones, Alignment.TOP_RIGHT);
		layout.addComponent(layoutId);
		//Descripcion
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Descripción:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		descripcion = new TextField();
		descripcion.addStyleName(R.Style.EDITABLE);
		descripcion.addStyleName(R.Style.LABEL_BOLD);
		descripcion.setWidth("300px");
		descripcion.setMaxLength(60);
		descripcion.focus();
		layout.addComponent(descripcion);
		//Periodo
		int cantidadPeriodos = 3;
		Parametro parametro = ParametroRN.Read(EscuelaView.ESCUELA_CANTIDADPERIODOS);
		if (parametro != null){
			try{
				cantidadPeriodos = Integer.parseInt(parametro.getValor().trim());
			}catch(Exception e){}
		}
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Periodo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		periodo = Dominios.Periodos.CreaComboBoxSegunPeriodo(cantidadPeriodos, false);
		descripcion.addStyleName(R.Style.EDITABLE);
		descripcion.addStyleName(R.Style.LABEL_BOLD);
		periodo.setWidth("200px");
		periodo.setNullSelectionAllowed(false);
		layout.addComponent(periodo);
		//Es Nota Final
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Es nota final:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		HorizontalLayout layoutCheck = new HorizontalLayout();
		layout.addComponent(layoutCheck);
		esNotaFinal = new CheckBox();
		esNotaFinal.setValue(false);
		esNotaFinal.setWidth("100px");
		esNotaFinal.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (esNotaFinal.getValue()){
					mostrarAlumno.setValue(true);
					mostrarAlumno.setReadOnly(true);
				}
				else{
					mostrarAlumno.setReadOnly(false);
				}
			}
		});
		layoutCheck.addComponent(esNotaFinal);		
		//Mostrar al alumno
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layoutCheck.addComponent(imageIcono);
		label = new Label("Mostrar al alumno:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layoutCheck.addComponent(label);
		mostrarAlumno = new CheckBox();
		mostrarAlumno.setValue(true);
		layoutCheck.addComponent(mostrarAlumno);
		table = new Table();
		ArmaTabla();
		layout.addComponent(new Label());
		layout.addComponent(new Label());
		layout.addComponent(table);
		
		if (modo.equals(Dominios.AccesoADatos.INSERT)) {
			descripcion.focus();
			ButtonGuardar.setVisible(true);
			ButtonDescartar.setVisible(true);
		}
		else{
			descripcion.setValue(planilla.getDescripcion());
			esNotaFinal.setValue(planilla.isEsNotaFinal());
			mostrarAlumno.setValue(planilla.isMostrarAlAlumno());
			periodo.setValue(planilla.getPeriodo());
			if (modo.equals(Dominios.AccesoADatos.SELECT)) {
				descripcion.setReadOnly(true);
				esNotaFinal.setReadOnly(true);
				mostrarAlumno.setReadOnly(true);
				periodo.setReadOnly(true);
				ButtonGuardar.setVisible(false);
				ButtonDescartar.setVisible(false);
			} else if (modo.equals(Dominios.AccesoADatos.UPDATE)) {
				periodo.setReadOnly(true);
				ButtonGuardar.setVisible(true);
				ButtonDescartar.setVisible(true);
			}
		}	
	}
	
	private void ArmaTabla() {
		table.setHeight("440px");
		table.setWidth("535px");
		table.setSelectable(true);
		CreaDataSource();
	}

	private void CreaDataSource() {
		if (asignatura != null){
			table.addContainerProperty("Orden",            Label.class,  0);
			table.addContainerProperty("Apellido, Nombre",Button.class,  "");
			table.addContainerProperty("Nota",       	   ComboBox.class, 0);
			table.setColumnWidth("Orden"             ,40);
			table.setColumnWidth("Apellido, Nombre" ,350);
			table.setColumnWidth("Nota"              ,80);
			for (AsignaturaAlumno asignaturaAlumno : AsignaturaAlumnoSelect.LeeAlumnosXAsignatura(asignatura.getId() ,true)){
				Label orden = new Label(String.valueOf(asignaturaAlumno.getOrden()));
				orden.setWidth("40px");
				final Alumno alumno = AlumnoRN.Read(asignaturaAlumno.getId().getAlumnoId());
				Button botonVer = new Button(alumno.toString());
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.setWidth("350px");
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = AlumnoWWView.CreaWindowABM();
						AlumnoABM alumnoABM = new AlumnoABM(alumno ,Dominios.AccesoADatos.SELECT ,ui ,windowABM);
						windowABM.setContent(alumnoABM);
						ui.addWindow(windowABM);
					}
				});
				ComboBox nota = Dominios.Nota.CreaComboBox();
				nota.setWidth("80px");
				if (planilla != null){
					PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXPlanillaXAlumno(planilla.getId() , asignaturaAlumno.getId().getAlumnoId());
					if (planillaAlumno != null){
						nota.setValue(planillaAlumno.getNota());
					}
				}
				table.addItem(new Object[] {orden, botonVer,nota},asignaturaAlumno.getId());
			}
		}
		else{
			table.removeAllItems();
		}
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == ButtonDescartar) {
			ui.removeWindow(window);
		} else if (event.getButton() == ButtonGuardar) {
			if (planilla == null){
				planilla = PlanillaRN.Inizializate();
			}
			int periodoNumero = 0;
			try{
				periodoNumero = Integer.parseInt(String.valueOf(periodo.getValue()));
			}catch (Exception e){}
			planilla.setAnio(asignatura.getAnio());
			planilla.setCursoId(asignatura.getCursoId());
			planilla.setDescripcion(descripcion.getValue());
			planilla.setEsNotaFinal(esNotaFinal.getValue());
			planilla.setMateriaId(asignatura.getMateriaId());
			planilla.setMostrarAlAlumno(mostrarAlumno.getValue());
			planilla.setPeriodo(periodoNumero);
			planilla.setProfesorId(asignatura.getProfesorId());
			RespuestaEntidad respuesta = PlanillaRN.Validate(planilla ,modo);
			if (respuesta.isError()) {
				MessageBox.showHTML(Icon.ERROR, "", respuesta.getMsgError(),ButtonId.OK);
			} else {
				MessageBox.showHTML(Icon.QUESTION, "",MensajeLabel.getConfirmaAccion(),new MessageBoxListener() {
					@Override
					public void buttonClicked(ButtonId buttonType) {
						if (buttonType == ButtonId.SAVE) {
							RespuestaEntidad respuesta = PlanillaRN.Save(planilla);
							for (Object object : table.getItemIds()){
								AsignaturaAlumnoId asignaturaAlumnoId = (AsignaturaAlumnoId)object;
								Item item = table.getItem(object);
								ComboBox nota = (ComboBox)(item.getItemProperty("Nota")).getValue();
								int notaNumero = 0;
								try{
									notaNumero = Integer.parseInt(String.valueOf(nota.getValue())); 
								}catch(Exception e){}
								PlanillaAlumno planillaAlumno = PlanillaAlumnoRN.Inizializate();
								planillaAlumno.setId(new PlanillaAlumnoId(planilla.getId(), asignaturaAlumnoId.getAlumnoId()));
								planillaAlumno.setNota(notaNumero);
								PlanillaAlumnoRN.Save(planillaAlumno);								
							}
							if (respuesta.isError()) {
								MessageBox.showHTML(Icon.ERROR, "",respuesta.getMsgError(),ButtonId.OK);
							} else {
								ui.removeWindow(window);
							}
						}
					}
				}, ButtonId.SAVE, ButtonId.CANCEL);
			}
		}
	}
}
