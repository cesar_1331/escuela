package ar.com.trisquel.escuela.componentes.personas.profesor;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import ar.com.trisquel.escuela.componentes.personas.alumno.AlumnoABM;
import ar.com.trisquel.escuela.componentes.personas.alumno.AlumnoWWView;
import ar.com.trisquel.escuela.data.contenedor.AsignaturaAlumnoContenedor;
import ar.com.trisquel.escuela.data.contenedor.AsignaturaContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.AlumnoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.CursoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ParametroRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.UsuarioRN;
import ar.com.trisquel.escuela.data.select.PlanillaAlumnoSelect;
import ar.com.trisquel.escuela.data.select.PlanillaSelect;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.Asignatura;
import ar.com.trisquel.escuela.data.tablas.AsignaturaAlumno;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.Planilla;
import ar.com.trisquel.escuela.data.tablas.PlanillaAlumno;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class MisAlumnosView extends VerticalLayout implements ClickListener, View {

	public static final String VIEW = "MisAlumnosWW";
	private static final String LINK_VER = "LINK_VER";
	private static final String COLUMNA_PERIODO1 = "COLUMNA_PERIODO1";
	private static final String COLUMNA_PERIODO2 = "COLUMNA_PERIODO2";
	private static final String COLUMNA_PERIODO3 = "COLUMNA_PERIODO3";
	private static final String COLUMNA_PERIODO4 = "COLUMNA_PERIODO4";
	private static final String COLUMNA_FINAL = "COLUMNA_FINAL";
	private ButtonIcon botonFiltros;
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonEliminar;
	private ButtonIcon botonRefrescar;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private InlineDateField filtroAnio;
	private ComboBox filtroAsignatura;
	private Table table;
	private VerticalLayout layoutFiltros;
	private VerticalLayout layoutTabla;
	private UI ui;
	private Usuario usuario;
	private int cantidadPeriodos;
	private List<Planilla> listPlanillaPeriodo1;
	private List<Planilla> listPlanillaPeriodo2;
	private List<Planilla> listPlanillaPeriodo3;
	private List<Planilla> listPlanillaPeriodo4;
	private List<Planilla> listPlanillaPeriodo10;
	
	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass(), ui);
		if (!control.isAcceso()) {
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		} else {
			onDraw();
		}
	}

	public void onDraw() {
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		
		Sesion sesion = (Sesion)ui.getSession().getAttribute(R.Session.SESSION);
		usuario = UsuarioRN.Read(sesion.getUsuarioId());
		cantidadPeriodos = ParametroRN.LeeEscuelaCantidadPeriodos();
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(EtiquetasLabel.getMisAlumnos().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true, this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonFiltros = new ButtonIcon(ButtonIcon.BUSCAR, true, this);
		botonFiltros.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonFiltros);
		botonFiltros.setEnabled(false);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		//botonAgregar.setEnabled(false);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		//botonModificar.setEnabled(false);
		layoutBotones.addComponent(botonModificar);
		botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true, this);
		botonEliminar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonEliminar);
		//botonEliminar.setEnabled(false);
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true, this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonExcel);
		//botonExcel.setEnabled(false);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true, this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonPDF);
		//botonPDF.setEnabled(false);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		layoutTabla = new VerticalLayout();
		ArmaTabla();
		layout.addComponent(layoutTabla);
		Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonFiltros) {
			layoutFiltros.setVisible(!layoutFiltros.isVisible());
		} else if (event.getButton() == botonRefrescar) {
			ArmaTabla();
		} else if (event.getButton() == botonAgregar) {
			Asignatura asignatura = (Asignatura)filtroAsignatura.getValue();
			if (asignatura != null){
				Window windowABM = CreaWindowABM();
				PlanillaABM planillaABM = new PlanillaABM(null ,asignatura ,Dominios.AccesoADatos.INSERT ,ui ,windowABM);
				windowABM.setContent(planillaABM);
				windowABM.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						CreaDataSource();
					}
				});
				ui.addWindow(windowABM);
			}
		} else if (event.getButton() == botonModificar) {
			Asignatura asignatura = (Asignatura)filtroAsignatura.getValue();
			if (asignatura != null){
				Window windowABM = CreaWindowABM();
				windowABM.setCaption(EtiquetasLabel.getBuscarPlanilla());
				windowABM.setWidth("550px");
				windowABM.setHeight("330px");
				PlanillaBuscar planillaBuscar = new PlanillaBuscar(asignatura ,ui ,windowABM ,Dominios.AccesoADatos.UPDATE);
				windowABM.setContent(planillaBuscar);
				windowABM.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						CreaDataSource();
					}
				});
				ui.addWindow(windowABM);
			}
		} else if (event.getButton() == botonEliminar) {
			Asignatura asignatura = (Asignatura)filtroAsignatura.getValue();
			if (asignatura != null){
				Window windowABM = CreaWindowABM();
				windowABM.setCaption(EtiquetasLabel.getBuscarPlanilla());
				windowABM.setWidth("550px");
				windowABM.setHeight("330px");
				PlanillaBuscar planillaBuscar = new PlanillaBuscar(asignatura ,ui ,windowABM ,Dominios.AccesoADatos.DELETE);
				windowABM.setContent(planillaBuscar);
				windowABM.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						CreaDataSource();
					}
				});
				ui.addWindow(windowABM);
			}
		} else if (event.getButton() == botonExcel) {
			Asignatura asignatura = (Asignatura)filtroAsignatura.getValue();
			if (asignatura != null){
				String fileName = PlanillaExportar.ExportarExcel(asignatura);
				ui.getPage().open(fileName ,"_blank", false);
			}
		} else if (event.getButton() == botonPDF) {
			Asignatura asignatura = (Asignatura)filtroAsignatura.getValue();
			if (asignatura != null){
				String fileName = PlanillaExportar.ExportarPDF(asignatura);
				ui.getPage().open(fileName ,"_blank", false);
			}
		}
	}

	private void ArmaFiltros() {
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(6);
		//A�o
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label label = new Label("A�o:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("50px");
		formFiltro.addComponent(label);
		filtroAnio = new InlineDateField();
		filtroAnio.addStyleName(R.Style.EDITABLE);
		filtroAnio.setWidth("200px");
		filtroAnio.addStyleName(R.Style.LABEL_BOLD);
		filtroAnio.setResolution(Resolution.YEAR);
		filtroAnio.setImmediate(true);
		formFiltro.addComponent(filtroAnio);
		//Curso
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Asignatura:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("70px");
		formFiltro.addComponent(label);
		filtroAsignatura = new ComboBox();
		filtroAsignatura.setInputPrompt(EtiquetasLabel.getSeleccionar());
		filtroAsignatura.addStyleName(R.Style.EDITABLE);
		filtroAsignatura.addStyleName(R.Style.LABEL_BOLD);
		filtroAsignatura.setWidth("400px");
		formFiltro.addComponent(filtroAsignatura);			
		layoutFiltros.addComponent(formFiltro);
		filtroAnio.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				LeeAsignaturas();
			}
		});
		filtroAnio.setValue(new Date(System.currentTimeMillis()));
	}
	
	private void LeeAsignaturas(){
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(filtroAnio.getValue());
		@SuppressWarnings("unchecked")
		BeanItemContainer<Asignatura> container = AsignaturaContenedor.LeeContainerXAnioXGradoXDivisionXProfesorXMateria(calendar.get(GregorianCalendar.YEAR), 0, usuario.getProfesorId(), 0);
		filtroAsignatura.setContainerDataSource(container);
		if (container.size() > 0){
			filtroAsignatura.setValue(container.getIdByIndex(0));
			filtroAsignatura.setNullSelectionAllowed(false);
		}
		else{
			filtroAsignatura.setInputPrompt("No tiene asignaturas asignadas");
			filtroAsignatura.setNullSelectionAllowed(false);
		}
	}
	
	private void ArmaTabla() {
		Asignatura asignatura = (Asignatura)filtroAsignatura.getValue();
		if (asignatura != null){
			Curso curso = CursoRN.Read(asignatura.getCursoId());
			if (curso != null){
				cantidadPeriodos = curso.getCantidadPeriodos();
			}
		}
		layoutTabla.removeAllComponents();
		table = new Table();
		layoutTabla.addComponent(table);
		table.setHeight("550px");
		table.setWidth("985px");
		table.setSelectable(true);
		table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				AsignaturaAlumno asignaturaAlumno = (AsignaturaAlumno)itemId;
				final Alumno alumno = AlumnoRN.Read(asignaturaAlumno.getId().getAlumnoId());
				Button botonVer = new Button(alumno.toString());
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = AlumnoWWView.CreaWindowABM();
						AlumnoABM alumnoABM = new AlumnoABM(alumno ,Dominios.AccesoADatos.SELECT ,ui ,windowABM);
						windowABM.setContent(alumnoABM);
						ui.addWindow(windowABM);
					}
				});
				return botonVer;
			}

		});
		table.addGeneratedColumn(COLUMNA_PERIODO1, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Button botonVer = new Button("0");
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Asignatura asignatura = (Asignatura)filtroAsignatura.getValue();
						Window windowABM = CreaWindowABM();
						windowABM.setWidth("950px");
						MisAlumnosPlanillaTrimestreView misAlumnosPlanilla = new MisAlumnosPlanillaTrimestreView(ui, asignatura, 1);
						windowABM.setContent(misAlumnosPlanilla);
						ui.addWindow(windowABM);
					}
				});
				final AsignaturaAlumno asignaturaAlumno = (AsignaturaAlumno)itemId;
				if (listPlanillaPeriodo1.size() > 0){
					PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXPlanillaXAlumno(listPlanillaPeriodo1.get(0).getId() , asignaturaAlumno.getId().getAlumnoId());
					if (planillaAlumno != null){
						botonVer.setCaption(String.valueOf(planillaAlumno.getNota()));
					}
				}
				return botonVer;
			}
		});
		table.addGeneratedColumn(COLUMNA_PERIODO2, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Button botonVer = new Button("0");
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Asignatura asignatura = (Asignatura)filtroAsignatura.getValue();
						Window windowABM = CreaWindowABM();
						windowABM.setWidth("950px");
						MisAlumnosPlanillaTrimestreView misAlumnosPlanilla = new MisAlumnosPlanillaTrimestreView(ui, asignatura, 2);
						windowABM.setContent(misAlumnosPlanilla);
						ui.addWindow(windowABM);
					}
				});
				AsignaturaAlumno asignaturaAlumno = (AsignaturaAlumno)itemId;
				if (listPlanillaPeriodo2.size() > 0){
					PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXPlanillaXAlumno(listPlanillaPeriodo2.get(0).getId() , asignaturaAlumno.getId().getAlumnoId());
					if (planillaAlumno != null){
						botonVer.setCaption(String.valueOf(planillaAlumno.getNota()));
					}
				}
				return botonVer;
			}
		});
		if (cantidadPeriodos == Dominios.Periodos.BIMESTRE || cantidadPeriodos == Dominios.Periodos.TRIMESTRE){
			table.addGeneratedColumn(COLUMNA_PERIODO3, new Table.ColumnGenerator() {
				public Component generateCell(Table source, Object itemId,Object columnId) {
					Button botonVer = new Button("0");
					botonVer.setPrimaryStyleName(R.Style.LINK);
					botonVer.addStyleName(R.Style.LABEL_BOLD);
					botonVer.addClickListener(new ClickListener() {
						@Override
						public void buttonClick(ClickEvent event) {
							Asignatura asignatura = (Asignatura)filtroAsignatura.getValue();
							Window windowABM = CreaWindowABM();
							windowABM.setWidth("950px");
							MisAlumnosPlanillaTrimestreView misAlumnosPlanilla = new MisAlumnosPlanillaTrimestreView(ui, asignatura, 3);
							windowABM.setContent(misAlumnosPlanilla);
							ui.addWindow(windowABM);
						}
					});
					AsignaturaAlumno asignaturaAlumno = (AsignaturaAlumno)itemId;
					if (listPlanillaPeriodo3.size() > 0){
						PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXPlanillaXAlumno(listPlanillaPeriodo3.get(0).getId() , asignaturaAlumno.getId().getAlumnoId());
						if (planillaAlumno != null){
							botonVer.setCaption(String.valueOf(planillaAlumno.getNota()));
						}
					}
					return botonVer;
				}
			});
			if (cantidadPeriodos == Dominios.Periodos.BIMESTRE){
				table.addGeneratedColumn(COLUMNA_PERIODO4, new Table.ColumnGenerator() {
					public Component generateCell(Table source, Object itemId,Object columnId) {
						Button botonVer = new Button("0");
						botonVer.setPrimaryStyleName(R.Style.LINK);
						botonVer.addStyleName(R.Style.LABEL_BOLD);
						botonVer.addClickListener(new ClickListener() {
							@Override
							public void buttonClick(ClickEvent event) {
								Asignatura asignatura = (Asignatura)filtroAsignatura.getValue();
								Window windowABM = CreaWindowABM();
								windowABM.setWidth("950px");
								MisAlumnosPlanillaTrimestreView misAlumnosPlanilla = new MisAlumnosPlanillaTrimestreView(ui, asignatura, 4);
								windowABM.setContent(misAlumnosPlanilla);
								ui.addWindow(windowABM);
							}
						});
						AsignaturaAlumno asignaturaAlumno = (AsignaturaAlumno)itemId;
						if (listPlanillaPeriodo4.size() > 0){
							PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXPlanillaXAlumno(listPlanillaPeriodo4.get(0).getId() , asignaturaAlumno.getId().getAlumnoId());
							if (planillaAlumno != null){
								botonVer.setCaption(String.valueOf(planillaAlumno.getNota()));
							}
						}
						return botonVer;
					}
				});
			}
		}
		table.addGeneratedColumn(COLUMNA_FINAL, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Button botonVer = new Button("0");
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Asignatura asignatura = (Asignatura)filtroAsignatura.getValue();
						Window windowABM = CreaWindowABM();
						windowABM.setWidth("950px");
						MisAlumnosPlanillaTrimestreView misAlumnosPlanilla = new MisAlumnosPlanillaTrimestreView(ui, asignatura, Dominios.Periodos.FINAL);
						windowABM.setContent(misAlumnosPlanilla);
						ui.addWindow(windowABM);
					}
				});
				AsignaturaAlumno asignaturaAlumno = (AsignaturaAlumno)itemId;
				if (listPlanillaPeriodo10.size() > 0){
					PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXPlanillaXAlumno(listPlanillaPeriodo10.get(0).getId() , asignaturaAlumno.getId().getAlumnoId());
					if (planillaAlumno != null){
						botonVer.setCaption(String.valueOf(planillaAlumno.getNota()));
					}
				}
				return botonVer;
			}
		});
		CreaDataSource();
	}

	private void CreaDataSource() {
		Asignatura asignatura = (Asignatura)filtroAsignatura.getValue();
		if (asignatura != null){
			listPlanillaPeriodo1  = PlanillaSelect.SelectXAnioXMateriaXcursoIdXPeriodo(asignatura.getAnio() , asignatura.getMateriaId(), asignatura.getCursoId(), 1, true);
			listPlanillaPeriodo2  = PlanillaSelect.SelectXAnioXMateriaXcursoIdXPeriodo(asignatura.getAnio() , asignatura.getMateriaId(), asignatura.getCursoId(), 2, true);
			listPlanillaPeriodo3  = PlanillaSelect.SelectXAnioXMateriaXcursoIdXPeriodo(asignatura.getAnio() , asignatura.getMateriaId(), asignatura.getCursoId(), 3, true);
			listPlanillaPeriodo4  = PlanillaSelect.SelectXAnioXMateriaXcursoIdXPeriodo(asignatura.getAnio() , asignatura.getMateriaId(), asignatura.getCursoId(), 4, true);
			listPlanillaPeriodo10 = PlanillaSelect.SelectXAnioXMateriaXcursoIdXPeriodo(asignatura.getAnio() , asignatura.getMateriaId(), asignatura.getCursoId(), 10, true);
			table.setContainerDataSource(AsignaturaAlumnoContenedor.LeeContainerAlumnosXAsignatura(asignatura.getId() ,true));
			if (cantidadPeriodos == Dominios.Periodos.BIMESTRE){
				table.setVisibleColumns((Object[]) new String[] {"orden" ,LINK_VER ,COLUMNA_PERIODO1 ,COLUMNA_PERIODO2 ,COLUMNA_PERIODO3 ,COLUMNA_PERIODO4 ,COLUMNA_FINAL});
				table.setColumnHeaders("" ,"Apellido y Nombre" ,"1� Bimestre" ,"2� Bimestre" ,"3� Bimestre" ,"4� Bimestre" ,"Final");
			}
			else if (cantidadPeriodos == Dominios.Periodos.TRIMESTRE){
				table.setVisibleColumns((Object[]) new String[] {"orden" ,LINK_VER ,COLUMNA_PERIODO1 ,COLUMNA_PERIODO2 ,COLUMNA_PERIODO3 ,COLUMNA_FINAL});
				table.setColumnHeaders("" ,"Apellido y Nombre" ,"1� Trimestre" ,"2� Trimestre" ,"3� Trimestre" ,"Final");
			}
			else if (cantidadPeriodos == Dominios.Periodos.SEMESTRE){
				table.setVisibleColumns((Object[]) new String[] {"orden" ,LINK_VER ,COLUMNA_PERIODO1 ,COLUMNA_PERIODO2 ,COLUMNA_FINAL});
				table.setColumnHeaders("" ,"Apellido y Nombre","1� Semestre" ,"2� Semestre"  ,"Final");
			}
		}
		else{
			table.setContainerDataSource(AsignaturaAlumnoContenedor.LeeContainerAlumnosXAsignatura(0 ,true));
			table.setVisibleColumns((Object[]) new String[] {"orden" ,LINK_VER ,COLUMNA_PERIODO1 ,COLUMNA_PERIODO2 ,COLUMNA_PERIODO3 ,COLUMNA_FINAL});
			table.setColumnHeaders("" ,"Apellido y Nombre" ,"1� Trimestre" ,"2� Trimestre" ,"3� Trimestre" ,"Final");
		}
	}
	
	public static Window CreaWindowABM(){
		Window windowABM = new Window(EtiquetasLabel.getPlanilla());
		windowABM.setModal(true);
		windowABM.setResizable(false);
		windowABM.setDraggable(false);
		windowABM.setWidth("650px");
		windowABM.setHeight("600px");
		windowABM.center();
		windowABM.setCloseShortcut(KeyCode.ESCAPE, null);
		return windowABM;
	}
}
