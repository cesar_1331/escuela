package ar.com.trisquel.escuela.componentes.personas.profesor;

import java.util.Date;
import java.util.GregorianCalendar;

import ar.com.trisquel.escuela.data.reglasdenegocio.AlumnoAsistenciaRN;
import ar.com.trisquel.escuela.data.select.AlumnoSelect;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.AlumnoAsistencia;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.identificadores.AlumnoDia;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class AsistenciaABM extends VerticalLayout implements ClickListener{
	private DateField fecha;
	private ButtonIcon ButtonGuardar;
	private ButtonIcon ButtonDescartar;
	private UI ui;
	private Window window;
	private Table table;
	private VerticalLayout layoutTabla;
	private Curso curso;
	private GregorianCalendar calendar;
	
	public AsistenciaABM(Curso curso ,UI ui, Window window) {
		this.ui = ui;
		this.window = window;
		this.curso = curso;
		GregorianCalendar calendarDia = new GregorianCalendar();
		calendarDia.setTime(new Date(System.currentTimeMillis()));
		calendar = new GregorianCalendar(
				calendarDia.get(GregorianCalendar.YEAR),
				calendarDia.get(GregorianCalendar.MONTH),
				calendarDia.get(GregorianCalendar.DAY_OF_MONTH));
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setColumns(3);
		addComponent(layout);
		//Id
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelId = new Label("Fecha:");
		labelId.addStyleName(R.Style.EDITABLE_LABEL);
		labelId.setWidth("40px");
		layout.addComponent(labelId);
		HorizontalLayout layoutId = new HorizontalLayout();
		fecha = new DateField();
		fecha.addStyleName(R.Style.EDITABLE);
		fecha.addStyleName(R.Style.LABEL_BOLD);
		fecha.setValue(calendar.getTime());
		fecha.setImmediate(true);
		fecha.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				CreaDataSource();
			}
		});
		layoutId.addComponent(fecha);
		HorizontalLayout layoutCurso = new HorizontalLayout();
		layoutId.addComponent(layoutCurso);
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		imageIcono.addStyleName(R.Style.MARGIN_MEDIO);
		layoutCurso.addComponent(imageIcono);
		Label label = new Label("Curso:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("40px");
		layoutCurso.addComponent(label);
		label = new Label(curso.toString());
		label.addStyleName(R.Style.EDITABLE);
		label.addStyleName(R.Style.LABEL_BOLD);
		label.setWidth("300px");
		layoutCurso.addComponent(label);
		//Botones
		HorizontalLayout layoutBotones = new HorizontalLayout();
		ButtonGuardar = new ButtonIcon(ButtonIcon.GUARDAR, true, this);
		ButtonGuardar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonGuardar);
		ButtonDescartar = new ButtonIcon(ButtonIcon.DESCARTAR, true, this);
		ButtonDescartar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonDescartar);
		layoutId.addComponent(layoutBotones);
		layoutId.setComponentAlignment(layoutBotones, Alignment.TOP_RIGHT);
		layout.addComponent(layoutId);
		/*
		 * Tabla
		 */
		layoutTabla = new VerticalLayout();
		addComponent(layoutTabla);
		ArmaTabla();
		ButtonGuardar.setVisible(true);
		ButtonDescartar.setVisible(true);
		fecha.focus();
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == ButtonDescartar) {
			ui.removeWindow(window);
		} else if (event.getButton() == ButtonGuardar) {
				MessageBox.showHTML(Icon.QUESTION, "",
						MensajeLabel.getConfirmaAccion(),
						new MessageBoxListener() {
							@Override
							public void buttonClicked(ButtonId buttonType) {
								if (buttonType == ButtonId.SAVE) {
									for (Object object : table.getItemIds()){
										Alumno alumno = (Alumno)object;
										Item item = table.getItem(object);
										CheckBox asistencia = (CheckBox)(item.getItemProperty("Asistencia")).getValue();
										CheckBox justificada = (CheckBox)(item.getItemProperty("Justificada")).getValue();
										AlumnoAsistencia alumnoAsistencia = AlumnoAsistenciaRN.Inizializate(alumno.getId(), fecha.getValue());
										if (asistencia.getValue()){
											alumnoAsistencia.setAsistencia(Dominios.Asistencia.PRESENTE);
										}
										else if (justificada.getValue()){
											alumnoAsistencia.setAsistencia(Dominios.Asistencia.JUSTIFICADA);
										}
										else{
											alumnoAsistencia.setAsistencia(Dominios.Asistencia.AUSENTE);
										}
										AlumnoAsistenciaRN.Save(alumnoAsistencia);
									}
									ui.removeWindow(window);
								}
							}
						},
						ButtonId.SAVE,
						ButtonId.CANCEL);
		}
	}

	
	private void ArmaTabla() {
		layoutTabla.removeAllComponents();
		table = new Table();
		layoutTabla.addComponent(table);
		table.setHeight("505px");
		table.setWidth("630px");
		table.setSelectable(true);
		CreaDataSource();
	}

	private void CreaDataSource() {
		table.removeAllItems();
		table.addContainerProperty("Apellido, Nombre" ,Label.class,  null);
		table.addContainerProperty("Asistencia" ,CheckBox.class,  null);
		table.addContainerProperty("Justificada" ,CheckBox.class,  null);
		table.setColumnWidth("Apellido, Nombre", 340);
		table.setColumnWidth("Asistencia" , 80);
		table.setColumnWidth("Justificada", 140);
		table.setColumnHeaders("Alumno" ,"Presente" ,"Inasistencia Justificada");
		
		if (curso != null){
			for (final Alumno alumno : AlumnoSelect.SelectXCursoId(curso.getId())){
				Label nombre = new Label(alumno.toString());
				nombre.addStyleName(R.Style.EDITABLE);
				nombre.addStyleName(R.Style.LABEL_BOLD);
				Object[] columnas = new Object[3];
				columnas[0] = nombre;
				GregorianCalendar calendarDia = new GregorianCalendar();
				calendarDia.setTime(fecha.getValue());
				final CheckBox asistencia = new CheckBox();
				asistencia.setValue(false);
				asistencia.setImmediate(true);
				final CheckBox justificada = new CheckBox();
				justificada.setValue(false);
				justificada.setImmediate(true);
				asistencia.addValueChangeListener(new ValueChangeListener() {
					@Override
					public void valueChange(ValueChangeEvent event) {
						if (asistencia.getValue()){
							justificada.setReadOnly(false);
							justificada.setValue(false);
							justificada.setReadOnly(true);
						}
						else{
							justificada.setReadOnly(false);
						}
					}
				});
				AlumnoAsistencia alumnoAsistencia = AlumnoAsistenciaRN.Read(new AlumnoDia(alumno.getId(), fecha.getValue()));
				if (alumnoAsistencia != null){
					if (alumnoAsistencia.getAsistencia().equals(Dominios.Asistencia.PRESENTE)){
						asistencia.setValue(true);
						justificada.setValue(false);
					}
					else if (alumnoAsistencia.getAsistencia().equals(Dominios.Asistencia.JUSTIFICADA)){
						asistencia.setValue(false);
						justificada.setValue(true);
					}
					else if (alumnoAsistencia.getAsistencia().equals(Dominios.Asistencia.AUSENTE)){
						asistencia.setValue(false);
						justificada.setValue(false);
					}
				}
				columnas[1] = asistencia;
				columnas[2] = justificada;
				table.addItem(columnas ,alumno);
			}
		}
		table.setCellStyleGenerator(new Table.CellStyleGenerator() {
			@Override
			public String getStyle(Table source, Object itemId,Object propertyId) {
		        if (propertyId != null && (propertyId.equals("Asistencia") || propertyId.equals("Justificada")))
		        	return R.Style.TABLE_CHECKBOX_EDITABLE;
		        else
		        	return null;
			}
		});
	}
}
