package ar.com.trisquel.escuela.componentes.personas.profesor;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import ar.com.trisquel.escuela.data.reglasdenegocio.AlumnoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.CursoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.MateriaRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ParametroRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ProfesorRN;
import ar.com.trisquel.escuela.data.select.AsignaturaAlumnoSelect;
import ar.com.trisquel.escuela.data.select.PlanillaAlumnoSelect;
import ar.com.trisquel.escuela.data.select.PlanillaSelect;
import ar.com.trisquel.escuela.data.tablas.Asignatura;
import ar.com.trisquel.escuela.data.tablas.AsignaturaAlumno;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.Planilla;
import ar.com.trisquel.escuela.data.tablas.PlanillaAlumno;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.ImageCaptcha;
import ar.com.trisquel.escuela.utiles.Recursos;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.vaadin.server.VaadinService;

public class PlanillaExportar {
	
	private static final int EXCEL = 1;
	private static final int PDF = 2;
	private static int cantidadPeriodos = ParametroRN.LeeEscuelaCantidadPeriodos();
	
	public PlanillaExportar() {
	}

	public static String ExportarExcel(Asignatura asignatura ){
		cantidadPeriodos = ParametroRN.LeeEscuelaCantidadPeriodos();
		Curso curso = CursoRN.Read(asignatura.getCursoId());
		if (curso != null){
			cantidadPeriodos = curso.getCantidadPeriodos();
		}	
		Workbook libro = new HSSFWorkbook();
		short fontHeigt = 14;
		CellStyle styleTitle = libro.createCellStyle();
		Font fuente = libro.createFont();
		fuente.setFontHeightInPoints(fontHeigt);
		fuente.setBoldweight(Font.BOLDWEIGHT_BOLD);
		styleTitle.setFont(fuente);
		
		CellStyle styleTituloColumna = libro.createCellStyle();
		fuente = libro.createFont();
		fuente.setBoldweight(Font.BOLDWEIGHT_BOLD);
		styleTituloColumna.setFont(fuente);
		styleTituloColumna.setFillBackgroundColor(IndexedColors.AQUA.getIndex());
		styleTituloColumna.setBottomBorderColor(IndexedColors.BLUE_GREY.getIndex());
		styleTituloColumna.setTopBorderColor(IndexedColors.BLUE_GREY.getIndex());
		styleTituloColumna.setBorderBottom(CellStyle.BORDER_MEDIUM);
		styleTituloColumna.setBorderTop(CellStyle.BORDER_MEDIUM);
		//A�adir una hoja al libro:
		Sheet hoja = libro.createSheet("Export");
		//Crear una fila:
		Row fila = hoja.createRow(0);
		//Se accede a la celda 2 de la fila 1:
		Cell celda = fila.createCell(0);
		// Generamos el contenido de la celda.
		HSSFRichTextString miContenido= new HSSFRichTextString(ParametroRN.LeeEscuelaNombre());
		celda.setCellValue(miContenido);
		celda.setCellStyle(styleTitle);
		
		fila = hoja.createRow(1);
		celda = fila.createCell(0);
		celda.setCellValue(MateriaRN.Read(asignatura.getMateriaId()).toString() + "  " + CursoRN.Read(asignatura.getCursoId()).toString());
		celda.setCellStyle(styleTitle);
		
		fila = hoja.createRow(2);
		celda = fila.createCell(0);
		celda.setCellValue(ProfesorRN.Read(asignatura.getProfesorId()).toString());
		celda.setCellStyle(styleTitle);
		
		fila = hoja.createRow(4);
		celda = fila.createCell(0);
		celda.setCellValue("Orden");
		celda.setCellStyle(styleTituloColumna);
		celda = fila.createCell(1);
		celda.setCellValue("Apellido, Nombre");
		celda.setCellStyle(styleTituloColumna);
		if (cantidadPeriodos == Dominios.Periodos.BIMESTRE){
			celda = fila.createCell(2);
			celda.setCellValue("1� Bimestre");
			celda.setCellStyle(styleTituloColumna);
			celda = fila.createCell(3);
			celda.setCellValue("2� Bimestre");
			celda.setCellStyle(styleTituloColumna);
			celda = fila.createCell(4);
			celda.setCellValue("3� Bimestre");
			celda.setCellStyle(styleTituloColumna);
			celda = fila.createCell(5);
			celda.setCellValue("4� Bimestre");
			celda.setCellStyle(styleTituloColumna);
			celda = fila.createCell(6);
			celda.setCellValue("Final");
			celda.setCellStyle(styleTituloColumna);
		}
		else if (cantidadPeriodos == Dominios.Periodos.TRIMESTRE){
			celda = fila.createCell(2);
			celda.setCellValue("1� Trimestre");
			celda.setCellStyle(styleTituloColumna);
			celda = fila.createCell(3);
			celda.setCellValue("2� Trimestre");
			celda.setCellStyle(styleTituloColumna);
			celda = fila.createCell(4);
			celda.setCellValue("3� Trimestre");
			celda.setCellStyle(styleTituloColumna);
			celda = fila.createCell(5);
			celda.setCellValue("Final");
			celda.setCellStyle(styleTituloColumna);
		}
		else if (cantidadPeriodos == Dominios.Periodos.SEMESTRE){
			celda = fila.createCell(2);
			celda.setCellValue("1� Semestre");
			celda.setCellStyle(styleTituloColumna);
			celda = fila.createCell(3);
			celda.setCellValue("2� Semestre");
			celda.setCellStyle(styleTituloColumna);
			celda = fila.createCell(4);
			celda.setCellValue("Final");
			celda.setCellStyle(styleTituloColumna);
		}
		hoja.createFreezePane(0, 5);
		
		ArmaCalendarioExcel(asignatura,hoja ,null ,EXCEL);
		
		hoja.autoSizeColumn(1);
		hoja.autoSizeColumn(2);
		hoja.autoSizeColumn(3);
		hoja.autoSizeColumn(4);
		hoja.autoSizeColumn(5);
		hoja.autoSizeColumn(6);
		
		String fileNombre = new String(ImageCaptcha.generarRamdom(20));
		String fileGuardar = VaadinService.getCurrent().getBaseDirectory()+ "/VAADIN/temp/"+fileNombre+".xls";
		OutputStream output;
		try {
			output = new FileOutputStream(fileGuardar);
			libro.write(output);
			output.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "./VAADIN/temp/"+fileNombre+".xls";
	}

	public static String  ExportarPDF(Asignatura asignatura ){
		cantidadPeriodos = ParametroRN.LeeEscuelaCantidadPeriodos();
		Curso curso = CursoRN.Read(asignatura.getCursoId());
		if (curso != null){
			cantidadPeriodos = curso.getCantidadPeriodos();
		}
		String fileNombre = new String(ImageCaptcha.generarRamdom(20));
		String fileGuardar = VaadinService.getCurrent().getBaseDirectory()+ "/VAADIN/temp/"+fileNombre+".pdf";
		try{
			Document document = new Document(PageSize.A4.rotate() );
			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fileGuardar));
			document.open();
			document.addAuthor(Recursos.Parametros.getAppTituloSistema());
			document.addCreationDate();
			document.addTitle(EtiquetasLabel.getMisAlumnos());

			XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
			String str = 
				"<table><tbody>"+
					"<tr><td><span style=\"font-size:16px\"><strong>"+ParametroRN.LeeEscuelaNombre()+ "</strong></span></td></tr>" +
					"<tr><td><span style=\"font-size:16px\"><strong>"+MateriaRN.Read(asignatura.getMateriaId()).toString() + "  " + CursoRN.Read(asignatura.getCursoId()).toString() + "</strong></span></td></tr>" +
					"<tr><td><span style=\"font-size:16px\"><strong>"+ProfesorRN.Read(asignatura.getProfesorId()).toString() + "</strong></span></td></tr>" +
				"</tbody></table>" +
				"<table border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%\"><tbody>"+ 
					"<tr>" +
						"<th align=\"center\" style=\"width: 40px;\">Orden</th>" +
						"<th align=\"center\" style=\"width: 350px;\">Apellido, Nombre</th>";
			
			if (cantidadPeriodos == Dominios.Periodos.BIMESTRE){
				str = str  + 
						"<th align=\"center\" style=\"width: 100px;\">1� Bimestre</th>" +
						"<th align=\"center\" style=\"width: 100px;\">2� Bimestre</th>" +
						"<th align=\"center\" style=\"width: 100px;\">3� Bimestre</th>" +
						"<th align=\"center\" style=\"width: 100px;\">4� Bimestre</th>" +
						"<th align=\"center\" style=\"width: 100px;\">Final</th>" +
						"</tr>";
			}
			else if (cantidadPeriodos == Dominios.Periodos.TRIMESTRE){
				str = str  + 
						"<th align=\"center\" style=\"width: 150px;\">1� Trimestre</th>" +
						"<th align=\"center\" style=\"width: 150px;\">2� Trimestre</th>" +
						"<th align=\"center\" style=\"width: 150px;\">3� Trimestre</th>" +
						"<th align=\"center\" style=\"width: 150px;\">Final</th>" +
						"</tr>";
			}
			else if (cantidadPeriodos == Dominios.Periodos.SEMESTRE){
				str = str  + 
						"<th align=\"center\" style=\"width: 150px;\">1� Semestre</th>" +
						"<th align=\"center\" style=\"width: 150px;\">2� Semestre</th>" +
						"<th align=\"center\" style=\"width: 150px;\">Final</th>" +
						"</tr>";
			}
			str = ArmaCalendarioExcel(asignatura ,null ,str ,PDF);
			str = str + "</tbody></table>";
			worker.parseXHtml(pdfWriter, document, new StringReader(str));
			document.close();
		}catch(Exception e){
			System.out.print(e.toString());
		}
		return "./VAADIN/temp/"+fileNombre+".pdf";
	}

	private static String ArmaCalendarioExcel(Asignatura asignatura ,Sheet hoja ,String html ,int mode){
		List<Planilla> listPlanillaPeriodo1  = PlanillaSelect.SelectXAnioXMateriaXcursoIdXPeriodo(asignatura.getAnio() , asignatura.getMateriaId(), asignatura.getCursoId(), 1, true);
		List<Planilla> listPlanillaPeriodo2  = PlanillaSelect.SelectXAnioXMateriaXcursoIdXPeriodo(asignatura.getAnio() , asignatura.getMateriaId(), asignatura.getCursoId(), 2, true);
		List<Planilla> listPlanillaPeriodo3  = PlanillaSelect.SelectXAnioXMateriaXcursoIdXPeriodo(asignatura.getAnio() , asignatura.getMateriaId(), asignatura.getCursoId(), 3, true);
		List<Planilla> listPlanillaPeriodo4  = PlanillaSelect.SelectXAnioXMateriaXcursoIdXPeriodo(asignatura.getAnio() , asignatura.getMateriaId(), asignatura.getCursoId(), 4, true);
		List<Planilla> listPlanillaPeriodo10 = PlanillaSelect.SelectXAnioXMateriaXcursoIdXPeriodo(asignatura.getAnio() , asignatura.getMateriaId(), asignatura.getCursoId(), 10, true);
		int row = 5;
		DecimalFormat df = new DecimalFormat("00");
		short fontHeigt = 12;
		CellStyle styleTitle = null;
		if (mode == EXCEL){
			styleTitle = hoja.getWorkbook().createCellStyle();
			Font fuente = hoja.getWorkbook().createFont();
			fuente.setFontHeightInPoints(fontHeigt);
			fuente.setBoldweight(Font.BOLDWEIGHT_BOLD);
			styleTitle.setFont(fuente);
		}			
		for (AsignaturaAlumno asignaturaAlumno : AsignaturaAlumnoSelect.LeeAlumnosXAsignatura(asignatura.getId() ,true)){
			int nota1 = 0 ,nota2 = 0 ,nota3 = 0 ,nota4 = 0 ,nota10 = 0; 
			if (listPlanillaPeriodo1.size() > 0){
				PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXPlanillaXAlumno(listPlanillaPeriodo1.get(0).getId() , asignaturaAlumno.getId().getAlumnoId());
				if (planillaAlumno != null)
					nota1 = planillaAlumno.getNota();
			}
			if (listPlanillaPeriodo2.size() > 0){
				PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXPlanillaXAlumno(listPlanillaPeriodo2.get(0).getId() , asignaturaAlumno.getId().getAlumnoId());
				if (planillaAlumno != null)
					nota2 = planillaAlumno.getNota();
			}
			if (listPlanillaPeriodo3.size() > 0){
				PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXPlanillaXAlumno(listPlanillaPeriodo3.get(0).getId() , asignaturaAlumno.getId().getAlumnoId());
				if (planillaAlumno != null)
					nota3 = planillaAlumno.getNota();
			}
			if (listPlanillaPeriodo4.size() > 0){
				PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXPlanillaXAlumno(listPlanillaPeriodo4.get(0).getId() , asignaturaAlumno.getId().getAlumnoId());
				if (planillaAlumno != null)
					nota4 = planillaAlumno.getNota();
			}
			if (listPlanillaPeriodo10.size() > 0){
				PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXPlanillaXAlumno(listPlanillaPeriodo10.get(0).getId() , asignaturaAlumno.getId().getAlumnoId());
				if (planillaAlumno != null){
					nota10 = planillaAlumno.getNota();
				}
			}
			
			if (mode == EXCEL){
				Row fila = hoja.createRow(row);
				Cell celda = fila.createCell(0);
				celda.setCellValue(df.format(asignaturaAlumno.getOrden()));
				celda = fila.createCell(1);
				celda.setCellValue(AlumnoRN.Read(asignaturaAlumno.getId().getAlumnoId()).toString());
				if (cantidadPeriodos == Dominios.Periodos.BIMESTRE){
					celda = fila.createCell(2);
					celda.setCellValue(nota1);
					celda = fila.createCell(3);
					celda.setCellValue(nota2);
					celda = fila.createCell(4);
					celda.setCellValue(nota3);
					celda = fila.createCell(5);
					celda.setCellValue(nota4);
					celda = fila.createCell(6);
					celda.setCellValue(nota10);
				}
				else if (cantidadPeriodos == Dominios.Periodos.TRIMESTRE){
					celda = fila.createCell(2);
					celda.setCellValue(nota1);
					celda = fila.createCell(3);
					celda.setCellValue(nota2);
					celda = fila.createCell(4);
					celda.setCellValue(nota3);
					celda = fila.createCell(5);
					celda.setCellValue(nota10);
					
				}
				else if (cantidadPeriodos == Dominios.Periodos.SEMESTRE){
					celda = fila.createCell(2);
					celda.setCellValue(nota1);
					celda = fila.createCell(3);
					celda.setCellValue(nota2);
					celda = fila.createCell(4);
					celda.setCellValue(nota10);
						}
				row++;
			}
			else if (mode == PDF){
				html = html + 
						"<tr>" +
							"<td>"+df.format(asignaturaAlumno.getOrden())+"</td>" +
							"<td>"+AlumnoRN.Read(asignaturaAlumno.getId().getAlumnoId()).toString()+"</td>";
				if (cantidadPeriodos == Dominios.Periodos.BIMESTRE){
					html = html + 
							"<td>"+String.valueOf(nota1)+"</td>" +
							"<td>"+String.valueOf(nota2)+"</td>" +
							"<td>"+String.valueOf(nota3)+"</td>" +
							"<td>"+String.valueOf(nota4)+"</td>" +
							"<td>"+String.valueOf(nota10)+"</td>" +
							"</tr>";
				}
				else if (cantidadPeriodos == Dominios.Periodos.TRIMESTRE){
					html = html + 
							"<td>"+String.valueOf(nota1)+"</td>" +
							"<td>"+String.valueOf(nota2)+"</td>" +
							"<td>"+String.valueOf(nota3)+"</td>" +
							"<td>"+String.valueOf(nota10)+"</td>" +
							"</tr>";
				}
				else if (cantidadPeriodos == Dominios.Periodos.SEMESTRE){
					html = html + 
							"<td>"+String.valueOf(nota1)+"</td>" +
							"<td>"+String.valueOf(nota2)+"</td>" +
							"<td>"+String.valueOf(nota10)+"</td>" +
							"</tr>";
				}
			}
		}
		return html;
	}
}