package ar.com.trisquel.escuela.componentes.personas.profesor;

import java.util.Date;
import java.util.GregorianCalendar;

import ar.com.trisquel.escuela.componentes.personas.alumno.AlumnoABM;
import ar.com.trisquel.escuela.componentes.personas.alumno.AlumnoWWView;
import ar.com.trisquel.escuela.data.contenedor.ProfesorCursoContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.UsuarioRN;
import ar.com.trisquel.escuela.data.select.AlumnoAsistenciaSelect;
import ar.com.trisquel.escuela.data.select.AlumnoSelect;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class MisCursosView extends VerticalLayout implements ClickListener, View {

	public static final String VIEW = "MisCursosWW";
	private ButtonIcon botonFiltros;
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonEliminar;
	private ButtonIcon botonRefrescar;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private InlineDateField filtroAnio;
	private ComboBox filtroCurso;
	private Table table;
	private VerticalLayout layoutFiltros;
	private VerticalLayout layoutTabla;
	private UI ui;
	private Usuario usuario;
	
	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass(), ui);
		if (!control.isAcceso()) {
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		} else {
			onDraw();
		}
	}

	public void onDraw() {
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		
		Sesion sesion = (Sesion)ui.getSession().getAttribute(R.Session.SESSION);
		usuario = UsuarioRN.Read(sesion.getUsuarioId());
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(EtiquetasLabel.getMisCursos().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true, this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonFiltros = new ButtonIcon(ButtonIcon.BUSCAR, true, this);
		botonFiltros.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonFiltros);
		botonFiltros.setEnabled(false);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		botonAgregar.setEnabled(false);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		//botonModificar.setEnabled(false);
		layoutBotones.addComponent(botonModificar);
		botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true, this);
		botonEliminar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonEliminar);
		botonEliminar.setEnabled(false);
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true, this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonExcel);
		//botonExcel.setEnabled(false);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true, this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonPDF);
		//botonPDF.setEnabled(false);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		layoutTabla = new VerticalLayout();
		layout.addComponent(layoutTabla);
		ArmaTabla();
		Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonFiltros) {
			layoutFiltros.setVisible(!layoutFiltros.isVisible());
		} else if (event.getButton() == botonRefrescar) {
			CreaDataSource();
		} else if (event.getButton() == botonAgregar) {
		} else if (event.getButton() == botonModificar) {
			Curso curso = (Curso)filtroCurso.getValue();
			if (curso != null){
				Window windowABM = CreaWindowABM();
				windowABM.setCaption("Asistencias");
				AsistenciaABM asistenciaABM = new AsistenciaABM(curso,ui, windowABM);
				windowABM.setContent(asistenciaABM);
				windowABM.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						CreaDataSource();
					}
				});
				ui.addWindow(windowABM);
			}
		} else if (event.getButton() == botonEliminar) {
		} else if (event.getButton() == botonExcel) {
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(filtroAnio.getValue());
			Curso curso = (Curso)filtroCurso.getValue();
			if (curso != null){
				String fileName = MisCursosExportar.ExportarExcel(curso, calendar.get(GregorianCalendar.YEAR));
				ui.getPage().open(fileName ,"_blank", false);
			}
		} else if (event.getButton() == botonPDF) {
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(filtroAnio.getValue());
			Curso curso = (Curso)filtroCurso.getValue();
			if (curso != null){
				String fileName = MisCursosExportar.ExportarPDF(curso, calendar.get(GregorianCalendar.YEAR));
				ui.getPage().open(fileName ,"_blank", false);
			}
		}
	}

	private void ArmaFiltros() {
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout formFiltro = new GridLayout();
		formFiltro.setHeight("30px");
		formFiltro.setColumns(6);
		//A�o
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label label = new Label("A�o:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("40px");
		formFiltro.addComponent(label);
		filtroAnio = new InlineDateField();
		filtroAnio.addStyleName(R.Style.EDITABLE);
		filtroAnio.setWidth("150px");
		filtroAnio.addStyleName(R.Style.LABEL_BOLD);
		filtroAnio.setResolution(Resolution.YEAR);
		filtroAnio.setImmediate(true);
		filtroAnio.setValue(new Date(System.currentTimeMillis()));
		formFiltro.addComponent(filtroAnio);
		//Curso
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		imageIcono.addStyleName(R.Style.MARGIN_MEDIO);
		formFiltro.addComponent(imageIcono);
		label = new Label("Curso:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("45px");
		formFiltro.addComponent(label);
		filtroCurso = new ComboBox();
		filtroCurso.setInputPrompt(EtiquetasLabel.getSeleccionar());
		filtroCurso.addStyleName(R.Style.EDITABLE);
		filtroCurso.addStyleName(R.Style.LABEL_BOLD);
		filtroCurso.setImmediate(true);
		filtroCurso.setWidth("100px");
		filtroCurso.setTextInputAllowed(false);
		@SuppressWarnings("unchecked")
		BeanItemContainer<Curso> container = ProfesorCursoContenedor.LeeContainerCursoXProfesor(usuario.getProfesorId());
		filtroCurso.setContainerDataSource(container);
		if (container.size() > 0){
			filtroCurso.setValue(container.getIdByIndex(0));
			filtroCurso.setNullSelectionAllowed(false);
		}
		else{
			filtroCurso.setInputPrompt("No tiene cursos asignadas");
			filtroCurso.setNullSelectionAllowed(false);
		}
		formFiltro.addComponent(filtroCurso);
		filtroCurso.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				CreaDataSource();
			}
		});
		layoutFiltros.addComponent(formFiltro);
		filtroAnio.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				CreaDataSource();
			}
		});
	}
	
	
	private void ArmaTabla() {
		layoutTabla.removeAllComponents();
		table = new Table();
		layoutTabla.addComponent(table);
		table.setHeight("550px");
		table.setWidth("985px");
		table.setSelectable(true);
		CreaDataSource();
	}

	private void CreaDataSource() {
		table.removeAllItems();
		Curso curso = (Curso)filtroCurso.getValue();
		table.addContainerProperty("Apellido, Nombre" ,Button.class,  null);
		table.addContainerProperty("Inasistencias" ,Label.class,  null);
		table.addContainerProperty("Inasistencias Justificadas" ,Label.class,  null);
		table.addContainerProperty("Inasistencias Injustificadas" ,Label.class,  null);
		table.setColumnWidth("Apellido, Nombre", 350);
		table.setColumnWidth("Inasistencias", 180);
		table.setColumnWidth("Inasistencias Justificadas", 180);
		table.setColumnWidth("Inasistencias Injustificadas", 180);
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(filtroAnio.getValue());
		if (curso != null){
			for (final Alumno alumno : AlumnoSelect.SelectXCursoId(curso.getId())){
				Button botonVer = new Button(alumno.toString());
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.setWidth("350px");
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = AlumnoWWView.CreaWindowABM();
						AlumnoABM alumnoABM = new AlumnoABM(alumno ,Dominios.AccesoADatos.SELECT ,ui ,windowABM);
						windowABM.setContent(alumnoABM);
						ui.addWindow(windowABM);
					}
				});
				int[] asistencia = AlumnoAsistenciaSelect.LeeInasistenciaXAlumnoXAnio(alumno.getId(),calendar.get(GregorianCalendar.YEAR) );
				Object[] columnas = new Object[4];
				columnas[0] = botonVer;
				Label inasistencia = new Label();
				inasistencia.addStyleName(R.Style.EDITABLE);
				inasistencia.addStyleName(R.Style.LABEL_BOLD);
				inasistencia.addStyleName(R.Style.TEXTO_ALINEACION_DERECHA);
				if (asistencia[0] + asistencia[1] > 0){
					inasistencia.setValue(String.valueOf(asistencia[0] + asistencia[1]));
					inasistencia.addStyleName(R.Style.LABEL_COLOR_ROJO);
				}
				columnas[1] = inasistencia;
				inasistencia = new Label();
				inasistencia.addStyleName(R.Style.EDITABLE);
				inasistencia.addStyleName(R.Style.LABEL_BOLD);
				inasistencia.addStyleName(R.Style.TEXTO_ALINEACION_DERECHA);
				if (asistencia[0] > 0){
					inasistencia.setValue(String.valueOf(asistencia[0]));
					inasistencia.addStyleName(R.Style.LABEL_COLOR_ROJO);
				}
				columnas[2] = inasistencia;
				inasistencia = new Label();
				inasistencia.addStyleName(R.Style.EDITABLE);
				inasistencia.addStyleName(R.Style.LABEL_BOLD);
				inasistencia.addStyleName(R.Style.TEXTO_ALINEACION_DERECHA);
				if (asistencia[1] > 0){
					inasistencia.setValue(String.valueOf(asistencia[1]));
					inasistencia.addStyleName(R.Style.LABEL_COLOR_ROJO);
				}
				columnas[3] = inasistencia;
				table.addItem(columnas ,alumno);
			}
		}
	}
	
	public static Window CreaWindowABM(){
		Window windowABM = new Window(EtiquetasLabel.getMisCursos());
		windowABM.setModal(true);
		windowABM.setResizable(false);
		windowABM.setDraggable(false);
		windowABM.setWidth("650px");
		windowABM.setHeight("600px");
		windowABM.center();
		windowABM.setCloseShortcut(KeyCode.ESCAPE, null);
		return windowABM;
	}
}
