package ar.com.trisquel.escuela.componentes.personas.profesor;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import ar.com.trisquel.escuela.data.reglasdenegocio.ParametroRN;
import ar.com.trisquel.escuela.data.select.AlumnoAsistenciaSelect;
import ar.com.trisquel.escuela.data.select.AlumnoSelect;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.utiles.ImageCaptcha;
import ar.com.trisquel.escuela.utiles.Recursos;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.vaadin.server.VaadinService;

public class MisCursosExportar {
	
	private static final int EXCEL = 1;
	private static final int PDF = 2;
	
	public MisCursosExportar() {
	}

	public static String ExportarExcel(Curso curso ,int anio){
		Workbook libro = new HSSFWorkbook();
		short fontHeigt = 14;
		CellStyle styleTitle = libro.createCellStyle();
		Font fuente = libro.createFont();
		fuente.setFontHeightInPoints(fontHeigt);
		fuente.setBoldweight(Font.BOLDWEIGHT_BOLD);
		styleTitle.setFont(fuente);
		
		CellStyle styleTituloColumna = libro.createCellStyle();
		fuente = libro.createFont();
		fuente.setBoldweight(Font.BOLDWEIGHT_BOLD);
		styleTituloColumna.setFont(fuente);
		styleTituloColumna.setFillBackgroundColor(IndexedColors.AQUA.getIndex());
		styleTituloColumna.setBottomBorderColor(IndexedColors.BLUE_GREY.getIndex());
		styleTituloColumna.setTopBorderColor(IndexedColors.BLUE_GREY.getIndex());
		styleTituloColumna.setBorderBottom(CellStyle.BORDER_MEDIUM);
		styleTituloColumna.setBorderTop(CellStyle.BORDER_MEDIUM);
		//A�adir una hoja al libro:
		Sheet hoja = libro.createSheet("Export");
		//Crear una fila:
		Row fila = hoja.createRow(0);
		//Se accede a la celda 2 de la fila 1:
		Cell celda = fila.createCell(0);
		// Generamos el contenido de la celda.
		HSSFRichTextString miContenido= new HSSFRichTextString(ParametroRN.LeeEscuelaNombre());
		celda.setCellValue(miContenido);
		celda.setCellStyle(styleTitle);
		
		fila = hoja.createRow(1);
		celda = fila.createCell(0);
		celda.setCellValue("A�o: " + String.valueOf(anio));
		celda.setCellStyle(styleTitle);
		
		fila = hoja.createRow(2);
		celda = fila.createCell(0);
		celda.setCellValue("Curso: " + curso.toString());
		celda.setCellStyle(styleTitle);
		
		fila = hoja.createRow(4);
		celda = fila.createCell(0);
		celda.setCellValue("Alumno");
		celda.setCellStyle(styleTituloColumna);
		celda = fila.createCell(1);
		celda.setCellValue("Inasistencias");
		celda.setCellStyle(styleTituloColumna);
		celda = fila.createCell(2);
		celda.setCellValue("Inasistencias Justificadas");
		celda.setCellStyle(styleTituloColumna);
		celda = fila.createCell(3);
		celda.setCellValue("Inasistencias Injustificadas");
		celda.setCellStyle(styleTituloColumna);
		hoja.createFreezePane(0, 5);
		
		ArmaAsistencia(curso, anio,hoja ,null ,EXCEL);
		
		hoja.autoSizeColumn(0);
		hoja.autoSizeColumn(1);
		hoja.autoSizeColumn(2);
		hoja.autoSizeColumn(3);
		
		String fileNombre = new String(ImageCaptcha.generarRamdom(20));
		String fileGuardar = VaadinService.getCurrent().getBaseDirectory()+ "/VAADIN/temp/"+fileNombre+".xls";
		OutputStream output;
		try {
			output = new FileOutputStream(fileGuardar);
			libro.write(output);
			output.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "./VAADIN/temp/"+fileNombre+".xls";
	}

	public static String  ExportarPDF(Curso curso ,int anio){
		String fileNombre = new String(ImageCaptcha.generarRamdom(20));
		String fileGuardar = VaadinService.getCurrent().getBaseDirectory()+ "/VAADIN/temp/"+fileNombre+".pdf";
		try{
			Document document = new Document(PageSize.A4 );
			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fileGuardar));
			document.open();
			document.addAuthor(Recursos.Parametros.getAppTituloSistema());
			document.addCreationDate();
			document.addTitle(EtiquetasLabel.getMisAlumnos());

			XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
			String str = 
				"<table><tbody>"+
					"<tr><td><span style=\"font-size:16px\"><strong>"+ParametroRN.LeeEscuelaNombre()+ "</strong></span></td></tr>" +
					"<tr><td><span style=\"font-size:16px\"><strong>"+"A�o: " + String.valueOf(anio) + "</strong></span></td></tr>" +
					"<tr><td><span style=\"font-size:16px\"><strong>"+"Curso: " + curso.toString() + "</strong></span></td></tr>" +
				"</tbody></table>" +
				"<table border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%\"><tbody>"+ 
					"<tr>" +
						"<th align=\"center\" style=\"width: 350px;\">Alumno</th>" +
						"<th align=\"center\" style=\"width: 150px;\">Inasistencias</th>" +			
						"<th align=\"center\" style=\"width: 150px;\">Inasistencias Justificadas</th>" +
						"<th align=\"center\" style=\"width: 150px;\">Inasistencias Injustificadas</th>" +
						"</tr>";
			str = ArmaAsistencia(curso, anio,null ,str ,PDF);
			str = str + "</tbody></table>";
			worker.parseXHtml(pdfWriter, document, new StringReader(str));
			document.close();
		}catch(Exception e){
			System.out.print(e.toString());
		}
		return "./VAADIN/temp/"+fileNombre+".pdf";
	}

	private static String ArmaAsistencia(Curso curso ,int anio ,Sheet hoja ,String html ,int mode){
		int row = 5;
		short fontHeigt = 12;
		CellStyle styleTitle = null;
		if (mode == EXCEL){
			styleTitle = hoja.getWorkbook().createCellStyle();
			Font fuente = hoja.getWorkbook().createFont();
			fuente.setFontHeightInPoints(fontHeigt);
			fuente.setBoldweight(Font.BOLDWEIGHT_BOLD);
			styleTitle.setFont(fuente);
		}			
		for (final Alumno alumno : AlumnoSelect.SelectXCursoId(curso.getId())){		
			int[] asistencia = AlumnoAsistenciaSelect.LeeInasistenciaXAlumnoXAnio(alumno.getId(),anio );
			if (mode == EXCEL){
				Row fila = hoja.createRow(row);
				Cell celda = fila.createCell(0);
				celda.setCellValue(alumno.toString());
				celda = fila.createCell(1);
				celda.setCellValue(asistencia[0]+asistencia[1]);
				celda = fila.createCell(2);
				celda.setCellValue(asistencia[0]);
				celda = fila.createCell(3);
				celda.setCellValue(asistencia[1]);
				row++;
			}
			else if (mode == PDF){
				html = html + 
						"<tr>" +
							"<td>"+alumno.toString()+"</td>" +
							"<td>"+String.valueOf(asistencia[0]+asistencia[1])+"</td>" +
							"<td>"+asistencia[0]+"</td>" +
							"<td>"+asistencia[1]+"</td>" +
							"</tr>";
			}
		}
		return html;
	}
}