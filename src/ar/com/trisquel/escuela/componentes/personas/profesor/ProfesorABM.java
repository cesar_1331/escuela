package ar.com.trisquel.escuela.componentes.personas.profesor;

import java.util.ArrayList;
import java.util.List;

import ar.com.trisquel.escuela.componentes.parametro.EscuelaView;
import ar.com.trisquel.escuela.componentes.user.UsuarioABM;
import ar.com.trisquel.escuela.componentes.user.UsuarioWWView;
import ar.com.trisquel.escuela.data.contenedor.MateriaContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.ParametroRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ProfesorMateriaRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ProfesorRN;
import ar.com.trisquel.escuela.data.select.ProfesorMateriaSelect;
import ar.com.trisquel.escuela.data.tablas.Materia;
import ar.com.trisquel.escuela.data.tablas.Parametro;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.data.tablas.identificadores.ProfesorMateriaId;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.BaseTheme;

@SuppressWarnings("serial")
public class ProfesorABM extends VerticalLayout implements ClickListener {
	private TextField id;
	private TextField nombre;
	private TextField apellido;
	private TextField titulo;
	private TextField domicilio;
	private TextField ciudad;
	private TextField provincia;
	private TextField telefono;
	private TextField celular;
	private CheckBox activo;
	private CheckBox esPreceptor;
	private TextArea observaciones;
	private ButtonIcon ButtonGuardar;
	private ButtonIcon ButtonDescartar;
	private Label profesorError;
	private VerticalLayout layoutMaterias;
	private UI ui;
	private String modo;
	private Window window;
	private Profesor profesor;
	private boolean backgroundFilaUno = true;
	private List<Materia> listMateria = new ArrayList<Materia>();

	public ProfesorABM(Profesor profesor, String modo, UI ui, Window window) {
		this.ui = ui;
		this.modo = modo;
		this.window = window;
		this.profesor = profesor;
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setWidth("100%");
		layout.setColumns(3);
		addComponent(layout);
		//Id
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelId = new Label("Id:");
		labelId.addStyleName(R.Style.EDITABLE_LABEL);
		labelId.setWidth("90px");
		layout.addComponent(labelId);
		HorizontalLayout layoutId = new HorizontalLayout();
		layoutId.setWidth("710px");
		id = new TextField();
		id.addStyleName(R.Style.EDITABLE);
		id.addStyleName(R.Style.LABEL_BOLD);
		layoutId.addComponent(id);
		//Botones
		HorizontalLayout layoutBotones = new HorizontalLayout();
		ButtonGuardar = new ButtonIcon(ButtonIcon.GUARDAR, true, this);
		ButtonGuardar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonGuardar);
		ButtonDescartar = new ButtonIcon(ButtonIcon.DESCARTAR, true, this);
		ButtonDescartar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonDescartar);
		layoutId.addComponent(layoutBotones);
		layoutId.setComponentAlignment(layoutBotones, Alignment.TOP_RIGHT);
		layout.addComponent(layoutId);
		//Nombre
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelNombre = new Label("Nombre:");
		labelNombre.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(labelNombre);
		nombre = new TextField();
		nombre.addStyleName(R.Style.EDITABLE);
		nombre.addStyleName(R.Style.LABEL_BOLD);
		nombre.addStyleName(R.Style.TEXTO_MAYUSCULA);
		nombre.setMaxLength(40);
		nombre.setWidth("200px");
		nombre.setNullRepresentation("");
		nombre.setNullSettingAllowed(false);
		layout.addComponent(nombre);
		//Apellido
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelApellido = new Label("Apellido:");
		labelApellido.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(labelApellido);
		apellido = new TextField();
		apellido.addStyleName(R.Style.EDITABLE);
		apellido.addStyleName(R.Style.LABEL_BOLD);
		apellido.addStyleName(R.Style.TEXTO_MAYUSCULA);
		apellido.setWidth("200px");
		apellido.setNullRepresentation("");
		apellido.setNullSettingAllowed(false);
		apellido.setMaxLength(40);
		layout.addComponent(apellido);
		//Titulo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelTitulo = new Label("Titulo:");
		labelTitulo.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(labelTitulo);
		titulo = new TextField();
		titulo.addStyleName(R.Style.EDITABLE);
		titulo.addStyleName(R.Style.LABEL_BOLD);
		titulo.setWidth("300px");
		titulo.setNullRepresentation("");
		titulo.setNullSettingAllowed(false);
		titulo.setMaxLength(60);
		layout.addComponent(titulo);
		//Domicilio
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelDomicilio = new Label("Domicilio:");
		labelDomicilio.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(labelDomicilio);
		domicilio = new TextField();
		domicilio.addStyleName(R.Style.EDITABLE);
		domicilio.addStyleName(R.Style.LABEL_BOLD);
		domicilio.setMaxLength(100);
		domicilio.setWidth("500px");
		domicilio.setNullRepresentation("");
		domicilio.setNullSettingAllowed(false);
		layout.addComponent(domicilio);		
		//Ciudad
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelCiudad = new Label("Ciudad:");
		labelCiudad.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(labelCiudad);
		ciudad = new TextField();
		ciudad.addStyleName(R.Style.EDITABLE);
		ciudad.addStyleName(R.Style.LABEL_BOLD);
		ciudad.setWidth("200px");
		ciudad.setMaxLength(40);
		ciudad.setNullRepresentation("");
		ciudad.setNullSettingAllowed(false);
		layout.addComponent(ciudad);
		//Provincia
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelProvincia = new Label("Provincia:");
		labelProvincia.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(labelProvincia);
		provincia = new TextField();
		provincia.addStyleName(R.Style.EDITABLE);
		provincia.addStyleName(R.Style.LABEL_BOLD);
		provincia.setWidth("200px");
		provincia.setMaxLength(40);
		provincia.setNullRepresentation("");
		provincia.setNullSettingAllowed(false);
		layout.addComponent(provincia);
		//Telefono
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelTelefono = new Label("Tel�fono:");
		labelTelefono.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(labelTelefono);
		telefono = new TextField();
		telefono.addStyleName(R.Style.EDITABLE);
		telefono.addStyleName(R.Style.LABEL_BOLD);
		telefono.setWidth("200px");
		telefono.setMaxLength(40);
		telefono.setNullRepresentation("");
		telefono.setNullSettingAllowed(false);
		layout.addComponent(telefono);
		//Celular
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelCelular = new Label("Celular:");
		labelCelular.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(labelCelular);
		celular = new TextField();
		celular.addStyleName(R.Style.EDITABLE);
		celular.addStyleName(R.Style.LABEL_BOLD);
		celular.setWidth("200px");
		celular.setMaxLength(40);
		celular.setNullRepresentation("");
		celular.setNullSettingAllowed(false);
		layout.addComponent(celular);
		//Es Preceptor
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelEsPreceptor = new Label("Es preceptor:");
		labelEsPreceptor.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(labelEsPreceptor);
		HorizontalLayout layoutPreceptorActivo = new HorizontalLayout();
		layout.addComponent(layoutPreceptorActivo);
		esPreceptor = new CheckBox();
		esPreceptor.setWidth("200px");
		layoutPreceptorActivo.addComponent(esPreceptor);
		//Activo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layoutPreceptorActivo.addComponent(imageIcono);
		Label labelActivo = new Label("Activo:");
		labelActivo.addStyleName(R.Style.EDITABLE_LABEL);
		labelActivo.setWidth("60px");
		layoutPreceptorActivo.addComponent(labelActivo);
		activo = new CheckBox();
		layoutPreceptorActivo.addComponent(activo);
		//Observaciones
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Observaciones:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		VerticalLayout layoutObservaciones = new VerticalLayout();
		layout.addComponent(layoutObservaciones);
		Label labelObservaciones = new Label();
		labelObservaciones.addStyleName(R.Style.EDITABLE);
		labelObservaciones.addStyleName(R.Style.LABEL_BOLD);
		labelObservaciones.setWidth("700px");
		layoutObservaciones.addComponent(labelObservaciones);
		observaciones = new TextArea();
		observaciones.addStyleName(R.Style.EDITABLE);
		observaciones.addStyleName(R.Style.LABEL_BOLD);
		observaciones.setMaxLength(1024);
		observaciones.setWidth("700px");
		observaciones.setHeight("80px");
		observaciones.setNullRepresentation("");
		observaciones.setNullSettingAllowed(false);
		layoutObservaciones.addComponent(observaciones);	

		layout.addComponent(new Label());
		layout.addComponent(new Label());
		profesorError = new Label();
		profesorError.addStyleName(R.Style.ERROR_VIEW);
		layout.addComponent(profesorError);
		
		//Materias
		layout.addComponent(new Label());
		layout.addComponent(new Label());
		layoutMaterias = new VerticalLayout();
		layoutMaterias.setWidth("100%");
		layout.addComponent(layoutMaterias);
		ArmaMaterias();		
		
		if (modo.equals(Dominios.AccesoADatos.INSERT)){
			id.setReadOnly(true);
			Parametro parametro = ParametroRN.Read(EscuelaView.ESCUELA_CIUDAD);
			if (parametro != null)
				ciudad.setValue(parametro.getValor());
			parametro = ParametroRN.Read(EscuelaView.ESCUELA_PROVINCIA);
			if (parametro != null)
				provincia.setValue(parametro.getValor());
			labelObservaciones.setVisible(false);
			activo.setValue(true);
			esPreceptor.setValue(false);
			activo.setEnabled(false);
			ButtonGuardar.setVisible(true);
			ButtonDescartar.setVisible(true);
		}
		else{
			id.setValue(String.valueOf(profesor.getId()));
			nombre.setValue(profesor.getNombre());
			apellido.setValue(profesor.getApellido());
			titulo.setValue(profesor.getTitulo());
			domicilio.setValue(profesor.getDomicilio());
			ciudad.setValue(profesor.getCiudad());
			provincia.setValue(profesor.getProvincia());
			telefono.setValue(profesor.getTelefono());
			celular.setValue(profesor.getCelular());
			activo.setValue(profesor.isActivo());
			esPreceptor.setValue(profesor.isEsPreceptor());
			observaciones.setValue(profesor.getObservaciones());
			labelObservaciones.setValue(profesor.getObservaciones());
			if (modo.equals(Dominios.AccesoADatos.SELECT)) {
				id.setReadOnly(true);
				nombre.setReadOnly(true);
				apellido.setReadOnly(true);
				titulo.setReadOnly(true);
				domicilio.setReadOnly(true);
				ciudad.setReadOnly(true);
				provincia.setReadOnly(true);
				telefono.setReadOnly(true);
				celular.setReadOnly(true);
				observaciones.setVisible(false);
				activo.setReadOnly(true);
				esPreceptor.setReadOnly(true);
				ButtonGuardar.setVisible(false);
				ButtonDescartar.setVisible(false);
			} else if (modo.equals(Dominios.AccesoADatos.UPDATE)) {
				id.setReadOnly(true);
				labelObservaciones.setVisible(false);
				ButtonGuardar.setVisible(true);
				ButtonDescartar.setVisible(true);
			}
		}
		focus();
	}
	
	private void ArmaMaterias(){
		layoutMaterias.removeAllComponents();
		HorizontalLayout layoutTitulo = new HorizontalLayout();
		layoutTitulo.setPrimaryStyleName(R.Style.TEXTO_SUBTITULO_COLOR);
		layoutTitulo.setWidth("100%");
		Label labelTitleServicio = new Label(EtiquetasLabel.getMaterias());
		labelTitleServicio.setWidth("690px");
		layoutTitulo.addComponent(labelTitleServicio);
		layoutMaterias.addComponent(layoutTitulo);
		if (profesor != null){
			listMateria = ProfesorMateriaSelect.SelectMateriasXProfesor(profesor.getId());
		}
		for (Materia materia : listMateria){
			ArmaFila(materia);
		}
		if (!modo.equals(Dominios.AccesoADatos.SELECT)){
			HorizontalLayout layoutAgregar = new HorizontalLayout();
			layoutAgregar.setStyleName(R.Style.EXPAND_WIDTH_ALL);
			if (backgroundFilaUno){
				layoutAgregar.addStyleName(R.Style.BACKGROUND_FILA_1);
			}
			else{
				layoutAgregar.addStyleName(R.Style.BACKGROUND_FILA_2);
			}
			final Button ButtonMostrarAgregar = new Button(EtiquetasLabel.getAgregar());
			ButtonMostrarAgregar.setPrimaryStyleName(BaseTheme.BUTTON_LINK);
			ButtonMostrarAgregar.addStyleName(R.Style.LABEL_COLOR_AZUL);
			ButtonMostrarAgregar.addStyleName(R.Style.LABEL_UNDERLINE);
			ButtonMostrarAgregar.setIcon(new ThemeResource("img/fila_add.png"));
			ButtonMostrarAgregar.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					ButtonMostrarAgregar.setVisible(false);
					ArmaFila(null);
				}
			});
			layoutAgregar.addComponent(ButtonMostrarAgregar);
			layoutMaterias.addComponent(layoutAgregar);
		}
	}
	
	private void ArmaFila(final Materia materia){
		HorizontalLayout layoutFila = new HorizontalLayout();
		if (backgroundFilaUno){
			layoutFila.addStyleName(R.Style.BACKGROUND_FILA_1);
		}
		else{
			layoutFila.addStyleName(R.Style.BACKGROUND_FILA_2);
		}
		layoutMaterias.addComponent(layoutFila);
		backgroundFilaUno = !backgroundFilaUno;
		if (materia != null){
			Label materiaNombre = new Label(materia.getNombre());
			materiaNombre.addStyleName(R.Style.EDITABLE);
			materiaNombre.addStyleName(R.Style.LABEL_BOLD);
			materiaNombre.setWidth("690px");
			layoutFila.addComponent(materiaNombre);
			ButtonIcon materiaEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true ,new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					if (profesor != null){
						ProfesorMateriaRN.Delete(ProfesorMateriaRN.Read(new ProfesorMateriaId(profesor.getId(),materia.getId())));
						ArmaMaterias();
					}
					else{
						listMateria.remove(materia);
						ArmaMaterias();
					}
				}
			});
			
			layoutFila.addComponent(materiaEliminar);
			layoutFila.setComponentAlignment(materiaEliminar ,Alignment.TOP_RIGHT);
			if (modo.equals(Dominios.AccesoADatos.SELECT)){
				materiaEliminar.setEnabled(false);
			}
		}
		else{
			final ComboBox comboMateria = new ComboBox();
			VerticalLayout layoutCombo = new VerticalLayout();
			layoutCombo.setWidth("680px");
			layoutFila.addComponent(layoutCombo);
			comboMateria.setContainerDataSource(MateriaContenedor.LeeContainer(true));
			comboMateria.setNullSelectionAllowed(false);
			comboMateria.setImmediate(true);
			comboMateria.setWidth("400px");
			layoutCombo.addComponent(comboMateria);
			HorizontalLayout layoutBotones = new HorizontalLayout();
			layoutFila.addComponent(layoutBotones);
			layoutFila.setComponentAlignment(layoutBotones ,Alignment.TOP_RIGHT);
			ButtonIcon materiaAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					if (comboMateria.getValue() != null){
						if (profesor != null){
							ProfesorMateriaRN.Save(ProfesorMateriaRN.Inizializate(profesor.getId(), ((Materia)comboMateria.getValue()).getId()));
							ArmaMaterias();
						}
						else{
							listMateria.add((Materia)comboMateria.getValue());
							ArmaMaterias();
						}
					}
				}
			});
			ButtonIcon materiaDescartar = new ButtonIcon(ButtonIcon.DESCARTAR, true, new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					ArmaMaterias();
				}
			});
			layoutBotones.addComponent(materiaAgregar);
			layoutBotones.addComponent(materiaDescartar);
		}
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == ButtonDescartar) {
			ButtonGuardar.setVisible(false);
			ButtonDescartar.setVisible(false);
			if (modo.equals(Dominios.AccesoADatos.INSERT) || modo.equals(Dominios.AccesoADatos.UPDATE)) {
				ui.removeWindow(window);
			}
		} else if (event.getButton() == ButtonGuardar) {
			if (profesor == null){
				profesor = ProfesorRN.Inizializate();
			}
			profesor.setNombre(nombre.getValue());
			profesor.setApellido(apellido.getValue());
			profesor.setTitulo(titulo.getValue());
			profesor.setDomicilio(domicilio.getValue());
			profesor.setCiudad(ciudad.getValue());
			profesor.setProvincia(provincia.getValue());
			profesor.setCelular(celular.getValue());
			profesor.setTelefono(telefono.getValue());
			profesor.setActivo(activo.getValue());
			profesor.setEsPreceptor(esPreceptor.getValue());
			profesor.setObservaciones(observaciones.getValue());
			RespuestaEntidad respuesta = ProfesorRN.Validate(profesor ,modo);
			if (respuesta.isError()) {
				MessageBox.showHTML(Icon.ERROR, "", respuesta.getMsgError(),ButtonId.OK);
			} else {
				MessageBox.showHTML(Icon.QUESTION, "",MensajeLabel.getConfirmaAccion(),new MessageBoxListener() {
					@Override
					public void buttonClicked(ButtonId buttonType) {
						if (buttonType == ButtonId.SAVE) {
							RespuestaEntidad respuesta = ProfesorRN.Save(profesor);
							if (respuesta.isError()) {
								MessageBox.showHTML(Icon.ERROR, "",respuesta.getMsgError(),ButtonId.OK);
							} else {
								if (modo.equals(Dominios.AccesoADatos.INSERT)){
									for (Materia materia : listMateria){
										ProfesorMateriaRN.Save(ProfesorMateriaRN.Inizializate(profesor.getId(), materia.getId()));
									}
									Window windowABM = UsuarioWWView.CreaWindowsABM();
									windowABM.setContent(new UsuarioABM(null ,Dominios.AccesoADatos.INSERT,ui ,windowABM ,0 ,profesor.getId()));
									ui.addWindow(windowABM);
								}
								if (modo.equals(Dominios.AccesoADatos.INSERT) || modo.equals(Dominios.AccesoADatos.UPDATE)) {
									ui.removeWindow(window);
								} else {
									onDraw();
								}
							}
						}
					}
				}, ButtonId.SAVE, ButtonId.CANCEL);
			}
		}
	}

}
