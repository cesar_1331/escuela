package ar.com.trisquel.escuela.componentes.personas.profesor;

import java.util.List;

import ar.com.trisquel.escuela.componentes.parametro.EscuelaView;
import ar.com.trisquel.escuela.data.reglasdenegocio.CursoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ParametroRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.PlanillaRN;
import ar.com.trisquel.escuela.data.select.PlanillaSelect;
import ar.com.trisquel.escuela.data.tablas.Asignatura;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.Parametro;
import ar.com.trisquel.escuela.data.tablas.Planilla;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class PlanillaBuscar extends VerticalLayout implements ClickListener {
	
	private Asignatura asignatura;
	private UI ui;
	private boolean backgroundFilaUno = true;
	private Window window;
	private String modo;
	public PlanillaBuscar(Asignatura asignatura ,UI ui ,Window window ,String modo){
		this.asignatura = asignatura; 
		this.ui = ui;
		this.window = window;
		this.modo = modo;
		onDraw();
	}
	
	public void onDraw(){
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		HorizontalLayout layoutCurso = new HorizontalLayout();
		layoutCurso.setHeight("30px");
		addComponent(layoutCurso);
		Curso curso = CursoRN.Read(asignatura.getCursoId());
		Label label = new Label("Curso:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.addStyleName(R.Style.TEXTO_BIG);
		label.setWidth("60px");
		layoutCurso.addComponent(label);
		label = new Label(curso.toString());
		label.addStyleName(R.Style.EDITABLE);
		label.addStyleName(R.Style.LABEL_BOLD);
		label.addStyleName(R.Style.TEXTO_BIG);
		layoutCurso.addComponent(label);
		
		Parametro parametro = ParametroRN.Read(EscuelaView.ESCUELA_CANTIDADPERIODOS);
		int cantidadPeriodos = 3;
		try{
			cantidadPeriodos = Integer.parseInt(parametro.getValor().trim());
		}catch(Exception e){}
		/**
		 * Lee las planillas
		 */
		List<Planilla> listPlanillaPeriodo  = PlanillaSelect.SelectXAnioXMateriaXcursoIdXPeriodo(asignatura.getAnio() , asignatura.getMateriaId(),asignatura.getCursoId(), 0, false);
		for (final Planilla planilla : listPlanillaPeriodo){
			HorizontalLayout layoutPlanillas = new HorizontalLayout();
			if (backgroundFilaUno){
				layoutPlanillas.addStyleName(R.Style.BACKGROUND_FILA_1);
			}
			else{
				layoutPlanillas.addStyleName(R.Style.BACKGROUND_FILA_2);
			}
			backgroundFilaUno = !backgroundFilaUno;
			addComponent(layoutPlanillas);
			ComboBox comboPeriodo = Dominios.Periodos.CreaComboBoxSegunPeriodo(cantidadPeriodos, false);
			comboPeriodo.setValue(planilla.getPeriodo());
			comboPeriodo.setWidth("200px");
			comboPeriodo.setReadOnly(true);
			layoutPlanillas.addComponent(comboPeriodo);
			TextField descripcion = new TextField();
			descripcion.addStyleName(R.Style.EDITABLE);
			descripcion.addStyleName(R.Style.LABEL_BOLD);
			descripcion.setWidth("300px");
			descripcion.setValue(planilla.getDescripcion());
			descripcion.setReadOnly(true);
			layoutPlanillas.addComponent(descripcion);
			if (modo.equals(Dominios.AccesoADatos.UPDATE)){
				ButtonIcon botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true);
				botonModificar.addStyleName(R.Style.PADDING_MEDIO);
				botonModificar.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = MisAlumnosView.CreaWindowABM();
						PlanillaABM planillaABM = new PlanillaABM(planilla ,asignatura ,Dominios.AccesoADatos.UPDATE ,ui ,windowABM);
						windowABM.setContent(planillaABM);
						ui.addWindow(windowABM);
						windowABM.addCloseListener(new CloseListener() {
							@Override
							public void windowClose(CloseEvent e) {
								ui.removeWindow(window);
							}
						});
					}
				});
				layoutPlanillas.addComponent(botonModificar);
			}
			if (modo.equals(Dominios.AccesoADatos.DELETE)){
				ButtonIcon botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true);
				botonEliminar.addStyleName(R.Style.PADDING_MEDIO);
				botonEliminar.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						MessageBox.showHTML(Icon.QUESTION, "", "�Realmente desea borrar la planilla de notas?", new MessageBoxListener() {
							@Override
							public void buttonClicked(ButtonId buttonType) {
								PlanillaRN.Delete(planilla);
								ui.removeWindow(window);
							}
						}, ButtonId.OK ,ButtonId.CANCEL);
					}
				});
				layoutPlanillas.addComponent(botonEliminar);
			}
		}
	}
	
	
	
	@Override
	public void buttonClick(ClickEvent event) {

	}

}
