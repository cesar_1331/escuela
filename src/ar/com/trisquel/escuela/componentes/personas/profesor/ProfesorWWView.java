package ar.com.trisquel.escuela.componentes.personas.profesor;

import org.vaadin.hene.popupbutton.PopupButton;

import ar.com.trisquel.escuela.componentes.user.UsuarioABM;
import ar.com.trisquel.escuela.componentes.user.UsuarioWWView;
import ar.com.trisquel.escuela.data.contenedor.ProfesorContenedor;
import ar.com.trisquel.escuela.data.select.UsuarioSelect;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class ProfesorWWView extends VerticalLayout implements ClickListener,View {

	public static final String VIEW = "ProfesorWW";
	private static final String LINK_VER = "LINK_VER";
	private static final String COLUMNA_ACCIONES = "COLUMNA_ACCIONES";
	private static final String COLUMNA_ESPRECEPTOR = "COLUMNA_ESPRECEPTOR";
	private ButtonIcon botonFiltros;
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonEliminar;
	private ButtonIcon botonRefrescar;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private ButtonIcon botonUsuario;
	private TextField filtroNombre;
	private CheckBox filtroEsPreceptor;
	private CheckBox filtroActivo;
	private Table table;
	private VerticalLayout layoutFiltros;
	private UI ui;

	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass(), ui);
		if (!control.isAcceso()) {
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		} else {
			onDraw();
		}
	}

	public void onDraw() {
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(EtiquetasLabel.getProfesoresPreceptores().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true, this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonFiltros = new ButtonIcon(ButtonIcon.BUSCAR, true, this);
		botonFiltros.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonFiltros);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		//botonAgregar.setEnabled(false);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		//botonModificar.setEnabled(false);
		layoutBotones.addComponent(botonModificar);
		botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true, this);
		botonEliminar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonEliminar);
		botonEliminar.setEnabled(false);
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true, this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonExcel);
		botonExcel.setEnabled(false);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true, this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonPDF);
		botonPDF.setEnabled(false);
		botonUsuario = new ButtonIcon(ButtonIcon.USUARIO, true, this);
		botonUsuario.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonUsuario);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		table = new Table();
		ArmaTabla();
		layout.addComponent(table);
		Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonFiltros) {
			layoutFiltros.setVisible(!layoutFiltros.isVisible());
		} else if (event.getButton() == botonRefrescar) {
			CreaDataSource();
		} else if (event.getButton() == botonModificar) {
			Profesor profesor = (Profesor)table.getValue();
			if (profesor != null){
				Window windowABM = CreaWindowABM();
				ProfesorABM profesorABM = new ProfesorABM(profesor ,Dominios.AccesoADatos.UPDATE ,ui ,windowABM);
				windowABM.setContent(profesorABM);
				windowABM.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						CreaDataSource();
					}
				});
				ui.addWindow(windowABM);
			}
		}
		else if (event.getButton() == botonAgregar){
			Window windowABM = CreaWindowABM();
			ProfesorABM profesorABM = new ProfesorABM(null ,Dominios.AccesoADatos.INSERT ,ui ,windowABM);
			windowABM.setContent(profesorABM);
			windowABM.addCloseListener(new CloseListener() {
				@Override
				public void windowClose(CloseEvent e) {
					CreaDataSource();
				}
			});
			ui.addWindow(windowABM);
		}
		else if (event.getButton() == botonEliminar){	
		}
		else if (event.getButton() == botonUsuario){
			Profesor profesor = (Profesor)table.getValue();
			if (profesor != null){
				Usuario usuario = UsuarioSelect.LeeUsuarioXProfesor(profesor.getId());
				Window windowABM = UsuarioWWView.CreaWindowsABM();
				if (usuario == null){
					windowABM.setContent(new UsuarioABM(null ,Dominios.AccesoADatos.INSERT ,ui ,windowABM ,0 ,profesor.getId()));
				}else{
					windowABM.setContent(new UsuarioABM(usuario ,Dominios.AccesoADatos.UPDATE ,ui ,windowABM ,0 ,profesor.getId()));
				}
				ui.addWindow(windowABM);
			}
		}
		else if (event.getButton() == botonExcel
				|| event.getButton() == botonPDF) {
			/**
			 * No hace nada
			 */
		}

	}

	private void ArmaFiltros() {
		
		layoutFiltros.setVisible(false);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(9);
		// Nombre y Apellido
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label labelId = new Label("Apellido y Nombre:");
		labelId.addStyleName(R.Style.EDITABLE_LABEL);
		labelId.setWidth("105px");
		formFiltro.addComponent(labelId);
		VerticalLayout layout = new VerticalLayout();
		layout.setWidth("400px");
		filtroNombre = new TextField();
		filtroNombre.setNullRepresentation("");
		filtroNombre.setNullSettingAllowed(false);
		filtroNombre.addStyleName(R.Style.EDITABLE);
		filtroNombre.addStyleName(R.Style.LABEL_BOLD);
		filtroNombre.setWidth("300px");
		layout.addComponent(filtroNombre);
		formFiltro.addComponent(layout);
		// Activo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label labelfiltroEsPreceptor = new Label("Solo preceptores:");
		labelfiltroEsPreceptor.addStyleName(R.Style.EDITABLE_LABEL);
		labelfiltroEsPreceptor.setWidth("90px");
		formFiltro.addComponent(labelfiltroEsPreceptor);
		filtroEsPreceptor = new CheckBox();
		filtroEsPreceptor.setValue(false);
		filtroEsPreceptor.setWidth("100px");
		formFiltro.addComponent(filtroEsPreceptor );
		layoutFiltros.addComponent(formFiltro);		
		// Activo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label labelActivo = new Label("Activos:");
		labelActivo.addStyleName(R.Style.EDITABLE_LABEL);
		labelActivo.setWidth("50px");
		formFiltro.addComponent(labelActivo);
		filtroActivo = new CheckBox();
		filtroActivo.setValue(true);
		formFiltro.addComponent(filtroActivo );
		layoutFiltros.addComponent(formFiltro);
		
	}

	private void ArmaTabla() {
		table.setHeight("550px");
		table.setWidth("985px");
		table.setSelectable(true);
		table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Profesor profesor = (Profesor)itemId;
				Button botonVer = new Button(profesor.toString());
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = CreaWindowABM();
						ProfesorABM profesorABM = new ProfesorABM(profesor ,Dominios.AccesoADatos.SELECT ,ui ,windowABM);
						windowABM.setContent(profesorABM);
						windowABM.addCloseListener(new CloseListener() {
							@Override
							public void windowClose(CloseEvent e) {
								CreaDataSource();
							}
						});
						ui.addWindow(windowABM);
					}
				});
				return botonVer;
			}
		});
		table.addGeneratedColumn(COLUMNA_ACCIONES, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Profesor profesor = (Profesor)itemId;
				final PopupButton botonAcciones = new PopupButton(EtiquetasLabel.getAccion());
        		botonAcciones.setPrimaryStyleName(R.Style.LINK_COLOR);
        		VerticalLayout layoutAcciones = new VerticalLayout();
        		botonAcciones.setContent(layoutAcciones);
        		if (profesor.isEsPreceptor()){
	        		Button botonCursos = new Button(EtiquetasLabel.getCursos() ,new ClickListener() {
						@Override
						public void buttonClick(ClickEvent event) {
							Window window = CreaWindowABM();
							window.setCaption(EtiquetasLabel.getCursos());
							window.setWidth("460px");
							window.setHeight("600px");
							ProfesorCursos profesorCursos = new ProfesorCursos(profesor);
							window.setContent(profesorCursos);
							ui.addWindow(window);
							botonAcciones.setPopupVisible(false);
						}
					});
	        		botonCursos.setPrimaryStyleName(R.Style.LINK_COLOR);
	        		layoutAcciones.addComponent(botonCursos);
        		}
        		Button botonAsignaturas = new Button(EtiquetasLabel.getAsignaturas() ,new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window window = CreaWindowABM();
						window.setCaption(EtiquetasLabel.getAsignaturas());
						window.setWidth("560px");
						window.setHeight("600px");
						ProfesorAsignaturas profesorAsignaturas = new ProfesorAsignaturas(profesor, ui);
						window.setContent(profesorAsignaturas);
						ui.addWindow(window);
						botonAcciones.setPopupVisible(false);
					}
				});
        		botonAsignaturas.setPrimaryStyleName(R.Style.LINK_COLOR);
        		layoutAcciones.addComponent(botonAsignaturas);
				return botonAcciones;
			}
		});
		table.addGeneratedColumn(COLUMNA_ESPRECEPTOR, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Profesor profesor = (Profesor)itemId;
				CheckBox esPreceptor = new CheckBox();
				esPreceptor.setValue(profesor.isEsPreceptor());
				esPreceptor.setReadOnly(true);
				return esPreceptor;
			}
		});
		CreaDataSource();
	}

	private void CreaDataSource() {
		table.setContainerDataSource(ProfesorContenedor.LeeContainerXNombreApellidoXActivo(filtroNombre.getValue() ,filtroEsPreceptor.getValue() ,filtroActivo.getValue()));
		table.setVisibleColumns((Object[]) new String[] {COLUMNA_ACCIONES ,LINK_VER ,"domicilio" ,"telefono" ,"celular",COLUMNA_ESPRECEPTOR ,"id"});
		table.setColumnHeaders("" ,"Apellido y Nombre" ,"Domicilio" ,"Tel�fono" ,"Celular","Es Preceptor","Id");
		table.setColumnWidth(COLUMNA_ACCIONES, 70);
		table.setColumnWidth(LINK_VER, 200);
		table.setColumnWidth("domicilio", 250);
		table.setColumnWidth("telefono", 150);
		table.setColumnWidth("celular", 150);
		table.setColumnWidth(COLUMNA_ESPRECEPTOR, 80);
		table.setColumnWidth("id", 60);
	}
	
	public static Window CreaWindowABM(){
		Window windowABM = new Window(EtiquetasLabel.getProfesoresPreceptores());
		windowABM.setModal(true);
		windowABM.setResizable(false);
		windowABM.setDraggable(false);
		windowABM.setWidth("835px");
		windowABM.setHeight("620px");
		windowABM.center();
		windowABM.setCloseShortcut(KeyCode.ESCAPE, null);
		return windowABM;
	}
}
