package ar.com.trisquel.escuela.componentes.personas.provedorcliente;

import ar.com.trisquel.escuela.data.contenedor.PersonasContenedor;
import ar.com.trisquel.escuela.data.tablas.Personas;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class PersonasWWView extends VerticalLayout implements ClickListener,View {

	public static final String VIEW = "PersonaWW";
	private static final String LINK_VER = "LINK_VER";
	private static final String COLUMNA_ACTIVO = "COLUMNA_ACTIVO";
	private static final String COLUMNA_ESCLIENTE = "COLUMNA_ESCLIENTE";
	private static final String COLUMNA_ESPROVEEDOR = "COLUMNA_ESPROVEEDOR";
	private static final String COLUMNA_CNDIVA = "COLUMNA_CNDIVA";
	private ButtonIcon botonFiltros;
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonEliminar;
	private ButtonIcon botonRefrescar;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private TextField filtroNombre;
	private CheckBox filtroEsCliente;
	private CheckBox filtroEsProveedor;
	private CheckBox filtroActivo;
	private Table table;
	private VerticalLayout layoutFiltros;
	private UI ui;

	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass(), ui);
		if (!control.isAcceso()) {
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		} else {
			onDraw();
		}
	}

	public void onDraw() {
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(EtiquetasLabel.getPersonas().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true, this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonFiltros = new ButtonIcon(ButtonIcon.BUSCAR, true, this);
		botonFiltros.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonFiltros);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		//botonAgregar.setEnabled(false);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		//botonModificar.setEnabled(false);
		layoutBotones.addComponent(botonModificar);
		botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true, this);
		botonEliminar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonEliminar);
		botonEliminar.setEnabled(false);
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true, this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonExcel);
		botonExcel.setEnabled(false);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true, this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonPDF);
		botonPDF.setEnabled(false);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		table = new Table();
		ArmaTabla();
		layout.addComponent(table);
		Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonFiltros) {
			layoutFiltros.setVisible(!layoutFiltros.isVisible());
		} else if (event.getButton() == botonRefrescar) {
			CreaDataSource();
		} else if (event.getButton() == botonModificar) {
			Personas personas = (Personas) table.getValue();
			if (personas != null) {
				Window windowABM = CreaWindowABM();
				PersonasABM personaABM = new PersonasABM(personas,Dominios.AccesoADatos.UPDATE, ui, windowABM);
				windowABM.setContent(personaABM);
				windowABM.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						CreaDataSource();
					}
				});
				ui.addWindow(windowABM);
			}
		} else if (event.getButton() == botonAgregar) {
			Window windowABM = CreaWindowABM();
			PersonasABM personaABM = new PersonasABM(null,Dominios.AccesoADatos.INSERT, ui, windowABM);
			windowABM.setContent(personaABM);
			windowABM.addCloseListener(new CloseListener() {
				@Override
				public void windowClose(CloseEvent e) {
					CreaDataSource();
				}
			});
			ui.addWindow(windowABM);
		} else if (event.getButton() == botonEliminar
				|| event.getButton() == botonExcel || event.getButton() == botonPDF) {
			/**
			 * No hace nada
			 */
		}

	}

	private void ArmaFiltros() {
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		layoutFiltros.setVisible(false);
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(12);
		//Nombre
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label label = new Label("Nombre:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("60px");
		formFiltro.addComponent(label);
		VerticalLayout layout = new VerticalLayout();
		layout.setWidth("350px");
		filtroNombre = new TextField();
		filtroNombre.setNullRepresentation("");
		filtroNombre.setNullSettingAllowed(false);
		filtroNombre.addStyleName(R.Style.EDITABLE);
		filtroNombre.addStyleName(R.Style.LABEL_BOLD);
		filtroNombre.setWidth("300px");
		layout.addComponent(filtroNombre);
		formFiltro.addComponent(layout);
		//Solo Clientes
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Solo Clientes:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		filtroEsCliente = new CheckBox();
		filtroEsCliente.setValue(false);
		filtroEsCliente.setWidth("80px");
		formFiltro.addComponent(filtroEsCliente);
		//Solo Proveedores
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Solo Proveedores:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		filtroEsProveedor = new CheckBox();
		filtroEsProveedor.setValue(false);
		filtroEsProveedor.setWidth("80px");
		formFiltro.addComponent(filtroEsProveedor);
		//Activo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Activo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		filtroActivo = new CheckBox();
		filtroActivo.setValue(true);
		formFiltro.addComponent(filtroActivo);		
		layoutFiltros.addComponent(formFiltro);

	}

	private void ArmaTabla() {
		table.setHeight("550px");
		table.setWidth("985px");
		table.setSelectable(true);
		table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Personas personas = (Personas) itemId;
				Button botonVer = new Button(personas.getNombre());
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = CreaWindowABM();
						PersonasABM personaABM = new PersonasABM(personas,Dominios.AccesoADatos.SELECT, ui, windowABM);
						windowABM.setContent(personaABM);
						ui.addWindow(windowABM);
					}
				});
				return botonVer;
			}
		});
		table.addGeneratedColumn(COLUMNA_CNDIVA, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Personas personas = (Personas) itemId;
				ComboBox comboCnd = Dominios.CondicionIVA.CreaComboBox(false);
				comboCnd.setValue(personas.getCondicionIva());
				comboCnd.setReadOnly(true);
				return comboCnd;
			}
		});
		table.addGeneratedColumn(COLUMNA_ESCLIENTE, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Personas personas = (Personas) itemId;
				CheckBox checkEsCLiente = new CheckBox();
				checkEsCLiente.setValue(personas.isEsCliente());
				checkEsCLiente.setReadOnly(true);
				return checkEsCLiente;
			}
		});
		table.addGeneratedColumn(COLUMNA_ESPROVEEDOR, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Personas personas = (Personas) itemId;
				CheckBox checkEsProveedor = new CheckBox();
				checkEsProveedor.setValue(personas.isEsProveedor());
				checkEsProveedor.setReadOnly(true);
				return checkEsProveedor;
			}
		});
		table.addGeneratedColumn(COLUMNA_ACTIVO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Personas personas = (Personas) itemId;
				CheckBox checkActivo = new CheckBox();
				checkActivo.setValue(personas.isActivo());
				checkActivo.setReadOnly(true);
				return checkActivo;
			}
		});
		CreaDataSource();
	}

	private void CreaDataSource() {
		table.setContainerDataSource(PersonasContenedor.LeeContainerXNombreXActivo(filtroNombre.getValue(), filtroEsCliente.getValue(), filtroEsProveedor.getValue(), filtroActivo.getValue()));
		table.setVisibleColumns((Object[]) new String[] {LINK_VER,"cuit" ,COLUMNA_CNDIVA,"domicilio", "telefono" ,COLUMNA_ESCLIENTE, COLUMNA_ESPROVEEDOR ,COLUMNA_ACTIVO ,"id" });
		table.setColumnHeaders("Nombre", "CUIT", "Cnd. IVA","Domicilio" ,"Tel�fono", "Es Cliente", "Es Proveedor" ,"Activo", "Id");
	}

	public static Window CreaWindowABM() {
		Window windowABM = new Window(EtiquetasLabel.getPersonas());
		windowABM.setModal(true);
		windowABM.setResizable(false);
		windowABM.setDraggable(false);
		windowABM.setWidth("660px");
		windowABM.setHeight("400px");
		windowABM.center();
		windowABM.setCloseShortcut(KeyCode.ESCAPE, null);
		return windowABM;
	}

}
