package ar.com.trisquel.escuela.componentes.personas.provedorcliente;

import ar.com.trisquel.escuela.data.contenedor.PersonasContenedor;
import ar.com.trisquel.escuela.data.tablas.Personas;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class PersonasBuscar extends VerticalLayout implements ClickListener{
	
	private static final String LINK_VER = "LINK_VER";
	private static final String COLUMNA_ESCLIENTE = "COLUMNA_ESCLIENTE";
	private static final String COLUMNA_ESPROVEEDOR = "COLUMNA_ESPROVEEDOR";
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonRefrescar;
	private TextField filtroNombre;
	private CheckBox filtroEsCliente;
	private CheckBox filtroEsProveedor;
	private Table table;
	private VerticalLayout layoutFiltros;
	private UI ui;
	private Window window;
	private Personas personas = null;
	
	public PersonasBuscar(UI ui ,Window window){
		this.window = window;
		this.ui = ui;
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true, this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonModificar);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		table = new Table();
		ArmaTabla();
		layout.addComponent(table);
		Label Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonRefrescar) {
			CreaDataSource();
		} else if (event.getButton() == botonModificar) {
			Personas personas = (Personas) table.getValue();
			if (personas != null) {
				Window windowABM = PersonasWWView.CreaWindowABM();
				PersonasABM personaABM = new PersonasABM(personas,Dominios.AccesoADatos.UPDATE, ui, windowABM);
				windowABM.setContent(personaABM);
				windowABM.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						CreaDataSource();
					}
				});
				ui.addWindow(windowABM);
			}
		} else if (event.getButton() == botonAgregar) {
			Window windowABM = PersonasWWView.CreaWindowABM();
			PersonasABM personaABM = new PersonasABM(null,Dominios.AccesoADatos.INSERT, ui, windowABM);
			windowABM.setContent(personaABM);
			windowABM.addCloseListener(new CloseListener() {
				@Override
				public void windowClose(CloseEvent e) {
					CreaDataSource();
				}
			});
			ui.addWindow(windowABM);
		}
	}

	private void ArmaFiltros() {
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(12);
		//Nombre
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label label = new Label("Nombre:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("60px");
		formFiltro.addComponent(label);
		VerticalLayout layout = new VerticalLayout();
		layout.setWidth("350px");
		filtroNombre = new TextField();
		filtroNombre.setNullRepresentation("");
		filtroNombre.setNullSettingAllowed(false);
		filtroNombre.addStyleName(R.Style.EDITABLE);
		filtroNombre.addStyleName(R.Style.LABEL_BOLD);
		filtroNombre.setWidth("300px");
		layout.addComponent(filtroNombre);
		formFiltro.addComponent(layout);
		//Solo Clientes
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Solo Clientes:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		filtroEsCliente = new CheckBox();
		filtroEsCliente.setValue(false);
		filtroEsCliente.setWidth("80px");
		formFiltro.addComponent(filtroEsCliente);
		//Solo Proveedores
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Solo Proveedores:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		filtroEsProveedor = new CheckBox();
		filtroEsProveedor.setValue(false);
		filtroEsProveedor.setWidth("80px");
		formFiltro.addComponent(filtroEsProveedor);		
		layoutFiltros.addComponent(formFiltro);
	}

	private void ArmaTabla() {
		table.setHeight("480px");
		table.setWidth("875px");
		table.setSelectable(true);
		table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Personas personas = (Personas) itemId;
				Button botonVer = new Button(personas.getNombre());
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						setPersonas(personas);
						ui.removeWindow(window);
					}
				});
				return botonVer;
			}
		});
		table.addGeneratedColumn(COLUMNA_ESCLIENTE, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Personas personas = (Personas) itemId;
				CheckBox checkEsCLiente = new CheckBox();
				checkEsCLiente.setValue(personas.isEsCliente());
				checkEsCLiente.setReadOnly(true);
				return checkEsCLiente;
			}
		});
		table.addGeneratedColumn(COLUMNA_ESPROVEEDOR, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Personas personas = (Personas) itemId;
				CheckBox checkEsProveedor = new CheckBox();
				checkEsProveedor.setValue(personas.isEsProveedor());
				checkEsProveedor.setReadOnly(true);
				return checkEsProveedor;
			}
		});
		CreaDataSource();
	}

	private void CreaDataSource() {
		table.setContainerDataSource(PersonasContenedor.LeeContainerXNombreXActivo(filtroNombre.getValue(), filtroEsCliente.getValue(), filtroEsProveedor.getValue(), true));
		table.setVisibleColumns((Object[]) new String[] {LINK_VER,"cuit","domicilio", "telefono" ,COLUMNA_ESCLIENTE, COLUMNA_ESPROVEEDOR ,"id" });
		table.setColumnHeaders("Nombre", "CUIT","Domicilio" ,"Tel�fono", "Es Cliente", "Es Proveedor" ,"Id");
	}
	
	public Personas getPersonas() {
		return personas;
	}

	public void setPersonas(Personas personas) {
		this.personas = personas;
	}
}
