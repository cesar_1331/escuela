package ar.com.trisquel.escuela.componentes.personas.provedorcliente;

import ar.com.trisquel.escuela.data.reglasdenegocio.PersonasRN;
import ar.com.trisquel.escuela.data.tablas.Personas;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class PersonasABM extends VerticalLayout implements ClickListener {

	private TextField id;
	private TextField nombre;
	private TextField cuit;
	private ComboBox condicionIva;
	private TextField ingresosBrutosNro;
	private TextField domicilio;
	private TextField ciudad;
	private TextField provincia;
	private TextField telefono;
	private TextArea observaciones;
	private CheckBox esProveedor;
	private CheckBox esCliente;
	private CheckBox activo;
	private ButtonIcon ButtonGuardar;
	private ButtonIcon ButtonDescartar;
	private UI ui;
	private String modo;
	private Window window;
	private Personas personas;

	public PersonasABM(Personas personas, String modo, UI ui, Window window) {
		this.ui = ui;
		this.modo = modo;
		this.window = window;
		this.personas = personas;
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setWidth("100%");
		layout.setColumns(3);
		addComponent(layout);
		//Id
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Id:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("90px");
		layout.addComponent(label);
		HorizontalLayout layoutId = new HorizontalLayout();
		layoutId.setWidth("550px");
		id = new TextField();
		id.addStyleName(R.Style.EDITABLE);
		id.addStyleName(R.Style.LABEL_BOLD);
		layoutId.addComponent(id);
		//Botones
		HorizontalLayout layoutBotones = new HorizontalLayout();
		ButtonGuardar = new ButtonIcon(ButtonIcon.GUARDAR, true, this);
		ButtonGuardar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonGuardar);
		ButtonDescartar = new ButtonIcon(ButtonIcon.DESCARTAR, true, this);
		ButtonDescartar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonDescartar);
		layoutId.addComponent(layoutBotones);
		layoutId.setComponentAlignment(layoutBotones, Alignment.TOP_RIGHT);
		layout.addComponent(layoutId);
		//Nombre
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Nombre:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		nombre = new TextField();
		nombre.addStyleName(R.Style.EDITABLE);
		nombre.addStyleName(R.Style.LABEL_BOLD);
		nombre.addStyleName(R.Style.TEXTO_MAYUSCULA);
		nombre.setWidth("350px");
		nombre.setMaxLength(60);
		layout.addComponent(nombre);
		//CUIT
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("CUIT:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		cuit = new TextField();
		cuit.addStyleName(R.Style.EDITABLE);
		cuit.addStyleName(R.Style.LABEL_BOLD);
		cuit.setWidth("110px");
		cuit.setMaxLength(15);
		layout.addComponent(cuit);		
		//Condiciones de IVA
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Condicion IVA:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		condicionIva = Dominios.CondicionIVA.CreaComboBox(false);
		condicionIva.setWidth("200px");
		layout.addComponent(condicionIva);		
		//Numero de Ingresos Brutos
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Nro Ing. Brutos:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		ingresosBrutosNro = new TextField();
		ingresosBrutosNro.addStyleName(R.Style.EDITABLE);
		ingresosBrutosNro.addStyleName(R.Style.LABEL_BOLD);
		ingresosBrutosNro.setWidth("200px");
		ingresosBrutosNro.setMaxLength(20);
		layout.addComponent(ingresosBrutosNro);		
		//Domicilio
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Domicilio:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		domicilio = new TextField();
		domicilio.addStyleName(R.Style.EDITABLE);
		domicilio.addStyleName(R.Style.LABEL_BOLD);
		domicilio.setWidth("500px");
		domicilio.setMaxLength(100);
		layout.addComponent(domicilio);		
		//Ciudad
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Ciudad:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		ciudad = new TextField();
		ciudad.addStyleName(R.Style.EDITABLE);
		ciudad.addStyleName(R.Style.LABEL_BOLD);
		ciudad.setWidth("250px");
		ciudad.setMaxLength(40);
		layout.addComponent(ciudad);		
		//Provincia
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Provincia:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		provincia = new TextField();
		provincia.addStyleName(R.Style.EDITABLE);
		provincia.addStyleName(R.Style.LABEL_BOLD);
		provincia.setWidth("250px");
		provincia.setMaxLength(40);
		layout.addComponent(provincia);		
		//Telefono
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Tel�fono:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		telefono = new TextField();
		telefono.addStyleName(R.Style.EDITABLE);
		telefono.addStyleName(R.Style.LABEL_BOLD);
		telefono.setWidth("250px");
		telefono.setMaxLength(40);
		layout.addComponent(telefono);			
		//Es Proveedor
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Es Proveedor:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		HorizontalLayout layoutES = new HorizontalLayout();
		layout.addComponent(layoutES);
		esProveedor = new CheckBox();
		esProveedor.setWidth("100px");
		layoutES.addComponent(esProveedor);		
		//Es Cliente
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layoutES.addComponent(imageIcono);
		label = new Label("Es Cliente:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("60px");
		layoutES.addComponent(label);
		esCliente = new CheckBox();
		layoutES.addComponent(esCliente);		
		//Activo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Activo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		activo = new CheckBox();
		layout.addComponent(activo);	
		//Observaciones
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Observaciones:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		VerticalLayout layoutObservaciones = new VerticalLayout();
		layout.addComponent(layoutObservaciones);
		observaciones = new TextArea();
		observaciones.addStyleName(R.Style.EDITABLE);
		observaciones.addStyleName(R.Style.LABEL_BOLD);
		observaciones.setWidth("500px");
		observaciones.setHeight("80px");
		observaciones.setMaxLength(2048);
		Label labelObservaciones = new Label();
		labelObservaciones.addStyleName(R.Style.EDITABLE);
		labelObservaciones.addStyleName(R.Style.LABEL_BOLD);
		labelObservaciones.setWidth("400px");
		layoutObservaciones.addComponent(observaciones);
		layoutObservaciones.addComponent(labelObservaciones);
		
		if (modo.equals(Dominios.AccesoADatos.INSERT)) {
			id.setReadOnly(true);
			activo.setValue(true);
			activo.setReadOnly(true);
			labelObservaciones.setVisible(false);
			ButtonGuardar.setVisible(true);
			ButtonDescartar.setVisible(true);
		}
		else{
			id.setValue(String.valueOf(personas.getId()));
			nombre.setValue(personas.getNombre());
			cuit.setValue(personas.getCuit());
			condicionIva.setValue(personas.getCondicionIva());
			ingresosBrutosNro.setValue(personas.getIngresosBrutosNro());
			domicilio.setValue(personas.getDomicilio());
			ciudad.setValue(personas.getCiudad());
			provincia.setValue(personas.getProvincia());
			telefono.setValue(personas.getTelefono());
			observaciones.setValue(personas.getObservaciones());
			esProveedor.setValue(personas.isEsProveedor());
			esCliente.setValue(personas.isEsCliente());
			activo.setValue(personas.isActivo());
			if (modo.equals(Dominios.AccesoADatos.SELECT)) {
				id.setReadOnly(true);
				nombre.setReadOnly(true);
				cuit.setReadOnly(true);
				condicionIva.setReadOnly(true);
				ingresosBrutosNro.setReadOnly(true);
				domicilio.setReadOnly(true);
				ciudad.setReadOnly(true);
				provincia.setReadOnly(true);
				telefono.setReadOnly(true);
				observaciones.setReadOnly(true);
				esProveedor.setReadOnly(true);
				esCliente.setReadOnly(true);
				activo.setReadOnly(true);
				observaciones.setVisible(false);
				ButtonGuardar.setVisible(false);
				ButtonDescartar.setVisible(false);
			} else if (modo.equals(Dominios.AccesoADatos.UPDATE)) {
				id.setReadOnly(true);
				labelObservaciones.setVisible(false);
				ButtonGuardar.setVisible(true);
				ButtonDescartar.setVisible(true);
			}
		}
		nombre.focus();
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == ButtonDescartar) {
			ui.removeWindow(window);
		} else if (event.getButton() == ButtonGuardar) {
			if (personas == null){
				personas = PersonasRN.Inizializate();
			}
			short cndIVA = 0;
			try{
				cndIVA = Short.parseShort(String.valueOf(condicionIva.getValue()));
			}catch (Exception e){}
			personas.setNombre(nombre.getValue());
			personas.setCuit(cuit.getValue());
			personas.setCondicionIva(cndIVA);
			personas.setIngresosBrutosNro(ingresosBrutosNro.getValue());
			personas.setDomicilio(domicilio.getValue());
			personas.setCiudad(ciudad.getValue());
			personas.setProvincia(provincia.getValue());
			personas.setTelefono(telefono.getValue());
			personas.setObservaciones(observaciones.getValue());
			personas.setEsProveedor(esProveedor.getValue());
			personas.setEsCliente(esCliente.getValue());
			personas.setActivo(activo.getValue());
			RespuestaEntidad respuesta = PersonasRN.Validate(personas ,modo);
			if (respuesta.isError()) {
				MessageBox.showHTML(Icon.ERROR, "", respuesta.getMsgError(),ButtonId.OK);
			} else {
				MessageBox.showHTML(Icon.QUESTION, "",MensajeLabel.getConfirmaAccion(),
						new MessageBoxListener() {
					@Override
					public void buttonClicked(ButtonId buttonType) {
						if (buttonType == ButtonId.SAVE) {
							RespuestaEntidad respuesta = PersonasRN.Save(personas);
							if (respuesta.isError()) {
								MessageBox.showHTML(Icon.ERROR, "",respuesta.getMsgError(),ButtonId.OK);
							} else {
								ui.removeWindow(window);
							}
						}
					}
				}, ButtonId.SAVE, ButtonId.CANCEL);
			}
		}
	}

	public Personas getPersonas() {
		return personas;
	}

}
