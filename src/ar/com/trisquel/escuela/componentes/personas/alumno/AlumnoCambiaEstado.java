package ar.com.trisquel.escuela.componentes.personas.alumno;

import ar.com.trisquel.escuela.data.reglasdenegocio.AlumnoRN;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class AlumnoCambiaEstado extends VerticalLayout implements ClickListener {
	private ComboBox comboEstadoNuevo;
	private TextArea observacionesEstado;
	private Alumno alumno;
	private ButtonIcon butonGrabar;
	private ButtonIcon butonGrabarIcono;
	private ButtonIcon butonDescartar;
	private ButtonIcon butonDescartarIcono;
	private UI ui ;
	private Window window;
	public AlumnoCambiaEstado(Alumno alumno ,UI ui ,Window window){
		this.alumno = alumno;
		this.ui = ui;
		this.window = window;
		onDraw();
	}
	
	private void onDraw(){
		removeAllComponents();
		setWidth("400px");
		addStyleName(R.Style.PADDING_MEDIO);
		
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(alumno.toString());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		addComponent(Separator);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setColumns(3);
		addComponent(layout);
		// Estado Actual
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Estado Actual:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("85px");
		label.setHeight("30px");
		layout.addComponent(label);
		ComboBox comboEstadoActual = Dominios.EstadoAlumno.CreaComboBox(false);
		comboEstadoActual.setValue(alumno.getEstado());
		comboEstadoActual.setReadOnly(true);
		comboEstadoActual.setWidth("250px");
		layout.addComponent(comboEstadoActual);
		// Estado Nuevo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Nuevo Estado:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("85px");
		label.setHeight("30px");
		layout.addComponent(label);
		comboEstadoNuevo = Dominios.EstadoAlumno.CreaComboBox(false);
		comboEstadoNuevo.setNullSelectionAllowed(false);
		comboEstadoNuevo.setValue(alumno.getEstado());
		comboEstadoNuevo.setWidth("250px");
		layout.addComponent(comboEstadoNuevo);
		// Observaciones
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Observaciones:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("85px");
		label.setHeight("80px");
		layout.addComponent(label);
		observacionesEstado = new TextArea();
		observacionesEstado.setNullRepresentation("");
		observacionesEstado.setValue(alumno.getEstadoObservaciones());
		observacionesEstado.setMaxLength(2048);
		observacionesEstado.setWidth("320px");
		observacionesEstado.setHeight("70px");
		layout.addComponent(observacionesEstado);
		
		layout.addComponent(new Label());
		layout.addComponent(new Label());
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layoutBotones.setWidth(null);
		layout.addComponent(layoutBotones);
		butonGrabarIcono = new ButtonIcon(ButtonIcon.GUARDAR, true, this);
		layoutBotones.addComponent(butonGrabarIcono);
		butonGrabar = new ButtonIcon(ButtonIcon.GUARDAR, false, this);
		layoutBotones.addComponent(butonGrabar);
		layoutBotones.addComponent(new Label(R.CaracteresEsteciales.SPACIO+R.CaracteresEsteciales.SPACIO ,ContentMode.HTML));
		butonDescartarIcono = new ButtonIcon(ButtonIcon.CANCELAR, true, this);
		layoutBotones.addComponent(butonDescartarIcono);
		butonDescartar = new ButtonIcon(ButtonIcon.CANCELAR, false, this);
		layoutBotones.addComponent(butonDescartar);
		
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == butonDescartarIcono || event.getButton() == butonDescartar){
			ui.removeWindow(window);
		}
		else if (event.getButton() == butonGrabarIcono || event.getButton() == butonGrabar){
			final String estado = String.valueOf(comboEstadoNuevo.getValue());
			if (estado.equals(alumno.getEstado())){
				MessageBox.showHTML(Icon.ERROR, "", "El nuevo estado es igual al estado actual", ButtonId.OK);
			}
			else{
				MessageBox.showHTML(Icon.QUESTION, "", "�Realmente desea cambiar el estado del alumno?", new MessageBoxListener() {
					@Override
					public void buttonClicked(ButtonId buttonType) {
						if (buttonType == ButtonId.YES){
							alumno.setEstado(estado);
							alumno.setEstadoObservaciones(observacionesEstado.getValue());
							AlumnoRN.Save(alumno);
							ui.removeWindow(window);
						}
					}
				}, ButtonId.YES ,ButtonId.NO);
			}
		}
	}

}
