package ar.com.trisquel.escuela.componentes.personas.alumno;

import java.text.SimpleDateFormat;
import java.util.List;

import ar.com.trisquel.escuela.data.reglasdenegocio.CursoRN;
import ar.com.trisquel.escuela.data.select.AlumnoParienteSelect;
import ar.com.trisquel.escuela.data.select.UsuarioSelect;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.AlumnoPariente;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class AlumnoVer extends VerticalLayout {

	private VerticalLayout layoutParientes;
	private boolean backgroundFilaUno = true;
	private Alumno alumno;

	public AlumnoVer(Alumno alumno) {
		this.alumno = alumno;
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setWidth("100%");
		layout.setColumns(3);
		addComponent(layout);
		//Id
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Id:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("90px");
		layout.addComponent(label);
		Label id = new Label();
		id.addStyleName(R.Style.EDITABLE);
		id.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(id);
		//Nombre
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Nombre:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		Label nombre = new Label();
		nombre.addStyleName(R.Style.EDITABLE);
		nombre.addStyleName(R.Style.LABEL_BOLD);
		nombre.addStyleName(R.Style.TEXTO_MAYUSCULA);
		layout.addComponent(nombre);
		//Apellido
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Apellido:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		Label apellido = new Label();
		apellido.addStyleName(R.Style.EDITABLE);
		apellido.addStyleName(R.Style.LABEL_BOLD);
		apellido.addStyleName(R.Style.TEXTO_MAYUSCULA);
		layout.addComponent(apellido);
		//DNI
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("DNI:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		Label dni = new Label();
		dni.addStyleName(R.Style.EDITABLE);
		dni.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(dni);		
		//Fecha de Nacimiento
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Fecha Nacimiento:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		Label fechaNacimiento = new Label();
		fechaNacimiento.addStyleName(R.Style.EDITABLE);
		fechaNacimiento.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(fechaNacimiento);		
		//Genero
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Sexo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		OptionGroup genero = Dominios.Sexo.CreaOptionGroup();
		genero.addStyleName(R.Style.EDITABLE);
		genero.addStyleName(R.Style.LABEL_BOLD);
		genero.addStyleName(R.Style.HORIZONTAL_GROUP);
		layout.addComponent(genero);
		//domicilio
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Domicilio:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		Label domicilio = new Label();
		domicilio.addStyleName(R.Style.EDITABLE);
		domicilio.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(domicilio);		
		//ciudad
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Ciudad:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		Label ciudad = new Label();
		ciudad.addStyleName(R.Style.EDITABLE);
		ciudad.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(ciudad);		
		//provincia
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Provincia:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		Label provincia = new Label();
		provincia.addStyleName(R.Style.EDITABLE);
		provincia.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(provincia);		
		//telefono
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Tel�fono:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		Label telefono = new Label();
		telefono.addStyleName(R.Style.EDITABLE);
		telefono.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(telefono);
		//curso
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Curso:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		Label curso = new Label();
		curso.addStyleName(R.Style.EDITABLE);
		curso.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(curso);		
		//observaciones
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Observaciones:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		VerticalLayout layoutObservaciones = new VerticalLayout();
		Label labelObservaciones = new Label();
		labelObservaciones.addStyleName(R.Style.EDITABLE);
		labelObservaciones.addStyleName(R.Style.LABEL_BOLD);
		labelObservaciones.setWidth("700px");
		layoutObservaciones.addComponent(labelObservaciones);
		layout.addComponent(layoutObservaciones);				
		//Estado
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Estado:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		HorizontalLayout layoutEstado = new HorizontalLayout();
		ComboBox estado = Dominios.EstadoAlumno.CreaComboBox(false);
		estado.setWidth("200px");
		layoutEstado.addComponent(estado);
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		imageIcono.addStyleName(R.Style.MARGIN_MEDIO);
		layoutEstado.addComponent(imageIcono);
		label = new Label("Observaciones:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layoutEstado.addComponent(label);
		Label estadoObservaciones = new Label();
		estadoObservaciones.addStyleName(R.Style.EDITABLE);
		estadoObservaciones.addStyleName(R.Style.LABEL_BOLD);
		estadoObservaciones.setWidth("400px");
		layoutEstado.addComponent(estadoObservaciones);
		layout.addComponent(layoutEstado);
		//telefono
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Usuario:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		Label usuario = new Label();
		usuario.addStyleName(R.Style.EDITABLE);
		usuario.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(usuario);
		//Parientes
		layout.addComponent(new Label());
		layout.addComponent(new Label());
		layoutParientes = new VerticalLayout();
		layoutParientes.setWidth("100%");
		layout.addComponent(layoutParientes);
		ArmaParientes();	
		layout.addComponent(new Label());
		layout.addComponent(new Label());
		
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
		Curso cursoAlumno = CursoRN.Read(alumno.getCursoId());
		id.setValue(String.valueOf(alumno.getId()));
		nombre.setValue(alumno.getNombre());
		apellido.setValue(alumno.getApellido());
		dni.setValue(String.valueOf(alumno.getDni()));
		fechaNacimiento.setValue(df.format(alumno.getFechaNacimiento()));
		genero.setValue(alumno.getGenero());
		domicilio.setValue(alumno.getDomicilio());
		ciudad.setValue(alumno.getCiudad());
		provincia.setValue(alumno.getProvincia());
		telefono.setValue(alumno.getTelefono());
		if (cursoAlumno != null)
			curso.setValue(cursoAlumno.toString());
		labelObservaciones.setValue(alumno.getObservaciones());
		estado.setValue(alumno.getEstado());
		estado.setReadOnly(true);
		genero.setReadOnly(true);
		Usuario usuarioAlumno = UsuarioSelect.LeeUsuarioXAlumno(alumno.getId());
		if (usuarioAlumno != null){
			usuario.setValue(usuarioAlumno.getId());
		}
		genero.focus();
	}

	private void ArmaParientes(){
		layoutParientes.removeAllComponents();
		HorizontalLayout layoutTitulo = new HorizontalLayout();
		layoutTitulo.setPrimaryStyleName(R.Style.TEXTO_SUBTITULO_COLOR);
		layoutTitulo.addStyleName(R.Style.EXPAND_WIDTH_ALL);
		layoutTitulo.setWidth("100%");
		Label labelTitleServicio = new Label(EtiquetasLabel.getParientes());
		labelTitleServicio.setWidth("685px");
		layoutTitulo.addComponent(labelTitleServicio);
		layoutParientes.addComponent(layoutTitulo);
		List<AlumnoPariente> listAlumnoPariente = AlumnoParienteSelect.SelectXAlumno(alumno.getId());
		for (AlumnoPariente alumnoPariente : listAlumnoPariente){
			ArmaFila(alumnoPariente);
		}
	}

	private void ArmaFila(final AlumnoPariente alumnoPariente){
		HorizontalLayout layoutFila = new HorizontalLayout();
		if (backgroundFilaUno){
			layoutFila.addStyleName(R.Style.BACKGROUND_FILA_1);
		}
		else{
			layoutFila.addStyleName(R.Style.BACKGROUND_FILA_2);
		}
		layoutParientes.addComponent(layoutFila);
		backgroundFilaUno = !backgroundFilaUno;
		//Nombre
		final TextField nombre = new TextField();
		nombre.setInputPrompt("Nombre");
		nombre.setWidth("300px");
		nombre.addStyleName(R.Style.EDITABLE);
		nombre.addStyleName(R.Style.LABEL_BOLD);
		nombre.addStyleName(R.Style.TEXTO_MAYUSCULA);
		layoutFila.addComponent(nombre);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layoutDatosPariente = new GridLayout();
		layoutDatosPariente.setColumns(3);
		layoutFila.addComponent(layoutDatosPariente);
		//Es Tutor
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layoutDatosPariente.addComponent(imageIcono);
		Label label = new Label("Es Tutor:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("80px");
		layoutDatosPariente.addComponent(label);
		final CheckBox esTutor = new CheckBox();
		esTutor.addStyleName(R.Style.EDITABLE);
		esTutor.addStyleName(R.Style.LABEL_BOLD);
		layoutDatosPariente.addComponent(esTutor);
		//Telefono
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layoutDatosPariente.addComponent(imageIcono);
		label = new Label("Tel�fono:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("80px");
		layoutDatosPariente.addComponent(label);
		final TextField telefono = new TextField();
		telefono.addStyleName(R.Style.EDITABLE);
		telefono.addStyleName(R.Style.LABEL_BOLD);
		telefono.setWidth("290px");
		layoutDatosPariente.addComponent(telefono);
		//Observaciones
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layoutDatosPariente.addComponent(imageIcono);
		label = new Label("Observaciones:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("80px");
		layoutDatosPariente.addComponent(label);
		VerticalLayout layoutObservaciones = new VerticalLayout();
		layoutObservaciones.setWidth("310px");
		final Label labelObservaciones = new Label();
		labelObservaciones.addStyleName(R.Style.EDITABLE);
		labelObservaciones.addStyleName(R.Style.LABEL_BOLD);
		labelObservaciones.setWidth("310px");
		layoutObservaciones.addComponent(labelObservaciones);
		layoutDatosPariente.addComponent(layoutObservaciones);
		nombre.setValue(alumnoPariente.getNombre());
		telefono.setValue(alumnoPariente.getTelefono());
		esTutor.setValue(alumnoPariente.getEsTutor());
		labelObservaciones.setValue(alumnoPariente.getObservaciones());
		nombre.setReadOnly(true);
		esTutor.setReadOnly(true);
		telefono.setReadOnly(true);
		labelObservaciones.setVisible(true);
	}

}
