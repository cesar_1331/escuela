package ar.com.trisquel.escuela.componentes.personas.alumno;

import java.util.List;

import ar.com.trisquel.escuela.componentes.parametro.EscuelaView;
import ar.com.trisquel.escuela.data.reglasdenegocio.AlumnoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ParametroRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.PlanillaAlumnoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ProfesorRN;
import ar.com.trisquel.escuela.data.select.PlanillaSelect;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.Asignatura;
import ar.com.trisquel.escuela.data.tablas.Parametro;
import ar.com.trisquel.escuela.data.tablas.Planilla;
import ar.com.trisquel.escuela.data.tablas.PlanillaAlumno;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.data.tablas.identificadores.PlanillaAlumnoId;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class MisAsignaturasMateria extends VerticalLayout implements ClickListener {
	private Asignatura asignatura; 
	private Alumno alumno;
	private boolean backgroundFilaUno = true;
	private boolean soloNotasAlumno;
	
	public MisAsignaturasMateria(Asignatura asignatura, long alumnoId ,boolean soloNotasAlumno ){
		this.asignatura = asignatura; 
		this.alumno = AlumnoRN.Read(alumnoId);
		this.soloNotasAlumno = soloNotasAlumno;
		onDraw();
	}
	
	private void onDraw(){
		removeAllComponents();
		setWidth("100%");
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(asignatura.toString().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		addComponent(Separator);
		GridLayout layout = new GridLayout();
		layout.addStyleName(R.Style.PADDING_MEDIO);
		layout.setColumns(2);
		addComponent(layout);
		//Nombre
		Label labelNombre = new Label("Profesor:");
		labelNombre.addStyleName(R.Style.EDITABLE_LABEL);
		labelNombre.addStyleName(R.Style.TEXTO_BIG);
		layout.addComponent(labelNombre);
		Profesor profesor = ProfesorRN.Read(asignatura.getProfesorId());
		Label profesorLabel = new Label();
		profesorLabel.addStyleName(R.Style.EDITABLE);
		profesorLabel.addStyleName(R.Style.LABEL_BOLD);
		profesorLabel.addStyleName(R.Style.TEXTO_BIG);
		profesorLabel.setValue(profesor.toString());
		layout.addComponent(profesorLabel);
		
		Parametro parametro = ParametroRN.Read(EscuelaView.ESCUELA_CANTIDADPERIODOS);
		int cantidadPeriodos = 3;
		try{
			cantidadPeriodos = Integer.parseInt(parametro.getValor().trim());
		}catch(Exception e){}
		/**
		 * Lee las planillas
		 */
		int periodo = 0;
		List<Planilla> listPlanillaPeriodo  = PlanillaSelect.SelectParaAlumnoXAnioXMateriaXcursoIdXPeriodo(asignatura.getAnio() , asignatura.getMateriaId(),asignatura.getCursoId(), 0, false ,soloNotasAlumno);
		for (final Planilla planilla : listPlanillaPeriodo){
			if (periodo != planilla.getPeriodo()){
				HorizontalLayout layoutTitulo = new HorizontalLayout();
				layoutTitulo.setPrimaryStyleName(R.Style.TEXTO_SUBTITULO_COLOR);
				Label periodoTitulo = new Label(Dominios.Periodos.NombrePeriodo(cantidadPeriodos, planilla.getPeriodo()).toUpperCase());
				periodoTitulo.setWidth("400px");
				layoutTitulo.addComponent(periodoTitulo);
				Label notaTitulo = new Label("NOTA");
				notaTitulo.setWidth("110px");
				layoutTitulo.addComponent(notaTitulo);
				addComponent(layoutTitulo);
				periodo = planilla.getPeriodo();
			}
			HorizontalLayout layoutPlanillas = new HorizontalLayout();
			layoutPlanillas.setHeight("30px");
			if (backgroundFilaUno){
				layoutPlanillas.addStyleName(R.Style.BACKGROUND_FILA_1);
			}
			else{
				layoutPlanillas.addStyleName(R.Style.BACKGROUND_FILA_2);
			}
			Label descripcion = new Label();
			descripcion.addStyleName(R.Style.EDITABLE);
			descripcion.addStyleName(R.Style.LABEL_BOLD);
			descripcion.addStyleName(R.Style.MARGIN_MEDIO);
			descripcion.setWidth("400px");
			descripcion.setValue(planilla.getDescripcion().toUpperCase());
			layoutPlanillas.addComponent(descripcion);
			PlanillaAlumno planillaAlumno = PlanillaAlumnoRN.Read(new PlanillaAlumnoId(planilla.getId(), alumno.getId()));
			if (planillaAlumno != null){
				Label nota = new Label("" ,ContentMode.HTML);
				nota.addStyleName(R.Style.EDITABLE);
				nota.addStyleName(R.Style.LABEL_BOLD);
				nota.setWidth("100px");
				nota.setValue(String.valueOf(planillaAlumno.getNota()));
				if (!planilla.isMostrarAlAlumno()){
					nota.setValue(String.valueOf(planillaAlumno.getNota()) +
							R.CaracteresEsteciales.SPACIO+R.CaracteresEsteciales.SPACIO+
							R.CaracteresEsteciales.SPACIO+R.CaracteresEsteciales.SPACIO+"(*)");
				}
				layoutPlanillas.addComponent(nota);
				backgroundFilaUno = !backgroundFilaUno;
				addComponent(layoutPlanillas);
			}		
		}
		if (!soloNotasAlumno){
			addComponent(new Label("<hr />" ,ContentMode.HTML));
			addComponent(new Label("(*) La nota no se muestra al alumno"));
		}
	}
	
	
	@Override
	public void buttonClick(ClickEvent event) {
	}

}
