package ar.com.trisquel.escuela.componentes.personas.alumno;

import java.util.ArrayList;
import java.util.List;

import ar.com.trisquel.escuela.componentes.parametro.EscuelaView;
import ar.com.trisquel.escuela.componentes.user.UsuarioABM;
import ar.com.trisquel.escuela.componentes.user.UsuarioWWView;
import ar.com.trisquel.escuela.data.contenedor.CursoContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.AlumnoParienteRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.AlumnoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.CursoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ParametroRN;
import ar.com.trisquel.escuela.data.select.AlumnoParienteSelect;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.AlumnoPariente;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.Parametro;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.BaseTheme;

@SuppressWarnings("serial")
public class AlumnoABM extends VerticalLayout implements ClickListener {

	private TextField id;
	private TextField nombre;
	private TextField apellido;
	private TextField dni;
	private DateField fechaNacimiento;
	private OptionGroup genero;
	private TextField domicilio;
	private TextField ciudad;
	private TextField provincia;
	private TextField telefono;
	private ComboBox curso;
	private TextArea observaciones;
	private ButtonIcon ButtonGuardar;
	private ButtonIcon ButtonDescartar;
	private Label alumnoError;
	private VerticalLayout layoutParientes;
	private boolean backgroundFilaUno = true;
	private List<AlumnoPariente> listAlumnoPariente = new ArrayList<AlumnoPariente>();
	private UI ui;
	private String modo;
	private Window window;
	private Alumno alumno;

	public AlumnoABM(Alumno alumno, String modo, UI ui, Window window) {
		this.ui = ui;
		this.modo = modo;
		this.window = window;
		this.alumno = alumno;
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setWidth("100%");
		layout.setColumns(3);
		addComponent(layout);
		//Id
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Id");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("90px");
		layout.addComponent(label);
		HorizontalLayout layoutId = new HorizontalLayout();
		layoutId.setWidth("710px");
		id = new TextField();
		id.addStyleName(R.Style.EDITABLE);
		id.addStyleName(R.Style.LABEL_BOLD);
		layoutId.addComponent(id);
		//Botones
		HorizontalLayout layoutBotones = new HorizontalLayout();
		ButtonGuardar = new ButtonIcon(ButtonIcon.GUARDAR, true, this);
		ButtonGuardar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonGuardar);
		ButtonDescartar = new ButtonIcon(ButtonIcon.DESCARTAR, true, this);
		ButtonDescartar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonDescartar);
		layoutId.addComponent(layoutBotones);
		layoutId.setComponentAlignment(layoutBotones, Alignment.TOP_RIGHT);
		layout.addComponent(layoutId);
		//Nombre
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Nombre:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		nombre = new TextField();
		nombre.addStyleName(R.Style.EDITABLE);
		nombre.addStyleName(R.Style.LABEL_BOLD);
		nombre.addStyleName(R.Style.TEXTO_MAYUSCULA);
		nombre.setMaxLength(40);
		nombre.setWidth("200px");
		nombre.setNullRepresentation("");
		nombre.setNullSettingAllowed(false);
		layout.addComponent(nombre);
		//Apellido
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Apellido:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		apellido = new TextField();
		apellido.addStyleName(R.Style.EDITABLE);
		apellido.addStyleName(R.Style.LABEL_BOLD);
		apellido.addStyleName(R.Style.TEXTO_MAYUSCULA);
		apellido.setMaxLength(40);
		apellido.setWidth("200px");
		apellido.setNullRepresentation("");
		apellido.setNullSettingAllowed(false);
		layout.addComponent(apellido);
		//DNI
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("DNI:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		dni = new TextField();
		dni.addStyleName(R.Style.EDITABLE);
		dni.addStyleName(R.Style.LABEL_BOLD);
		dni.setMaxLength(10);
		dni.setWidth("100px");
		dni.setNullRepresentation("");
		dni.setNullSettingAllowed(false);
		layout.addComponent(dni);		
		//Fecha de Nacimiento
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Fecha Nacimiento:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		fechaNacimiento = new DateField();
		fechaNacimiento.addStyleName(R.Style.EDITABLE);
		fechaNacimiento.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(fechaNacimiento);		
		//Genero
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Sexo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		genero = Dominios.Sexo.CreaOptionGroup();
		genero.addStyleName(R.Style.EDITABLE);
		genero.addStyleName(R.Style.LABEL_BOLD);
		genero.addStyleName(R.Style.HORIZONTAL_GROUP);
		layout.addComponent(genero);
		//domicilio
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Domicilio:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		domicilio = new TextField();
		domicilio.addStyleName(R.Style.EDITABLE);
		domicilio.addStyleName(R.Style.LABEL_BOLD);
		domicilio.setMaxLength(100);
		domicilio.setWidth("400px");
		domicilio.setNullRepresentation("");
		domicilio.setNullSettingAllowed(false);
		layout.addComponent(domicilio);		
		//ciudad
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Ciudad:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		ciudad = new TextField();
		ciudad.addStyleName(R.Style.EDITABLE);
		ciudad.addStyleName(R.Style.LABEL_BOLD);
		ciudad.setMaxLength(40);
		ciudad.setWidth("200px");
		ciudad.setNullRepresentation("");
		ciudad.setNullSettingAllowed(false);
		layout.addComponent(ciudad);		
		//provincia
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Provincia:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		provincia = new TextField();
		provincia.addStyleName(R.Style.EDITABLE);
		provincia.addStyleName(R.Style.LABEL_BOLD);
		provincia.setMaxLength(40);
		provincia.setWidth("200px");
		provincia.setNullRepresentation("");
		provincia.setNullSettingAllowed(false);
		layout.addComponent(provincia);		
		//telefono
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Tel�fono:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		telefono = new TextField();
		telefono.addStyleName(R.Style.EDITABLE);
		telefono.addStyleName(R.Style.LABEL_BOLD);
		telefono.setMaxLength(40);
		telefono.setWidth("200px");
		telefono.setNullRepresentation("");
		telefono.setNullSettingAllowed(false);
		layout.addComponent(telefono);
		//curso
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Curso:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		curso = new ComboBox();
		curso.addStyleName(R.Style.EDITABLE);
		curso.addStyleName(R.Style.LABEL_BOLD);
		curso.setContainerDataSource(CursoContenedor.LeeContainerXGradoXTurno(0, "", true));
		curso.setWidth("200px");
		layout.addComponent(curso);		
		
		//observaciones
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Observaciones:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		VerticalLayout layoutObservaciones = new VerticalLayout();
		final Label labelObservaciones = new Label();
		labelObservaciones.addStyleName(R.Style.EDITABLE);
		labelObservaciones.addStyleName(R.Style.LABEL_BOLD);
		labelObservaciones.setWidth("700px");
		layoutObservaciones.addComponent(labelObservaciones);
		observaciones = new TextArea();
		observaciones.addStyleName(R.Style.EDITABLE);
		observaciones.addStyleName(R.Style.LABEL_BOLD);
		observaciones.setMaxLength(2048);
		observaciones.setWidth("700px");
		observaciones.setHeight("60px");
		observaciones.setNullRepresentation("");
		observaciones.setNullSettingAllowed(false);
		layoutObservaciones.addComponent(observaciones);
		layout.addComponent(layoutObservaciones);				
		//Materias
		layout.addComponent(new Label());
		layout.addComponent(new Label());
		layoutParientes = new VerticalLayout();
		layoutParientes.setWidth("100%");
		layout.addComponent(layoutParientes);
		ArmaParientes();	
		layout.addComponent(new Label());
		layout.addComponent(new Label());
		alumnoError = new Label();
		alumnoError.addStyleName(R.Style.ERROR_VIEW);
		layout.addComponent(alumnoError);
		if (modo.equals(Dominios.AccesoADatos.INSERT)){
			id.setReadOnly(true);
			Parametro parametro = ParametroRN.Read(EscuelaView.ESCUELA_CIUDAD);
			if (parametro != null)
				ciudad.setValue(parametro.getValor());
			parametro = ParametroRN.Read(EscuelaView.ESCUELA_PROVINCIA);
			if (parametro != null)
				provincia.setValue(parametro.getValor());
			labelObservaciones.setVisible(false);
			ButtonGuardar.setVisible(true);
			ButtonDescartar.setVisible(true);
		}
		else{
			id.setValue(String.valueOf(alumno.getId()));
			nombre.setValue(alumno.getNombre());
			apellido.setValue(alumno.getApellido());
			dni.setValue(String.valueOf(alumno.getDni()));
			fechaNacimiento.setValue(alumno.getFechaNacimiento());
			genero.setValue(alumno.getGenero());
			domicilio.setValue(alumno.getDomicilio());
			ciudad.setValue(alumno.getCiudad());
			provincia.setValue(alumno.getProvincia());
			telefono.setValue(alumno.getTelefono());
			curso.setValue(CursoRN.Read(alumno.getCursoId()));
			observaciones.setValue(alumno.getObservaciones());
			labelObservaciones.setValue(alumno.getObservaciones());
			if (modo.equals(Dominios.AccesoADatos.SELECT)) {
				id.setReadOnly(true);
				nombre.setReadOnly(true);
				apellido.setReadOnly(true);
				dni.setReadOnly(true);
				fechaNacimiento.setReadOnly(true);
				genero.setReadOnly(true);
				domicilio.setReadOnly(true);
				ciudad.setReadOnly(true);
				provincia.setReadOnly(true);
				telefono.setReadOnly(true);
				observaciones.setVisible(false);
				curso.setReadOnly(true);
				ButtonGuardar.setVisible(false);
				ButtonDescartar.setVisible(false);
			} else if (modo.equals(Dominios.AccesoADatos.UPDATE)) {
				id.setReadOnly(true);
				labelObservaciones.setVisible(false);
				ButtonGuardar.setVisible(true);
				ButtonDescartar.setVisible(true);
			}
		}
		focus();
	}

	private void ArmaParientes(){
		layoutParientes.removeAllComponents();
		HorizontalLayout layoutTitulo = new HorizontalLayout();
		layoutTitulo.setPrimaryStyleName(R.Style.TEXTO_SUBTITULO_COLOR);
		layoutTitulo.addStyleName(R.Style.EXPAND_WIDTH_ALL);
		layoutTitulo.setWidth("100%");
		Label labelTitleServicio = new Label(EtiquetasLabel.getParientes());
		labelTitleServicio.setWidth("690px");
		layoutTitulo.addComponent(labelTitleServicio);
		layoutParientes.addComponent(layoutTitulo);
		if (alumno != null){
			listAlumnoPariente = AlumnoParienteSelect.SelectXAlumno(alumno.getId());
		}
		for (AlumnoPariente alumnoPariente : listAlumnoPariente){
			ArmaFila(alumnoPariente);
		}
		if (!modo.equals(Dominios.AccesoADatos.SELECT)){
			HorizontalLayout layoutAgregar = new HorizontalLayout();
			layoutAgregar.setStyleName(R.Style.EXPAND_WIDTH_ALL);
			if (backgroundFilaUno){
				layoutAgregar.addStyleName(R.Style.BACKGROUND_FILA_1);
			}
			else{
				layoutAgregar.addStyleName(R.Style.BACKGROUND_FILA_2);
			}
			final Button ButtonMostrarAgregar = new Button(EtiquetasLabel.getAgregar());
			ButtonMostrarAgregar.setPrimaryStyleName(BaseTheme.BUTTON_LINK);
			ButtonMostrarAgregar.addStyleName(R.Style.LABEL_COLOR_AZUL);
			ButtonMostrarAgregar.addStyleName(R.Style.LABEL_UNDERLINE);
			ButtonMostrarAgregar.setIcon(new ThemeResource("img/fila_add.png"));
			ButtonMostrarAgregar.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					ButtonMostrarAgregar.setVisible(false);
					ArmaFila(null);
				}
			});
			layoutAgregar.addComponent(ButtonMostrarAgregar);
			layoutParientes.addComponent(layoutAgregar);
		}
	}

	private void ArmaFila(final AlumnoPariente alumnoPariente){
		HorizontalLayout layoutFila = new HorizontalLayout();
		if (backgroundFilaUno){
			layoutFila.addStyleName(R.Style.BACKGROUND_FILA_1);
		}
		else{
			layoutFila.addStyleName(R.Style.BACKGROUND_FILA_2);
		}
		layoutParientes.addComponent(layoutFila);
		backgroundFilaUno = !backgroundFilaUno;
		//Nombre
		final TextField nombre = new TextField();
		nombre.setInputPrompt("Nombre");
		nombre.setWidth("300px");
		nombre.addStyleName(R.Style.EDITABLE);
		nombre.addStyleName(R.Style.LABEL_BOLD);
		nombre.addStyleName(R.Style.TEXTO_MAYUSCULA);
		nombre.setNullRepresentation("");
		nombre.setNullSettingAllowed(false);
		layoutFila.addComponent(nombre);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layoutDatosPariente = new GridLayout();
		layoutDatosPariente.setColumns(3);
		layoutFila.addComponent(layoutDatosPariente);
		//Es Tutor
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layoutDatosPariente.addComponent(imageIcono);
		Label label = new Label("Es Tutor:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("80px");
		layoutDatosPariente.addComponent(label);
		final CheckBox esTutor = new CheckBox();
		esTutor.addStyleName(R.Style.EDITABLE);
		esTutor.addStyleName(R.Style.LABEL_BOLD);
		layoutDatosPariente.addComponent(esTutor);
		//Telefono
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layoutDatosPariente.addComponent(imageIcono);
		label = new Label("Tel�fono:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("80px");
		layoutDatosPariente.addComponent(label);
		final TextField telefono = new TextField();
		telefono.addStyleName(R.Style.EDITABLE);
		telefono.addStyleName(R.Style.LABEL_BOLD);
		telefono.setWidth("290px");
		telefono.setNullRepresentation("");
		telefono.setNullSettingAllowed(false);
		layoutDatosPariente.addComponent(telefono);
		//Observaciones
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layoutDatosPariente.addComponent(imageIcono);
		label = new Label("Observaciones:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("80px");
		layoutDatosPariente.addComponent(label);
		VerticalLayout layoutObservaciones = new VerticalLayout();
		layoutObservaciones.setWidth("310px");
		final Label labelObservaciones = new Label();
		labelObservaciones.addStyleName(R.Style.EDITABLE);
		labelObservaciones.addStyleName(R.Style.LABEL_BOLD);
		labelObservaciones.setWidth("290px");
		layoutObservaciones.addComponent(labelObservaciones);
		final TextArea observaciones = new TextArea();
		observaciones.addStyleName(R.Style.EDITABLE);
		observaciones.addStyleName(R.Style.LABEL_BOLD);
		observaciones.setWidth("290px");
		observaciones.setHeight("50px");
		observaciones.setNullRepresentation("");
		observaciones.setNullSettingAllowed(false);
		layoutObservaciones.addComponent(observaciones);
		layoutDatosPariente.addComponent(layoutObservaciones);
		VerticalLayout layoutBotones = new VerticalLayout();
		final ButtonIcon materiaGuardar = new ButtonIcon(ButtonIcon.GUARDAR, true, new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				AlumnoPariente alumnoParienteSave = alumnoPariente;
				String modo = Dominios.AccesoADatos.UPDATE;
				if (alumno != null){
					if (alumnoParienteSave == null){
						alumnoParienteSave = AlumnoParienteRN.Inizializate(alumno.getId());
						modo = Dominios.AccesoADatos.INSERT;
					}
					alumnoParienteSave.setNombre(nombre.getValue());
					alumnoParienteSave.setEsTutor(esTutor.getValue());
					alumnoParienteSave.setTelefono(telefono.getValue());
					alumnoParienteSave.setObservaciones(observaciones.getValue());
					RespuestaEntidad res = AlumnoParienteRN.Validate(alumnoParienteSave ,modo);
					if (res.isError()){
						MessageBox.showHTML(Icon.ERROR, "", res.getMsgError(), ButtonId.OK);
					}
					else{
						AlumnoParienteRN.Save(alumnoParienteSave);
						ArmaParientes();
					}
				}
				else{
					if (alumnoParienteSave == null){
						alumnoParienteSave = new AlumnoPariente();
						modo = Dominios.AccesoADatos.INSERT;
					}
					alumnoParienteSave.setNombre(nombre.getValue());
					alumnoParienteSave.setEsTutor(esTutor.getValue());
					alumnoParienteSave.setTelefono(telefono.getValue());
					alumnoParienteSave.setObservaciones(observaciones.getValue());
					RespuestaEntidad res = AlumnoParienteRN.Validate(alumnoParienteSave ,modo);
					if (res.isError()){
						MessageBox.showHTML(Icon.ERROR, "", res.getMsgError(), ButtonId.OK);
					}
					else{
						listAlumnoPariente.add(alumnoParienteSave);
						ArmaParientes();
					}
				}
			}
		});
		final ButtonIcon materiaEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true ,new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				MessageBox.showHTML(Icon.QUESTION , "", MensajeLabel.getConfirmaBorrarDato(), new MessageBoxListener(){
					@Override
					public void buttonClicked(ButtonId buttonType) {
						if (buttonType == ButtonId.YES){
							if (alumno != null){
								AlumnoParienteRN.Delete(alumnoPariente);
								ArmaParientes();
							}
							else{
								listAlumnoPariente.remove(alumnoPariente);
								ArmaParientes();
							}
						}
					}
					
				}, ButtonId.YES ,ButtonId.NO );
			}
		});
		final ButtonIcon materiaDescartar = new ButtonIcon(ButtonIcon.DESCARTAR, true ,new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				ArmaParientes();
			}
		});
		final ButtonIcon materiaModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true ,new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				nombre.setReadOnly(false);
				esTutor.setReadOnly(false);
				telefono.setReadOnly(false);
				observaciones.setReadOnly(false);
				observaciones.setVisible(true);
				curso.setReadOnly(false);
				labelObservaciones.setVisible(false);
				materiaGuardar.setVisible(true);
				materiaDescartar.setVisible(true);
				materiaEliminar.setVisible(false);
				event.getButton().setVisible(false);
			}
		});
		layoutBotones.addComponent(materiaGuardar);
		layoutBotones.addComponent(materiaModificar);
		layoutBotones.addComponent(materiaDescartar);
		layoutBotones.addComponent(materiaEliminar);
		layoutFila.addComponent(layoutBotones);
		if (alumnoPariente != null){
			nombre.setValue(alumnoPariente.getNombre());
			telefono.setValue(alumnoPariente.getTelefono());
			esTutor.setValue(alumnoPariente.getEsTutor());
			observaciones.setValue(alumnoPariente.getObservaciones());
			labelObservaciones.setValue(alumnoPariente.getObservaciones());
			nombre.setReadOnly(true);
			esTutor.setReadOnly(true);
			telefono.setReadOnly(true);
			observaciones.setReadOnly(true);
			observaciones.setVisible(false);
			labelObservaciones.setVisible(true);
			if (modo.equals(Dominios.AccesoADatos.SELECT)){
				materiaGuardar.setVisible(false);
				materiaDescartar.setVisible(false);
				materiaModificar.setVisible(true);
				materiaEliminar.setVisible(true);
				materiaModificar.setEnabled(false);
				materiaEliminar.setEnabled(false);
			}
			else{
				materiaGuardar.setVisible(false);
				materiaDescartar.setVisible(false);
				materiaModificar.setVisible(true);
				materiaEliminar.setVisible(true);
			}
		}
		else{
			observaciones.setVisible(true);
			labelObservaciones.setVisible(false);
			materiaGuardar.setVisible(true);
			materiaDescartar.setVisible(true);
			materiaModificar.setVisible(false);
			materiaEliminar.setVisible(false);
		}
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == ButtonDescartar) {
			ButtonGuardar.setVisible(false);
			ButtonDescartar.setVisible(false);
			if (modo.equals(Dominios.AccesoADatos.INSERT) || modo.equals(Dominios.AccesoADatos.UPDATE)) {
				ui.removeWindow(window);
			}
		} else if (event.getButton() == ButtonGuardar) {
			if (alumno == null){
				Sesion sesion = (Sesion)ui.getSession().getAttribute(R.Session.SESSION);
				alumno = AlumnoRN.Inizializate(sesion.getUsuarioId());
			}
			long dniNumero = 0;
			long cursoId = 0;
			try{
				dniNumero = Long.parseLong(dni.getValue());
				if (curso.getValue() != null)
					cursoId = ((Curso)curso.getValue()).getId();
			}catch(NumberFormatException e){
				dni.setValue("0");
			}
			alumno.setNombre(nombre.getValue());
			alumno.setApellido(apellido.getValue());
			alumno.setCiudad(ciudad.getValue());
			alumno.setDomicilio(domicilio.getValue());
			alumno.setProvincia(provincia.getValue());
			alumno.setTelefono(telefono.getValue());
			alumno.setObservaciones(observaciones.getValue());
			alumno.setFechaNacimiento(fechaNacimiento.getValue());
			alumno.setGenero(String.valueOf(genero.getValue()));
			alumno.setDni(dniNumero);
			alumno.setCursoId(cursoId);
			RespuestaEntidad respuesta = AlumnoRN.Validate(alumno ,modo);
			if (respuesta.isError()) {
				MessageBox.showHTML(Icon.ERROR, "", respuesta.getMsgError(),ButtonId.OK);
			} else {
				MessageBox.showHTML(Icon.QUESTION, "",MensajeLabel.getConfirmaAccion(),new MessageBoxListener() {
					@Override
					public void buttonClicked(ButtonId buttonType) {
						if (buttonType == ButtonId.SAVE) {
							RespuestaEntidad respuesta = AlumnoRN.Save(alumno);
							if (respuesta.isError()) {
								MessageBox.showHTML(Icon.ERROR, "",respuesta.getMsgError(),ButtonId.OK);
							} else {
								AlumnoRN.AfterSave(alumno, modo);
								if (modo.equals(Dominios.AccesoADatos.INSERT)){
									for (AlumnoPariente alumnoPariente : listAlumnoPariente){
										AlumnoPariente alumnoParienteSave = AlumnoParienteRN.Inizializate(alumno.getId());
										alumnoParienteSave.setNombre(alumnoPariente.getNombre());
										alumnoParienteSave.setEsTutor(alumnoPariente.getEsTutor());
										alumnoParienteSave.setTelefono(alumnoPariente.getTelefono());
										alumnoParienteSave.setObservaciones(alumnoPariente.getObservaciones());
										AlumnoParienteRN.Save(alumnoParienteSave);
									}
									Window windowABM = UsuarioWWView.CreaWindowsABM();
									windowABM.setContent(new UsuarioABM(null ,Dominios.AccesoADatos.INSERT,ui ,windowABM ,alumno.getId() ,0));
									ui.addWindow(windowABM);
								}
								if (modo.equals(Dominios.AccesoADatos.INSERT) || modo.equals(Dominios.AccesoADatos.UPDATE)) {
									ui.removeWindow(window);
								} else {
									onDraw();
								}
							}
						}
					}
				}, ButtonId.SAVE, ButtonId.CANCEL);
			}
		}
	}

}
