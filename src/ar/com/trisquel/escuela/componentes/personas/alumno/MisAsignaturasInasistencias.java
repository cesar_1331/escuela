package ar.com.trisquel.escuela.componentes.personas.alumno;

import java.text.SimpleDateFormat;

import ar.com.trisquel.escuela.data.reglasdenegocio.AlumnoRN;
import ar.com.trisquel.escuela.data.select.AlumnoAsistenciaSelect;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.AlumnoAsistencia;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class MisAsignaturasInasistencias extends VerticalLayout implements ClickListener {
	private int anio; 
	private Alumno alumno;
	
	public MisAsignaturasInasistencias(long alumnoId ,int anio ){
		this.alumno = AlumnoRN.Read(alumnoId);
		this.anio = anio;
		onDraw();
	}
	
	private void onDraw(){
		removeAllComponents();
		setWidth("100%");
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(alumno.toString().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		addComponent(Separator);
		Table table = new Table();
		table.setWidth("315px");
		table.setHeight("490px");
		table.setSelectable(false);
		addComponent(table);
		
		table.addContainerProperty("Inasistencia"   ,Label.class,  null);
		table.addContainerProperty("Justificada"    ,HorizontalLayout.class,  null);
		table.addContainerProperty("Injustificada"  ,HorizontalLayout.class,  null);
		table.setColumnWidth("Inasistencia"   ,100);
		table.setColumnWidth("Justificada"    ,70);
		table.setColumnWidth("Injustificada"   ,80);
		
		int nroJus = 0;
		int nroInj = 0;
		ThemeResource resource = new ThemeResource("img/fila_select.png");
		for (AlumnoAsistencia alumnoAsistencia : AlumnoAsistenciaSelect.SelectXAlumnoXAnio(alumno.getId(), anio)){
			SimpleDateFormat dateF = new SimpleDateFormat("dd/MM/yy");
			Label fecha = new Label(dateF.format(alumnoAsistencia.getId().getDia()));
			fecha.addStyleName(R.Style.EDITABLE);
			fecha.addStyleName(R.Style.LABEL_BOLD);
			Image justificada = new Image();
			justificada.addStyleName(R.Style.ALINEACION_VERTICAL_CENTRADO);
			Image injustificada = new Image();
			injustificada.addStyleName(R.Style.ALINEACION_VERTICAL_CENTRADO);
			if (alumnoAsistencia.getAsistencia().equals(Dominios.Asistencia.JUSTIFICADA)){
				nroJus++;
				justificada.setSource(resource);
				table.addItem(new Object[] {fecha, new HorizontalLayout(justificada),new HorizontalLayout(injustificada)},alumnoAsistencia.getId());
			}
			else if (alumnoAsistencia.getAsistencia().equals(Dominios.Asistencia.AUSENTE)){
				nroInj++;
				injustificada.setSource(resource);
				table.addItem(new Object[] {fecha,new HorizontalLayout(justificada),new HorizontalLayout(injustificada)},alumnoAsistencia.getId());
			}
		}
		Label fecha = new Label("TOTAL");
		fecha.addStyleName(R.Style.EDITABLE_LABEL);
		fecha.addStyleName(R.Style.LABEL_BOLD);
		Label justificada = new Label();
		justificada.addStyleName(R.Style.EDITABLE_LABEL);
		justificada.addStyleName(R.Style.LABEL_BOLD);
		justificada.addStyleName(R.Style.TEXTO_ALINEACION_CENTRO);
		justificada.setValue(String.valueOf(nroJus));
		Label injustificada = new Label();
		injustificada.addStyleName(R.Style.EDITABLE_LABEL);
		injustificada.addStyleName(R.Style.LABEL_BOLD);
		injustificada.addStyleName(R.Style.TEXTO_ALINEACION_CENTRO);
		injustificada.setValue(String.valueOf(nroInj));
		table.addItem(new Object[] {fecha, new HorizontalLayout(justificada),new HorizontalLayout(injustificada)},null);
	}
	
	
	@Override
	public void buttonClick(ClickEvent event) {
	}

}
