package ar.com.trisquel.escuela.componentes.personas.alumno;

import java.util.Date;
import java.util.GregorianCalendar;

import ar.com.trisquel.escuela.data.contenedor.AsignaturaAlumnoContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.AlumnoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.CursoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.MateriaRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ParametroRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ProfesorRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.UsuarioRN;
import ar.com.trisquel.escuela.data.select.AlumnoAsistenciaSelect;
import ar.com.trisquel.escuela.data.select.PlanillaAlumnoSelect;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.Asignatura;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.Materia;
import ar.com.trisquel.escuela.data.tablas.PlanillaAlumno;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class MisAsignaturasView extends VerticalLayout implements ClickListener,View {

	public static final String VIEW = "MisAsignaturasWW";
	private static final String LINK_VER = "LINK_VER";
	private static final String COLUMNA_PROFESOR = "COLUMNA_PROFESOR";
	private static final String COLUMNA_PERIODO1 = "COLUMNA_PERIODO1";
	private static final String COLUMNA_PERIODO2 = "COLUMNA_PERIODO2";
	private static final String COLUMNA_PERIODO3 = "COLUMNA_PERIODO3";
	private static final String COLUMNA_PERIODO4 = "COLUMNA_PERIODO4";
	private static final String COLUMNA_FINAL = "COLUMNA_FINAL";
	private ButtonIcon botonFiltros;
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonEliminar;
	private ButtonIcon botonRefrescar;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private InlineDateField filtroAnio;
	private Table table;
	private VerticalLayout layoutFiltros;
	private VerticalLayout layoutAsistencia;
	private UI ui;
	private Usuario usuario;
	private Alumno alumno;
	private int cantidadPeriodos;
	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass(), ui);
		if (!control.isAcceso()) {
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		} else {
			onDraw();
		}
	}

	public void onDraw() {
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		
		Sesion sesion = (Sesion)ui.getSession().getAttribute(R.Session.SESSION);
		usuario = UsuarioRN.Read(sesion.getUsuarioId());
		cantidadPeriodos = ParametroRN.LeeEscuelaCantidadPeriodos();
		alumno = AlumnoRN.Read(usuario.getAlumnoId());
		Curso curso = CursoRN.Read(alumno.getCursoId());
		if (curso != null)
			cantidadPeriodos = curso.getCantidadPeriodos();
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(EtiquetasLabel.getMisAsignaturas().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true, this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonFiltros = new ButtonIcon(ButtonIcon.BUSCAR, true, this);
		botonFiltros.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonFiltros);
		//botonFiltros.setEnabled(false);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		botonAgregar.setEnabled(false);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		botonModificar.setEnabled(false);
		layoutBotones.addComponent(botonModificar);
		botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true, this);
		botonEliminar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonEliminar);
		botonEliminar.setEnabled(false);
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true, this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonExcel);
		//botonExcel.setEnabled(false);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true, this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonPDF);
		//botonPDF.setEnabled(false);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		table = new Table();
		layout.addComponent(table);
		layoutAsistencia = new VerticalLayout();
		layout.addComponent(layoutAsistencia);
		ArmaTabla();
		Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonFiltros) {
			layoutFiltros.setVisible(!layoutFiltros.isVisible());
		} else if (event.getButton() == botonRefrescar) {
			CreaDataSource();
			ArmaAsistencia();
		} else if (event.getButton() == botonAgregar) {
		} else if (event.getButton() == botonModificar) {
		} else if (event.getButton() == botonEliminar) {
		} else if (event.getButton() == botonExcel){
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(filtroAnio.getValue());
			String fileName = MisAsignaturasExportar.ExportarExcel(alumno, calendar.get(GregorianCalendar.YEAR));
			ui.getPage().open(fileName ,"_blank", false);
		}else if (event.getButton() == botonPDF) {
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(filtroAnio.getValue());
			String fileName = MisAsignaturasExportar.ExportarPDF(alumno, calendar.get(GregorianCalendar.YEAR));
			ui.getPage().open(fileName ,"_blank", false);
		}

	}

	private void ArmaFiltros() {
		layoutFiltros.setVisible(false);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(6);
		//A�o
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label label = new Label("A�o:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("50px");
		formFiltro.addComponent(label);
		filtroAnio = new InlineDateField();
		filtroAnio.addStyleName(R.Style.EDITABLE);
		filtroAnio.setWidth("200px");
		filtroAnio.addStyleName(R.Style.LABEL_BOLD);
		filtroAnio.setResolution(Resolution.YEAR);
		filtroAnio.setImmediate(true);
		formFiltro.addComponent(filtroAnio);			
		layoutFiltros.addComponent(formFiltro);
		filtroAnio.setValue(new Date(System.currentTimeMillis()));
	}
	
	private void ArmaTabla() {
		table.setHeight("420px");
		table.setWidth("985px");
		table.setSelectable(true);
		table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Asignatura asignatura = (Asignatura)itemId;
				Materia materia = MateriaRN.Read(asignatura.getMateriaId());
				Button botonVer = new Button(materia.getNombre());
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = CreaWindowABM();
						MisAsignaturasMateria misAsignaturasMateria = new MisAsignaturasMateria(asignatura ,usuario.getAlumnoId() ,true );
						windowABM.setContent(misAsignaturasMateria);
						ui.addWindow(windowABM);
					}
				});
				return botonVer;
			}

		});
		table.addGeneratedColumn(COLUMNA_PROFESOR, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Asignatura asignatura = (Asignatura)itemId;
				Profesor profesor = ProfesorRN.Read(asignatura.getProfesorId());
				Label labelProfesor = new Label(profesor.toString());
				return labelProfesor;
			}
		});
		table.addGeneratedColumn(COLUMNA_PERIODO1, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Label nota = new Label();
				Asignatura asignatura = (Asignatura)itemId;
				PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXAnioXMateriaXcursoIdXPeriodo(usuario.getAlumnoId(), asignatura.getAnio(), asignatura.getMateriaId(), asignatura.getCursoId(), 1);
				if (planillaAlumno != null){
					nota.setValue(String.valueOf(planillaAlumno.getNota()));
				}
				return nota;
			}
		});
		table.addGeneratedColumn(COLUMNA_PERIODO2, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Label nota = new Label();
				Asignatura asignatura = (Asignatura)itemId;
				PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXAnioXMateriaXcursoIdXPeriodo(usuario.getAlumnoId(), asignatura.getAnio(), asignatura.getMateriaId(), asignatura.getCursoId(), 2);
				if (planillaAlumno != null){
					nota.setValue(String.valueOf(planillaAlumno.getNota()));
				}
				return nota;
			}
		});
		if (cantidadPeriodos == Dominios.Periodos.BIMESTRE || cantidadPeriodos == Dominios.Periodos.TRIMESTRE){
			table.addGeneratedColumn(COLUMNA_PERIODO3, new Table.ColumnGenerator() {
				public Component generateCell(Table source, Object itemId,Object columnId) {
					Label nota = new Label();
					Asignatura asignatura = (Asignatura)itemId;
					PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXAnioXMateriaXcursoIdXPeriodo(usuario.getAlumnoId(), asignatura.getAnio(), asignatura.getMateriaId(), asignatura.getCursoId(), 3);
					if (planillaAlumno != null){
						nota.setValue(String.valueOf(planillaAlumno.getNota()));
					}
					return nota;
				}
			});
			if (cantidadPeriodos == Dominios.Periodos.BIMESTRE){
				table.addGeneratedColumn(COLUMNA_PERIODO4, new Table.ColumnGenerator() {
					public Component generateCell(Table source, Object itemId,Object columnId) {
						Label nota = new Label();
						Asignatura asignatura = (Asignatura)itemId;
						PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXAnioXMateriaXcursoIdXPeriodo(usuario.getAlumnoId(), asignatura.getAnio(), asignatura.getMateriaId(), asignatura.getCursoId(), 4);
						if (planillaAlumno != null){
							nota.setValue(String.valueOf(planillaAlumno.getNota()));
						}
						return nota;
					}
				});
			}
		}
		table.addGeneratedColumn(COLUMNA_FINAL, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Label nota = new Label();
				Asignatura asignatura = (Asignatura)itemId;
				PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXAnioXMateriaXcursoIdXPeriodo(usuario.getAlumnoId(), asignatura.getAnio(), asignatura.getMateriaId(), asignatura.getCursoId(), 10);
				if (planillaAlumno != null){
					nota.setValue(String.valueOf(planillaAlumno.getNota()));
				}
				return nota;
			}
		});
		CreaDataSource();
		ArmaAsistencia();
	}

	private void CreaDataSource() {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(filtroAnio.getValue()); 
		table.setContainerDataSource(AsignaturaAlumnoContenedor.LeeContainerAsignaturasXAlumno(usuario.getAlumnoId(), calendar.get(GregorianCalendar.YEAR), true));
		if (cantidadPeriodos == Dominios.Periodos.BIMESTRE){
			table.setVisibleColumns((Object[]) new String[] {LINK_VER ,COLUMNA_PROFESOR ,COLUMNA_PERIODO1 ,COLUMNA_PERIODO2 ,COLUMNA_PERIODO3 ,COLUMNA_PERIODO4 ,COLUMNA_FINAL});
			table.setColumnHeaders("Asignatura" ,"Profesor" ,"1� Bimestre" ,"2� Bimestre" ,"3� Bimestre" ,"4� Bimestre" ,"Final");
			table.setColumnWidth(LINK_VER, 230);
			table.setColumnWidth(COLUMNA_PROFESOR, 230);
			table.setColumnWidth(COLUMNA_PERIODO1, 80);
			table.setColumnWidth(COLUMNA_PERIODO2, 80);
			table.setColumnWidth(COLUMNA_PERIODO3, 80);
			table.setColumnWidth(COLUMNA_PERIODO4, 80);
			table.setColumnWidth(COLUMNA_FINAL, 80);
		}
		else if (cantidadPeriodos == Dominios.Periodos.TRIMESTRE){
			table.setVisibleColumns((Object[]) new String[] {LINK_VER ,COLUMNA_PROFESOR ,COLUMNA_PERIODO1 ,COLUMNA_PERIODO2 ,COLUMNA_PERIODO3 ,COLUMNA_FINAL});
			table.setColumnHeaders("Asignatura" ,"Profesor" ,"1� Trimestre" ,"2� Trimestre" ,"3� Trimestre" ,"Final");
			table.setColumnWidth(LINK_VER, 270);
			table.setColumnWidth(COLUMNA_PROFESOR, 270);
			table.setColumnWidth(COLUMNA_PERIODO1, 90);
			table.setColumnWidth(COLUMNA_PERIODO2, 90);
			table.setColumnWidth(COLUMNA_PERIODO3, 90);
			table.setColumnWidth(COLUMNA_FINAL, 90);
		}
		else if (cantidadPeriodos == Dominios.Periodos.SEMESTRE){
			table.setVisibleColumns((Object[]) new String[] {LINK_VER ,COLUMNA_PROFESOR ,COLUMNA_PERIODO1 ,COLUMNA_PERIODO2 ,COLUMNA_FINAL});
			table.setColumnHeaders("Asignatura" ,"Profesor","1� Semestre" ,"2� Semestre"  ,"Final");
			table.setColumnWidth(LINK_VER, 300);
			table.setColumnWidth(COLUMNA_PROFESOR, 300);
			table.setColumnWidth(COLUMNA_PERIODO1, 100);
			table.setColumnWidth(COLUMNA_PERIODO2, 100);
			table.setColumnWidth(COLUMNA_FINAL, 100);
		}
	}
	
	private void ArmaAsistencia(){
		layoutAsistencia.removeAllComponents();
		layoutAsistencia.setWidth("100%");
		layoutAsistencia.setHeight("100%");
		layoutAsistencia.addStyleName(R.Style.BORDER_REDONDEADO);
		layoutAsistencia.addStyleName(R.Style.PADDING_MEDIO);
		layoutAsistencia.addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		layoutAsistencia.addStyleName(R.Style.EXPAND_WIDTH_ALL);
		final GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(filtroAnio.getValue());
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Button botonVer = new Button("INASISTENCIAS");
		botonVer.setPrimaryStyleName(R.Style.LINK_TITLE);
		botonVer.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				Window windowABM = CreaWindowABM();
				windowABM.setCaption("Inasistencias");
				windowABM.setWidth("340px");
				MisAsignaturasInasistencias misAsignaturasInasistencias = new MisAsignaturasInasistencias(alumno.getId(), calendar.get(GregorianCalendar.YEAR));
				windowABM.setContent(misAsignaturasInasistencias);
				ui.addWindow(windowABM);
			}
		});
		LayoutTitle.addComponent(botonVer);
		layoutAsistencia.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		layoutAsistencia.addComponent(Separator);
		/*
		 * Lee inasistencias
		 */ 
		int[] inasistencias = AlumnoAsistenciaSelect.LeeInasistenciaXAlumnoXAnio(alumno.getId(), calendar.get(GregorianCalendar.YEAR));
		
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout formInasistencia = new GridLayout();
		formInasistencia.addStyleName(R.Style.MARGIN_LARGO);
		formInasistencia.setColumns(3);
		layoutAsistencia.addComponent(formInasistencia);
		//Justificada
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formInasistencia.addComponent(imageIcono);
		Label label = new Label("JUSTIFICADAS:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.addStyleName(R.Style.LABEL_BOLD);
		label.setWidth("95px");
		formInasistencia.addComponent(label);
		label = new Label(String.valueOf(inasistencias[0]));
		label.addStyleName(R.Style.EDITABLE);
		label.addStyleName(R.Style.LABEL_BOLD);
		label.addStyleName(R.Style.TEXTO_ALINEACION_DERECHA);
		label.setWidth("30px");
		formInasistencia.addComponent(label);
		//InJustificada
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formInasistencia.addComponent(imageIcono);
		label = new Label("INJUSTIFICADAS:" + "<hr>" ,ContentMode.HTML);
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.addStyleName(R.Style.LABEL_BOLD);
		label.setWidth("95px");
		formInasistencia.addComponent(label);
		label = new Label(String.valueOf(inasistencias[1]) + "<hr>" ,ContentMode.HTML);
		label.addStyleName(R.Style.EDITABLE);
		label.addStyleName(R.Style.LABEL_BOLD);
		label.addStyleName(R.Style.TEXTO_ALINEACION_DERECHA);
		label.setWidth("30px");
		formInasistencia.addComponent(label);
		//Total
		formInasistencia.addComponent(new Label());
		formInasistencia.addComponent(new Label());
		label = new Label(String.valueOf(inasistencias[0]+inasistencias[1]));
		label.addStyleName(R.Style.EDITABLE);
		label.addStyleName(R.Style.LABEL_BOLD);
		label.addStyleName(R.Style.TEXTO_ALINEACION_DERECHA);
		label.setWidth("30px");
		formInasistencia.addComponent(label);
		
	}
	
	public static Window CreaWindowABM(){
		Window windowABM = new Window(EtiquetasLabel.getPlanilla());
		windowABM.setModal(true);
		windowABM.setResizable(false);
		windowABM.setDraggable(false);
		windowABM.setWidth("530px");
		windowABM.setHeight("600px");
		windowABM.center();
		windowABM.setCloseShortcut(KeyCode.ESCAPE, null);
		return windowABM;
	}
}
