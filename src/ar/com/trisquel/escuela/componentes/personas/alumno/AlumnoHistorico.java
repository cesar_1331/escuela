package ar.com.trisquel.escuela.componentes.personas.alumno;

import java.util.Date;
import java.util.GregorianCalendar;

import ar.com.trisquel.escuela.data.contenedor.AsignaturaAlumnoContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.CursoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.MateriaRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ParametroRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ProfesorRN;
import ar.com.trisquel.escuela.data.select.PlanillaAlumnoSelect;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.Asignatura;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.Materia;
import ar.com.trisquel.escuela.data.tablas.PlanillaAlumno;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class AlumnoHistorico extends VerticalLayout implements ClickListener {
	private static final String LINK_VER = "LINK_VER";
	private static final String COLUMNA_PROFESOR = "COLUMNA_PROFESOR";
	private static final String COLUMNA_PERIODO1 = "COLUMNA_PERIODO1";
	private static final String COLUMNA_PERIODO2 = "COLUMNA_PERIODO2";
	private static final String COLUMNA_PERIODO3 = "COLUMNA_PERIODO3";
	private static final String COLUMNA_PERIODO4 = "COLUMNA_PERIODO4";
	private static final String COLUMNA_FINAL = "COLUMNA_FINAL";
	private Alumno alumno;
	private Table table;
	private InlineDateField filtroAnio;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private UI ui;
	private int cantidadPeriodos;
	
	public AlumnoHistorico(Alumno alumno ,UI ui){
		this.alumno = alumno;
		this.ui = ui;
		onDraw();
	}
	
	private void onDraw(){
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setColumns(8);
		addComponent(layout);
		//Nombre
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Alumno:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.addStyleName(R.Style.TEXTO_BIG);
		label.setWidth("80px");
		layout.addComponent(label);
		Label cursoLabel = new Label(alumno.toString());
		cursoLabel.addStyleName(R.Style.EDITABLE);
		cursoLabel.addStyleName(R.Style.LABEL_BOLD);
		cursoLabel.addStyleName(R.Style.TEXTO_BIG);
		cursoLabel.setWidth("500px");
		layout.addComponent(cursoLabel);
		//A�o
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("A�o:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("50px");
		layout.addComponent(label);
		filtroAnio = new InlineDateField();
		filtroAnio.addStyleName(R.Style.EDITABLE);
		filtroAnio.setWidth("230px");
		filtroAnio.addStyleName(R.Style.LABEL_BOLD);
		filtroAnio.setResolution(Resolution.YEAR);
		filtroAnio.setValue(new Date(System.currentTimeMillis()));
		filtroAnio.setImmediate(true);
		filtroAnio.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				CreaDataSource();
			}
		});
		layout.addComponent(filtroAnio);		
		
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true, this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(botonExcel);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true, this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(botonPDF);
		
		cantidadPeriodos = ParametroRN.LeeEscuelaCantidadPeriodos();
		Curso curso = CursoRN.Read(alumno.getCursoId());
		if (curso != null)
			cantidadPeriodos = curso.getCantidadPeriodos();
		
		table = new Table();
		addComponent(table);
		ArmaTabla();
	}
	
	private void ArmaTabla() {
		table.setHeight("450px");
		table.setWidth("930px");
		table.setSelectable(true);
		table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Asignatura asignatura = (Asignatura)itemId;
				Materia materia = MateriaRN.Read(asignatura.getMateriaId());
				Button botonVer = new Button(materia.getNombre());
				botonVer.setWidth("210px");
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = MisAsignaturasView.CreaWindowABM();
						MisAsignaturasMateria misAsignaturasMateria = new MisAsignaturasMateria(asignatura ,alumno.getId() ,false);
						windowABM.setContent(misAsignaturasMateria);
						ui.addWindow(windowABM);
					}
				});
				return botonVer;
			}

		});
		table.addGeneratedColumn(COLUMNA_PROFESOR, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Asignatura asignatura = (Asignatura)itemId;
				Profesor profesor = ProfesorRN.Read(asignatura.getProfesorId());
				Label labelProfesor = new Label(profesor.toString());
				labelProfesor.setWidth("210px");
				return labelProfesor;
			}
		});
		table.addGeneratedColumn(COLUMNA_PERIODO1, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Label nota = new Label();
				nota.setWidth("90px");
				Asignatura asignatura = (Asignatura)itemId;
				PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXAnioXMateriaXcursoIdXPeriodo(alumno.getId(), asignatura.getAnio(), asignatura.getMateriaId(), asignatura.getCursoId(), 1);
				if (planillaAlumno != null){
					nota.setValue(String.valueOf(planillaAlumno.getNota()));
				}
				return nota;
			}
		});
		table.addGeneratedColumn(COLUMNA_PERIODO2, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Label nota = new Label();
				nota.setWidth("90px");
				Asignatura asignatura = (Asignatura)itemId;
				PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXAnioXMateriaXcursoIdXPeriodo(alumno.getId(), asignatura.getAnio(), asignatura.getMateriaId(), asignatura.getCursoId(), 2);
				if (planillaAlumno != null){
					nota.setValue(String.valueOf(planillaAlumno.getNota()));
				}
				return nota;
			}
		});
		if (cantidadPeriodos == Dominios.Periodos.BIMESTRE || cantidadPeriodos == Dominios.Periodos.TRIMESTRE){
			table.addGeneratedColumn(COLUMNA_PERIODO3, new Table.ColumnGenerator() {
				public Component generateCell(Table source, Object itemId,Object columnId) {
					Label nota = new Label();
					nota.setWidth("90px");
					Asignatura asignatura = (Asignatura)itemId;
					PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXAnioXMateriaXcursoIdXPeriodo(alumno.getId(), asignatura.getAnio(), asignatura.getMateriaId(), asignatura.getCursoId(), 3);
					if (planillaAlumno != null){
						nota.setValue(String.valueOf(planillaAlumno.getNota()));
					}
					return nota;
				}
			});
			if (cantidadPeriodos == Dominios.Periodos.BIMESTRE){
				table.addGeneratedColumn(COLUMNA_PERIODO4, new Table.ColumnGenerator() {
					public Component generateCell(Table source, Object itemId,Object columnId) {
						Label nota = new Label();
						nota.setWidth("90px");
						Asignatura asignatura = (Asignatura)itemId;
						PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXAnioXMateriaXcursoIdXPeriodo(alumno.getId(), asignatura.getAnio(), asignatura.getMateriaId(), asignatura.getCursoId(), 4);
						if (planillaAlumno != null){
							nota.setValue(String.valueOf(planillaAlumno.getNota()));
						}
						return nota;
					}
				});
			}
		}
		table.addGeneratedColumn(COLUMNA_FINAL, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Label nota = new Label();
				nota.setWidth("90px");
				Asignatura asignatura = (Asignatura)itemId;
				PlanillaAlumno planillaAlumno = PlanillaAlumnoSelect.LeeXAnioXMateriaXcursoIdXPeriodo(alumno.getId(), asignatura.getAnio(), asignatura.getMateriaId(), asignatura.getCursoId(), 10);
				if (planillaAlumno != null){
					nota.setValue(String.valueOf(planillaAlumno.getNota()));
				}
				return nota;
			}
		});
		CreaDataSource();
	}

	private void CreaDataSource() {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(filtroAnio.getValue()); 
		table.setContainerDataSource(AsignaturaAlumnoContenedor.LeeContainerAsignaturasXAlumno(alumno.getId(), calendar.get(GregorianCalendar.YEAR), true));
		if (cantidadPeriodos == Dominios.Periodos.BIMESTRE){
			table.setVisibleColumns((Object[]) new String[] {LINK_VER ,COLUMNA_PROFESOR ,COLUMNA_PERIODO1 ,COLUMNA_PERIODO2 ,COLUMNA_PERIODO3 ,COLUMNA_PERIODO4 ,COLUMNA_FINAL});
			table.setColumnHeaders("Asignatura" ,"Profesor" ,"1� Bimestre" ,"2� Bimestre" ,"3� Bimestre" ,"4� Bimestre" ,"Final");
		}
		else if (cantidadPeriodos == Dominios.Periodos.TRIMESTRE){
			table.setVisibleColumns((Object[]) new String[] {LINK_VER ,COLUMNA_PROFESOR ,COLUMNA_PERIODO1 ,COLUMNA_PERIODO2 ,COLUMNA_PERIODO3 ,COLUMNA_FINAL});
			table.setColumnHeaders("Asignatura" ,"Profesor" ,"1� Trimestre" ,"2� Trimestre" ,"3� Trimestre" ,"Final");
		}
		else if (cantidadPeriodos == Dominios.Periodos.SEMESTRE){
			table.setVisibleColumns((Object[]) new String[] {LINK_VER ,COLUMNA_PROFESOR ,COLUMNA_PERIODO1 ,COLUMNA_PERIODO2 ,COLUMNA_FINAL});
			table.setColumnHeaders("Asignatura" ,"Profesor","1� Semestre" ,"2� Semestre"  ,"Final");
		}
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonExcel){
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(filtroAnio.getValue());
			String fileName = MisAsignaturasExportar.ExportarExcel(alumno, calendar.get(GregorianCalendar.YEAR));
			ui.getPage().open(fileName ,"_blank", false);
		}else if (event.getButton() == botonPDF) {
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(filtroAnio.getValue());
			String fileName = MisAsignaturasExportar.ExportarPDF(alumno, calendar.get(GregorianCalendar.YEAR));
			ui.getPage().open(fileName ,"_blank", false);
		}
		
	}
}
