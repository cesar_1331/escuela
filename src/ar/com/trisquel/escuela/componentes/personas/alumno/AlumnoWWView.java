package ar.com.trisquel.escuela.componentes.personas.alumno;

import org.vaadin.hene.popupbutton.PopupButton;

import ar.com.trisquel.escuela.componentes.user.UsuarioABM;
import ar.com.trisquel.escuela.componentes.user.UsuarioWWView;
import ar.com.trisquel.escuela.data.contenedor.AlumnoContenedor;
import ar.com.trisquel.escuela.data.contenedor.CursoContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.CursoRN;
import ar.com.trisquel.escuela.data.select.UsuarioSelect;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class AlumnoWWView extends VerticalLayout implements ClickListener, View {

	public static final String VIEW = "AlumnoWW";
	private static final String LINK_VER = "LINK_VER";
	private static final String COLUMNA_ACCIONES = "COLUMNA_ACCIONES";
	private static final String COLUMNA_ESTADO = "COLUMNA_ESTADO";
	private static final String COLUMNA_CURSO = "COLUMNA_CURSO";
	private static final String WINDOW_WIDTH = "835px";
	private static final String WINDOW_HEIGHT = "620px";
	private ButtonIcon botonFiltros;
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonEliminar;
	private ButtonIcon botonRefrescar;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private ButtonIcon botonUsuario;
	private TextField filtroNombre;
	private TextField filtroDNI;
	private ComboBox filtroEstado;
	private ComboBox filtroCurso;
	private Table table;
	private VerticalLayout layoutFiltros;
	private UI ui;

	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass(), ui);
		if (!control.isAcceso()) {
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		} else {
			onDraw();
		}
	}

	public void onDraw() {
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(EtiquetasLabel.getAlumnos().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true, this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonFiltros = new ButtonIcon(ButtonIcon.BUSCAR, true, this);
		botonFiltros.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonFiltros);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		//botonAgregar.setEnabled(false);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		//botonModificar.setEnabled(false);
		layoutBotones.addComponent(botonModificar);
		botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true, this);
		botonEliminar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonEliminar);
		botonEliminar.setEnabled(false);
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true, this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonExcel);
		botonExcel.setEnabled(false);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true, this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonPDF);
		botonPDF.setEnabled(false);
		botonUsuario = new ButtonIcon(ButtonIcon.USUARIO, true, this);
		botonUsuario.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonUsuario);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		table = new Table();
		ArmaTabla();
		layout.addComponent(table);
		Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonFiltros) {
			layoutFiltros.setVisible(!layoutFiltros.isVisible());
		} else if (event.getButton() == botonRefrescar) {
			CreaDataSource();
		} else if (event.getButton() == botonModificar) {
			Alumno alumno = (Alumno)table.getValue();
			if (alumno != null){
				Window windowABM = CreaWindowABM();
				AlumnoABM alumnoABM = new AlumnoABM(alumno ,Dominios.AccesoADatos.UPDATE ,ui ,windowABM);
				windowABM.setContent(alumnoABM);
				windowABM.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						CreaDataSource();
					}
				});
				ui.addWindow(windowABM);
			}
		}
		else if (event.getButton() == botonAgregar){
			Window windowABM = CreaWindowABM();
			AlumnoABM alumnoABM = new AlumnoABM(null ,Dominios.AccesoADatos.INSERT ,ui ,windowABM);
			windowABM.setContent(alumnoABM);
			windowABM.addCloseListener(new CloseListener() {
				@Override
				public void windowClose(CloseEvent e) {
					CreaDataSource();
				}
			});
			ui.addWindow(windowABM);
		}
		else if (event.getButton() == botonUsuario){
			Alumno alumno = (Alumno)table.getValue();
			if (alumno != null){
				Usuario usuario = UsuarioSelect.LeeUsuarioXAlumno(alumno.getId());
				Window windowABM = UsuarioWWView.CreaWindowsABM();
				if (usuario == null){
					windowABM.setContent(new UsuarioABM(null ,Dominios.AccesoADatos.INSERT ,ui ,windowABM ,alumno.getId() ,0));
				}else{
					windowABM.setContent(new UsuarioABM(usuario ,Dominios.AccesoADatos.UPDATE ,ui ,windowABM ,alumno.getId() ,0));
				}
				ui.addWindow(windowABM);
			}
		}
		else if (event.getButton() == botonEliminar
				|| event.getButton() == botonExcel
				|| event.getButton() == botonPDF) {
			/**
			 * No hace nada
			 */
		}

	}

	private void ArmaFiltros() {
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		layoutFiltros.setVisible(false);
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(6);
		//Nombre y Apellido
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label label = new Label("Apellido y Nombre:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("105px");
		formFiltro.addComponent(label);
		VerticalLayout layout = new VerticalLayout();
		layout.setWidth("400px");
		filtroNombre = new TextField();
		filtroNombre.setNullRepresentation("");
		filtroNombre.setNullSettingAllowed(false);
		filtroNombre.addStyleName(R.Style.EDITABLE);
		filtroNombre.addStyleName(R.Style.LABEL_BOLD);
		filtroNombre.setWidth("300px");
		layout.addComponent(filtroNombre);
		formFiltro.addComponent(layout);
		//DNI
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("DNI:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("40px");
		formFiltro.addComponent(label);
		filtroDNI = new TextField();
		filtroDNI.setNullRepresentation("0");
		filtroDNI.setNullSettingAllowed(false);
		filtroDNI.addStyleName(R.Style.EDITABLE);
		filtroDNI.addStyleName(R.Style.LABEL_BOLD);
		filtroDNI.setWidth("100px");
		formFiltro.addComponent(filtroDNI);
		//Estado
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Estado:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		filtroEstado = Dominios.EstadoAlumno.CreaComboBox(true);
		filtroEstado.setInputPrompt(EtiquetasLabel.getTodos());
		filtroEstado.setNullSelectionAllowed(false);
		filtroEstado.setWidth("200px");
		formFiltro.addComponent(filtroEstado);
		//Curso
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Curso:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		filtroCurso = new ComboBox();
		filtroCurso.setContainerDataSource(CursoContenedor.LeeContainerXGradoXTurno(0, "", true));
		filtroCurso.setInputPrompt(EtiquetasLabel.getTodos());
		filtroCurso.setWidth("200px");
		formFiltro.addComponent(filtroCurso);
		layoutFiltros.addComponent(formFiltro);
		
	}

	private void ArmaTabla() {
		table.setHeight("550px");
		table.setWidth("985px");
		table.setSelectable(true);
		table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Alumno alumno = (Alumno)itemId;
				Button botonVer = new Button(alumno.getApellido().trim() + ", " + alumno.getNombre());
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = CreaWindowABM(); 
						AlumnoVer alumnoVer = new AlumnoVer(alumno );
						windowABM.setContent(alumnoVer);
						ui.addWindow(windowABM);
					}
				});
				return botonVer;
			}
		});
		table.addGeneratedColumn(COLUMNA_ESTADO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Alumno alumno = (Alumno)itemId;
				ComboBox comboEstado = Dominios.EstadoAlumno.CreaComboBox(false);
				comboEstado.setValue(alumno.getEstado());
				comboEstado.setReadOnly(true);
				return comboEstado;
			}
		});
		table.addGeneratedColumn(COLUMNA_CURSO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Alumno alumno = (Alumno)itemId;
				Label labelCurso = new Label();
				if (alumno.getCursoId() > 0){
					labelCurso.setValue(CursoRN.Read(alumno.getCursoId()).toString());
				}
				return labelCurso;
			}
		});
		table.addGeneratedColumn(COLUMNA_ACCIONES, new Table.ColumnGenerator() {
        	public Component generateCell(Table source, Object itemId, Object columnId) {
        		final Alumno alumno = (Alumno)itemId;
        		final PopupButton botonAcciones = new PopupButton(EtiquetasLabel.getAccion());
        		botonAcciones.setPrimaryStyleName(R.Style.LINK_COLOR);
        		VerticalLayout layoutAcciones = new VerticalLayout();
        		botonAcciones.setContent(layoutAcciones);
        		Button botonAsignaturas = new Button(EtiquetasLabel.getAsignaturas() ,new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window window = CreaWindowABM();
						window.setCaption(EtiquetasLabel.getAsignaturas());
						window.setWidth("800px");
						window.setHeight("600px");
						AlumnoAsignaturas alumnoAsignaturas = new AlumnoAsignaturas(alumno, ui);
						window.setContent(alumnoAsignaturas);
						ui.addWindow(window);
						botonAcciones.setPopupVisible(false);
					}
				});
        		botonAsignaturas.setPrimaryStyleName(R.Style.LINK_COLOR);
        		layoutAcciones.addComponent(botonAsignaturas);
        		Button botonCambiarEstado = new Button(EtiquetasLabel.getCambiarEstado() ,new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window window = CreaWindowABM();
						window.setCaption(EtiquetasLabel.getCambiarEstado());
						window.setWidth("450px");
						window.setHeight("300px");
						AlumnoCambiaEstado alumnoCambiaEstado = new AlumnoCambiaEstado(alumno, ui, window); 
						window.setContent(alumnoCambiaEstado);
						window.addCloseListener(new CloseListener() {
							@Override
							public void windowClose(CloseEvent e) {
								CreaDataSource();
							}
						});
						ui.addWindow(window);
						botonAcciones.setPopupVisible(false);
					}
				});
        		botonCambiarEstado.setPrimaryStyleName(R.Style.LINK_COLOR);
        		layoutAcciones.addComponent(botonCambiarEstado);
        		Button botonHistorico = new Button(EtiquetasLabel.getHistorico() ,new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window window = CreaWindowABM();
						window.setCaption(EtiquetasLabel.getHistorico());
						window.setWidth("950px");
						window.setHeight("550px");
						AlumnoHistorico alumnoHistorico = new AlumnoHistorico(alumno ,ui); 
						window.setContent(alumnoHistorico);
						ui.addWindow(window);
						botonAcciones.setPopupVisible(false);
					}
				});
        		botonHistorico.setPrimaryStyleName(R.Style.LINK_COLOR);
        		layoutAcciones.addComponent(botonHistorico);
        		return botonAcciones;
        	}
		});
		CreaDataSource();
	}

	private void CreaDataSource() {
		long dniNro = 0;
		try{
			dniNro = Long.parseLong(filtroDNI.getValue());
		}
		catch (NumberFormatException e){
			filtroDNI.setValue("0");
		}
		String estado = null; 
		if (filtroEstado.getValue() != null && !filtroEstado.getValue().toString().equals("TOD")){
			estado = filtroEstado.getValue().toString(); 
		}
		long cursoId = 0;
		if (filtroCurso.getValue() != null){
			Curso curso = (Curso)filtroCurso.getValue();
			cursoId = curso.getId();
		}
		table.setContainerDataSource(AlumnoContenedor.LeeContainerXNombreApellidoXDniXEstadoXCurso(filtroNombre.getValue(),dniNro ,estado ,cursoId));
		table.setVisibleColumns((Object[]) new String[] {COLUMNA_ACCIONES ,LINK_VER ,"dni" ,"domicilio" ,"telefono" ,COLUMNA_CURSO,COLUMNA_ESTADO ,"id"});
		table.setColumnHeaders("" ,"Apellido, Nombre" ,"DNI" ,"Domicilio" ,"Tel�fono" ,"Curso" ,"Estado" ,"Id");
		table.setColumnWidth(COLUMNA_ACCIONES, 65);
		table.setColumnWidth(LINK_VER, 250);
		table.setColumnWidth("dni", 80);
		table.setColumnWidth("domicilio", 250);
		table.setColumnWidth("telefono", 150);
		table.setColumnWidth(COLUMNA_CURSO, 60);
		table.setColumnWidth(COLUMNA_ESTADO, 120);
		table.setColumnWidth("id", 60);
	}

	public static Window CreaWindowABM(){
		Window windowABM = new Window(EtiquetasLabel.getAlumnos());
		windowABM.setModal(true);
		windowABM.setResizable(false);
		windowABM.setDraggable(false);
		windowABM.setWidth(WINDOW_WIDTH);
		windowABM.setHeight(WINDOW_HEIGHT);
		windowABM.center();
		windowABM.setCloseShortcut(KeyCode.ESCAPE, null);
		return windowABM;
	}
}
