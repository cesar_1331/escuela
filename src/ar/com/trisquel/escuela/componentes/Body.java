package ar.com.trisquel.escuela.componentes;

import ar.com.trisquel.escuela.componentes.asignatura.AsignaturaWWView;
import ar.com.trisquel.escuela.componentes.asignatura.CursoWWView;
import ar.com.trisquel.escuela.componentes.comprobante.ComprobanteBalanceView;
import ar.com.trisquel.escuela.componentes.comprobante.ComprobanteBloqueoView;
import ar.com.trisquel.escuela.componentes.comprobante.ComprobantesView;
import ar.com.trisquel.escuela.componentes.concepto.ConceptoWWView;
import ar.com.trisquel.escuela.componentes.materias.MateriaWWView;
import ar.com.trisquel.escuela.componentes.otros.ArticuloView;
import ar.com.trisquel.escuela.componentes.otros.ArticuloWWView;
import ar.com.trisquel.escuela.componentes.otros.ContactoView;
import ar.com.trisquel.escuela.componentes.otros.ErrorLogWWView;
import ar.com.trisquel.escuela.componentes.otros.ErrorView;
import ar.com.trisquel.escuela.componentes.otros.MensajeWWView;
import ar.com.trisquel.escuela.componentes.otros.MiMenuView;
import ar.com.trisquel.escuela.componentes.otros.NotificacionView;
import ar.com.trisquel.escuela.componentes.otros.NotificacionWWView;
import ar.com.trisquel.escuela.componentes.parametro.EscuelaView;
import ar.com.trisquel.escuela.componentes.personas.alumno.AlumnoWWView;
import ar.com.trisquel.escuela.componentes.personas.alumno.MisAsignaturasView;
import ar.com.trisquel.escuela.componentes.personas.profesor.MisAlumnosView;
import ar.com.trisquel.escuela.componentes.personas.profesor.MisCursosView;
import ar.com.trisquel.escuela.componentes.personas.profesor.ProfesorWWView;
import ar.com.trisquel.escuela.componentes.personas.provedorcliente.PersonasWWView;
import ar.com.trisquel.escuela.componentes.tipocomprobante.TipoComprobanteWWView;
import ar.com.trisquel.escuela.componentes.user.ForgotPasswordView;
import ar.com.trisquel.escuela.componentes.user.LoginView;
import ar.com.trisquel.escuela.componentes.user.UserPreferencesView;
import ar.com.trisquel.escuela.componentes.user.UsuarioWWView;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class Body extends VerticalLayout implements ClickListener {

	private VerticalLayout layoutAreaLeft = new VerticalLayout();
	private Sesion sesion; 
	public Body() {
	}

	public Body(Component... children) {
		super(children);
	}
	
	public void onDraw()
	{
		removeAllComponents();
		sesion =(Sesion)getUI().getSession().getAttribute(R.Session.SESSION);
		
		setPrimaryStyleName("body-layout");

		HorizontalLayout bodyNotificationLayout = new HorizontalLayout();
		bodyNotificationLayout.setWidth("1000px");
		addComponent(bodyNotificationLayout);
		setComponentAlignment(bodyNotificationLayout ,Alignment.TOP_CENTER);
		
		
		VerticalLayout bodyLayoutMaster = new VerticalLayout();
		bodyLayoutMaster.setPrimaryStyleName("body-content-layout");
		bodyLayoutMaster.setWidth("100%");
		VerticalLayout bodyLayout = new VerticalLayout();
		bodyLayout.setPrimaryStyleName("body-content-layout");
		bodyLayout.setWidth("100%");
		bodyLayoutMaster.addComponent(bodyLayout);
		
		bodyNotificationLayout.addComponent(bodyLayoutMaster);
		bodyNotificationLayout.setComponentAlignment(bodyLayoutMaster ,Alignment.TOP_CENTER);
		
		
		bodyNotificationLayout.setExpandRatio(bodyLayoutMaster, 1.0f);
		
		Navigator navigator = new Navigator(getUI() ,bodyLayout );
		navigator.addView(R.Paginas.INICIO ,ArticuloView.class);
		navigator.addView(ArticuloView.VIEW ,ArticuloView.class);
		navigator.addView(ContactoView.VIEW ,ContactoView.class);
		navigator.addView(LoginView.VIEW ,LoginView.class);
		navigator.addView(ArticuloWWView.VIEW ,ArticuloWWView.class );
		navigator.addView(UserPreferencesView.VIEW,UserPreferencesView.class);
		navigator.addView(ForgotPasswordView.VIEW,ForgotPasswordView.class); 
		navigator.addView(UsuarioWWView.VIEW,UsuarioWWView.class);
		navigator.addView(MateriaWWView.VIEW ,MateriaWWView.class);
		navigator.addView(ProfesorWWView.VIEW ,ProfesorWWView.class);
		navigator.addView(AlumnoWWView.VIEW ,AlumnoWWView.class);
		navigator.addView(AsignaturaWWView.VIEW ,AsignaturaWWView.class);
		navigator.addView(CursoWWView.VIEW ,CursoWWView.class);
		navigator.addView(MisAlumnosView.VIEW ,MisAlumnosView.class);
		navigator.addView(MisCursosView.VIEW ,MisCursosView.class);
		navigator.addView(MisAsignaturasView.VIEW ,MisAsignaturasView.class);
		navigator.addView(EscuelaView.VIEW ,EscuelaView.class);
		navigator.addView(NotificacionWWView.VIEW ,NotificacionWWView.class);
		navigator.addView(NotificacionView.VIEW ,NotificacionView.class);
		navigator.addView(ErrorLogWWView.VIEW ,ErrorLogWWView.class);
		navigator.addView(MensajeWWView.VIEW ,MensajeWWView.class);
		navigator.addView(ErrorView.VIEW ,ErrorView.class);
		navigator.addView(MiMenuView.VIEW ,MiMenuView.class);
		navigator.addView(TipoComprobanteWWView.VIEW ,TipoComprobanteWWView.class);
		navigator.addView(ConceptoWWView.VIEW ,ConceptoWWView.class);
		navigator.addView(PersonasWWView.VIEW ,PersonasWWView.class);
		navigator.addView(ComprobantesView.VIEW ,ComprobantesView.class);
		navigator.addView(ComprobanteBloqueoView.VIEW ,ComprobanteBloqueoView.class);
		navigator.addView(ComprobanteBalanceView.VIEW ,ComprobanteBalanceView.class);
		
		//try{
			String NavegateTo = sesion.getNavegarA();
			if (NavegateTo == null || NavegateTo.trim().isEmpty()){
				sesion.setArticuloSeleccionado(R.Paginas.INICIO); 
				getUI().getSession().setAttribute(R.Session.SESSION , sesion);
				navigator.navigateTo(R.Paginas.INICIO);}
			else{
				navigator.navigateTo(NavegateTo.trim());}
//		}
//		catch (Exception e){
//			navigator.navigateTo(ErrorView.VIEW);
//			String traza = "";
//			for (StackTraceElement stackTrace : e.getStackTrace()){
//				traza = traza + stackTrace.getClassName() + " linea(" + stackTrace.getLineNumber() + ") metodo: " + stackTrace.getMethodName() + "<br>"; 
//			}
//			ErrorLog errorLog = new ErrorLog(0l, e.toString(), traza, false);
//			ErrorLogRN.Save(errorLog);
//		}
	}

	
	@Override
	public void buttonClick(ClickEvent event) {

	}

	public VerticalLayout getLayoutAreaLeft() {
		return layoutAreaLeft;
	}

	public void setLayoutAreaLeft(VerticalLayout layoutAreaLeft) {
		this.layoutAreaLeft = layoutAreaLeft;
	}

}
