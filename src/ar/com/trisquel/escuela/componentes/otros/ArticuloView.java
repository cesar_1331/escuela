package ar.com.trisquel.escuela.componentes.otros;

import ar.com.trisquel.escuela.data.reglasdenegocio.ArticuloRN;
import ar.com.trisquel.escuela.data.tablas.Articulo;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class ArticuloView extends HorizontalLayout implements View {
 
	public static final String VIEW = "Articulo";
	private Articulo articulo;
	private Sesion sesion;
	private UI ui;
	public ArticuloView() { 
	}

	public ArticuloView(Component... children) {
		super(children);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		onDraw();
	}
	
	public void onDraw()
	{
		removeAllComponents();
		sesion = (Sesion)ui.getSession().getAttribute(R.Session.SESSION);
		
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label();
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>");
		Separator.setContentMode(ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Lee Datos 
		 */
		if(!sesion.getArticuloSeleccionado().trim().isEmpty())
		{
			String Page = String.valueOf(sesion.getArticuloSeleccionado());
			articulo = ArticuloRN.Read(Page);
		}
		else
		{
			articulo = ArticuloRN.Read(R.Paginas.INICIO);
		}
		if (articulo != null)
		{
			if (!articulo.getTitulo().trim().isEmpty()){
				Titulo.setValue(articulo.getTitulo());
			}
			else{
				LayoutTitle.setVisible(false);
				Separator.setVisible(false);
			}
			
			Label Content = new Label(articulo.getContenido());
			Content.setContentMode(ContentMode.HTML);
			layout.addComponent(Content);
		}
		else{
			articulo = ArticuloRN.Read(R.Paginas.ERROR);
			if (articulo != null){
				Titulo.setValue(articulo.getTitulo());
				Label Content = new Label(articulo.getContenido());
				Content.setContentMode(ContentMode.HTML);
				layout.addComponent(Content);
			}
			else{
				Label Content = new Label("<h1>Error page not foud</h1>");
				Content.setContentMode(ContentMode.HTML);
				layout.addComponent(Content);
			}
		}
		Label separador = new Label();
		separador.addStyleName(R.Style.PADDING_MEDIO);
		layout.addComponent(separador);
	}

}
