package ar.com.trisquel.escuela.componentes.otros;

import ar.com.trisquel.escuela.data.reglasdenegocio.NotificacionPersonaRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.NotificacionRN;
import ar.com.trisquel.escuela.data.tablas.Notificacion;
import ar.com.trisquel.escuela.data.tablas.NotificacionPersona;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class NotificacionVer extends VerticalLayout {
	private NotificacionPersona notificacionPersona;

	public NotificacionVer(NotificacionPersona notificacionPersona) {
		this.notificacionPersona = notificacionPersona; 
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		Notificacion notificacion = NotificacionRN.Read(notificacionPersona.getId().getNotificacionId());
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setSpacing(true);
		layout.setColumns(3);
		addComponent(layout);
		//Titulo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Titulo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("70px");
		layout.addComponent(label);
		Label titulo = new Label(notificacionPersona.getTitulo());
		titulo.addStyleName(R.Style.EDITABLE);
		titulo.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(titulo);
		//Fecha de alta
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Fecha de Alta:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		DateField altaFecha = new DateField();
		altaFecha.setValue(notificacionPersona.getFechaAlta());
		altaFecha.setResolution(Resolution.SECOND);
		altaFecha.setReadOnly(true);
		layout.addComponent(altaFecha);
		//Leido
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Leido:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		HorizontalLayout layoutLeido = new HorizontalLayout();
		layout.addComponent(layoutLeido);
		CheckBox leido = new CheckBox();
		leido.setValue(notificacionPersona.isLeido());
		leido.setReadOnly(true);
		leido.setWidth("50px");
		layoutLeido.addComponent(leido);
		DateField leidoFecha = new DateField();
		leidoFecha.setValue(notificacionPersona.getFechaLeido());
		leidoFecha.setReadOnly(true);
		leidoFecha.setResolution(Resolution.SECOND);
		layoutLeido.addComponent(leidoFecha);
		//Leido
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Descripción:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		Label descripcion = new Label(notificacion.getDescripcion() ,ContentMode.HTML);
		descripcion.addStyleName(R.Style.EDITABLE);
		descripcion.addStyleName(R.Style.LABEL_BOLD);
		descripcion.setWidth("520px");
		layout.addComponent(descripcion);
		if (!notificacionPersona.isLeido()){
			NotificacionPersonaRN.MarcarLeido(notificacionPersona);
		}
	}
}
