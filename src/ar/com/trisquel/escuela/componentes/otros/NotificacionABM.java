package ar.com.trisquel.escuela.componentes.otros;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ar.com.trisquel.escuela.data.reglasdenegocio.AlumnoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.NotificacionPersonaRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.NotificacionRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ProfesorRN;
import ar.com.trisquel.escuela.data.select.NotificacionPersonaSelect;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.Notificacion;
import ar.com.trisquel.escuela.data.tablas.NotificacionPersona;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.data.tablas.identificadores.NotificacionPersonaId;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class NotificacionABM extends VerticalLayout implements ClickListener {
	private TextField id;
	private TextField titulo;
	private ComboBox prioridad;
	private Table table;
	private RichTextArea descripcion;
	private ButtonIcon ButtonGuardar;
	private ButtonIcon ButtonDescartar;
	private ButtonIcon ButtonAgregar;
	private ButtonIcon ButtonBorrar;
	private UI ui;
	private String modo;
	private Window window;
	private Notificacion notificacion;
	private List<NotificacionPersona> listNotificacionPersona = new ArrayList<NotificacionPersona>(); 

	public NotificacionABM(Notificacion notificacion, String modo, UI ui, Window window) {
		this.notificacion = notificacion;
		this.ui = ui;
		this.modo = modo;
		this.window = window;
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setWidth("100%");
		layout.setColumns(3);
		addComponent(layout);
		//Id
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Id:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("60px");
		layout.addComponent(label);
		HorizontalLayout layoutId = new HorizontalLayout();
		layoutId.setWidth("700px");
		id = new TextField();
		id.addStyleName(R.Style.EDITABLE);
		id.addStyleName(R.Style.LABEL_BOLD);
		layoutId.addComponent(id);
		//Botones
		HorizontalLayout layoutBotones = new HorizontalLayout();
		ButtonGuardar = new ButtonIcon(ButtonIcon.GUARDAR, true, this);
		ButtonGuardar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonGuardar);
		ButtonDescartar = new ButtonIcon(ButtonIcon.DESCARTAR, true, this);
		ButtonDescartar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonDescartar);
		layoutId.addComponent(layoutBotones);
		layoutId.setComponentAlignment(layoutBotones, Alignment.TOP_RIGHT);
		layout.addComponent(layoutId);
		//Titulo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Titulo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		titulo = new TextField();
		titulo.addStyleName(R.Style.EDITABLE);
		titulo.addStyleName(R.Style.LABEL_BOLD);
		titulo.addStyleName(R.Style.TEXTO_MAYUSCULA);
		titulo.setWidth("300px");
		titulo.setMaxLength(60);
		layout.addComponent(titulo);
		//Prioridad
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Prioridad:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		prioridad = Dominios.Prioridad.CreaComboBox(false);
		prioridad.setWidth("150px");
		layout.addComponent(prioridad);
		//Descripcion
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Descripci�n:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		VerticalLayout layoutDescripcion = new VerticalLayout();
		layout.addComponent(layoutDescripcion);
		descripcion = new RichTextArea();
		descripcion.addStyleName(R.Style.EDITABLE);
		descripcion.addStyleName(R.Style.LABEL_BOLD);
		descripcion.setWidth("690px");
		descripcion.setHeight("230px");
		layoutDescripcion.addComponent(descripcion);
		Label descripcionLabel = new Label("" ,ContentMode.HTML);
		descripcionLabel.addStyleName(R.Style.EDITABLE);
		descripcionLabel.addStyleName(R.Style.LABEL_BOLD);
		descripcionLabel.setWidth("690px");
		layoutDescripcion.addComponent(descripcionLabel);
		//Notificar a 
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Notificar a:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		HorizontalLayout layoutNotificar = new HorizontalLayout();
		layout.addComponent(layoutNotificar);
		table = new Table();
		table.setWidth("690px");
		table.setHeight("230px");
		table.setSelectable(true);
		layoutNotificar.addComponent(table);
		VerticalLayout layoutNotificarBotones = new VerticalLayout();
		layoutNotificarBotones.addStyleName(R.Style.PADDING_MINIMO);
		layoutNotificar.addComponent(layoutNotificarBotones);
		ButtonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true ,this);
		layoutNotificarBotones.addComponent(ButtonAgregar);
		ButtonBorrar = new ButtonIcon(ButtonIcon.ELIMINAR, true ,this);
		layoutNotificarBotones.addComponent(ButtonBorrar);
		if (modo.equals(Dominios.AccesoADatos.INSERT)){
			id.setReadOnly(true);
			prioridad.setValue(Dominios.Prioridad.NORMAL);
			descripcionLabel.setVisible(false);
			ButtonGuardar.setVisible(true);
			ButtonDescartar.setVisible(true);
		}
		else{
			id.setValue(String.valueOf(notificacion.getId()));
			titulo.setValue(notificacion.getTitulo());
			prioridad.setValue(notificacion.getPrioridad());
			descripcion.setValue(notificacion.getDescripcion());
			descripcionLabel.setValue(notificacion.getDescripcion());
			listNotificacionPersona = NotificacionPersonaSelect.SelectXNotificacion(notificacion.getId());
			if (modo.equals(Dominios.AccesoADatos.SELECT)) {
				id.setReadOnly(true);
				titulo.setReadOnly(true);
				prioridad.setReadOnly(true);
				descripcion.setReadOnly(true);
				descripcion.setVisible(false);
				ButtonGuardar.setVisible(false);
				ButtonDescartar.setVisible(false);
				ButtonAgregar.setVisible(false);
				ButtonBorrar.setVisible(false);
			} else if (modo.equals(Dominios.AccesoADatos.UPDATE)) {
				id.setReadOnly(true);
				descripcionLabel.setVisible(false);
				ButtonGuardar.setVisible(true);
				ButtonDescartar.setVisible(true);
			}
		}
		CreaDataSource();
		titulo.focus();
	}
	private void CreaDataSource() {
		table.removeAllItems();
		if (modo.equals(Dominios.AccesoADatos.SELECT)){
			table.addContainerProperty("Tipo"              ,Label.class,  null);
			table.addContainerProperty("Apellido y Nombre" ,Label.class,  null);
			table.addContainerProperty("Leido"             ,Label.class, null);
			table.setColumnWidth("Tipo"              ,80);
			table.setColumnWidth("Apellido y Nombre" ,400);
			table.setColumnWidth("Leido"             ,140);
			table.setColumnHeaders("Tipo" ,"Apellido y Nombre" ,"Leido");
		}
		else{
			table.addContainerProperty("Tipo"              ,Label.class,  null);
			table.addContainerProperty("Apellido y Nombre" ,Label.class,  null);
			table.addContainerProperty("Borrar"            ,Button.class, null);
			table.setColumnWidth("Tipo"              ,80);
			table.setColumnWidth("Apellido y Nombre" ,480);
			table.setColumnWidth("Borrar"            ,60);
			table.setColumnHeaders("Tipo" ,"Apellido y Nombre" ,"");
		}
		for (final NotificacionPersona notificacionPersona : listNotificacionPersona){
			Label tipo = new Label();
			String nombre = "";
			if (notificacionPersona.getId().getTipo().equals(Dominios.TipoPersonaNotificacion.ALUMNO)){
				Alumno alumno = AlumnoRN.Read(notificacionPersona.getId().getPersonaId());
				nombre = alumno.toString();
				tipo.setValue("ALUMNO");
			}
			else if (notificacionPersona.getId().getTipo().equals(Dominios.TipoPersonaNotificacion.PROFESOR)){
				Profesor profesor = ProfesorRN.Read(notificacionPersona.getId().getPersonaId());
				nombre = profesor.toString();
				tipo.setValue("PROFESOR");
			}
			Label persona = new Label(nombre);
			ButtonIcon borrar = new ButtonIcon(ButtonIcon.ELIMINAR, true);
			if (modo.equals(Dominios.AccesoADatos.SELECT)) {
				borrar.setEnabled(false);
			}
			else{
				borrar.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						MessageBox.showHTML(Icon.QUESTION, "", "�Realmente desea borrar todas las personas a las que se va a notificar?"
								,new MessageBoxListener() {
									@Override
									public void buttonClicked(ButtonId buttonType) {
										if (buttonType == ButtonId.OK){
											if (notificacion != null){
												NotificacionPersonaRN.Delete(notificacionPersona);
											}else{
												listNotificacionPersona.remove(notificacionPersona);
											}
											CreaDataSource();
										}
									}
								} 
								,ButtonId.OK ,ButtonId.CANCEL);
					}
				});
			}
			Label fechaLeido = new Label();
			SimpleDateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm");
			if (notificacionPersona.getFechaLeido() != null)
				fechaLeido.setValue(df.format(notificacionPersona.getFechaLeido()));
			if (modo.equals(Dominios.AccesoADatos.SELECT)){
				table.addItem(new Object[] {tipo, persona,fechaLeido},notificacionPersona);
			}
			else{
				table.addItem(new Object[] {tipo, persona,borrar},notificacionPersona);
			}
		}
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == ButtonDescartar) {
			ui.removeWindow(window);
		}else if (event.getButton() == ButtonAgregar) {
			Window window = NotificacionWWView.CreaWindowABM();
			window.setCaption("Notificar A");
			final NotificacionPersonaBuscar notificacionPersonaBuscar = new NotificacionPersonaBuscar(listNotificacionPersona);
			window.setContent(notificacionPersonaBuscar);
			window.addCloseListener(new CloseListener() {
				@Override
				public void windowClose(CloseEvent e) {
					listNotificacionPersona = notificacionPersonaBuscar.getListNotificacionPersona(); 
					CreaDataSource();
				}
			});
			ui.addWindow(window);
			
		}else if (event.getButton() == ButtonBorrar) {
			MessageBox.showHTML(Icon.QUESTION, "", "�Realmente desea borrar todas las personas a las que se va a notificar?"
					,new MessageBoxListener() {
						@Override
						public void buttonClicked(ButtonId buttonType) {
							if (buttonType == ButtonId.OK){
								if (notificacion != null){
									for (NotificacionPersona notificacionPersona : listNotificacionPersona ){
										NotificacionPersonaRN.Delete(notificacionPersona);
									}
								}else{
									listNotificacionPersona.clear();
								}
								CreaDataSource();
							}
						}
					} 
					,ButtonId.OK ,ButtonId.CANCEL);
			
		} else if (event.getButton() == ButtonGuardar) {
			if (notificacion == null){
				Sesion sesion = (Sesion)ui.getSession().getAttribute(R.Session.SESSION);
				notificacion = NotificacionRN.Inizializate(sesion.getUsuarioId());
			}
			notificacion.setTitulo(titulo.getValue());
			notificacion.setDescripcion(descripcion.getValue());
			notificacion.setPrioridad(Integer.parseInt(String.valueOf(prioridad.getValue())));
			RespuestaEntidad respuesta = NotificacionRN.Validate(notificacion ,modo);
			if (respuesta.isError()) {
				MessageBox.showHTML(Icon.ERROR, "", respuesta.getMsgError(),ButtonId.OK);
			} else {
				MessageBox.showHTML(Icon.QUESTION, "",MensajeLabel.getConfirmaAccion(),new MessageBoxListener() {
					@Override
					public void buttonClicked(ButtonId buttonType) {
						if (buttonType == ButtonId.SAVE) {
							RespuestaEntidad respuesta = NotificacionRN.Save(notificacion);
							if (respuesta.isError()) {
								MessageBox.showHTML(Icon.ERROR, "",respuesta.getMsgError(),ButtonId.OK);
							} else {
								for (NotificacionPersona notificacionPersona : listNotificacionPersona ){
									notificacionPersona.setId(new NotificacionPersonaId(notificacion.getId(), notificacionPersona.getId().getTipo() , notificacionPersona.getId().getPersonaId()));
									notificacionPersona.setFechaAlta(notificacion.getFechaAlta());
									notificacionPersona.setPrioridad(notificacion.getPrioridad());
									notificacionPersona.setTitulo(notificacion.getTitulo());
									NotificacionPersonaRN.Add(notificacionPersona);
								}
								ui.removeWindow(window);
							}
						}
					}
				}, ButtonId.SAVE, ButtonId.CANCEL);
			}
		}
	}

}
