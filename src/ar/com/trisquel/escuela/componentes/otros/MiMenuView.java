package ar.com.trisquel.escuela.componentes.otros;

import ar.com.trisquel.escuela.EscuelaUI;
import ar.com.trisquel.escuela.componentes.asignatura.AsignaturaWWView;
import ar.com.trisquel.escuela.componentes.asignatura.CursoWWView;
import ar.com.trisquel.escuela.componentes.comprobante.ComprobanteBalanceView;
import ar.com.trisquel.escuela.componentes.comprobante.ComprobanteBloqueoView;
import ar.com.trisquel.escuela.componentes.comprobante.ComprobantesView;
import ar.com.trisquel.escuela.componentes.concepto.ConceptoWWView;
import ar.com.trisquel.escuela.componentes.materias.MateriaWWView;
import ar.com.trisquel.escuela.componentes.parametro.EscuelaView;
import ar.com.trisquel.escuela.componentes.personas.alumno.AlumnoWWView;
import ar.com.trisquel.escuela.componentes.personas.alumno.MisAsignaturasView;
import ar.com.trisquel.escuela.componentes.personas.profesor.MisAlumnosView;
import ar.com.trisquel.escuela.componentes.personas.profesor.MisCursosView;
import ar.com.trisquel.escuela.componentes.personas.profesor.ProfesorWWView;
import ar.com.trisquel.escuela.componentes.personas.provedorcliente.PersonasWWView;
import ar.com.trisquel.escuela.componentes.tipocomprobante.TipoComprobanteWWView;
import ar.com.trisquel.escuela.componentes.user.UserPreferencesView;
import ar.com.trisquel.escuela.componentes.user.UsuarioWWView;
import ar.com.trisquel.escuela.data.reglasdenegocio.PermisoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ProfesorRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.UsuarioRN;
import ar.com.trisquel.escuela.data.tablas.Permiso;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.data.tablas.identificadores.PermisoId;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonImage;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

@SuppressWarnings("serial")
public class MiMenuView extends VerticalLayout implements ClickListener, View, LayoutClickListener {
	
	public static final String VIEW = "MiMenuWW";
	private ButtonImage ButtonPreferencias;
	private ButtonImage ButtonMisAsignaturas;
	private ButtonImage ButtonMisAlumnos;
	private ButtonImage ButtonMisCursos;
	private ButtonImage ButtonAlumnos;
	private ButtonImage ButtonProfesores;
	private ButtonImage ButtonMaterias;
	private ButtonImage ButtonAsignaturas;
	private ButtonImage ButtonCurso;
	private ButtonImage ButtonPaginas;
	private ButtonImage ButtonUsuarios;
	private ButtonImage ButtonNotificaciones;
	private ButtonImage ButtonMensaje;
	private ButtonImage ButtonErrores;
	private ButtonImage ButtonDatosEscuela;
	private ButtonImage ButtonTipoComprobante;
	private ButtonImage ButtonConceptos;
	private ButtonImage ButtonPersonas;
	private ButtonImage ButtonComprobante;
	private ButtonImage ButtonComprobanteBloqueo;
	private ButtonImage ButtonBalance;
	private UI ui;
	private Sesion sesion;
	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass(), ui);
		if (!control.isAcceso()) {
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		} else {
			onDraw();
		}
	}
	
	private void onDraw(){
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		sesion = (Sesion)ui.getSession().getAttribute(R.Session.SESSION);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label("Hola " + sesion.getUsuarioNombre().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		addComponent(Separator);
		/*
		 *  Botones
		 */
		Panel panel = new Panel();
		panel.setWidth("96%");
		panel.setHeight("550px");
		panel.addStyleName(Reindeer.PANEL_LIGHT);
		panel.addStyleName(R.Style.MARGIN_MEDIO);
		addComponent(panel);
		VerticalLayout layoutOpciones = new VerticalLayout();
		panel.setContent(layoutOpciones);
		ButtonPreferencias = new ButtonImage(EtiquetasLabel.getMiCuenta() ,new ThemeResource("img/icono_micuenta.png") ,this);
		ButtonMisAsignaturas = new ButtonImage(EtiquetasLabel.getMisAsignaturas() ,new ThemeResource("img/icono_misasignaturas.png") ,this);
		ButtonMisAlumnos = new ButtonImage(EtiquetasLabel.getMisAlumnos() ,new ThemeResource("img/icono_alumnos.png") ,this);
		ButtonMisCursos = new ButtonImage(EtiquetasLabel.getMisCursos() ,new ThemeResource("img/icono_cursos.png") ,this);
		ButtonAlumnos = new ButtonImage(EtiquetasLabel.getAlumnos() ,new ThemeResource("img/icono_alumnos.png") ,this);
		ButtonProfesores = new ButtonImage(EtiquetasLabel.getProfesoresPreceptores() ,new ThemeResource("img/icono_profesores.png") ,this);
		ButtonMaterias = new ButtonImage(EtiquetasLabel.getMaterias() ,new ThemeResource("img/icono_materias.png") ,this);
		ButtonCurso = new ButtonImage(EtiquetasLabel.getCursos() ,new ThemeResource("img/icono_cursos.png") ,this);
		ButtonAsignaturas = new ButtonImage(EtiquetasLabel.getAsignaturas() ,new ThemeResource("img/icono_asignaturas.png") ,this);
		ButtonPaginas = new ButtonImage(EtiquetasLabel.getPaginas() ,new ThemeResource("img/icono_paginas.png") ,this);
		ButtonDatosEscuela = new ButtonImage(EtiquetasLabel.getDatosEscuela() ,new ThemeResource("img/icono_parametros.png") ,this);
		ButtonUsuarios = new ButtonImage(EtiquetasLabel.getUsuarios() ,new ThemeResource("img/icono_usuarios.png") ,this);
		ButtonNotificaciones = new ButtonImage(EtiquetasLabel.getNotificaciones() ,new ThemeResource("img/icono_notificaciones.png") ,this);
		ButtonMensaje = new ButtonImage(EtiquetasLabel.getMensajes() ,new ThemeResource("img/icono_mensajes.png") ,this);
		ButtonErrores = new ButtonImage(EtiquetasLabel.getErrores() ,new ThemeResource("img/icono_errorlog.png") ,this);		
		ButtonTipoComprobante = new ButtonImage(EtiquetasLabel.getTipoComprobantes() ,new ThemeResource("img/icono_tipocomprobante.png") ,this);
		ButtonComprobante = new ButtonImage(EtiquetasLabel.getComprobantes() ,new ThemeResource("img/icono_comprobante.png") ,this);
		ButtonConceptos = new ButtonImage(EtiquetasLabel.getConceptos() ,new ThemeResource("img/icono_concepto.png") ,this);
		ButtonPersonas = new ButtonImage(EtiquetasLabel.getPersonas() ,new ThemeResource("img/icono_personas.png") ,this);
		ButtonComprobanteBloqueo = new ButtonImage(EtiquetasLabel.getBloqueosComprobantes() ,new ThemeResource("img/icono_bloqueo.png") ,this);
		ButtonBalance = new ButtonImage(EtiquetasLabel.getBalance() ,new ThemeResource("img/icono_balance.png") ,this);
		GridLayout layout = new GridLayout();
		layout.addStyleName(R.Style.PADDING_MEDIO);
		layout.setSpacing(true);
		layout.setColumns(7);
		layoutOpciones.addComponent(layout);
		Usuario usuario = UsuarioRN.Read(sesion.getUsuarioId());
		if (usuario.getTipoUsuario() == Dominios.TipoUsuario.ALUMNO || usuario.getTipoUsuario() == Dominios.TipoUsuario.PADRE){
			layout.addComponent(ButtonPreferencias);
			layout.addComponent(ButtonMisAsignaturas);
		}
		if (usuario.getTipoUsuario() == Dominios.TipoUsuario.PROFESOR){
			layout.addComponent(ButtonPreferencias);
			layout.addComponent(ButtonMisAlumnos);
			Profesor profesor = ProfesorRN.Read(usuario.getProfesorId());
			if (profesor.isEsPreceptor()){
				layout.addComponent(ButtonMisCursos);
			}
		}
		if (usuario.getTipoUsuario() == Dominios.TipoUsuario.ADMINISTRATIVO || usuario.getTipoUsuario() == Dominios.TipoUsuario.ADMINISTRADOR){
			layout.addComponent(ButtonPreferencias);
			Permiso permiso = PermisoRN.Read(new PermisoId(usuario.getId(), AlumnoWWView.VIEW));
			if (permiso != null && permiso.isAcceso()){
				layout.addComponent(ButtonAlumnos);
			}
			permiso = PermisoRN.Read(new PermisoId(usuario.getId(), ProfesorWWView.VIEW));
			if (permiso != null && permiso.isAcceso()){
				layout.addComponent(ButtonProfesores);
			}
			permiso = PermisoRN.Read(new PermisoId(usuario.getId(), MateriaWWView.VIEW));
			if (permiso != null && permiso.isAcceso()){
				layout.addComponent(ButtonMaterias);
			}
			permiso = PermisoRN.Read(new PermisoId(usuario.getId(), CursoWWView.VIEW));
			if (permiso != null && permiso.isAcceso()){
				layout.addComponent(ButtonCurso);
			}
			permiso = PermisoRN.Read(new PermisoId(usuario.getId(), AsignaturaWWView.VIEW));
			if (permiso != null && permiso.isAcceso()){
				layout.addComponent(ButtonAsignaturas);
			}
			permiso = PermisoRN.Read(new PermisoId(usuario.getId(), NotificacionWWView.VIEW));
			if (permiso != null && permiso.isAcceso()){
				layout.addComponent(ButtonNotificaciones);
			}
			permiso = PermisoRN.Read(new PermisoId(usuario.getId(), MensajeWWView.VIEW));
			if (permiso != null && permiso.isAcceso()){
				layout.addComponent(ButtonMensaje);
			}
			permiso = PermisoRN.Read(new PermisoId(usuario.getId(), EscuelaView.VIEW));
			if (permiso != null && permiso.isAcceso()){
				layout.addComponent(ButtonDatosEscuela);
			}
			LayoutTitle = new HorizontalLayout();
			LayoutTitle.setWidth("100%");
			Titulo = new Label(EtiquetasLabel.getAdministrarCuentas());
			Titulo.setStyleName(R.Style.TITLE);
			LayoutTitle.addComponent(Titulo);
			layoutOpciones.addComponent(LayoutTitle);
			Separator = new Label("<hr>", ContentMode.HTML);
			layoutOpciones.addComponent(Separator);
			layout = new GridLayout();
			layout.addStyleName(R.Style.PADDING_MEDIO);
			layout.setSpacing(true);
			layout.setColumns(7);
			layoutOpciones.addComponent(layout);
			permiso = PermisoRN.Read(new PermisoId(usuario.getId(), TipoComprobanteWWView.VIEW));
			if (permiso != null && permiso.isAcceso()){
				layout.addComponent(ButtonTipoComprobante);
			}
			permiso = PermisoRN.Read(new PermisoId(usuario.getId(), ConceptoWWView.VIEW));
			if (permiso != null && permiso.isAcceso()){
				layout.addComponent(ButtonConceptos);
			}
			permiso = PermisoRN.Read(new PermisoId(usuario.getId(), PersonasWWView.VIEW));
			if (permiso != null && permiso.isAcceso()){
				layout.addComponent(ButtonPersonas);
			}
			permiso = PermisoRN.Read(new PermisoId(usuario.getId(), ComprobantesView.VIEW));
			if (permiso != null && permiso.isAcceso()){
				layout.addComponent(ButtonComprobante);
			}
			permiso = PermisoRN.Read(new PermisoId(usuario.getId(), ComprobanteBloqueoView.VIEW));
			if (permiso != null && permiso.isAcceso()){
				layout.addComponent(ButtonBalance);
			}
			permiso = PermisoRN.Read(new PermisoId(usuario.getId(), ComprobanteBalanceView.VIEW));
			if (permiso != null && permiso.isAcceso()){
				layout.addComponent(ButtonComprobanteBloqueo);
			}
		}
		if (usuario.getTipoUsuario() == Dominios.TipoUsuario.ADMINISTRADOR){
			LayoutTitle = new HorizontalLayout();
			LayoutTitle.setWidth("100%");
			Titulo = new Label(EtiquetasLabel.getAdministrarSitio());
			Titulo.setStyleName(R.Style.TITLE);
			LayoutTitle.addComponent(Titulo);
			layoutOpciones.addComponent(LayoutTitle);
			Separator = new Label("<hr>", ContentMode.HTML);
			layoutOpciones.addComponent(Separator);
			layout = new GridLayout();
			layout.addStyleName(R.Style.PADDING_MEDIO);
			layout.setSpacing(true);
			layout.setColumns(7);
			layoutOpciones.addComponent(layout);
			Permiso permiso = PermisoRN.Read(new PermisoId(usuario.getId(), ArticuloWWView.VIEW));
			if (permiso != null && permiso.isAcceso()){
				layout.addComponent(ButtonPaginas);
			}
			permiso = PermisoRN.Read(new PermisoId(usuario.getId(), UsuarioWWView.VIEW));
			if (permiso != null && permiso.isAcceso()){
				layout.addComponent(ButtonUsuarios);
			}
			permiso = PermisoRN.Read(new PermisoId(usuario.getId(), ErrorLogWWView.VIEW));
			if (permiso != null && permiso.isAcceso()){
				layout.addComponent(ButtonErrores);
			}
		}
	}


	@Override
	public void buttonClick(ClickEvent event) {
//		
	}

	@Override
	public void layoutClick(LayoutClickEvent event) {
		if (event.getSource() == ButtonPreferencias){
			sesion.setNavegarA(UserPreferencesView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		 if (event.getSource() == ButtonMisAlumnos){
			sesion.setNavegarA(MisAlumnosView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getSource() == ButtonMisCursos){
			sesion.setNavegarA(MisCursosView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getSource() == ButtonMisAsignaturas){
			sesion.setNavegarA(MisAsignaturasView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getSource() == ButtonAlumnos){
			sesion.setNavegarA(AlumnoWWView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getSource() == ButtonProfesores){
			sesion.setNavegarA(ProfesorWWView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getSource() == ButtonMaterias){
			sesion.setNavegarA(MateriaWWView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getSource() == ButtonAsignaturas){
			sesion.setNavegarA(AsignaturaWWView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getSource() == ButtonCurso){
			sesion.setNavegarA(CursoWWView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getSource() == ButtonNotificaciones){
			sesion.setNavegarA(NotificacionWWView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getSource() == ButtonMensaje){
			sesion.setNavegarA(MensajeWWView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getSource() == ButtonDatosEscuela){
			sesion.setNavegarA(EscuelaView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getSource() == ButtonTipoComprobante){
			sesion.setNavegarA(TipoComprobanteWWView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getSource() == ButtonConceptos){
			sesion.setNavegarA(ConceptoWWView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getSource() == ButtonPersonas){
			sesion.setNavegarA(PersonasWWView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getSource() == ButtonComprobante){
			sesion.setNavegarA(ComprobantesView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getSource() == ButtonComprobanteBloqueo){
			sesion.setNavegarA(ComprobanteBloqueoView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getSource() == ButtonBalance){
			sesion.setNavegarA(ComprobanteBalanceView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getSource() == ButtonPaginas){
			sesion.setNavegarA(ArticuloWWView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		
		else if (event.getSource() == ButtonUsuarios){
			sesion.setNavegarA(UsuarioWWView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getSource() == ButtonErrores){
			sesion.setNavegarA(ErrorLogWWView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
	}

}
