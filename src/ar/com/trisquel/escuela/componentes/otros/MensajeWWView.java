package ar.com.trisquel.escuela.componentes.otros;

import ar.com.trisquel.escuela.data.contenedor.MensajesContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.MensajesRN;
import ar.com.trisquel.escuela.data.tablas.Mensajes;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class MensajeWWView extends VerticalLayout implements ClickListener,View {

	public static final String VIEW = "MensajeWW";
	private static final String LINK_VER = "LINK_VER";
	private static final String COLUMNA_LEIDO = "COLUMNA_LEIDO";
	private ButtonIcon botonFiltros;
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonEliminar;
	private ButtonIcon botonRefrescar;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private TextField filtroNombre;
	private CheckBox filtroLeido;
	private Table table;
	private VerticalLayout layoutFiltros;
	private UI ui;

	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass(), ui);
		if (!control.isAcceso()) {
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		} else {
			onDraw();
		}
	}

	public void onDraw() {
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(EtiquetasLabel.getMensajes().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true, this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonFiltros = new ButtonIcon(ButtonIcon.BUSCAR, true, this);
		botonFiltros.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonFiltros);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		botonAgregar.setEnabled(false);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		botonModificar.setEnabled(false);
		layoutBotones.addComponent(botonModificar);
		botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true, this);
		botonEliminar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonEliminar);
		botonEliminar.setEnabled(false);
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true, this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonExcel);
		botonExcel.setEnabled(false);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true, this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonPDF);
		botonPDF.setEnabled(false);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		table = new Table();
		ArmaTabla();
		layout.addComponent(table);
		Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonFiltros) {
			layoutFiltros.setVisible(!layoutFiltros.isVisible());
		} else if (event.getButton() == botonRefrescar) {
			CreaDataSource();
		} else if (event.getButton() == botonModificar) {
		} else if (event.getButton() == botonAgregar) {
		} else if (event.getButton() == botonEliminar
				|| event.getButton() == botonExcel
				|| event.getButton() == botonPDF) {
		}

	}

	private void ArmaFiltros() {
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		layoutFiltros.setVisible(false);
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(6);
		//Nombre
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label label = new Label("Nombre:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("110px");
		formFiltro.addComponent(label);
		VerticalLayout layout = new VerticalLayout();
		layout.setWidth("350px");
		filtroNombre = new TextField();
		filtroNombre.setNullRepresentation("");
		filtroNombre.setNullSettingAllowed(false);
		filtroNombre.addStyleName(R.Style.EDITABLE);
		filtroNombre.addStyleName(R.Style.LABEL_BOLD);
		filtroNombre.setWidth("300px");
		layout.addComponent(filtroNombre);
		formFiltro.addComponent(layout);
		//Estado
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Leido:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		filtroLeido = new CheckBox();
		filtroLeido.setValue(false);
		formFiltro.addComponent(filtroLeido);
		layoutFiltros.addComponent(formFiltro);

	}

	private void ArmaTabla() {
		table.setHeight("550px");
		table.setWidth("985px");
		table.setSelectable(true);
		table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Mensajes mensajes = (Mensajes) itemId;
				Button botonVer = new Button(mensajes.getNombre());
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = CreaWindowABM();
						MensajeVer mensajeVer = new MensajeVer(mensajes);
						windowABM.setContent(mensajeVer);
						ui.addWindow(windowABM);
					}
				});
				return botonVer;
			}
		});
		table.addGeneratedColumn(COLUMNA_LEIDO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Mensajes mensajes = (Mensajes) itemId;
				CheckBox leido = new CheckBox();
				leido.setValue(mensajes.isLeido());
				leido.addValueChangeListener(new ValueChangeListener() {
					@Override
					public void valueChange(ValueChangeEvent event) {
						mensajes.setLeido(!mensajes.isLeido());
						MensajesRN.Save(mensajes);
					}
				});
				return leido;
			}
		});
		table.setCellStyleGenerator(new Table.CellStyleGenerator() {
			@Override
			public String getStyle(Table source, Object itemId,Object propertyId) {
		        if (propertyId != null &&propertyId.equals(COLUMNA_LEIDO))
		        	return R.Style.TABLE_CHECKBOX_EDITABLE;
		        else
		        	return null;
			}
		});
		CreaDataSource();
	}

	private void CreaDataSource() {
		table.setContainerDataSource(MensajesContenedor.LeeContainerXNombre(filtroNombre.getValue(), filtroLeido.getValue()));
		table.setVisibleColumns((Object[]) new String[] { LINK_VER, "mail", "asunto", "fechaHora",COLUMNA_LEIDO ,"id" });
		table.setColumnHeaders("Nombre", "Mail", "Asunto","Fecha", "Leido" ,"Id");
	}

	public static Window CreaWindowABM() {
		Window windowABM = new Window(EtiquetasLabel.getMensajes());
		windowABM.setModal(true);
		windowABM.setResizable(false);
		windowABM.setDraggable(false);
		windowABM.setWidth("600px");
		windowABM.setHeight("400px");
		windowABM.center();
		windowABM.setCloseShortcut(KeyCode.ESCAPE, null);
		return windowABM;
	}

}
