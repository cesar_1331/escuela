package ar.com.trisquel.escuela.componentes.otros;

import java.util.List;

import ar.com.trisquel.escuela.data.contenedor.AlumnoContenedor;
import ar.com.trisquel.escuela.data.contenedor.CursoContenedor;
import ar.com.trisquel.escuela.data.contenedor.ProfesorContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.CursoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.NotificacionPersonaRN;
import ar.com.trisquel.escuela.data.select.AlumnoSelect;
import ar.com.trisquel.escuela.data.select.ProfesorSelect;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.NotificacionPersona;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class NotificacionPersonaBuscar extends VerticalLayout implements ClickListener, ValueChangeListener {
	
	private OptionGroup option;
	private HorizontalLayout layoutAlumnosCurso;
	private HorizontalLayout layoutAlumnosTodos;
	private HorizontalLayout layoutProfesoresTodos;
	private VerticalLayout layoutAlumnos;
	private VerticalLayout layoutProfesores;
	private List<NotificacionPersona> listNotificacionPersona;
	
	
	public NotificacionPersonaBuscar(List<NotificacionPersona> listNotificacionPersona ){
		this.listNotificacionPersona = listNotificacionPersona;
		onDraw();
	}
	
	public void onDraw(){
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		setSpacing(true);
		setWidth("100%");
		option = new OptionGroup();
		option.addItem(1);
		option.setItemCaption(1, "Seleccionar todos los alumnos de un curso");
		option.addItem(2);
		option.setItemCaption(2, "Seleccionar todos los alumnos");
		option.addItem(3);
		option.setItemCaption(3, "Seleccionar todos los profesores");
		option.addItem(4);
		option.setItemCaption(4, "Seleccionar alumnos");
		option.addItem(5);
		option.setItemCaption(5, "Seleccionar profesores");
		option.setImmediate(true);
		option.addValueChangeListener(this);
		addComponent(option);
		option.focus();
		Label Separator = new Label("<hr>", ContentMode.HTML);
		addComponent(Separator);
		layoutAlumnosCurso = new HorizontalLayout();
		layoutAlumnosCurso.setVisible(false);
		addComponent(layoutAlumnosCurso);
		layoutAlumnosTodos = new HorizontalLayout();
		layoutAlumnosTodos.setVisible(false);
		addComponent(layoutAlumnosTodos);
		layoutProfesoresTodos = new HorizontalLayout();
		layoutProfesoresTodos.setVisible(false);
		addComponent(layoutProfesoresTodos);
		layoutAlumnos = new VerticalLayout();
		layoutAlumnos.setVisible(false);
		addComponent(layoutAlumnos);
		layoutProfesores = new VerticalLayout();
		layoutProfesores.setVisible(false);
		addComponent(layoutProfesores);
		SeleccionarAlumnosCurso();
		SeleccionarTodosAlumnos();
		SeleccionarTodosProfesores();
		SeleccionarAlumnos();
		SeleccionarProfesores();
	}
	

	@Override
	public void valueChange(ValueChangeEvent event) {
		layoutAlumnosCurso.setVisible(false);
		layoutAlumnosTodos.setVisible(false);
		layoutProfesoresTodos.setVisible(false);
		layoutAlumnos.setVisible(false);
		layoutProfesores.setVisible(false);
		if (option.getValue() != null){
			int opcion = Integer.parseInt(String.valueOf(option.getValue()));
			switch (opcion) {
			case 1:
				layoutAlumnosCurso.setVisible(true);
				break;
			case 2:
				layoutAlumnosTodos.setVisible(true);
				break;
			case 3:
				layoutProfesoresTodos.setVisible(true);
				break;
			case 4:
				layoutAlumnos.setVisible(true);
				break;
			case 5:
				layoutProfesores.setVisible(true);
				break;
			}
		}
	}
	
	private void SeleccionarAlumnosCurso(){
		layoutAlumnosCurso.setSpacing(true);
		Label label = new Label("Seleccionar todos los alumnos del curso: ");
		label.addStyleName(R.Style.LABEL_BOLD);
		layoutAlumnosCurso.addComponent(label);
		final ComboBox comboCurso = new ComboBox();
		comboCurso.setContainerDataSource(CursoContenedor.LeeContainerXGradoXTurno(0, "", true));
		comboCurso.setNullSelectionAllowed(false);
		comboCurso.setWidth("200px");
		layoutAlumnosCurso.addComponent(comboCurso);
		Button botonAgregar = new Button(EtiquetasLabel.getAgregar());
		botonAgregar.setPrimaryStyleName(R.Style.BUTTON_COLOR);
		botonAgregar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				if (comboCurso.getValue() == null){
					MessageBox.showHTML(Icon.ERROR, "" 
							,"Debe seleccionar el curso"
							,ButtonId.OK);
				}
				else{
					MessageBox.showHTML(Icon.QUESTION, "" 
							,"�Realmente desea agregar a todos los alumnos del curso seleccionado?"
							,new MessageBoxListener() {
								@Override
								public void buttonClicked(ButtonId buttonType) {
									if (buttonType == ButtonId.OK){
										Curso curso = (Curso)comboCurso.getValue();
										for (Alumno alumno : AlumnoSelect.SelectXCursoId(curso.getId())){
											NotificacionPersona notificacionPersona = NotificacionPersonaRN.Inizializate(0, Dominios.TipoPersonaNotificacion.ALUMNO, alumno.getId());
											listNotificacionPersona.add(notificacionPersona);
										}
										MessageBox.showHTML(Icon.INFO, "" 
												,"Se agregaron todos los alumnos que estan cursando"
												,ButtonId.OK);
									}
								}
							}
							, ButtonId.OK 
							,ButtonId.CANCEL);
				}
			}
		});
		layoutAlumnosCurso.addComponent(botonAgregar);
	}
	private void SeleccionarTodosAlumnos(){
		layoutAlumnosTodos.setSpacing(true);
		Label label = new Label("Seleccionar todos los alumnos");
		label.addStyleName(R.Style.LABEL_BOLD);
		layoutAlumnosTodos.addComponent(label);
		Button botonAgregar = new Button(EtiquetasLabel.getAgregar());
		botonAgregar.setPrimaryStyleName(R.Style.BUTTON_COLOR);
		botonAgregar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				MessageBox.showHTML(Icon.QUESTION, "" 
						,"�Realmente desea agregar a todos los alumnos que estan cursando?"
						,new MessageBoxListener() {
							@Override
							public void buttonClicked(ButtonId buttonType) {
								if (buttonType == ButtonId.OK){
									for (Alumno alumno : AlumnoSelect.SelectXNombreApellidoXDniXEstadoXCurso("", 0, Dominios.EstadoAlumno.CURSANDO, 0)){
										NotificacionPersona notificacionPersona = NotificacionPersonaRN.Inizializate(0, Dominios.TipoPersonaNotificacion.ALUMNO, alumno.getId());
										listNotificacionPersona.add(notificacionPersona);
									}
									MessageBox.showHTML(Icon.INFO, "" 
											,"Se agregaron todos los alumnos que estan cursando"
											,ButtonId.OK);
								}
							}
						}
						, ButtonId.OK 
						,ButtonId.CANCEL);
			}
		});
		layoutAlumnosTodos.addComponent(botonAgregar);
	}
	private void SeleccionarTodosProfesores(){
		layoutProfesoresTodos.setSpacing(true);
		
		Label label = new Label("Seleccionar todos los profesores");
		label.addStyleName(R.Style.LABEL_BOLD);
		layoutProfesoresTodos.addComponent(label);
		Button botonAgregar = new Button(EtiquetasLabel.getAgregar());
		botonAgregar.setPrimaryStyleName(R.Style.BUTTON_COLOR);
		botonAgregar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				MessageBox.showHTML(Icon.QUESTION, "" 
						,"�Realmente desea agregar a todos los profesores activos?"
						,new MessageBoxListener() {
							@Override
							public void buttonClicked(ButtonId buttonType) {
								if (buttonType == ButtonId.OK){
									
									for (Profesor profesor : ProfesorSelect.SelectXNombreApellidoXActivo("",false , true)){
										NotificacionPersona notificacionPersona = NotificacionPersonaRN.Inizializate(0, Dominios.TipoPersonaNotificacion.PROFESOR, profesor.getId());
										listNotificacionPersona.add(notificacionPersona);
									}
									MessageBox.showHTML(Icon.INFO, "" 
											,"Se agregaron todos los profesores activos"
											,ButtonId.OK);
								}
							}
						}
						, ButtonId.OK 
						,ButtonId.CANCEL);
			}
		});
		layoutProfesoresTodos.addComponent(botonAgregar);
	}
	private void SeleccionarAlumnos(){
		layoutAlumnos.setSpacing(true);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(7);
		//Nombre y Apellido
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label label = new Label("Apellido y Nombre:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("110px");
		formFiltro.addComponent(label);
		VerticalLayout layout = new VerticalLayout();
		layout.setWidth("400px");
		final TextField filtroNombre = new TextField();
		filtroNombre.setNullRepresentation("");
		filtroNombre.setNullSettingAllowed(false);
		filtroNombre.addStyleName(R.Style.EDITABLE);
		filtroNombre.addStyleName(R.Style.LABEL_BOLD);
		filtroNombre.setWidth("300px");
		layout.addComponent(filtroNombre);
		formFiltro.addComponent(layout);
		//DNI
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("DNI:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("40px");
		formFiltro.addComponent(label);
		final TextField filtroDNI = new TextField();
		filtroDNI.setNullRepresentation("0");
		filtroDNI.setNullSettingAllowed(false);
		filtroDNI.addStyleName(R.Style.EDITABLE);
		filtroDNI.addStyleName(R.Style.LABEL_BOLD);
		filtroDNI.setWidth("100px");
		formFiltro.addComponent(filtroDNI);
		ButtonIcon botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true);
		formFiltro.addComponent(botonRefrescar);
		layoutAlumnos.addComponent(formFiltro);
		final Table table = new Table(); 
		table.setHeight("385px");
		table.setWidth("775px");
		table.setSelectable(true);
		table.addGeneratedColumn("AGREGAR", new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Alumno alumno = (Alumno)itemId;
				ButtonIcon botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true);
				botonAgregar.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						NotificacionPersona notificacionPersona = NotificacionPersonaRN.Inizializate(0, Dominios.TipoPersonaNotificacion.ALUMNO, alumno.getId());
						listNotificacionPersona.add(notificacionPersona);
						MessageBox.showHTML(Icon.INFO, "" 
								,"Se agrego el alumno " + alumno.toString()
								,ButtonId.OK);
					}
				});
				return botonAgregar;
			}

		});
		table.addGeneratedColumn("NOMBRE", new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Alumno alumno = (Alumno)itemId;
				return new Label(alumno.toString());
			}
		});
		table.addGeneratedColumn("CURSO", new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Alumno alumno = (Alumno)itemId;
				Label labelCurso = new Label();
				if (alumno.getCursoId() > 0){
					labelCurso.setValue(CursoRN.Read(alumno.getCursoId()).toString());
				}
				return labelCurso;
			}
		});
		layoutAlumnos.addComponent(table);
		CreaDataSourceAlumno(table ,filtroNombre.getValue() ,filtroDNI.getValue());
		botonRefrescar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				CreaDataSourceAlumno(table ,filtroNombre.getValue() ,filtroDNI.getValue());
			}
		});
	}
	
	private void CreaDataSourceAlumno(Table table ,String filtroNombre ,String dni) {
		long dniNro = 0;
		try{
			dniNro = Long.parseLong(dni);
		}
		catch (NumberFormatException e){
		}
		table.setContainerDataSource(AlumnoContenedor.LeeContainerXNombreApellidoXDniXEstadoXCurso(filtroNombre,dniNro ,Dominios.EstadoAlumno.CURSANDO ,0));
		table.setVisibleColumns((Object[]) new String[] {"AGREGAR" ,"NOMBRE" ,"dni" ,"domicilio" ,"telefono" ,"CURSO"});
		table.setColumnHeaders("" ,"Apellido, Nombre" ,"DNI" ,"Domicilio" ,"Tel�fono" ,"Curso");
	}
	
	private void SeleccionarProfesores(){
		layoutProfesores.setSpacing(true);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(4);
		formFiltro.setSpacing(true);
		// Nombre y Apellido
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label labelId = new Label("Apellido y Nombre:");
		labelId.addStyleName(R.Style.EDITABLE_LABEL);
		labelId.setWidth("110px");
		formFiltro.addComponent(labelId);
		VerticalLayout layout = new VerticalLayout();
		layout.setWidth("330px");
		final TextField filtroNombre = new TextField();
		filtroNombre.setNullRepresentation("");
		filtroNombre.setNullSettingAllowed(false);
		filtroNombre.addStyleName(R.Style.EDITABLE);
		filtroNombre.addStyleName(R.Style.LABEL_BOLD);
		filtroNombre.setWidth("300px");
		layout.addComponent(filtroNombre);
		formFiltro.addComponent(layout);
		ButtonIcon botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true);
		formFiltro.addComponent(botonRefrescar);
		layoutProfesores.addComponent(formFiltro);
		final Table table = new Table(); 
		table.setHeight("385px");
		table.setWidth("775px");
		table.setSelectable(true);
		table.addGeneratedColumn("AGREGAR", new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Profesor profesor = (Profesor)itemId;
				ButtonIcon botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true);
				botonAgregar.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						NotificacionPersona notificacionPersona = NotificacionPersonaRN.Inizializate(0, Dominios.TipoPersonaNotificacion.PROFESOR, profesor.getId());
						listNotificacionPersona.add(notificacionPersona);
						MessageBox.showHTML(Icon.INFO, "" 
								,"Se agrego el profesor " + profesor.toString()
								,ButtonId.OK);
					}
				});
				return botonAgregar;
			}

		});
		table.addGeneratedColumn("NOMBRE", new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Profesor profesor = (Profesor)itemId;
				return new Label(profesor.toString());
			}
		});
		layoutProfesores.addComponent(table);
		CreaDataSourceProfesor(table ,filtroNombre.getValue());
		botonRefrescar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				CreaDataSourceProfesor(table ,filtroNombre.getValue());
			}
		});
	}
	private void CreaDataSourceProfesor(Table table ,String filtroNombre) {
		table.setContainerDataSource(ProfesorContenedor.LeeContainerXNombreApellidoXActivo(filtroNombre ,false ,true));
		table.setVisibleColumns((Object[]) new String[] {"AGREGAR" ,"NOMBRE" ,"domicilio" ,"telefono"});
		table.setColumnHeaders("" ,"Apellido y Nombre" ,"Domicilio" ,"Tel�fono");
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		// TODO Auto-generated method stub

	}

	public List<NotificacionPersona> getListNotificacionPersona() {
		return listNotificacionPersona;
	}

}
