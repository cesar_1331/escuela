package ar.com.trisquel.escuela.componentes.otros;

import java.util.Date;

import org.vaadin.openesignforms.ckeditor.CKEditorConfig;
import org.vaadin.openesignforms.ckeditor.CKEditorTextField;

import ar.com.trisquel.escuela.data.reglasdenegocio.ArticuloRN;
import ar.com.trisquel.escuela.data.tablas.Articulo;
import ar.com.trisquel.escuela.language.ArticuloLabel;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class ArticuloABM extends VerticalLayout implements ClickListener {
	
	private TextField id;
	private TextField titulo;
	private CKEditorTextField WYSING;
	private CKEditorConfig config ;
	private Articulo articulo = null;
	private ButtonIcon ButtonGuardar;
	private ButtonIcon ButtonDescartar;
	private Label articuloError;
	private UI ui;
	private String modo;
	private Window window;
	
	public ArticuloABM(Articulo articulo ,String modo ,UI ui ,Window window){
		this.articulo = articulo;
		this.ui = ui;
		this.modo= modo;
		this.window = window; 
		onDraw();
	}
	
	public void onDraw(){
		removeAllComponents(); 
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setWidth("100%");
		layout.setColumns(3);
		addComponent(layout);		
		//Id
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelId = new Label(ArticuloLabel.getId());
		labelId.addStyleName(R.Style.EDITABLE_LABEL);
		labelId.setWidth("70px");
		layout.addComponent(labelId);
		HorizontalLayout layoutId = new HorizontalLayout();
		layoutId.setWidth("800px");
		id = new TextField();
		id.addStyleName(R.Style.EDITABLE);
		id.addStyleName(R.Style.LABEL_BOLD);
		layoutId.addComponent(id);
		//Botones
		HorizontalLayout layoutBotones = new HorizontalLayout();
		ButtonGuardar = new ButtonIcon(ButtonIcon.GUARDAR ,true ,this);
		ButtonGuardar.addStyleName(R.Style.PADDING_MINIMO);
		ButtonGuardar.setTabIndex(12);
		layoutBotones.addComponent(ButtonGuardar);
		ButtonDescartar = new ButtonIcon(ButtonIcon.DESCARTAR ,true ,this);
		ButtonDescartar.addStyleName(R.Style.PADDING_MINIMO);
		ButtonDescartar.setTabIndex(11);
		layoutBotones.addComponent(ButtonDescartar);
		layoutId.addComponent(layoutBotones);
		layoutId.setComponentAlignment(layoutBotones, Alignment.TOP_RIGHT);
		layout.addComponent(layoutId);
		//Titulo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelTitulo = new Label(ArticuloLabel.getTitulo());
		labelTitulo.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(labelTitulo);
		titulo = new TextField();
		titulo.setWidth("300px");
		titulo.setMaxLength(40);
		titulo.addStyleName(R.Style.EDITABLE);
		titulo.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(titulo);
		//Contenido
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelContenido = new Label(ArticuloLabel.getContenido());
		labelContenido.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(labelContenido);
		config = new CKEditorConfig();
        config.useCompactTags();
        config.disableElementsPath();
        config.setResizeDir(CKEditorConfig.RESIZE_DIR.HORIZONTAL);
        config.disableSpellChecker();
        config.setToolbarCanCollapse(false);
        config.setWidth("800px");
        WYSING = new CKEditorTextField(config);
		WYSING.setImmediate(true);
		WYSING.setHeight("500");
		WYSING.addStyleName(R.Style.EDITABLE);
		WYSING.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(WYSING);
		layout.addComponent(new Label());
		layout.addComponent(new Label());
		articuloError = new Label();
		articuloError.addStyleName(R.Style.ERROR_VIEW);
		layout.addComponent(articuloError);

		id.setValue(String.valueOf(articulo.getId()));
		titulo.setValue(articulo.getTitulo().trim());
		WYSING.setValue(articulo.getContenido());
		id.setReadOnly(true);
		if (modo.equals(Dominios.AccesoADatos.SELECT)){
			titulo.setReadOnly(true);
			WYSING.setReadOnly(true);
			ButtonGuardar.setVisible(false);
			ButtonDescartar.setVisible(false);
		}
		else if (modo.equals(Dominios.AccesoADatos.UPDATE)){
			titulo.setReadOnly(false);
			WYSING.setReadOnly(false);
			ButtonGuardar.setVisible(true);
			ButtonDescartar.setVisible(true);
		}
		focus();
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == ButtonDescartar){
			titulo.setReadOnly(true);
			WYSING.setReadOnly(true);
			ButtonGuardar.setVisible(false);
			ButtonDescartar.setVisible(false);
			if (modo.equals(Dominios.AccesoADatos.UPDATE)){
				ui.removeWindow(window);
			}
		}
		else if (event.getButton() == ButtonGuardar){
			Sesion sesion = (Sesion)ui.getSession().getAttribute(R.Session.SESSION);
			articulo.setTitulo(titulo.getValue());
			articulo.setContenido(WYSING.getValue());
			articulo.setFechaHora(new Date(System.currentTimeMillis()));
			articulo.setUsuario(sesion.getUsuarioId());
			RespuestaEntidad respuesta = ArticuloRN.Validate(articulo ,Dominios.AccesoADatos.UPDATE);
			if (respuesta.isError()){
				MessageBox.showHTML(Icon.ERROR, "", respuesta.getMsgError(), ButtonId.OK);
			}
			else{
				MessageBox.showHTML(Icon.QUESTION, "", MensajeLabel.getConfirmaAccion(), new MessageBoxListener() {
					@Override
					public void buttonClicked(ButtonId buttonType) {
						if (buttonType == ButtonId.SAVE){
							RespuestaEntidad respuesta = ArticuloRN.Save(articulo);
							if (respuesta.isError()){
								MessageBox.showHTML(Icon.ERROR, "", respuesta.getMsgError(), ButtonId.OK);
							}
							else{
								
								if (modo.equals(Dominios.AccesoADatos.UPDATE)){
									ui.removeWindow(window);
								}
								else{
									onDraw();
								}
							}
						}
					}
				}, ButtonId.SAVE ,ButtonId.CANCEL);
			}
		}
	}

}
