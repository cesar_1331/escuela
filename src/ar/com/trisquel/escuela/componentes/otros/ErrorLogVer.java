package ar.com.trisquel.escuela.componentes.otros;

import ar.com.trisquel.escuela.data.tablas.ErrorLog;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class ErrorLogVer extends VerticalLayout {
	private ErrorLog errorLog;

	public ErrorLogVer(ErrorLog errorLog) {
		this.errorLog = errorLog;
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setColumns(3);
		addComponent(layout);
		//Id
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Id:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("70px");
		layout.addComponent(label);
		TextField id = new TextField();
		id.setValue(String.valueOf(errorLog.getId()));
		id.addStyleName(R.Style.EDITABLE);
		id.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(id);
		id.setReadOnly(true);
		id.focus();
		//Causa
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Causa:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		label = new Label(errorLog.getCausa());
		label.addStyleName(R.Style.EDITABLE);
		label.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(label);
		//Fecha
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Fecha:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		label = new Label(errorLog.getFechaHora().toString());
		label.addStyleName(R.Style.EDITABLE);
		label.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(label);
		//
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Mensaje:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		label = new Label(errorLog.getTraza() ,ContentMode.HTML);
		label.addStyleName(R.Style.EDITABLE);
		label.addStyleName(R.Style.LABEL_BOLD);
		label.setWidth("500px");
		layout.addComponent(label);		
	}

	
}
