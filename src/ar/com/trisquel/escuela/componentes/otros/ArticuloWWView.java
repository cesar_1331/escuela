package ar.com.trisquel.escuela.componentes.otros;

import ar.com.trisquel.escuela.data.contenedor.ArticuloContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.ArticuloRN;
import ar.com.trisquel.escuela.data.tablas.Articulo;
import ar.com.trisquel.escuela.language.ArticuloLabel;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class ArticuloWWView extends VerticalLayout implements View ,ClickListener{
	public static final String VIEW = "ArticuloWW";
	private static final String LINK_VER = "LINK_VER";
	private ButtonIcon botonFiltros;
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonEliminar;
	private ButtonIcon botonRefrescar;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private TextField filtroTitulo;
	private Table table;
	private VerticalLayout layoutFiltros;
	private UI ui;
	
	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass() ,ui); 
		if (!control.isAcceso()){
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		}
		else{
			inicializa();
			onDraw();
		}
	}
	
	public void onDraw()
	{	
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(EtiquetasLabel.getPaginas().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>" ,ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true ,this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonFiltros = new ButtonIcon(ButtonIcon.BUSCAR, true ,this);
		botonFiltros.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonFiltros);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true ,this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		botonAgregar.setEnabled(false);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true ,this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		//botonModificar.setEnabled(false);
		layoutBotones.addComponent(botonModificar);
		botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true ,this);
		botonEliminar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonEliminar);
		botonEliminar.setEnabled(false);
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true ,this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonExcel);
		botonExcel.setEnabled(false);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true ,this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonPDF);
		botonPDF.setEnabled(false);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		table = new Table();
        ArmaTabla();
        layout.addComponent(table);
        Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
		
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonFiltros){
			layoutFiltros.setVisible(!layoutFiltros.isVisible());
		}
		else if (event.getButton() == botonRefrescar){
			CreaDataSource();
		}
		else if (event.getButton() == botonModificar){
			Articulo articulo = (Articulo)table.getValue();
			if (articulo != null){
				Window windowABM = new Window(EtiquetasLabel.getPaginas());
				windowABM.setModal(true);
				windowABM.setResizable(false);
				windowABM.setDraggable(false);
				windowABM.setWidth("950px");
				windowABM.setHeight("620px");
				windowABM.center();
				windowABM.setCloseShortcut(KeyCode.ESCAPE, null);
				ArticuloABM articuloABM = new ArticuloABM(articulo ,Dominios.AccesoADatos.UPDATE ,ui ,windowABM);
				windowABM.setContent(articuloABM);
				windowABM.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						CreaDataSource();
					}
				});
				ui.addWindow(windowABM);
			}
		}
		else if (event.getButton() == botonAgregar || 
				event.getButton() == botonEliminar ||
				event.getButton() == botonExcel ||
				event.getButton() == botonPDF ){
			/**
			 * No hace nada
			 */
		}
		
	}
	
	private void ArmaFiltros(){
		layoutFiltros.setVisible(false);
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(3);
		Label labelId = new Label(ArticuloLabel.getTitulo());
		labelId.addStyleName(R.Style.EDITABLE_LABEL);
		labelId.setWidth("50px");
		formFiltro.addComponent(labelId);
		filtroTitulo = new TextField();
		filtroTitulo.setNullRepresentation("");
		filtroTitulo.setNullSettingAllowed(false);
		filtroTitulo.addStyleName(R.Style.EDITABLE);
		filtroTitulo.addStyleName(R.Style.LABEL_BOLD);
		filtroTitulo.setWidth("200px");
		formFiltro.addComponent(filtroTitulo);
		layoutFiltros.addComponent(formFiltro);
		
	}
	
	private void ArmaTabla(){
        table.setHeight("550px");
        table.setWidth("985px");
        table.setSelectable(true);
        table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
        	public Component generateCell(Table source, Object itemId, Object columnId) {
        		final Articulo articulo = (Articulo)itemId;
        		Button botonVer = new Button(articulo.getTitulo());
        		botonVer.setPrimaryStyleName(R.Style.LINK);
        		botonVer.addStyleName(R.Style.LABEL_BOLD);
        		botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						if (articulo != null){
							Window windowABM = new Window(EtiquetasLabel.getPaginas());
							windowABM.setModal(true);
							windowABM.setResizable(false);
							windowABM.setDraggable(false);
							windowABM.setWidth("950px");
							windowABM.setHeight("620px");
							windowABM.center();
							windowABM.setCloseShortcut(KeyCode.ESCAPE, null);
							ArticuloABM articuloABM = new ArticuloABM(articulo ,Dominios.AccesoADatos.SELECT ,ui ,windowABM);
							windowABM.setContent(articuloABM);
							windowABM.addCloseListener(new CloseListener() {
								@Override
								public void windowClose(CloseEvent e) {
									CreaDataSource();
								}
							});
							ui.addWindow(windowABM);
						}
					}
				});
        		return botonVer; 
        	}
        	
        });
        CreaDataSource();
	}
	
	private void CreaDataSource(){
		table.setContainerDataSource(ArticuloContenedor.LeeContainerXTitulo(filtroTitulo.getValue()));
		table.setVisibleColumns((Object[]) new String[] {"id",LINK_VER,"fechaHora" , "usuario"});
        table.setColumnHeaders(ArticuloLabel.getTituloId(),ArticuloLabel.getTituloTitulo(),ArticuloLabel.getTituloModificado() , ArticuloLabel.getTituloResponsable() );
	}
	
	public static void inicializa(){
		Sesion sesion = (Sesion)UI.getCurrent().getSession().getAttribute(R.Session.SESSION);
		String usuario = "admin";
		if (sesion.getUsuarioId() != null){
			usuario = sesion.getUsuarioId();
		}
		Articulo articulo = ArticuloRN.Inizializate(R.Paginas.INICIO ,R.Paginas.INICIO ,usuario);
		ArticuloRN.Add(articulo);
		articulo = ArticuloRN.Inizializate(R.Paginas.AUTORIDADES ,R.Paginas.AUTORIDADES ,usuario);
		ArticuloRN.Add(articulo); 
		articulo = ArticuloRN.Inizializate(R.Paginas.ERROR ,R.Paginas.ERROR ,usuario);
		ArticuloRN.Add(articulo); 
		articulo = ArticuloRN.Inizializate(R.Paginas.HISTORIA ,R.Paginas.HISTORIA  ,usuario);
		ArticuloRN.Add(articulo); 
		articulo = ArticuloRN.Inizializate(R.Paginas.MAILREESTABLECERCLAVE ,R.Paginas.MAILREESTABLECERCLAVE ,usuario);
		ArticuloRN.Add(articulo); 
		articulo = ArticuloRN.Inizializate(R.Paginas.OFERTA_ACADEMICA ,R.Paginas.OFERTA_ACADEMICA ,usuario);
		ArticuloRN.Add(articulo);
		articulo = ArticuloRN.Inizializate(R.Paginas.ACCESO_DENEGADO ,R.Paginas.ACCESO_DENEGADO ,usuario);
		ArticuloRN.Add(articulo);
	}
}
