package ar.com.trisquel.escuela.componentes.otros;

import java.util.Date;
import java.util.GregorianCalendar;

import ar.com.trisquel.escuela.data.contenedor.NotificacionContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.NotificacionRN;
import ar.com.trisquel.escuela.data.tablas.Notificacion;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class NotificacionWWView extends VerticalLayout implements ClickListener, View {
	public static final String VIEW = "NotificacionWW";
	private static final String LINK_VER = "LINK_VER";
	private static final String COLUMNA_CANCELADA = "COLUMNA_CANCELADA";
	private static final String COLUMNA_LEIDO = "COLUMNA_LEIDO";
	private ButtonIcon botonFiltros;
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonEliminar;
	private ButtonIcon botonRefrescar;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private TextField filtroTitulo;
	private DateField filtroFechaDesde;
	private DateField filtroFechaHasta;
	private CheckBox filtroCancelado;
	private CheckBox filtroLeido;
	private Table table;
	private VerticalLayout layoutFiltros;
	private UI ui;

	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass(), ui);
		if (!control.isAcceso()) {
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		} else {
			onDraw();
		}
	}

	public void onDraw() {
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(EtiquetasLabel.getNotificaciones().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true, this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonFiltros = new ButtonIcon(ButtonIcon.BUSCAR, true, this);
		botonFiltros.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonFiltros);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		//botonAgregar.setEnabled(false);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		//botonModificar.setEnabled(false);
		layoutBotones.addComponent(botonModificar);
		botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true, this);
		botonEliminar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonEliminar);
		//botonEliminar.setEnabled(false);
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true, this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonExcel);
		botonExcel.setEnabled(false);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true, this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonPDF);
		botonPDF.setEnabled(false);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		table = new Table();
		ArmaTabla();
		layout.addComponent(table);
		Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonFiltros) {
			layoutFiltros.setVisible(!layoutFiltros.isVisible());
		} else if (event.getButton() == botonRefrescar) {
			CreaDataSource();
		} else if (event.getButton() == botonModificar) {
			Notificacion notificacion = (Notificacion) table.getValue();
			if (notificacion != null) {
				if (notificacion.isCancelada()){
					MessageBox.showHTML(Icon.INFO, "", "La notificacion ya esta cancelada", ButtonId.OK);
				}
				else if (notificacion.isLeido()){
					MessageBox.showHTML(Icon.INFO, "", "La notificacion ya fue leida por todos los notificados", ButtonId.OK);
				}
				else{
					Window windowABM = CreaWindowABM();
					NotificacionABM notificacionABM = new NotificacionABM(notificacion,Dominios.AccesoADatos.UPDATE, ui, windowABM);
					windowABM.setContent(notificacionABM);
					windowABM.addCloseListener(new CloseListener() {
						@Override
						public void windowClose(CloseEvent e) {
							CreaDataSource();
						}
					});
					ui.addWindow(windowABM);
				}
			}
		} else if (event.getButton() == botonAgregar) {
			Window windowABM = CreaWindowABM();
			NotificacionABM notificacionABM = new NotificacionABM(null,Dominios.AccesoADatos.INSERT, ui, windowABM);
			windowABM.setContent(notificacionABM);
			windowABM.addCloseListener(new CloseListener() {
				@Override
				public void windowClose(CloseEvent e) {
					CreaDataSource();
				}
			});
			ui.addWindow(windowABM);
		} else if (event.getButton() == botonEliminar){
			final Notificacion notificacion = (Notificacion) table.getValue();
			if (notificacion != null) {
				if (notificacion.isCancelada()){
					MessageBox.showHTML(Icon.INFO, "", "La notificacion ya esta cancelada", ButtonId.OK);
				}
				else if (notificacion.isLeido()){
					MessageBox.showHTML(Icon.INFO, "", "La notificacion ya fue leida por todos los notificados", ButtonId.OK);
				}
				else{
					MessageBox.showHTML(Icon.QUESTION, ""
							,"¿Realmete desea cancelar la notificación?"
							,new MessageBoxListener() {	
								@Override
								public void buttonClicked(ButtonId buttonType) {
									if (buttonType == ButtonId.OK){
										NotificacionRN.Cancelar(notificacion);
										CreaDataSource();
									}
								}
							}
							,ButtonId.OK 
							,ButtonId.CANCEL);
				}
			}
			
		} else if (event.getButton() == botonExcel
				|| event.getButton() == botonPDF) {
			/**
			 * No hace nada
			 */
		}

	}

	private void ArmaFiltros() {
		layoutFiltros.setVisible(false);
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(new Date(System.currentTimeMillis()));
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(6);
		//Titulo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label label = new Label("Titulo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("55px");
		formFiltro.addComponent(label);
		VerticalLayout layout = new VerticalLayout();
		layout.setWidth("400px");
		filtroTitulo = new TextField();
		filtroTitulo.setNullRepresentation("");
		filtroTitulo.setNullSettingAllowed(false);
		filtroTitulo.addStyleName(R.Style.EDITABLE);
		filtroTitulo.addStyleName(R.Style.LABEL_BOLD);
		filtroTitulo.setWidth("300px");
		layout.addComponent(filtroTitulo);
		formFiltro.addComponent(layout);
		//Cancelado
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Cancelados:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("80px");
		formFiltro.addComponent(label);
		filtroCancelado = new CheckBox();
		filtroCancelado.setValue(false);
		formFiltro.addComponent(filtroCancelado);
		//Periodo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Periodo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		HorizontalLayout layoutPeriodo = new HorizontalLayout();
		filtroFechaDesde = new DateField();
		filtroFechaDesde.setWidth("100px");
		filtroFechaDesde.setResolution(Resolution.DAY);
		layoutPeriodo.addComponent(filtroFechaDesde);
		filtroFechaHasta = new DateField();
		filtroFechaHasta.setWidth("100px");
		filtroFechaHasta.setResolution(Resolution.DAY);
		layoutPeriodo.addComponent(filtroFechaHasta);
		calendar.clear(GregorianCalendar.HOUR);
		calendar.clear(GregorianCalendar.MINUTE);
		calendar.set(GregorianCalendar.HOUR ,23);
		calendar.set(GregorianCalendar.MINUTE ,59);
		filtroFechaHasta.setValue(calendar.getTime());
		calendar.clear(GregorianCalendar.HOUR);
		calendar.clear(GregorianCalendar.MINUTE);
		calendar.add(GregorianCalendar.DAY_OF_YEAR ,-7);
		filtroFechaDesde.setValue(calendar.getTime());
		
		formFiltro.addComponent(layoutPeriodo);
		//Leido
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Solo No Leidos:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		filtroLeido = new CheckBox();
		filtroLeido.setValue(false);
		formFiltro.addComponent(filtroLeido);
		layoutFiltros.addComponent(formFiltro);

	}

	private void ArmaTabla() {
		table.setHeight("550px");
		table.setWidth("985px");
		table.setSelectable(true);
		table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Notificacion notificacion = (Notificacion) itemId;
				Button botonVer = new Button(notificacion.getTitulo());
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = CreaWindowABM();
						NotificacionABM notificacionABM = new NotificacionABM(notificacion,Dominios.AccesoADatos.SELECT, ui, windowABM);
						windowABM.setContent(notificacionABM);
						ui.addWindow(windowABM);
					}
				});
				return botonVer;
			}
		});
		table.addGeneratedColumn(COLUMNA_CANCELADA, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Notificacion notificacion = (Notificacion) itemId;
				CheckBox cancelada = new CheckBox();
				cancelada.setValue(notificacion.isCancelada());
				cancelada.setReadOnly(true);
				return cancelada;
			}
		});
		table.addGeneratedColumn(COLUMNA_LEIDO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Notificacion notificacion = (Notificacion) itemId;
				CheckBox leido = new CheckBox();
				leido.setValue(notificacion.isLeido());
				leido.setReadOnly(true);
				return leido;
			}
		});
		CreaDataSource();
	}

	private void CreaDataSource() {
		table.setContainerDataSource(NotificacionContenedor.LeeContainerXPeriodoXTituloXCanceladoXSoloNoLeido(filtroFechaDesde.getValue() , filtroFechaHasta.getValue(), filtroTitulo.getValue(), filtroCancelado.getValue(), filtroLeido.getValue()));
		table.setVisibleColumns((Object[]) new String[] {LINK_VER, COLUMNA_LEIDO ,COLUMNA_CANCELADA ,"fechaAlta","responsableAlta", "id" });
		table.setColumnHeaders("Titulo", "Leido", "Cancelada","Fecha de Alta", "Responsable", "Id");
		table.setColumnWidth(LINK_VER, 350);
	}

	public static Window CreaWindowABM() {
		Window windowABM = new Window(EtiquetasLabel.getNotificaciones());
		windowABM.setModal(true);
		windowABM.setResizable(false);
		windowABM.setDraggable(false);
		windowABM.setWidth("800px");
		windowABM.setHeight("600px");
		windowABM.center();
		windowABM.setCloseShortcut(KeyCode.ESCAPE, null);
		return windowABM;
	}

}
