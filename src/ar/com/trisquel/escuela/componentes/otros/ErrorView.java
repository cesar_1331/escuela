package ar.com.trisquel.escuela.componentes.otros;

import ar.com.trisquel.escuela.data.reglasdenegocio.ArticuloRN;
import ar.com.trisquel.escuela.data.tablas.Articulo;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class ErrorView extends VerticalLayout implements View {
 
	public static final String VIEW = "ErrorView";
	private Articulo articulo;
	public ErrorView() { 
	}

	public ErrorView(Component... children) {
		super(children);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		onDraw();
	}
	
	public void onDraw()
	{
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label();
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		addComponent(LayoutTitle);
		Label Separator = new Label("<hr>");
		Separator.setContentMode(ContentMode.HTML);
		addComponent(Separator);
		/*
		 * Lee Datos 
		 */
		articulo = ArticuloRN.Read(R.Paginas.ERROR);
		if (articulo != null){
			Titulo.setValue(articulo.getTitulo());
			Label Content = new Label(articulo.getContenido());
			Content.setContentMode(ContentMode.HTML);
			addComponent(Content);
		}
		else{
			Label Content = new Label("<h1>Error page not foud</h1>");
			Content.setContentMode(ContentMode.HTML);
			addComponent(Content);
		}
	}

}
