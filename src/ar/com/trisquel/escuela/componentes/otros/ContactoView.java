package ar.com.trisquel.escuela.componentes.otros;

import java.util.ArrayList;
import java.util.Date;

import ar.com.trisquel.escuela.data.reglasdenegocio.MensajesRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.UsuarioRN;
import ar.com.trisquel.escuela.data.tablas.Mensajes;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.EnvioMail;
import ar.com.trisquel.escuela.utiles.ErrorFieldMessage;
import ar.com.trisquel.escuela.utiles.ImageCaptcha;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.data.validator.EmailValidator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ErrorMessage.ErrorLevel;
import com.vaadin.server.StreamResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class ContactoView extends VerticalLayout implements View ,ClickListener {

	public static final String VIEW = "Contacto";
	private TextField Name;
	private TextField Mail;
	private TextField Subject;
	private TextField Captcha;
	private RichTextArea Message;
	private Button ButtonSend;
	private String StringCapcha;
	private boolean MensajeEnviado = false;
	private UI ui;
	private Sesion sesion;
	private Usuario usuario;
	public ContactoView() {
	}

	public ContactoView(Component... children) {
		super(children);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		sesion = (Sesion)ui.getSession().getAttribute(R.Session.SESSION);
		ControlAcceso control = ControlAcceso.Control(this.getClass() ,ui); 
		if (!control.isAcceso()){
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		}
		else{
			onDraw();
		}
	}

	public void onDraw(){

		removeAllComponents();
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		Label Title = new Label(EtiquetasLabel.getContacto());
		Title.addStyleName(R.Style.TITLE);
		Title.addStyleName(R.Style.PADDING_MINIMO);
		addComponent(Title);
		Label labelSeparator = new Label("<hr>" ,ContentMode.HTML);
		labelSeparator.addStyleName(R.Style.EXPAND_WIDTH_ALL);
		addComponent(labelSeparator);
		Image imagen = new Image();
		imagen.setSource(new ThemeResource("img/contacto.png"));
		imagen.setWidth("100%");
		addComponent(imagen);
		if (MensajeEnviado){
			Label MensajeEnvio = new Label(MensajeLabel.getMensajeEnviado());
			MensajeEnvio.addStyleName(R.Style.LABEL);
			MensajeEnvio.addStyleName(R.Style.LABEL_BOLD);
			MensajeEnvio.addStyleName(R.Style.PADDING_MEDIO);
			addComponent(MensajeEnvio);
			Label vacio = new Label();
			vacio.addStyleName(R.Style.PADDING_MEDIO);
			vacio.setHeight("550px");
			addComponent(vacio);
		}
		else{		
			ThemeResource resourceIcono = new ThemeResource("img/icono.png");
			Image imageIcono;
			GridLayout content = new GridLayout();
			content.setColumns(3);
			content.addComponent(new Label());
			content.addComponent(new Label());
			Label label = new Label(EtiquetasLabel.getTodosLosCamposSonObligatorios());
			label.addStyleName(R.Style.EDITABLE);
			label.addStyleName(R.Style.LABEL_BOLD);
			content.addComponent(label);
			// Nombre
			imageIcono = new Image();
			imageIcono.setSource(resourceIcono);
			content.addComponent(imageIcono);
			Label labelNombre = new Label(EtiquetasLabel.getName());
			labelNombre.addStyleName(R.Style.EDITABLE_LABEL);
			labelNombre.setWidth("100px");
			content.addComponent(labelNombre);
			Name = new TextField();
			Name.setComponentError(null);
			Name.addStyleName(R.Style.LABEL_BOLD);
			Name.addStyleName(R.Style.EDITABLE);
			Name.setWidth("300px");
			Name.setMaxLength(50);
			content.addComponent(Name);
			// Mail
			imageIcono = new Image();
			imageIcono.setSource(resourceIcono);
			content.addComponent(imageIcono);
			Label labelMail = new Label(EtiquetasLabel.getMail());
			labelMail.addStyleName(R.Style.EDITABLE_LABEL);
			content.addComponent(labelMail);
			Mail = new TextField();
			Mail.setComponentError(null);
			Mail.addStyleName(R.Style.LABEL_BOLD);
			Mail.addStyleName(R.Style.EDITABLE);
			Mail.setMaxLength(320);
			Mail.setWidth("350px");
			content.addComponent(Mail);
			// Asunto
			imageIcono = new Image();
			imageIcono.setSource(resourceIcono);
			content.addComponent(imageIcono);
			Label labelAsunto = new Label(EtiquetasLabel.getAsunto());
			labelAsunto.addStyleName(R.Style.EDITABLE_LABEL);
			content.addComponent(labelAsunto);
			Subject = new TextField();
			Subject.setComponentError(null);
			Subject.addStyleName(R.Style.LABEL_BOLD);
			Subject.addStyleName(R.Style.EDITABLE);
			Subject.setMaxLength(50);
			Subject.setWidth("300px");
			content.addComponent(Subject);
			// Imagen Captcha
			ImageCaptcha imageCaptcha = new ImageCaptcha();
			StringCapcha = imageCaptcha.getTextoCaptcha();
			StreamResource.StreamSource imagesource = imageCaptcha;
			StreamResource resource = new StreamResource(imagesource, imageCaptcha.getFileCaptcha());
			content.addComponent(new Label());
			content.addComponent(new Label());
			content.addComponent(new Image(null, resource));
			
			imageIcono = new Image();
			imageIcono.setSource(resourceIcono);
			content.addComponent(imageIcono);
			Label labelCaptcha = new Label(EtiquetasLabel.getIngreseElTextoDeLaImagen());
			labelCaptcha.addStyleName(R.Style.EDITABLE_LABEL);
			content.addComponent(labelCaptcha);
			Captcha = new TextField();
			Captcha.setComponentError(null);
			Captcha.addStyleName(R.Style.LABEL_BOLD);
			Captcha.addStyleName(R.Style.EDITABLE);
			content.addComponent(Captcha);
			// Mensaje
			imageIcono = new Image();
			imageIcono.setSource(resourceIcono);
			content.addComponent(imageIcono);
			Label labelMensaje = new Label(EtiquetasLabel.getMensaje());
			labelMensaje.addStyleName(R.Style.EDITABLE_LABEL);
			content.addComponent(labelMensaje);
			Message = new RichTextArea();
			Message.setComponentError(null);
			Message.addStyleName(R.Style.LABEL_BOLD);
			Message.addStyleName(R.Style.EDITABLE);
			content.addComponent(Message);
			
			
			content.addComponent(new Label());
			content.addComponent(new Label());
			HorizontalLayout Botones = new HorizontalLayout();
			ButtonSend = new Button(EtiquetasLabel.getEnviar());
			ButtonSend.setPrimaryStyleName(R.Style.BUTTON_COLOR);
			ButtonSend.addClickListener(this);
			Botones.addComponent(ButtonSend);
			content.addComponent(Botones);
			
			addComponent(content);
			usuario = UsuarioRN.Read(sesion.getUsuarioId());
			if (usuario != null){
				Name.setValue(usuario.getNombre());
				Mail.setValue(usuario.getMailPrincipal());
			}
		}
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == ButtonSend)
		{
			
			Name.setComponentError(null);
			Mail.setComponentError(null);
			Subject.setComponentError(null);
			Message.setComponentError(null);
			EmailValidator emailValidator = new EmailValidator(MensajeLabel.getMailInvalido());
			if (Name.getValue().trim().length() == 0 )
			{
				Name.setComponentError(new ErrorFieldMessage(MensajeLabel.getDebeIngresarElNombre(), ErrorLevel.ERROR));
				MessageBox.showHTML(Icon.WARN, "", MensajeLabel.getDebeIngresarElNombre(), ButtonId.OK);
				Name.focus();
			}
			else if (Mail.getValue().trim().length() == 0)
			{
				Mail.setComponentError(new ErrorFieldMessage(MensajeLabel.getDebeIngresarElMail(), ErrorLevel.ERROR));
				MessageBox.showHTML(Icon.WARN, "", MensajeLabel.getDebeIngresarElMail(), ButtonId.OK);
				Mail.focus();
			}
			else if (!emailValidator.isValid(Mail.getValue()))
			{
				Mail.setComponentError(new ErrorFieldMessage(emailValidator.getErrorMessage(), ErrorLevel.ERROR));
				MessageBox.showHTML(Icon.WARN, "",emailValidator.getErrorMessage(), ButtonId.OK);
				Mail.focus();
			}
			else if(Subject.getValue().trim().length() == 0)
			{
				Subject.setComponentError(new ErrorFieldMessage(MensajeLabel.getDebeIngresarElAsunto(), ErrorLevel.ERROR));
				MessageBox.showHTML(Icon.WARN, "",MensajeLabel.getDebeIngresarElAsunto() , ButtonId.OK);
				Subject.focus();
			}
			else if(!Captcha.getValue().trim().equals(StringCapcha))
			{
				Captcha.setComponentError(new ErrorFieldMessage(MensajeLabel.getElTextoIngresadoNoEsCorrecto(), ErrorLevel.ERROR));
				MessageBox.showHTML(Icon.WARN, "",MensajeLabel.getElTextoIngresadoNoEsCorrecto(), ButtonId.OK);
				Captcha.focus();
			}
			else if(Message.getValue().trim().length() == 0)
			{
				Message.setComponentError(new ErrorFieldMessage(MensajeLabel.getDebeIngresarElMensaje(), ErrorLevel.ERROR));
				MessageBox.showHTML(Icon.WARN, "",MensajeLabel.getDebeIngresarElMensaje(), ButtonId.OK);
				Message.focus();
			}
			else
			{
				MessageBox.showHTML(Icon.QUESTION,"", MensajeLabel.getConfirmaEnvioMensaje(), new MessageBoxListener(){

					@Override
					public void buttonClicked(ButtonId buttonType) {
						if (buttonType.equals(ButtonId.OK)){
							MensajeEnviado = true;
							Mensajes usuarioMsg = new Mensajes();
							usuarioMsg.setAsunto(Subject.getValue());
							usuarioMsg.setMail(Mail.getValue());
							usuarioMsg.setMensaje(Message.getValue());
							usuarioMsg.setNombre(Name.getValue());
							usuarioMsg.setFechaHora(new Date(System.currentTimeMillis()));
							usuarioMsg.setTipo(Dominios.TipoMensajeEnviado.MENSAJE);
							if (usuario != null)
								usuarioMsg.setUsuarioId(usuario.getId());
							else
								usuarioMsg.setUsuarioId("");
							MensajesRN.Save(usuarioMsg);
							String cuerpo = Name.getValue() +"<br />" + Mail.getValue() + "<br />" + Message.getValue();
							EnvioMail.Enviar("cesarcardozo83@gmail.com", Subject.getValue(), cuerpo, new ArrayList<String>());
							
							onDraw();
						}
					}
				}
				, ButtonId.OK
				, ButtonId.CANCEL);
			}
		}
	}
}
