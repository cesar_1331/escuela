package ar.com.trisquel.escuela.componentes;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.Cookie;

import org.vaadin.hene.popupbutton.PopupButton;

import ar.com.trisquel.escuela.EscuelaUI;
import ar.com.trisquel.escuela.componentes.otros.ArticuloView;
import ar.com.trisquel.escuela.componentes.otros.ContactoView;
import ar.com.trisquel.escuela.componentes.otros.MiMenuView;
import ar.com.trisquel.escuela.componentes.otros.NotificacionView;
import ar.com.trisquel.escuela.componentes.user.LoginView;
import ar.com.trisquel.escuela.data.ImagenData;
import ar.com.trisquel.escuela.data.reglasdenegocio.UsuarioRN;
import ar.com.trisquel.escuela.data.select.NotificacionPersonaSelect;
import ar.com.trisquel.escuela.data.tablas.NotificacionPersona;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.Recursos;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.server.StreamResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.BaseTheme;

@SuppressWarnings("serial")
public class Header extends VerticalLayout implements ClickListener, com.vaadin.event.MouseEvents.ClickListener{

	public Header() {
	}
	private HorizontalLayout Login;
	private Button ButtonLogin;
	private Button ButtonInicioLogo;
	private Button ButtonInicio;
	private Button ButtonAutoridades;
	private Button ButtonOfertaAcademica;
	private Button ButtonContacto;
	private Button ButtonHistoria;
	
	private Button ButtonMiMenu;
	private Button ButtonMiMenuHeader;
	private Button ButtonLogout;
	private PopupButton ButtonUsuario;
	private Image ButtonNotificaciones;
	private Sesion sesion;
	
	public void onDraw()
	{
		removeAllComponents();
		sesion = (Sesion)getUI().getSession().getAttribute(R.Session.SESSION);
		setPrimaryStyleName(R.StyleHeader.LAYOUT);
		
		VerticalLayout headerVertical = new VerticalLayout();
		headerVertical.setWidth("1000px");
		addComponent(headerVertical);
		setComponentAlignment(headerVertical ,Alignment.TOP_CENTER);
		
		HorizontalLayout header = new HorizontalLayout();
		headerVertical.addComponent(header);
		
		//Imagen del Titulo
		ButtonInicioLogo = new Button();;
		ButtonInicioLogo.setPrimaryStyleName(R.Style.LINK);
		ButtonInicioLogo.setIcon(new ThemeResource("img/title.png"));
		ButtonInicioLogo.setWidth("195px");
		ButtonInicioLogo.addClickListener(this);
		ButtonInicioLogo.setDescription("<h3>"+Recursos.Parametros.getAppTituloSistema()+"</h3>");
		header.addComponent(ButtonInicioLogo);
		
		VerticalLayout LayoutVacio = new VerticalLayout();
		LayoutVacio.setWidth("5px");
		header.addComponent(LayoutVacio);
		
		VerticalLayout Layout = new VerticalLayout();
		Layout.setWidth("550px");
		header.addComponent(Layout);
		//Vacio
		LayoutVacio = new VerticalLayout();
		LayoutVacio.setHeight("3px");
		Layout.addComponent(LayoutVacio);
		//Resto de los datos 
		HorizontalLayout LayoutDatos = new HorizontalLayout();
		LayoutDatos.setHeight("25px");
		Layout.addComponent(LayoutDatos);
		//Login
		VerticalLayout layoutLogin = new VerticalLayout();
		layoutLogin.setWidth("250px");
		layoutLogin.setHeight("100%");
		header.addComponent(layoutLogin);
		Login = new HorizontalLayout();
		Login.setHeight("95%");
		layoutLogin.addComponent(Login);
		LayoutDatos.addComponent(new Label(R.CaracteresEsteciales.SPACIO + R.CaracteresEsteciales.SPACIO ,ContentMode.HTML));
		//Menu Bar Layout
		HorizontalLayout layoutBotones = new HorizontalLayout();
		ButtonInicio = new Button();
		ButtonInicio.setPrimaryStyleName(R.Style.LINK);
		ButtonInicio.setIcon(new ThemeResource("img/boton-home.png"));
		ButtonInicio.setDescription("<h3>"+Recursos.Parametros.getAppTituloSistema()+"</h3>");
		ButtonInicio.addClickListener(this);
		
		ButtonMiMenuHeader = new Button(EtiquetasLabel.getMiMenu() ,this);
		ButtonMiMenuHeader.setPrimaryStyleName(R.Style.BUTTON_HEADER);
		ButtonAutoridades = new Button(EtiquetasLabel.getAutoridades() ,this);
		ButtonAutoridades.setPrimaryStyleName(R.Style.BUTTON_HEADER);
		ButtonOfertaAcademica = new Button(EtiquetasLabel.getOfertaAcademica() ,this);
		ButtonOfertaAcademica.setPrimaryStyleName(R.Style.BUTTON_HEADER);
		ButtonContacto = new Button(EtiquetasLabel.getContacto() ,this);
		ButtonContacto.setPrimaryStyleName(R.Style.BUTTON_HEADER);
		ButtonHistoria = new Button(EtiquetasLabel.getHistoria() ,this);
		ButtonHistoria.setPrimaryStyleName(R.Style.BUTTON_HEADER);		
		
		layoutBotones.addComponent(ButtonInicio);
		layoutBotones.addComponent(ButtonMiMenuHeader);
		layoutBotones.addComponent(ButtonAutoridades);
		layoutBotones.addComponent(ButtonOfertaAcademica);
		layoutBotones.addComponent(ButtonHistoria);
		layoutBotones.addComponent(ButtonContacto);
		layoutBotones.setComponentAlignment(ButtonMiMenuHeader ,Alignment.MIDDLE_LEFT);
		layoutBotones.setComponentAlignment(ButtonAutoridades ,Alignment.MIDDLE_LEFT);
		layoutBotones.setComponentAlignment(ButtonOfertaAcademica ,Alignment.MIDDLE_LEFT);
		layoutBotones.setComponentAlignment(ButtonHistoria ,Alignment.MIDDLE_LEFT);
		layoutBotones.setComponentAlignment(ButtonContacto ,Alignment.MIDDLE_LEFT);
		Layout.addComponent(layoutBotones);
		ButtonMiMenuHeader.setVisible(false);
		DrawLogin();
	}
	
	public void DrawLogin()
	{
		Login.removeAllComponents();
		VerticalLayout layoutImage = new VerticalLayout();
		layoutImage.setWidth("30px");
		Image imageLogin = new Image();
		layoutImage.addComponent(imageLogin);
		layoutImage.setComponentAlignment(imageLogin, Alignment.TOP_CENTER);
		imageLogin.setPrimaryStyleName(BaseTheme.BUTTON_LINK);
		imageLogin.addStyleName(R.Style.CURSOR_POINTER);
		imageLogin.addStyleName(R.Style.IMAGEN_SCALE_30);
		imageLogin.addClickListener(this);
		ButtonNotificaciones = new Image();
		ButtonNotificaciones.setWidth("30px");
		ButtonLogin = new Button(EtiquetasLabel.getLogin() ,this);
		ButtonLogin.setPrimaryStyleName(R.StyleHeader.LINK);
		ButtonMiMenu = new Button(EtiquetasLabel.getMiMenu() ,this);
		ButtonMiMenu.setWidth("110px");
		ButtonMiMenu.setPrimaryStyleName(R.Style.LINK_COLOR);
		ButtonMiMenuHeader.setVisible(false);	
		if (sesion.getUsuarioId() == null || sesion.getUsuarioId().isEmpty()){
			Login.addComponent(ButtonNotificaciones);
			Login.setComponentAlignment(ButtonNotificaciones ,Alignment.BOTTOM_LEFT);
			imageLogin.setSource(new ThemeResource("img/user.png"));
			Login.addComponent(layoutImage);
			Login.setComponentAlignment(layoutImage ,Alignment.BOTTOM_LEFT);
			Login.addComponent(ButtonLogin);
			Login.setComponentAlignment(ButtonLogin ,Alignment.BOTTOM_LEFT);
		}
		else {
			Usuario usuario = UsuarioRN.Read(sesion.getUsuarioId());
			if (usuario.getThumbnail() != null){
				ImagenData blob = new ImagenData(usuario.getThumbnail()); 
				imageLogin.setSource(new StreamResource(blob ,usuario.toString()+".png"));
			}
			else{
				imageLogin.setSource(new ThemeResource("img/user.png"));
			}
			Login.addComponent(ButtonNotificaciones);
			Login.addComponent(layoutImage);
			Login.setComponentAlignment(ButtonNotificaciones ,Alignment.BOTTOM_LEFT);
			Login.setComponentAlignment(layoutImage ,Alignment.BOTTOM_LEFT);
			ButtonUsuario = new PopupButton(sesion.getUsuarioId());
			ButtonUsuario.setPrimaryStyleName(R.StyleHeader.LINK);
			VerticalLayout LayoutUsuario = new VerticalLayout();
			Label separator = new Label();
			ButtonUsuario.setContent(LayoutUsuario);
			LayoutUsuario.addComponent(ButtonMiMenu);
			ButtonMiMenuHeader.setVisible(true);
			if (usuario.getTipoUsuario() == Dominios.TipoUsuario.ALUMNO || usuario.getTipoUsuario() == Dominios.TipoUsuario.PADRE){
				ButtonNotificaciones.setPrimaryStyleName(BaseTheme.BUTTON_LINK);
				ButtonNotificaciones.addStyleName(R.Style.CURSOR_POINTER);
				ButtonNotificaciones.addStyleName(R.Style.IMAGEN_SCALE_30);
				ButtonNotificaciones.addClickListener(this);
				ButtonNotificaciones.setSource(new ThemeResource("img/icono-notificacion.png"));
			}
			if (usuario.getTipoUsuario() == Dominios.TipoUsuario.PROFESOR){
				ButtonNotificaciones.setPrimaryStyleName(BaseTheme.BUTTON_LINK);
				ButtonNotificaciones.addStyleName(R.Style.CURSOR_POINTER);
				ButtonNotificaciones.addStyleName(R.Style.IMAGEN_SCALE_30);
				ButtonNotificaciones.addClickListener(this);
				ButtonNotificaciones.setSource(new ThemeResource("img/icono-notificacion.png"));
			}
			separator = new Label();
			separator.addStyleName(R.Style.EXPAND_WIDTH_ALL);
			separator.addStyleName(R.Style.BORDER_MINIMO_ABAJO);
			LayoutUsuario.addComponent(separator);
			ButtonLogout = new Button(EtiquetasLabel.getLogout() ,this);
			ButtonLogout.setWidth("110px");
			ButtonLogout.setPrimaryStyleName(R.Style.LINK_COLOR);
			LayoutUsuario.addComponent(ButtonLogout);
			separator = new Label(R.CaracteresEsteciales.SPACIO ,ContentMode.HTML);
			separator.addStyleName(R.Style.EXPAND_WIDTH_ALL);
			LayoutUsuario.addComponent(separator);
			Login.addComponent(ButtonUsuario);
			Login.setComponentAlignment(ButtonUsuario ,Alignment.BOTTOM_LEFT);
			LeeNotificaciones(usuario); 
		}	
		
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		/**
		 * Botones de la barra de navegacion 
		 */
		if (event.getButton() == ButtonInicio || event.getButton() == ButtonInicioLogo){
			sesion.setNavegarA(R.Paginas.INICIO);
			sesion.setArticuloSeleccionado(R.Paginas.INICIO);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getButton() == ButtonOfertaAcademica){
			sesion.setNavegarA(ArticuloView.VIEW);
			sesion.setArticuloSeleccionado(R.Paginas.OFERTA_ACADEMICA);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getButton() == ButtonAutoridades){
			sesion.setNavegarA(ArticuloView.VIEW);
			sesion.setArticuloSeleccionado(R.Paginas.AUTORIDADES);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getButton() == ButtonHistoria){
			sesion.setNavegarA(ArticuloView.VIEW);
			sesion.setArticuloSeleccionado(R.Paginas.HISTORIA);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getButton() == ButtonContacto){
			sesion.setNavegarA(ContactoView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getButton() == ButtonLogin){
			sesion.setNavegarA(LoginView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		/**
		 * Botones de acciones de usuario
		 */
		else if (event.getButton() == ButtonMiMenu || event.getButton() == ButtonMiMenuHeader) {
			sesion.setNavegarA(MiMenuView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
			ButtonUsuario.setPopupVisible(false);
		}
		else if (event.getButton() == ButtonLogout){
			sesion.setNavegarA(R.Paginas.INICIO);
			sesion.setArticuloSeleccionado(R.Paginas.INICIO);
			sesion.setUsuarioId(null);
			sesion.setUsuarioNombre("");
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
			((EscuelaUI)getUI()).getHeader().DrawLogin();
			Cookie cookie = new Cookie(Dominios.RECORDARME ,"");
	        cookie.setPath(VaadinService.getCurrentRequest().getContextPath());
	        cookie.setMaxAge(1);
	        VaadinService.getCurrentResponse().addCookie(cookie);
	        ButtonUsuario.setPopupVisible(false);
		}
	}
	
	@Override
	public void click(com.vaadin.event.MouseEvents.ClickEvent event) {
		if (event.getSource() == ButtonNotificaciones){
			sesion.setNavegarA(NotificacionView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
			ButtonNotificaciones.setSource(new ThemeResource("img/icono-notificacion.png"));
			ButtonNotificaciones.setDescription("Notificaciones");
			ButtonUsuario.setPopupVisible(false);
		}
		else if (sesion.getUsuarioId() == null || sesion.getUsuarioId().isEmpty()){
			sesion.setNavegarA(LoginView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else{
			ButtonUsuario.setPopupVisible(true);
		}
	}
	
	private void LeeNotificaciones(Usuario usuario){
		GregorianCalendar calendarDesde = new GregorianCalendar();
		calendarDesde.setTime(new Date(System.currentTimeMillis()));
		calendarDesde.add(GregorianCalendar.YEAR, -10);
		GregorianCalendar calendarHasta = new GregorianCalendar();
		calendarHasta.setTime(new Date(System.currentTimeMillis()));
		List<NotificacionPersona> listNotificacionPersona = new ArrayList<NotificacionPersona>(); 
		if (usuario.getTipoUsuario() == Dominios.TipoUsuario.ALUMNO && usuario.getAlumnoId() > 0){
			listNotificacionPersona = NotificacionPersonaSelect.SelectXPersonaXPeriodoXSoloNoLeido(Dominios.TipoPersonaNotificacion.ALUMNO ,usuario.getAlumnoId() ,calendarDesde.getTime() ,calendarHasta.getTime(), true);
		}
		else if (usuario.getTipoUsuario() == Dominios.TipoUsuario.PROFESOR && usuario.getProfesorId() > 0){
			listNotificacionPersona = NotificacionPersonaSelect.SelectXPersonaXPeriodoXSoloNoLeido(Dominios.TipoPersonaNotificacion.PROFESOR,usuario.getProfesorId() ,calendarDesde.getTime() ,calendarHasta.getTime(), true);
		}
		if (listNotificacionPersona.size() > 0){
			ButtonNotificaciones.setSource(new ThemeResource("img/icono-notificacion-pte.png"));
			ButtonNotificaciones.setDescription("Tiene " + String.valueOf(listNotificacionPersona.size()) + " notificaciones pendienes de leer");
		}
	}
}
