package ar.com.trisquel.escuela.componentes.asignatura;

import java.util.GregorianCalendar;

import ar.com.trisquel.escuela.data.contenedor.CursoContenedor;
import ar.com.trisquel.escuela.data.contenedor.MateriaContenedor;
import ar.com.trisquel.escuela.data.contenedor.ProfesorContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.AsignaturaRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.CursoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.MateriaRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ProfesorRN;
import ar.com.trisquel.escuela.data.tablas.Asignatura;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.Materia;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class AsignaturaABM extends VerticalLayout implements ClickListener {

	private TextField id;
	private InlineDateField anio;
	private ComboBox profesor;
	private ComboBox materia;
	private ComboBox curso;
	private TextArea observaciones;
	private ButtonIcon ButtonGuardar;
	private ButtonIcon ButtonDescartar;
	private Label asignaturaError;
	private UI ui;
	private String modo;
	private Window window;
	private Asignatura asignatura;

	public AsignaturaABM(Asignatura asignatura, String modo, UI ui, Window window) {
		this.ui = ui;
		this.modo = modo;
		this.window = window;
		this.asignatura = asignatura;
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setWidth("100%");
		layout.setColumns(3);
		addComponent(layout);
		//Id
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Id:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("70px");
		layout.addComponent(label);
		HorizontalLayout layoutId = new HorizontalLayout();
		layoutId.setWidth("600px");
		id = new TextField();
		id.addStyleName(R.Style.EDITABLE);
		id.addStyleName(R.Style.LABEL_BOLD);
		layoutId.addComponent(id);
		//Botones
		HorizontalLayout layoutBotones = new HorizontalLayout();
		ButtonGuardar = new ButtonIcon(ButtonIcon.GUARDAR, true, this);
		ButtonGuardar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonGuardar);
		ButtonDescartar = new ButtonIcon(ButtonIcon.DESCARTAR, true, this);
		ButtonDescartar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonDescartar);
		layoutId.addComponent(layoutBotones);
		layoutId.setComponentAlignment(layoutBotones, Alignment.TOP_RIGHT);
		layout.addComponent(layoutId);
		//A�o
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("A�o:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		VerticalLayout layoutAnio = new VerticalLayout();
		layout.addComponent(layoutAnio);
		anio = new InlineDateField();
		anio.addStyleName(R.Style.EDITABLE);
		anio.addStyleName(R.Style.LABEL_BOLD);
		anio.setResolution(Resolution.YEAR);
		layoutAnio.addComponent(anio);
		Label anioLabel = new Label();
		anioLabel.addStyleName(R.Style.EDITABLE);
		anioLabel.addStyleName(R.Style.LABEL_BOLD);
		layoutAnio.addComponent(anioLabel);
		//Materia
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Materia:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		materia = new ComboBox();
		materia.addStyleName(R.Style.EDITABLE);
		materia.addStyleName(R.Style.LABEL_BOLD);
		materia.setContainerDataSource(MateriaContenedor.LeeContainerXNombre("" ,true));
		materia.setWidth("300px");
		materia.setInputPrompt(EtiquetasLabel.getSeleccioneMateria());
		materia.setImmediate(true);
		materia.setNullSelectionAllowed(false);
		materia.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				Materia materiaSeleccionada = (Materia)materia.getValue();
				if (materiaSeleccionada != null){
					profesor.setContainerDataSource(ProfesorContenedor.LeeContainerXMateria(materiaSeleccionada.getId()));
				}
				else{
					profesor.removeAllItems();
				}
			}
		});
		layout.addComponent(materia);
		//Profesor
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Profesor:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		profesor = new ComboBox();
		profesor.addStyleName(R.Style.EDITABLE);
		profesor.addStyleName(R.Style.LABEL_BOLD);
		profesor.setWidth("300px");
		profesor.setNullSelectionAllowed(false);
		profesor.setImmediate(true);
		profesor.setInputPrompt(EtiquetasLabel.getSeleccioneProfesor());
		layout.addComponent(profesor);				
		//Grado
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Curso:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		curso = new ComboBox();
		curso.addStyleName(R.Style.EDITABLE);
		curso.addStyleName(R.Style.LABEL_BOLD);
		curso.setWidth("60px");
		curso.setNullSelectionAllowed(false);
		curso.setContainerDataSource(CursoContenedor.LeeContainerXGradoXTurno(0, "", true));
		layout.addComponent(curso);				
		//Observaciones
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Observaciones:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		VerticalLayout layoutObservaciones = new VerticalLayout();
		Label observacionesLabel = new Label();
		observacionesLabel.addStyleName(R.Style.EDITABLE);
		observacionesLabel.addStyleName(R.Style.LABEL_BOLD);
		observacionesLabel.setWidth("500px");
		layoutObservaciones.addComponent(observacionesLabel);
		observaciones = new TextArea();
		observaciones.addStyleName(R.Style.EDITABLE);
		observaciones.addStyleName(R.Style.LABEL_BOLD);
		observaciones.setWidth("500px");
		observaciones.setHeight("50px");
		observaciones.setMaxLength(1024);
		layoutObservaciones.addComponent(observaciones);
		layout.addComponent(layoutObservaciones);		
		layout.addComponent(new Label());
		layout.addComponent(new Label());
		asignaturaError = new Label();
		asignaturaError.addStyleName(R.Style.ERROR_VIEW);
		layout.addComponent(asignaturaError);
		if (modo.equals(Dominios.AccesoADatos.INSERT)){
			anio.focus();
			anioLabel.setVisible(false);
			id.setReadOnly(true);
			observacionesLabel.setVisible(false);
			ButtonGuardar.setVisible(true);
			ButtonDescartar.setVisible(true);
		}
		else{
			GregorianCalendar calendar = new GregorianCalendar(asignatura.getAnio(), 1, 1);
			id.setValue(String.valueOf(asignatura.getId()));
			anio.setValue(calendar.getTime());
			anioLabel.setValue(String.valueOf(asignatura.getAnio()));
			materia.setValue(MateriaRN.Read(asignatura.getMateriaId()));
			profesor.setValue(ProfesorRN.Read(asignatura.getProfesorId()));
			curso.setValue(CursoRN.Read(asignatura.getCursoId()));
			observaciones.setValue(asignatura.getObservaciones());
			observacionesLabel.setValue(asignatura.getObservaciones());
			id.setReadOnly(true);
			if (modo.equals(Dominios.AccesoADatos.SELECT)) {
				anio.setVisible(false);
				profesor.setReadOnly(true);
				materia.setReadOnly(true);
				curso.setReadOnly(true);
				observaciones.setReadOnly(true);
				observaciones.setVisible(false);
				ButtonGuardar.setVisible(false);
				ButtonDescartar.setVisible(false);
			} else if (modo.equals(Dominios.AccesoADatos.UPDATE)) {
				anio.setVisible(false);
				materia.setReadOnly(true);
				curso.setReadOnly(true);
				observacionesLabel.setVisible(false);
				ButtonGuardar.setVisible(true);
				ButtonDescartar.setVisible(true);
			}
			profesor.focus();
		}
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == ButtonDescartar) {
			ui.removeWindow(window);
		} else if (event.getButton() == ButtonGuardar) {
			if (asignatura == null ){
				asignatura = AsignaturaRN.Inizializate();
			}
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(anio.getValue());
			long materiaId = 0;
			long profesorId = 0;
			long cursoId = 0;
			try{
				materiaId = ((Materia)materia.getValue()).getId();
				profesorId = ((Profesor)profesor.getValue()).getId();
				cursoId = ((Curso)curso.getValue()).getId();
			}catch (Exception e){	
			}
			asignatura.setAnio(calendar.get(GregorianCalendar.YEAR));
			asignatura.setMateriaId(materiaId);
			asignatura.setProfesorId(profesorId);
			asignatura.setCursoId(cursoId);
			asignatura.setObservaciones(observaciones.getValue());
			RespuestaEntidad respuesta = AsignaturaRN.Validate(asignatura ,modo);
			if (respuesta.isError()) {
				MessageBox.showHTML(Icon.ERROR, "", respuesta.getMsgError(),ButtonId.OK);
			} else {
				MessageBox.showHTML(Icon.QUESTION, "",MensajeLabel.getConfirmaAccion(),new MessageBoxListener() {
					@Override
					public void buttonClicked(ButtonId buttonType) {
						if (buttonType == ButtonId.SAVE) {
							RespuestaEntidad respuesta = AsignaturaRN.Save(asignatura);
							if (respuesta.isError()) {
								MessageBox.showHTML(Icon.ERROR, "",respuesta.getMsgError(),ButtonId.OK);
							} else {
								ui.removeWindow(window);
							}
						}
					}
				}, ButtonId.SAVE, ButtonId.CANCEL);
			}
		}
	}

}
