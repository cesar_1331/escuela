package ar.com.trisquel.escuela.componentes.asignatura;

import ar.com.trisquel.escuela.data.reglasdenegocio.CursoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ParametroRN;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class CursoABM extends VerticalLayout implements ClickListener {

	private TextField id;
	private ComboBox grado;
	private TextField division;
	private ComboBox turno;
	private ComboBox cantidadPeriodos;
	private CheckBox activo;
	private ButtonIcon ButtonGuardar;
	private ButtonIcon ButtonDescartar;
	private UI ui;
	private String modo;
	private Window window;
	private Curso curso;

	public CursoABM(Curso curso, String modo, UI ui, Window window) {
		this.ui = ui;
		this.modo = modo;
		this.window = window;
		this.curso = curso;
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setWidth("100%");
		layout.setColumns(3);
		addComponent(layout);
		//Id
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Id:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("70px");
		layout.addComponent(label);
		HorizontalLayout layoutId = new HorizontalLayout();
		layoutId.setWidth("400px");
		id = new TextField();
		id.addStyleName(R.Style.EDITABLE);
		id.addStyleName(R.Style.LABEL_BOLD);
		layoutId.addComponent(id);
		//Botones
		HorizontalLayout layoutBotones = new HorizontalLayout();
		ButtonGuardar = new ButtonIcon(ButtonIcon.GUARDAR, true, this);
		ButtonGuardar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonGuardar);
		ButtonDescartar = new ButtonIcon(ButtonIcon.DESCARTAR, true, this);
		ButtonDescartar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonDescartar);
		layoutId.addComponent(layoutBotones);
		layoutId.setComponentAlignment(layoutBotones, Alignment.TOP_RIGHT);
		layout.addComponent(layoutId);
		//Grado
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Grado:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		grado = Dominios.Grado.CreaComboBox();
		grado.addStyleName(R.Style.EDITABLE);
		grado.addStyleName(R.Style.LABEL_BOLD);
		grado.setValue(1);
		grado.setWidth("60px");
		grado.setNullSelectionAllowed(false);
		layout.addComponent(grado);		
		//Division
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Divisi�n:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		division = new TextField();
		division.addStyleName(R.Style.EDITABLE);
		division.addStyleName(R.Style.LABEL_BOLD);
		division.addStyleName(R.Style.TEXTO_MAYUSCULA);
		division.setMaxLength(2);
		division.setWidth("40px");
		layout.addComponent(division);		
		//Division
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Turno:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		turno = Dominios.Turno.CreaComboBox(false);
		turno.addStyleName(R.Style.EDITABLE);
		turno.addStyleName(R.Style.LABEL_BOLD);
		turno.setNullSelectionAllowed(false);
		turno.setValue(Dominios.Turno.MANIANA);
		turno.setWidth("100px");
		layout.addComponent(turno);
		//Cantidad de Periodos
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Cantidad de Periodos:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		cantidadPeriodos = Dominios.Periodos.CreaComboBox(false);
		cantidadPeriodos.setNullSelectionAllowed(false);
		cantidadPeriodos.setValue(ParametroRN.LeeEscuelaCantidadPeriodos());
		cantidadPeriodos.setWidth("100px");
		layout.addComponent(cantidadPeriodos);
		//Activo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Activo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		activo = new CheckBox();
		layout.addComponent(activo);		
		
		if (modo.equals(Dominios.AccesoADatos.INSERT)){
			id.setReadOnly(true);
			activo.setValue(true);
			activo.setReadOnly(true);
			ButtonGuardar.setVisible(true);
			ButtonDescartar.setVisible(true);
			grado.focus();
		}
		else{
			id.setValue(String.valueOf(curso.getId()));
			grado.setValue(curso.getGrado());
			division.setValue(curso.getDivision());
			turno.setValue(curso.getTurno());
			activo.setValue(curso.isActivo());
			cantidadPeriodos.setValue(curso.getCantidadPeriodos());
			id.setReadOnly(true);
			if (modo.equals(Dominios.AccesoADatos.SELECT)) {
				grado.setReadOnly(true);
				division.setReadOnly(true);
				turno.setReadOnly(true);
				activo.setReadOnly(true);
				ButtonGuardar.setVisible(false);
				ButtonDescartar.setVisible(false);
			} else if (modo.equals(Dominios.AccesoADatos.UPDATE)) {
				grado.setReadOnly(true);
				division.setReadOnly(true);
				ButtonGuardar.setVisible(true);
				ButtonDescartar.setVisible(true);
			}
			turno.focus();
		}
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == ButtonDescartar) {
			ui.removeWindow(window);
		} else if (event.getButton() == ButtonGuardar) {
			if (curso == null ){
				curso = CursoRN.Inizializate();
			}
			int gradoId = 0;
			int cantidadPeriodo = 0;
			try{
				gradoId = Integer.parseInt(String.valueOf(grado.getValue()));
				cantidadPeriodo = Integer.parseInt(String.valueOf(cantidadPeriodos.getValue()));
			}catch (Exception e){	
			}
			curso.setGrado(gradoId);
			curso.setDivision(division.getValue());
			curso.setTurno(String.valueOf(turno.getValue()));
			curso.setCantidadPeriodos(cantidadPeriodo);
			curso.setActivo(activo.getValue());
			RespuestaEntidad respuesta = CursoRN.Validate(curso ,modo);
			if (respuesta.isError()) {
				MessageBox.showHTML(Icon.ERROR, "", respuesta.getMsgError(),ButtonId.OK);
			} else {
				MessageBox.showHTML(Icon.QUESTION, "",MensajeLabel.getConfirmaAccion(),new MessageBoxListener() {
					@Override
					public void buttonClicked(ButtonId buttonType) {
						if (buttonType == ButtonId.SAVE) {
							RespuestaEntidad respuesta = CursoRN.Save(curso);
							if (respuesta.isError()) {
								MessageBox.showHTML(Icon.ERROR, "",respuesta.getMsgError(),ButtonId.OK);
							} else {
								ui.removeWindow(window);
							}
						}
					}
				}, ButtonId.SAVE, ButtonId.CANCEL);
			}
		}
	}

}
