package ar.com.trisquel.escuela.componentes.asignatura;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import ar.com.trisquel.escuela.data.reglasdenegocio.AsignaturaAlumnoRN;
import ar.com.trisquel.escuela.data.select.AlumnoSelect;
import ar.com.trisquel.escuela.data.select.AsignaturaSelect;
import ar.com.trisquel.escuela.data.select.CursoSelect;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.Asignatura;
import ar.com.trisquel.escuela.data.tablas.AsignaturaAlumno;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.identificadores.AsignaturaAlumnoId;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class CursoAsociaAlumnoAsignatura extends VerticalLayout implements ClickListener {
	private ButtonIcon botonSi;
	private ButtonIcon botonSiIcono;
	private ButtonIcon botonNo;
	private ButtonIcon botonNoIcono;
	private InlineDateField anio;
	private UI ui;
	private Window window;
	private Curso curso;
	
	public CursoAsociaAlumnoAsignatura(UI ui ,Window window ,Curso curso ){
		this.ui = ui; 
		this.window = window;
		this.curso = curso;
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setMargin(true);
		mainLayout.setSpacing(true);
		Label message = new Label("" ,ContentMode.HTML);
		if (curso != null)
			message.setValue("Se van a asociar los alumnos a todas las asignaturas del curso seleccionado<br />Debe seleccionar el a�o");
		else
			message.setValue("Se van a asociar los alumnos a todas las asignaturas de todos los cursos<br />Debe seleccionar el a�o");
		message.addStyleName(R.Style.LABEL_BOLD);
		message.setWidth("380px");
		mainLayout.addComponent(message);
		mainLayout.setComponentAlignment(message, Alignment.MIDDLE_CENTER);
		
		anio = new InlineDateField();
		anio.setResolution(Resolution.YEAR);
		anio.setValue(new Date(System.currentTimeMillis()));
		mainLayout.addComponent(anio);
		mainLayout.setComponentAlignment(anio, Alignment.MIDDLE_CENTER);
		// Add Buttons
		HorizontalLayout buttonLayout = new HorizontalLayout();
		mainLayout.addComponent(buttonLayout);
		mainLayout.setComponentAlignment(buttonLayout, Alignment.MIDDLE_CENTER);
		botonSiIcono = new ButtonIcon(ButtonIcon.CONFIRMAR, true, this);
		buttonLayout.addComponent(botonSiIcono);
		botonSi = new ButtonIcon(ButtonIcon.CONFIRMAR, false, this);
		buttonLayout.addComponent(botonSi);
		buttonLayout.addComponent(new Label(R.CaracteresEsteciales.SPACIO+R.CaracteresEsteciales.SPACIO+R.CaracteresEsteciales.SPACIO ,ContentMode.HTML));
		botonNoIcono = new ButtonIcon(ButtonIcon.CANCELAR, true, this);
		buttonLayout.addComponent(botonNoIcono);
		botonNo = new ButtonIcon(ButtonIcon.CANCELAR, false, this);
		buttonLayout.addComponent(botonNo);
		addComponent(mainLayout);
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonSi || event.getButton() == botonSiIcono){
			MessageBox.showHTML(Icon.QUESTION, "", MensajeLabel.getConfirmaAccion(), new MessageBoxListener(){
				@Override
				public void buttonClicked(ButtonId buttonType) {
					if (buttonType.equals(ButtonId.YES)){
						GregorianCalendar calendar = new GregorianCalendar();
						calendar.setTime(anio.getValue());
						if (curso != null){
							List<Alumno> listAlumno = AlumnoSelect.SelectXCursoId(curso.getId());
							List<Asignatura> listAsignatura = AsignaturaSelect.LeeAsignaturaXAnioXCurso(calendar.get(GregorianCalendar.YEAR), curso.getId());
							int orden = 1;
							for (Alumno alumno : listAlumno){
								for (Asignatura asignatura : listAsignatura){
									AsignaturaAlumno asignaturaAlumno = AsignaturaAlumnoRN.Inizializate();
									asignaturaAlumno.setId(new AsignaturaAlumnoId(asignatura.getId() ,alumno.getId() ));
									asignaturaAlumno.setOrden(orden);
									AsignaturaAlumnoRN.Add(asignaturaAlumno);
								}
								orden++;
							}
							ui.removeWindow(window);
							Window windowAlumnos = CursoWWView.CreaWindowABM();
							windowAlumnos.setWidth("955px");
							windowAlumnos.setHeight("600px");
							CursoAlumnos cursoAlumnos = new CursoAlumnos(curso ,ui);
							windowAlumnos.setContent(cursoAlumnos);
							ui.addWindow(windowAlumnos);
						}
						else{
							List<Curso> listCurso = CursoSelect.SelectXGradoXTurno(0, "", true);
							for (Curso cursoSeleccionado : listCurso){
								List<Alumno> listAlumno = AlumnoSelect.SelectXCursoId(cursoSeleccionado.getId());
								List<Asignatura> listAsignatura = AsignaturaSelect.LeeAsignaturaXAnioXCurso(calendar.get(GregorianCalendar.YEAR), cursoSeleccionado.getId());
								int orden = 1;
								for (Alumno alumno : listAlumno){
									for (Asignatura asignatura : listAsignatura){
										AsignaturaAlumno asignaturaAlumno = AsignaturaAlumnoRN.Inizializate();
										asignaturaAlumno.setId(new AsignaturaAlumnoId(asignatura.getId() ,alumno.getId() ));
										asignaturaAlumno.setOrden(orden);
										AsignaturaAlumnoRN.Add(asignaturaAlumno);
									}
									orden++;
								}
							}
							ui.removeWindow(window);
							MessageBox.showHTML(Icon.QUESTION, "", "Se asociaron los alumnos a todos los cursos", ButtonId.OK);
						}
					}
				}
			}
			,ButtonId.YES 
			,ButtonId.NO);
		}
		else if (event.getButton() == botonNo || event.getButton() == botonNoIcono){
			ui.removeWindow(window);
		}
	}

}
