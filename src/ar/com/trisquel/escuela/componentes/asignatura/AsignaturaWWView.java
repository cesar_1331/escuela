package ar.com.trisquel.escuela.componentes.asignatura;

import java.util.Date;
import java.util.GregorianCalendar;

import ar.com.trisquel.escuela.data.contenedor.AsignaturaContenedor;
import ar.com.trisquel.escuela.data.contenedor.MateriaContenedor;
import ar.com.trisquel.escuela.data.contenedor.ProfesorContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.CursoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.MateriaRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ProfesorRN;
import ar.com.trisquel.escuela.data.tablas.Asignatura;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.Materia;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class AsignaturaWWView extends VerticalLayout implements ClickListener,View {

	public static final String VIEW = "AsignaturaWW";
	private static final String LINK_VER = "LINK_VER";
	private static final String COLUMNA_ANIO = "COLUMNA_ANIO";
	private static final String COLUMNA_MATERIA = "COLUMNA_MATERIA";
	private static final String COLUMNA_PROFESOR = "COLUMNA_PROFESOR";
	private ButtonIcon botonFiltros;
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonEliminar;
	private ButtonIcon botonRefrescar;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private InlineDateField anio;
	private ComboBox profesor;
	private ComboBox materia;
	private ComboBox filtroCurso;
	private Table table;
	private VerticalLayout layoutFiltros;
	private UI ui;

	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass(), ui);
		if (!control.isAcceso()) {
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		} else {
			onDraw();
		}
	}

	public void onDraw() {
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(EtiquetasLabel.getAsignaturas().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true, this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonFiltros = new ButtonIcon(ButtonIcon.BUSCAR, true, this);
		botonFiltros.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonFiltros);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		//botonAgregar.setEnabled(false);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		//botonModificar.setEnabled(false);
		layoutBotones.addComponent(botonModificar);
		botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true, this);
		botonEliminar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonEliminar);
		//botonEliminar.setEnabled(false);
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true, this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonExcel);
		botonExcel.setEnabled(false);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true, this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonPDF);
		botonPDF.setEnabled(false);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		table = new Table();
		ArmaTabla();
		layout.addComponent(table);
		Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonFiltros) {
			layoutFiltros.setVisible(!layoutFiltros.isVisible());
		} else if (event.getButton() == botonRefrescar) {
			CreaDataSource();
		} else if (event.getButton() == botonModificar) {
			Asignatura asignatura = (Asignatura)table.getValue();
			if (asignatura != null){
				Window windowABM = CreaWindowABM();
				AsignaturaABM asignaturaABM = new AsignaturaABM(asignatura ,Dominios.AccesoADatos.UPDATE ,ui ,windowABM);
				windowABM.setContent(asignaturaABM);
				windowABM.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						CreaDataSource();
					}
				});
				ui.addWindow(windowABM);
			}
		} else if (event.getButton() == botonAgregar){
			Window windowABM = CreaWindowABM();
			AsignaturaABM asignaturaABM = new AsignaturaABM(null ,Dominios.AccesoADatos.INSERT ,ui ,windowABM);
			windowABM.setContent(asignaturaABM);
			windowABM.addCloseListener(new CloseListener() {
				@Override
				public void windowClose(CloseEvent e) {
					CreaDataSource();
				}
			});
			ui.addWindow(windowABM);
		} else if (event.getButton() == botonEliminar
				|| event.getButton() == botonExcel
				|| event.getButton() == botonPDF) {
			/**
			 * No hace nada
			 */
		}

	}

	private void ArmaFiltros() {
		
		layoutFiltros.setVisible(false);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(6);
		layoutFiltros.addComponent(formFiltro);
		//A�o
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label label = new Label("A�o:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("60px");
		formFiltro.addComponent(label);
		anio = new InlineDateField();
		anio.addStyleName(R.Style.EDITABLE);
		anio.addStyleName(R.Style.LABEL_BOLD);
		anio.setResolution(Resolution.YEAR);
		anio.setValue(new Date(System.currentTimeMillis()));
		anio.setWidth("200px");
		formFiltro.addComponent(anio);
		//Materia
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Materia:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		materia = new ComboBox();
		materia.addStyleName(R.Style.EDITABLE);
		materia.addStyleName(R.Style.LABEL_BOLD);
		materia.setContainerDataSource(MateriaContenedor.LeeContainerXNombre("" ,true));
		materia.setWidth("300px");
		materia.setInputPrompt(EtiquetasLabel.getTodos());
		formFiltro.addComponent(materia);
		//Grado
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Grado:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		HorizontalLayout layoutGrado = new HorizontalLayout();
		layoutGrado.setWidth("250px");
		formFiltro.addComponent(layoutGrado);
		filtroCurso = new ComboBox();
		filtroCurso.addStyleName(R.Style.EDITABLE);
		filtroCurso.addStyleName(R.Style.LABEL_BOLD);
		filtroCurso.setWidth("100px");
		filtroCurso.setInputPrompt(EtiquetasLabel.getTodos());
		layoutGrado.addComponent(filtroCurso);
		//Profesor
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Profesor:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		profesor = new ComboBox();
		profesor.addStyleName(R.Style.EDITABLE);
		profesor.addStyleName(R.Style.LABEL_BOLD);
		profesor.setWidth("300px");
		profesor.setContainerDataSource(ProfesorContenedor.LeeContainerXNombreApellidoXActivo("",false , true));
		profesor.setInputPrompt(EtiquetasLabel.getTodos());
		formFiltro.addComponent(profesor);				
		
	}

	private void ArmaTabla() {
		table.setHeight("550px");
		table.setWidth("985px");
		table.setSelectable(true);
		table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Asignatura asignatura = (Asignatura)itemId;
				Button botonVer = new Button(CursoRN.Read(asignatura.getCursoId()).toString());
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = CreaWindowABM();
						AsignaturaABM asignaturaABM = new AsignaturaABM(asignatura ,Dominios.AccesoADatos.SELECT ,ui ,windowABM);
						windowABM.setContent(asignaturaABM);
						ui.addWindow(windowABM);
					}
				});
				return botonVer;
			}
		});
		table.addGeneratedColumn(COLUMNA_PROFESOR, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Asignatura asignatura = (Asignatura)itemId;
				return new Label(ProfesorRN.Read(asignatura.getProfesorId()).toString());
			}
		});
		table.addGeneratedColumn(COLUMNA_MATERIA, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Asignatura asignatura = (Asignatura)itemId;
				return new Label(MateriaRN.Read(asignatura.getMateriaId()).toString());
			}
		});
		table.addGeneratedColumn(COLUMNA_ANIO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Asignatura asignatura = (Asignatura)itemId;
				return new Label(String.valueOf(asignatura.getAnio()));
			}
		});
		CreaDataSource();
	}

	private void CreaDataSource() {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(anio.getValue());
		long materiaId = 0;
		long profesorId = 0;
		long cursoId = 0;
		try{
			if (materia.getValue() != null)
				materiaId = ((Materia)materia.getValue()).getId();
			if (profesor.getValue() != null)
				profesorId = ((Profesor)profesor.getValue()).getId();
			if (filtroCurso != null)
				cursoId = ((Curso)filtroCurso.getValue()).getId();
		}catch (Exception e){	
			System.out.print(e.toString());
		}
		table.setContainerDataSource(AsignaturaContenedor.LeeContainerXAnioXGradoXDivisionXProfesorXMateria(calendar.get(GregorianCalendar.YEAR) ,cursoId ,profesorId , materiaId));
		table.setVisibleColumns((Object[]) new String[] {COLUMNA_ANIO,LINK_VER ,COLUMNA_PROFESOR ,COLUMNA_MATERIA ,"id"});
		table.setColumnHeaders("A�o" ,"Curso" ,"Profesor" ,"Materia" ,"Id");
	}
	
	public Window CreaWindowABM(){
		Window windowABM = new Window(EtiquetasLabel.getAsignaturas());
		windowABM.setModal(true);
		windowABM.setResizable(false);
		windowABM.setDraggable(false);
		windowABM.setWidth("750px");
		windowABM.setHeight("350px");
		windowABM.center();
		windowABM.setCloseShortcut(KeyCode.ESCAPE, null);
		return windowABM; 
	}
}
