package ar.com.trisquel.escuela.componentes.asignatura;

import org.vaadin.hene.popupbutton.PopupButton;

import ar.com.trisquel.escuela.data.contenedor.CursoContenedor;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class CursoWWView extends VerticalLayout implements ClickListener, View {

	public static final String VIEW = "CursoWW";
	private static final String LINK_VER = "LINK_VER";
	private static final String COLUMNA_ACTIVO = "COLUMNA_ACTIVO";
	private static final String COLUMNA_TURNO = "COLUMNA_TURNO";
	private static final String COLUMNA_ACCIONES = "COLUMNA_ACCIONES";
	private ButtonIcon botonFiltros;
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonEliminar;
	private ButtonIcon botonRefrescar;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private ButtonIcon botonCopiar;
	private ButtonIcon botonAsociar;
	private ComboBox filtroGrado;
	private ComboBox filtroTurno;
	private CheckBox filtroActivo;
	private Table table;
	private VerticalLayout layoutFiltros;
	private UI ui;

	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass(), ui);
		if (!control.isAcceso()) {
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		} else {
			onDraw();
		}
	}

	public void onDraw() {
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(EtiquetasLabel.getCursos().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true, this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonFiltros = new ButtonIcon(ButtonIcon.BUSCAR, true, this);
		botonFiltros.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonFiltros);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		//botonAgregar.setEnabled(false);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		//botonModificar.setEnabled(false);
		layoutBotones.addComponent(botonModificar);
		botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true, this);
		botonEliminar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonEliminar);
		botonEliminar.setEnabled(false);
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true, this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonExcel);
		botonExcel.setEnabled(false);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true, this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonPDF);
		botonPDF.setEnabled(false);
		botonCopiar = new ButtonIcon(ButtonIcon.COPIAR, true, this);
		botonCopiar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonCopiar);
		botonAsociar = new ButtonIcon(ButtonIcon.ASOCIAR, true, this);
		botonAsociar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAsociar);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		table = new Table();
		ArmaTabla();
		layout.addComponent(table);
		Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonFiltros) {
			layoutFiltros.setVisible(!layoutFiltros.isVisible());
		} else if (event.getButton() == botonRefrescar) {
			CreaDataSource();
		} else if (event.getButton() == botonModificar) {
			Curso curso = (Curso)table.getValue();
			if (curso != null){
				Window windowABM = CreaWindowABM();
				CursoABM cursoABM = new CursoABM(curso ,Dominios.AccesoADatos.UPDATE ,ui ,windowABM);
				windowABM.setContent(cursoABM);
				windowABM.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						CreaDataSource();
					}
				});
				ui.addWindow(windowABM);
			}
		} else if (event.getButton() == botonAgregar) {
			Window windowABM = CreaWindowABM();
			CursoABM cursoABM = new CursoABM(null ,Dominios.AccesoADatos.INSERT ,ui ,windowABM);
			windowABM.setContent(cursoABM);
			windowABM.addCloseListener(new CloseListener() {
				@Override
				public void windowClose(CloseEvent e) {
					CreaDataSource();
				}
			});
			ui.addWindow(windowABM);
		} else if (event.getButton() == botonCopiar) {
			Window window = CreaWindowABM();
			window.setWidth("400px");
			window.setHeight("250px");
			CursoCopiarAsignaturas cursoCopiarAsignaturas = new CursoCopiarAsignaturas(ui, window);
			window.setContent(cursoCopiarAsignaturas);
			ui.addWindow(window);
		} else if (event.getButton() == botonAsociar) {
			Window window = CreaWindowABM();
			window.setWidth("400px");
			window.setHeight("200px");
			CursoAsociaAlumnoAsignatura cursoAsociaAlumnoAsignatura = new CursoAsociaAlumnoAsignatura(ui,window ,null);
			window.setContent(cursoAsociaAlumnoAsignatura);
			ui.addWindow(window);
		} else if (event.getButton() == botonEliminar
				|| event.getButton() == botonExcel
				|| event.getButton() == botonPDF) {
			/**
			 * No hace nada
			 */
		}

	}

	private void ArmaFiltros() {
		layoutFiltros.setVisible(false);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(9);
		layoutFiltros.addComponent(formFiltro);
		//Grado
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label label = new Label("Grado:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		HorizontalLayout layoutGradoDivision = new HorizontalLayout();
		formFiltro.addComponent(layoutGradoDivision);
		HorizontalLayout layoutGrado = new HorizontalLayout();
		layoutGrado.setWidth("100px");
		filtroGrado = Dominios.Grado.CreaComboBox();
		filtroGrado.setWidth("80px");
		filtroGrado.setInputPrompt(EtiquetasLabel.getTodos());
		layoutGrado.addComponent(filtroGrado);
		layoutGradoDivision.addComponent(layoutGrado);
		//Turno
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Turno:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("40px");
		formFiltro.addComponent(label);
		filtroTurno = Dominios.Turno.CreaComboBox(true);
		filtroTurno.addStyleName(R.Style.EDITABLE);
		filtroTurno.addStyleName(R.Style.LABEL_BOLD);
		filtroTurno.setWidth("100px");
		filtroTurno.setInputPrompt(EtiquetasLabel.getTodos());
		formFiltro.addComponent(filtroTurno);
		//Turno
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Activo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("40px");
		formFiltro.addComponent(label);
		filtroActivo = new CheckBox();
		filtroActivo.addStyleName(R.Style.EDITABLE);
		filtroActivo.addStyleName(R.Style.LABEL_BOLD);
		filtroActivo.setValue(true);
		formFiltro.addComponent(filtroActivo);
		layoutFiltros.addComponent(formFiltro);
		
	}

	private void ArmaTabla() {
		table.setHeight("550px");
		table.setWidth("985px");
		table.setSelectable(true);
		table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Curso curso = (Curso)itemId;
				Button botonVer = new Button(curso.toString());
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = CreaWindowABM();
						CursoABM cursoABM = new CursoABM(curso ,Dominios.AccesoADatos.SELECT ,ui ,windowABM);
						windowABM.setContent(cursoABM);
						ui.addWindow(windowABM);
					}
				});
				return botonVer;
			}

		});
		table.addGeneratedColumn(COLUMNA_TURNO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Curso curso = (Curso)itemId;
				ComboBox turno = Dominios.Turno.CreaComboBox(false);
				turno.setValue(curso.getTurno());
				turno.setReadOnly(true);
				return turno;
			}

		});
		table.addGeneratedColumn(COLUMNA_ACTIVO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Curso curso = (Curso)itemId;
				CheckBox activo = new CheckBox();
				activo.setValue(curso.isActivo());
				activo.setReadOnly(true);
				return activo;
			}

		});
		table.addGeneratedColumn(COLUMNA_ACCIONES, new Table.ColumnGenerator() {
        	public Component generateCell(Table source, Object itemId, Object columnId) {
        		final Curso curso = (Curso)itemId;
        		final PopupButton botonAcciones = new PopupButton(EtiquetasLabel.getAccion());
        		botonAcciones.setPrimaryStyleName(R.Style.LINK_COLOR);
        		VerticalLayout layoutAcciones = new VerticalLayout();
        		botonAcciones.setContent(layoutAcciones);
        		Button botonAsignaturas = new Button(EtiquetasLabel.getAsignaturas() ,new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window window = CreaWindowABM();
						window.setCaption(EtiquetasLabel.getAsignaturas());
						window.setWidth("700px");
						window.setHeight("500px");
						CursoAsignaturas cursoAsignaturas = new CursoAsignaturas(curso ,ui);
						window.setContent(cursoAsignaturas);
						ui.addWindow(window);
						botonAcciones.setPopupVisible(false);
					}
				});
        		botonAsignaturas.setPrimaryStyleName(R.Style.LINK_COLOR);
        		layoutAcciones.addComponent(botonAsignaturas);
        		Button botonAlumnos = new Button(EtiquetasLabel.getAlumnos() ,new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window window = CreaWindowABM();
						window.setCaption(EtiquetasLabel.getAlumnos());
						window.setWidth("955px");
						window.setHeight("600px");
						CursoAlumnos cursoAlumnos = new CursoAlumnos(curso ,ui);
						window.setContent(cursoAlumnos);
						ui.addWindow(window);
						botonAcciones.setPopupVisible(false);
					}
				});
        		botonAlumnos.setPrimaryStyleName(R.Style.LINK_COLOR);
        		layoutAcciones.addComponent(botonAlumnos);
        		Button botonAlumnoEstado = new Button(EtiquetasLabel.getCambiarEstadoAlumnos() ,new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window window = CreaWindowABM();
						window.setCaption(EtiquetasLabel.getCambiarEstadoAlumnos());
						window.setWidth("1000px");
						window.setHeight("600px");
						CursoAlumnoEstado cursoAlumnoEstado = new CursoAlumnoEstado(curso ,ui ,window);
						window.setContent(cursoAlumnoEstado);
						ui.addWindow(window);
						botonAcciones.setPopupVisible(false);
					}
				});
        		botonAlumnoEstado.setPrimaryStyleName(R.Style.LINK_COLOR);
        		layoutAcciones.addComponent(botonAlumnoEstado);
        		Button botonAsociar = new Button(EtiquetasLabel.getAsociarAlumnoAsignatura() ,new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window window = CreaWindowABM();
						window.setCaption(EtiquetasLabel.getAsociarAlumnoAsignatura());
						window.setWidth("400px");
						window.setHeight("200px");
						CursoAsociaAlumnoAsignatura cursoAsociaAlumnoAsignatura = new CursoAsociaAlumnoAsignatura(ui,window ,curso);
						window.setContent(cursoAsociaAlumnoAsignatura);
						ui.addWindow(window);
						botonAcciones.setPopupVisible(false);
					}
				});
        		botonAsociar.setPrimaryStyleName(R.Style.LINK_COLOR);
        		layoutAcciones.addComponent(botonAsociar);
        		return botonAcciones;
        	}
		});
		CreaDataSource();
	}

	private void CreaDataSource() {
		int grado = 0;
		String turno = "";
		try{
			if (filtroTurno.getValue() != null && !String.valueOf(filtroTurno.getValue()).equals(Dominios.TODOS_STRING_COMBO))
				turno = String.valueOf(filtroTurno.getValue());
			if (filtroGrado.getValue() != null)
				grado = Integer.parseInt(String.valueOf(filtroGrado.getValue()));
		}catch (Exception e){	
			System.out.print(e.toString());
		}
		table.setContainerDataSource(CursoContenedor.LeeContainerXGradoXTurno(grado, turno, filtroActivo.getValue()));
		table.setVisibleColumns((Object[]) new String[] {COLUMNA_ACCIONES ,LINK_VER ,COLUMNA_TURNO ,COLUMNA_ACTIVO ,"id"});
		table.setColumnHeaders("" ,"Curso" ,"Turno" ,"Activo" ,"Id");
	}
	
	public static Window CreaWindowABM(){
		Window windowABM = new Window(EtiquetasLabel.getCursos());
		windowABM.setModal(true);
		windowABM.setResizable(false);
		windowABM.setDraggable(false);
		windowABM.setWidth("500px");
		windowABM.setHeight("200px");
		windowABM.center();
		windowABM.setCloseShortcut(KeyCode.ESCAPE, null);
		return windowABM; 
	}

}
