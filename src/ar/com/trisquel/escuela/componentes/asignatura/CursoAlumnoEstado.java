package ar.com.trisquel.escuela.componentes.asignatura;

import ar.com.trisquel.escuela.componentes.personas.alumno.AlumnoABM;
import ar.com.trisquel.escuela.componentes.personas.alumno.AlumnoWWView;
import ar.com.trisquel.escuela.data.contenedor.CursoContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.AlumnoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.CursoRN;
import ar.com.trisquel.escuela.data.select.AlumnoSelect;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class CursoAlumnoEstado extends VerticalLayout implements ClickListener {
	private Curso curso;
	private UI ui;
	private Table table;
	private ButtonIcon botonGuardar;
	private Window window;
	public CursoAlumnoEstado(Curso curso ,UI ui,Window window){
		this.curso = curso;
		this.ui = ui;
		this.window = window;
		onDraw();
	}
	
	public void onDraw(){
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		HorizontalLayout layouCursoGrabar = new HorizontalLayout();
		layouCursoGrabar.setWidth("100%");
		addComponent(layouCursoGrabar);
		GridLayout layout = new GridLayout();
		layout.setColumns(3);
		layouCursoGrabar.addComponent(layout);
		//Curso
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Curso:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.addStyleName(R.Style.TEXTO_BIG);
		label.setWidth("60px");
		layout.addComponent(label);
		Label cursoLabel = new Label(curso.toString());
		cursoLabel.addStyleName(R.Style.EDITABLE);
		cursoLabel.addStyleName(R.Style.LABEL_BOLD);
		cursoLabel.addStyleName(R.Style.TEXTO_BIG);
		layout.addComponent(cursoLabel);
		botonGuardar = new ButtonIcon(ButtonIcon.GUARDAR, true ,this);
		botonGuardar.addStyleName(R.Style.MARGIN_DERECHA_LARGO);
		layouCursoGrabar.addComponent(botonGuardar);
		layouCursoGrabar.setComponentAlignment(botonGuardar ,Alignment.TOP_RIGHT);
		/*
		 * Tabla
		 */
		HorizontalLayout layoutTitulo = new HorizontalLayout();
		layoutTitulo.setPrimaryStyleName(R.Style.TEXTO_SUBTITULO_COLOR);
		addComponent(layoutTitulo);
		Label labelTitleServicio = new Label(EtiquetasLabel.getAlumnos());
		labelTitleServicio.setWidth("985px");
		layoutTitulo.addComponent(labelTitleServicio);
		/*
		 * Tabla
		 */
		table = new Table();
		ArmaTabla();
		addComponent(table);
		Label Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}
	
	private void ArmaTabla() {
		table.setWidth("980px");
		table.setHeight("490px");
		table.setSelectable(true);
		CreaDataSource();
	}

	private void CreaDataSource() {
		table.addContainerProperty("Apellido, Nombre",Button.class,  null);
		table.addContainerProperty("DNI"           ,Label.class, null);
		table.addContainerProperty("Estado"        ,ComboBox.class, null);
		table.addContainerProperty("Curso"         ,ComboBox.class, null);
		table.addContainerProperty("Observaciones" ,TextArea.class, null);
		table.setColumnWidth("Apellido, Nombre" ,330);
		table.setColumnWidth("DNI"            ,90);
		table.setColumnWidth("Estado"         ,140);
		table.setColumnWidth("Curso"          ,120);
		table.setColumnWidth("Observaciones"  ,210);
		table.setColumnHeaders("Apellido, Nombre" ,"DNI" ,"Estado" ,"Curso" ,"Observaciones del Estado" );
		
		for (final Alumno alumno : AlumnoSelect.SelectXCursoId(curso.getId())){
			Button botonVer = new Button(alumno.toString());
			botonVer.setPrimaryStyleName(R.Style.LINK);
			botonVer.addStyleName(R.Style.LABEL_BOLD);
			botonVer.setWidth("330px");
			botonVer.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					Window windowABM = AlumnoWWView.CreaWindowABM();
					AlumnoABM alumnoABM = new AlumnoABM(alumno ,Dominios.AccesoADatos.SELECT ,ui ,windowABM);
					windowABM.setContent(alumnoABM);
					ui.addWindow(windowABM);
				}
			});
			Label dni = new Label(String.valueOf(alumno.getDni()));
			dni.setWidth("100px");
			final ComboBox estado = Dominios.EstadoAlumno.CreaComboBox(false);
			estado.setNullSelectionAllowed(false);
			estado.setWidth("135px");
			estado.setImmediate(true);
			final ComboBox comboCurso = new ComboBox();
			comboCurso.setContainerDataSource(CursoContenedor.LeeContainerXGradoXTurno(0, "", true));
			comboCurso.setImmediate(true);
			comboCurso.setWidth("110px");
			estado.addValueChangeListener(new ValueChangeListener() {
				@Override
				public void valueChange(ValueChangeEvent event) {
					if (!String.valueOf(estado.getValue()).equals(Dominios.EstadoAlumno.CURSANDO)){
						comboCurso.setValue(null);
						comboCurso.setReadOnly(true);
					}
					else{
						comboCurso.setReadOnly(false);
						comboCurso.setValue(CursoRN.Read(alumno.getCursoId()));
					}
				}
			});
			TextArea observacionesEstado = new TextArea();
			observacionesEstado.setNullRepresentation("");
			observacionesEstado.setValue(alumno.getEstadoObservaciones());
			observacionesEstado.setMaxLength(2048);
			observacionesEstado.setWidth("200px");
			observacionesEstado.setHeight("55px");
			estado.setValue(alumno.getEstado());
			comboCurso.setValue(CursoRN.Read(alumno.getCursoId()));
			table.addItem(new Object[] {botonVer,dni ,estado ,comboCurso ,observacionesEstado},alumno);
		}
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonGuardar){
			MessageBox.showHTML(Icon.QUESTION, "",MensajeLabel.getConfirmaAccion(),new MessageBoxListener() {
				@Override
				public void buttonClicked(ButtonId buttonType) {
					if (buttonType == ButtonId.SAVE) {
						for (Object object : table.getItemIds()){
							Alumno alumno = (Alumno)object;
							Item item = table.getItem(object);
//							long cursoOld = alumno.getCursoId();
							ComboBox estadoCombo = (ComboBox)(item.getItemProperty("Estado")).getValue();
							ComboBox cursoCombo = (ComboBox)(item.getItemProperty("Curso")).getValue();
							TextArea estadoObservaciones = (TextArea)(item.getItemProperty("Observaciones")).getValue();
							alumno.setEstado(String.valueOf(estadoCombo.getValue()));
							Curso curso = (Curso)cursoCombo.getValue();
							if (curso != null){
								alumno.setCursoId(curso.getId());
							}else{
								alumno.setCursoId(0);
							}
							alumno.setObservaciones(estadoObservaciones.getValue());
							AlumnoRN.Save(alumno);
//							if (cursoOld != alumno.getCursoId()){
//								for (AsignaturaAlumno asignaturaAlumno : AsignaturaAlumnoSelect.LeeAsignaturasXAlumnoXCurso(alumno.getId() , cursoOld, true)){
//									asignaturaAlumno.setCursando(false);
//									AsignaturaAlumnoRN.Save(asignaturaAlumno);
//								}
//							}
						}
						ui.removeWindow(window);
					}
				}
			}, ButtonId.SAVE, ButtonId.CANCEL);
		}
	}
}
