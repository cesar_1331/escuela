package ar.com.trisquel.escuela.componentes.asignatura;

import java.util.Date;
import java.util.GregorianCalendar;

import ar.com.trisquel.escuela.componentes.materias.MateriaABM;
import ar.com.trisquel.escuela.componentes.materias.MateriaWWView;
import ar.com.trisquel.escuela.componentes.personas.alumno.AlumnoABM;
import ar.com.trisquel.escuela.componentes.personas.alumno.AlumnoWWView;
import ar.com.trisquel.escuela.componentes.personas.profesor.ProfesorABM;
import ar.com.trisquel.escuela.componentes.personas.profesor.ProfesorWWView;
import ar.com.trisquel.escuela.data.contenedor.AlumnoContenedor;
import ar.com.trisquel.escuela.data.contenedor.AsignaturaAlumnoContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.AsignaturaAlumnoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.MateriaRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ProfesorRN;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.AsignaturaAlumno;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.Materia;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class CursoAlumnos extends VerticalLayout implements ClickListener {
	private static final String LINK_VER = "LINK_VER";
	private static final String COLUMNA_MATERIA = "COLUMNA_MATERIA";
	private static final String COLUMNA_PROFESOR = "COLUMNA_PROFESOR";
	private static final String COLUMNA_CURSANDO = "COLUMNA_CURSANDO";
	private Curso curso;
	private UI ui;
	private Table table;
	private Table tableMaterias; 
	private InlineDateField filtroAnio;
	public CursoAlumnos(Curso curso ,UI ui){
		this.curso = curso;
		this.ui = ui;
		onDraw();
	}
	
	public void onDraw(){
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setWidth("100%");
		layout.setColumns(3);
		addComponent(layout);
		//Id
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Curso:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.addStyleName(R.Style.TEXTO_BIG);
		label.setWidth("60px");
		layout.addComponent(label);
		Label cursoLabel = new Label(curso.toString());
		cursoLabel.addStyleName(R.Style.EDITABLE);
		cursoLabel.addStyleName(R.Style.LABEL_BOLD);
		cursoLabel.addStyleName(R.Style.TEXTO_BIG);
		cursoLabel.setWidth("830px");
		layout.addComponent(cursoLabel);
		VerticalLayout layoutAsignaturas = new VerticalLayout();
		addComponent(layoutAsignaturas);
		/*
		 * Tabla
		 */
		HorizontalLayout layoutTitulo = new HorizontalLayout();
		layoutTitulo.setPrimaryStyleName(R.Style.TEXTO_SUBTITULO_COLOR);
		layoutTitulo.setHeight("28px");
		layoutAsignaturas.addComponent(layoutTitulo);
		Label labelTitleServicio = new Label(EtiquetasLabel.getAlumnos());
		labelTitleServicio.setWidth("403px");
		layoutTitulo.addComponent(labelTitleServicio);
		Label labelTitleMateria = new Label(EtiquetasLabel.getMaterias());
		labelTitleMateria.setWidth("220px");
		layoutTitulo.addComponent(labelTitleMateria);
		filtroAnio = new InlineDateField();
		filtroAnio.setValue(new Date(System.currentTimeMillis()));
		filtroAnio.setWidth("315px");
		filtroAnio.setResolution(Resolution.YEAR);
		filtroAnio.addStyleName(R.Style.LABEL_COLOR);
		filtroAnio.setImmediate(true);
		filtroAnio.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				Alumno alumno = (Alumno)table.getValue();
				CreaDataSourceMaterias(alumno);
			}
		});
		layoutTitulo.addComponent(filtroAnio);
		/*
		 * Tabla
		 */
		HorizontalLayout layoutTablas = new HorizontalLayout();
		addComponent(layoutTablas);
		table = new Table();
		tableMaterias = new Table();
		ArmaTabla();
		layoutTablas.addComponent(table);
		layoutTablas.addComponent(new Label(R.CaracteresEsteciales.SPACIO+R.CaracteresEsteciales.SPACIO ,ContentMode.HTML));
		layoutTablas.addComponent(tableMaterias);
		Label Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}
	
	private void ArmaTabla() {
		table.setWidth("400px");
		table.setHeight("480px");
		table.setSelectable(true);
		table.setImmediate(true);
		table.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				Alumno alumno = (Alumno)table.getValue();
				CreaDataSourceMaterias(alumno);
			}
		});
		table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Alumno alumno = (Alumno)itemId;
				Button botonVer = new Button(alumno.getApellido().trim() + ", " + alumno.getNombre());
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = AlumnoWWView.CreaWindowABM();
						AlumnoABM alumnoABM = new AlumnoABM(alumno ,Dominios.AccesoADatos.SELECT ,ui ,windowABM);
						windowABM.setContent(alumnoABM);
						ui.addWindow(windowABM);
						windowABM.focus();
					}
				});
				return botonVer;
			}
		});
		CreaDataSource();
		
		tableMaterias.setWidth("535px");
		tableMaterias.setHeight("480px");
		tableMaterias.setSelectable(true);
		tableMaterias.addGeneratedColumn(COLUMNA_MATERIA, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				AsignaturaAlumnoContenedor.ProfesorMateriaCursando asignatura = (AsignaturaAlumnoContenedor.ProfesorMateriaCursando)itemId;
				final Materia materia = MateriaRN.Read(asignatura.getMateriaId());
				Button botonVer = new Button(materia.getNombre());
				botonVer.setWidth("200px");
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = MateriaWWView.CreaWindowsABM();
						MateriaABM materiaABM = new MateriaABM(materia ,Dominios.AccesoADatos.SELECT ,ui ,windowABM);
						windowABM.setContent(materiaABM);
						ui.addWindow(windowABM);
						windowABM.focus();
					}
				});
				return botonVer;
			}
		});
		tableMaterias.addGeneratedColumn(COLUMNA_PROFESOR, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				AsignaturaAlumnoContenedor.ProfesorMateriaCursando asignatura = (AsignaturaAlumnoContenedor.ProfesorMateriaCursando)itemId;
				final Profesor profesor = ProfesorRN.Read(asignatura.getProfesorId());
				Button botonVer = new Button(profesor.toString());
				botonVer.setWidth("230px");
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = ProfesorWWView.CreaWindowABM();
						ProfesorABM profesorABM = new ProfesorABM(profesor ,Dominios.AccesoADatos.SELECT ,ui ,windowABM);
						windowABM.setContent(profesorABM);
						ui.addWindow(windowABM);
						windowABM.focus();
					}
				});
				return botonVer;
			}
		});
		tableMaterias.addGeneratedColumn(COLUMNA_CURSANDO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final AsignaturaAlumnoContenedor.ProfesorMateriaCursando asignatura = (AsignaturaAlumnoContenedor.ProfesorMateriaCursando)itemId;
				final CheckBox cursando = new CheckBox();
				cursando.setWidth("60px");
				cursando.setValue(asignatura.getAsignaturaAlumno().isCursando());
				cursando.addValueChangeListener(new ValueChangeListener() {
					@Override
					public void valueChange(ValueChangeEvent event) {
						AsignaturaAlumno asignaturaAlumno = asignatura.getAsignaturaAlumno(); 
						asignaturaAlumno.setCursando(cursando.getValue());
						AsignaturaAlumnoRN.Save(asignaturaAlumno);
					}
				});
				return cursando;
			}
		});
		
		CreaDataSourceMaterias(null);
	}

	private void CreaDataSource() {
		table.setContainerDataSource(AlumnoContenedor.LeeContainerXCurso(curso.getId()));
		table.setVisibleColumns((Object[]) new String[] {LINK_VER ,"dni"});
		table.setColumnHeaders("Apellido, Nombre" ,"DNI");
	}
	
	private void CreaDataSourceMaterias(Alumno alumno) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(filtroAnio.getValue());
		if (alumno != null){
			tableMaterias.setContainerDataSource(AsignaturaAlumnoContenedor.LeeContainerProfesorMateriaCursandoXAlumnoXCurso(alumno.getId() ,calendar.get(GregorianCalendar.YEAR) ,curso.getId() ,false));
		}
		else{
			tableMaterias.removeAllItems();
		}
		tableMaterias.setVisibleColumns((Object[]) new String[] {COLUMNA_MATERIA ,COLUMNA_PROFESOR ,COLUMNA_CURSANDO});
		tableMaterias.setColumnHeaders("Materia" ,"Profesor" ,"Cursando");
		tableMaterias.setColumnWidth(COLUMNA_MATERIA ,200);
		tableMaterias.setColumnWidth(COLUMNA_PROFESOR ,230); 
		tableMaterias.setColumnWidth(COLUMNA_CURSANDO ,60);
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
	
	}


}
