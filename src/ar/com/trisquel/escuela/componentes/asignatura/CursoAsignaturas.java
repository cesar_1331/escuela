package ar.com.trisquel.escuela.componentes.asignatura;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import ar.com.trisquel.escuela.componentes.materias.MateriaABM;
import ar.com.trisquel.escuela.componentes.materias.MateriaWWView;
import ar.com.trisquel.escuela.data.contenedor.ProfesorContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.AsignaturaRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ProfesorRN;
import ar.com.trisquel.escuela.data.select.AsignaturaSelect;
import ar.com.trisquel.escuela.data.select.MateriaSelect;
import ar.com.trisquel.escuela.data.tablas.Asignatura;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.Materia;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class CursoAsignaturas extends VerticalLayout implements ClickListener {
	private InlineDateField asignaturaAnio;
	private CheckBox materiaAsociada;
	private Curso curso;
	private VerticalLayout layoutMaterias;
	private UI ui;
	private boolean backgroundFilaUno;
	
	public CursoAsignaturas(Curso curso ,UI ui){
		this.curso = curso;
		this.ui = ui;
		onDraw();
	}
	
	public void onDraw(){
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setWidth("100%");
		layout.setColumns(3);
		addComponent(layout);
		//Id
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Curso:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.addStyleName(R.Style.TEXTO_BIG);
		label.setWidth("60px");
		layout.addComponent(label);
		Label cursoLabel = new Label(curso.toString());
		cursoLabel.addStyleName(R.Style.EDITABLE);
		cursoLabel.addStyleName(R.Style.LABEL_BOLD);
		cursoLabel.addStyleName(R.Style.TEXTO_BIG);
		cursoLabel.setWidth("630px");
		layout.addComponent(cursoLabel);
		VerticalLayout layoutAsignaturas = new VerticalLayout();
		addComponent(layoutAsignaturas);
		/*
		 * Tabla
		 */
		HorizontalLayout layoutTitulo = new HorizontalLayout();
		layoutTitulo.setPrimaryStyleName(R.Style.TEXTO_SUBTITULO_COLOR);
		layoutTitulo.setHeight("30px");
		layoutAsignaturas.addComponent(layoutTitulo);
		Label labelTitleServicio = new Label(EtiquetasLabel.getAsignaturas());
		labelTitleServicio.setWidth("230px");
		layoutTitulo.addComponent(labelTitleServicio);
		asignaturaAnio = new InlineDateField();
		asignaturaAnio.setResolution(Resolution.YEAR);
		asignaturaAnio.addStyleName(R.Style.LABEL_COLOR);
		asignaturaAnio.setWidth("200px");
		asignaturaAnio.setValue(new Date(System.currentTimeMillis()));
		asignaturaAnio.setImmediate(true);
		asignaturaAnio.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				GregorianCalendar gregorianCalendar = new GregorianCalendar(); 
				gregorianCalendar.setTime(asignaturaAnio.getValue());
		 		if (AsignaturaSelect.LeeAsignaturaXAnioXCurso(gregorianCalendar.get(GregorianCalendar.YEAR), curso.getId()).size() > 0)
		 			materiaAsociada.setValue(true);
		 		else 
		 			materiaAsociada.setValue(false);
				ArmaMaterias();
			}
		});
		layoutTitulo.addComponent(asignaturaAnio);
		materiaAsociada = new CheckBox("Materias asociadas");
		materiaAsociada.setImmediate(true);
		materiaAsociada.setWidth("250px");
		GregorianCalendar gregorianCalendar = new GregorianCalendar(); 
		gregorianCalendar.setTime(asignaturaAnio.getValue());
 		if (AsignaturaSelect.LeeAsignaturaXAnioXCurso(gregorianCalendar.get(GregorianCalendar.YEAR), curso.getId()).size() > 0)
 			materiaAsociada.setValue(true);
 		else 
 			materiaAsociada.setValue(false);
		materiaAsociada.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				ArmaMaterias();
			}
		});
		layoutTitulo.addComponent(materiaAsociada);
		layoutMaterias = new VerticalLayout();
		ArmaMaterias();
		layoutAsignaturas.addComponent(layoutMaterias);
	}
	
	private void ArmaMaterias() {
		layoutMaterias.removeAllComponents();
		List<Materia> listMateria = MateriaSelect.SelectXNombre("" ,true);
		for (final Materia materia : listMateria){
			final GregorianCalendar gregorianCalendar = new GregorianCalendar(); 
			gregorianCalendar.setTime(asignaturaAnio.getValue());
			final Asignatura asignatura = AsignaturaSelect.LeeAsignaturaXAnioXCursoXMateria(gregorianCalendar.get(GregorianCalendar.YEAR), curso.getId(), materia.getId());
			if ((materiaAsociada.getValue()  && asignatura != null) || 
				(!materiaAsociada.getValue() && asignatura == null)){
				HorizontalLayout layoutFila = new HorizontalLayout();
				if (backgroundFilaUno){
					layoutFila.addStyleName(R.Style.BACKGROUND_FILA_1);
				}
				else{
					layoutFila.addStyleName(R.Style.BACKGROUND_FILA_2);
				}
				layoutMaterias.addComponent(layoutFila);
				backgroundFilaUno = !backgroundFilaUno;
				
				Button botonVer = new Button(materia.getNombre());
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.setWidth("325px");
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = MateriaWWView.CreaWindowsABM();
						MateriaABM materiaABM = new MateriaABM(materia ,Dominios.AccesoADatos.SELECT ,ui ,windowABM);
						windowABM.setContent(materiaABM);
						ui.addWindow(windowABM);
					}
				});
				layoutFila.addComponent(botonVer);
				final ComboBox profesorComboBox = new ComboBox();
				profesorComboBox.setContainerDataSource(ProfesorContenedor.LeeContainerXMateria(materia.getId()));
				profesorComboBox.setInputPrompt(EtiquetasLabel.getMateriaNoAsociadoAlCurso());
				profesorComboBox.setWidth("325px");
				layoutFila.addComponent(profesorComboBox);
				final ButtonIcon botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true);
				final ButtonIcon botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true);
				final ButtonIcon botonAsociar = new ButtonIcon(ButtonIcon.ASOCIAR, true);
				final ButtonIcon botonDescartar = new ButtonIcon(ButtonIcon.DESCARTAR, true);
				final ButtonIcon botonGuardar = new ButtonIcon(ButtonIcon.GUARDAR, true);
				layoutFila.addComponent(botonGuardar);
				layoutFila.addComponent(botonAsociar);
				layoutFila.addComponent(botonDescartar);
				layoutFila.addComponent(botonModificar);
				layoutFila.addComponent(botonEliminar);
				if (asignatura != null){
					profesorComboBox.setValue(ProfesorRN.Read(asignatura.getProfesorId()));
					profesorComboBox.setReadOnly(true);
					botonGuardar.setVisible(false);
					botonModificar.setVisible(true);
					botonEliminar.setVisible(true);
					botonAsociar.setVisible(false);
					botonDescartar.setVisible(false);
				}
				else{
					profesorComboBox.setReadOnly(true);
					botonGuardar.setVisible(false);
					botonAsociar.setVisible(true);
					botonDescartar.setEnabled(false);
					botonModificar.setVisible(false);
					botonEliminar.setVisible(false);
				}
				botonAsociar.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						profesorComboBox.setReadOnly(false);
						botonAsociar.setVisible(false);
						botonGuardar.setVisible(true);
						botonDescartar.setVisible(true);
						botonDescartar.setEnabled(true);
					}
				});
				botonDescartar.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						ArmaMaterias();
					}
				});
				botonEliminar.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						AsignaturaRN.Delete(asignatura);
						ArmaMaterias();
					}
				});
				botonModificar.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						profesorComboBox.setReadOnly(false);
						botonModificar.setVisible(false);
						botonEliminar.setVisible(false);
						botonGuardar.setVisible(true);
						botonDescartar.setVisible(true);
						botonDescartar.setEnabled(true);
					}
				});
				botonGuardar.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Profesor profesor = (Profesor)profesorComboBox.getValue();
						if (profesor != null){
							Asignatura asignaturaSave = AsignaturaSelect.LeeAsignaturaXAnioXCursoXMateria(gregorianCalendar.get(GregorianCalendar.YEAR), curso.getId(), materia.getId());
							if (asignaturaSave == null){
								asignaturaSave = AsignaturaRN.Inizializate();
								asignaturaSave.setMateriaId(materia.getId());
								asignaturaSave.setCursoId(curso.getId());
								asignaturaSave.setAnio(gregorianCalendar.get(GregorianCalendar.YEAR));
								asignaturaSave.setProfesorId(profesor.getId());
								asignaturaSave.setObservaciones("");
							}
							AsignaturaRN.Save(asignaturaSave);
						ArmaMaterias();
						}
					}
				});
			}
		}

	}

	
	@Override
	public void buttonClick(ClickEvent event) {
	
	}


}
