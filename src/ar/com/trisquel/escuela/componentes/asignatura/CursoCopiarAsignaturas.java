package ar.com.trisquel.escuela.componentes.asignatura;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import ar.com.trisquel.escuela.data.reglasdenegocio.AsignaturaRN;
import ar.com.trisquel.escuela.data.select.AsignaturaSelect;
import ar.com.trisquel.escuela.data.tablas.Asignatura;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class CursoCopiarAsignaturas extends VerticalLayout implements ClickListener {
	
	private ButtonIcon botonSi;
	private ButtonIcon botonSiIcono;
	private ButtonIcon botonNo;
	private ButtonIcon botonNoIcono;
	private InlineDateField anioOrigen;
	private InlineDateField anioDestino;
	private UI ui;
	private Window window;
	
	public CursoCopiarAsignaturas(UI ui ,Window window){
		this.ui = ui; 
		this.window = window;
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setMargin(true);
		mainLayout.setSpacing(true);
		Label message = new Label("Se van copiar las asignaturas con los profesores<br />Seleccione el a�o origen" ,ContentMode.HTML);
		message.addStyleName(R.Style.LABEL_BOLD);
		message.setWidth("380px");
		mainLayout.addComponent(message);
		mainLayout.setComponentAlignment(message, Alignment.MIDDLE_CENTER);
		
		anioOrigen = new InlineDateField();
		anioOrigen.setResolution(Resolution.YEAR);
		anioOrigen.setValue(new Date(System.currentTimeMillis()));
		mainLayout.addComponent(anioOrigen);
		mainLayout.setComponentAlignment(anioOrigen, Alignment.MIDDLE_CENTER);
		
		message = new Label("Seleccione el a�o destino" ,ContentMode.HTML);
		message.addStyleName(R.Style.LABEL_BOLD);
		message.setWidth("380px");
		mainLayout.addComponent(message);
		mainLayout.setComponentAlignment(message, Alignment.MIDDLE_CENTER);
		
		anioDestino = new InlineDateField();
		anioDestino.setResolution(Resolution.YEAR);
		anioDestino.setValue(new Date(System.currentTimeMillis()));
		mainLayout.addComponent(anioDestino);
		mainLayout.setComponentAlignment(anioDestino, Alignment.MIDDLE_CENTER);
		
		// Add Buttons
		HorizontalLayout buttonLayout = new HorizontalLayout();
		mainLayout.addComponent(buttonLayout);
		mainLayout.setComponentAlignment(buttonLayout, Alignment.MIDDLE_CENTER);
		botonSiIcono = new ButtonIcon(ButtonIcon.CONFIRMAR, true, this);
		buttonLayout.addComponent(botonSiIcono);
		botonSi = new ButtonIcon(ButtonIcon.CONFIRMAR, false, this);
		buttonLayout.addComponent(botonSi);
		buttonLayout.addComponent(new Label(R.CaracteresEsteciales.SPACIO+R.CaracteresEsteciales.SPACIO+R.CaracteresEsteciales.SPACIO ,ContentMode.HTML));
		botonNoIcono = new ButtonIcon(ButtonIcon.CANCELAR, true, this);
		buttonLayout.addComponent(botonNoIcono);
		botonNo = new ButtonIcon(ButtonIcon.CANCELAR, false, this);
		buttonLayout.addComponent(botonNo);
		addComponent(mainLayout);
	}
	
	
	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonSi || event.getButton() == botonSiIcono){
			final GregorianCalendar calendarOrigen = new GregorianCalendar();
			calendarOrigen.setTime(anioOrigen.getValue());
			final GregorianCalendar calendarDestino = new GregorianCalendar();
			calendarDestino.setTime(anioDestino.getValue());
			if (calendarOrigen.get(GregorianCalendar.YEAR) == calendarDestino.get(GregorianCalendar.YEAR)){
				MessageBox.showHTML(Icon.ERROR, "", "El a�o destino debe ser distinto del a�o origen", ButtonId.OK);
			}
			else{
				MessageBox.showHTML(Icon.QUESTION, "", MensajeLabel.getConfirmaAccion(), new MessageBoxListener(){
					@Override
					public void buttonClicked(ButtonId buttonType) {
						if (buttonType.equals(ButtonId.YES)){
							List<Asignatura> listAsignatura = AsignaturaSelect.LeeAsignaturaXAnio(calendarOrigen.get(GregorianCalendar.YEAR));
							for (Asignatura asignatura : listAsignatura){
								Asignatura asignaturaSave = AsignaturaSelect.LeeAsignaturaXAnioXCursoXMateria(calendarDestino.get(GregorianCalendar.YEAR), asignatura.getCursoId() ,asignatura.getMateriaId());
								if (asignaturaSave == null){
									asignaturaSave = AsignaturaRN.Inizializate();
									asignaturaSave.setAnio(calendarDestino.get(GregorianCalendar.YEAR));
									asignaturaSave.setCursoId(asignatura.getCursoId());
									asignaturaSave.setMateriaId(asignatura.getMateriaId());
									asignaturaSave.setObservaciones("");
									asignaturaSave.setProfesorId(asignatura.getProfesorId());
									AsignaturaRN.Add(asignaturaSave);
								}
							}
							MessageBox.showHTML(Icon.ERROR, "", "Se crearon las asignaturas con los profesores", ButtonId.OK);
							ui.removeWindow(window);
						}
					}
				}
				,ButtonId.YES 
				,ButtonId.NO);
			}
		}
		else if (event.getButton() == botonNo || event.getButton() == botonNoIcono){
			ui.removeWindow(window);
		}
	}


}
