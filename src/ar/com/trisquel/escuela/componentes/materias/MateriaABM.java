package ar.com.trisquel.escuela.componentes.materias;

import ar.com.trisquel.escuela.data.reglasdenegocio.MateriaRN;
import ar.com.trisquel.escuela.data.tablas.Materia;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class MateriaABM extends VerticalLayout implements ClickListener {
	private TextField id;
	private TextField nombre;
	private CheckBox activa;
	private ButtonIcon ButtonGuardar;
	private ButtonIcon ButtonDescartar;
	private Label materiaError;
	private UI ui;
	private Materia materia;
	private String modo;
	private Window window;

	public MateriaABM(Materia materia, String modo, UI ui, Window window) {
		this.ui = ui;
		this.modo = modo;
		this.window = window;
		this.materia = materia;
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setWidth("100%");
		layout.setColumns(3);
		addComponent(layout);
		//Id
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelId = new Label("Id:");
		labelId.addStyleName(R.Style.EDITABLE_LABEL);
		labelId.setWidth("70px");
		layout.addComponent(labelId);
		HorizontalLayout layoutId = new HorizontalLayout();
		layoutId.setWidth("400px");
		id = new TextField();
		id.addStyleName(R.Style.EDITABLE);
		id.addStyleName(R.Style.LABEL_BOLD);
		layoutId.addComponent(id);
		//Botones
		HorizontalLayout layoutBotones = new HorizontalLayout();
		ButtonGuardar = new ButtonIcon(ButtonIcon.GUARDAR, true, this);
		ButtonGuardar.addStyleName(R.Style.PADDING_MINIMO);
		ButtonGuardar.setTabIndex(11);
		layoutBotones.addComponent(ButtonGuardar);
		ButtonDescartar = new ButtonIcon(ButtonIcon.DESCARTAR, true, this);
		ButtonDescartar.addStyleName(R.Style.PADDING_MINIMO);
		ButtonDescartar.setTabIndex(12);
		layoutBotones.addComponent(ButtonDescartar);
		layoutId.addComponent(layoutBotones);
		layoutId.setComponentAlignment(layoutBotones, Alignment.TOP_RIGHT);
		layout.addComponent(layoutId);
		//Nombre
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelTitulo = new Label("Nombre:");
		labelTitulo.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(labelTitulo);
		nombre = new TextField();
		nombre.addStyleName(R.Style.EDITABLE);
		nombre.addStyleName(R.Style.LABEL_BOLD);
		nombre.addStyleName(R.Style.TEXTO_MAYUSCULA);
		nombre.setMaxLength(40);
		nombre.setWidth("250px");
		layout.addComponent(nombre);
		//Nombre
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Activa:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		activa = new CheckBox();
		layout.addComponent(activa);
		
		layout.addComponent(new Label());
		layout.addComponent(new Label());
		materiaError = new Label();
		materiaError.addStyleName(R.Style.ERROR_VIEW);
		layout.addComponent(materiaError);
		
		if (modo.equals(Dominios.AccesoADatos.INSERT)){
			id.setReadOnly(true);
			activa.setValue(true);
			activa.setReadOnly(true);
			ButtonGuardar.setVisible(true);
			ButtonDescartar.setVisible(true);
		}
		else{
			id.setValue(String.valueOf(materia.getId()));
			id.setReadOnly(true);
			nombre.setValue(materia.getNombre());
			activa.setValue(materia.isActiva());
			if (modo.equals(Dominios.AccesoADatos.SELECT)) {
				nombre.setReadOnly(true);
				activa.setReadOnly(true);
				ButtonGuardar.setVisible(false);
				ButtonDescartar.setVisible(false);
			} else if (modo.equals(Dominios.AccesoADatos.UPDATE)) {
				ButtonGuardar.setVisible(true);
				ButtonDescartar.setVisible(true);
			}
		}
		nombre.focus();
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == ButtonDescartar) {
			if (modo.equals(Dominios.AccesoADatos.INSERT) || modo.equals(Dominios.AccesoADatos.UPDATE)) {
				ui.removeWindow(window);
			}
			else{
				onDraw();
			}
		} else if (event.getButton() == ButtonGuardar) {
			if (materia == null){
				materia = MateriaRN.Inizializate();
			}
			materia.setNombre(nombre.getValue());
			materia.setActiva(activa.getValue());
			RespuestaEntidad respuesta = MateriaRN.Validate(materia ,modo);
			if (respuesta.isError()) {
				MessageBox.showHTML(Icon.ERROR, "", respuesta.getMsgError(),ButtonId.OK);
			} else {
				MessageBox.showHTML(Icon.QUESTION, "",MensajeLabel.getConfirmaAccion(),new MessageBoxListener() {
					@Override
					public void buttonClicked(ButtonId buttonType) {
						if (buttonType == ButtonId.SAVE) {
							RespuestaEntidad respuesta = MateriaRN.Save(materia);
							if (respuesta.isError()) {
								MessageBox.showHTML(Icon.ERROR, "",respuesta.getMsgError(),ButtonId.OK);
							} else {

								if (modo.equals(Dominios.AccesoADatos.INSERT) || modo.equals(Dominios.AccesoADatos.UPDATE)) {
									ui.removeWindow(window);
								} else {
									onDraw();
								}
							}
						}
					}
				}, ButtonId.SAVE, ButtonId.CANCEL);
			}
		}
	}


}
