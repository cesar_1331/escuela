package ar.com.trisquel.escuela.componentes.materias;

import ar.com.trisquel.escuela.data.contenedor.MateriaContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.MateriaRN;
import ar.com.trisquel.escuela.data.select.MateriaSelect;
import ar.com.trisquel.escuela.data.tablas.Materia;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class MateriaWWView extends VerticalLayout implements ClickListener,View {
	public static final String VIEW = "MateriaWW";
	private static final String LINK_VER = "LINK_VER";
	private ButtonIcon botonFiltros;
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonEliminar;
	private ButtonIcon botonRefrescar;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private TextField filtroNombre;
	private CheckBox filtroActivo;
	private Table table;
	private VerticalLayout layoutFiltros;
	private UI ui;

	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass(), ui);
		if (!control.isAcceso()) {
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		} else {
			onDraw();
		}
	}

	public void onDraw() {
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(EtiquetasLabel.getMaterias().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true, this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonFiltros = new ButtonIcon(ButtonIcon.BUSCAR, true, this);
		botonFiltros.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonFiltros);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		//botonAgregar.setEnabled(false);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		//botonModificar.setEnabled(false);
		layoutBotones.addComponent(botonModificar);
		botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true, this);
		botonEliminar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonEliminar);
		botonEliminar.setEnabled(false);
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true, this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonExcel);
		botonExcel.setEnabled(false);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true, this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonPDF);
		botonPDF.setEnabled(false);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		table = new Table();
		ArmaTabla();
		layout.addComponent(table);
		Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonFiltros) {
			layoutFiltros.setVisible(!layoutFiltros.isVisible());
		} else if (event.getButton() == botonRefrescar) {
			CreaDataSource();
		} else if (event.getButton() == botonModificar) {
			Materia materia = (Materia)table.getValue();
			if (materia != null){
				Window windowABM = CreaWindowsABM();
				MateriaABM materiaABM = new MateriaABM(materia ,Dominios.AccesoADatos.UPDATE ,ui ,windowABM);
				windowABM.setContent(materiaABM);
				windowABM.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						CreaDataSource();
					}
				});
				ui.addWindow(windowABM);
			}
		}
		else if (event.getButton() == botonAgregar){
			Window windowABM = CreaWindowsABM();
			MateriaABM materiaABM = new MateriaABM(null ,Dominios.AccesoADatos.INSERT ,ui ,windowABM);
			windowABM.setContent(materiaABM);
			windowABM.addCloseListener(new CloseListener() {
				@Override
				public void windowClose(CloseEvent e) {
					CreaDataSource();
				}
			});
			ui.addWindow(windowABM);
		}
		else if (event.getButton() == botonEliminar){
			
		}
		else if (event.getButton() == botonExcel
				|| event.getButton() == botonPDF) {
			/**
			 * No hace nada
			 */
		}

	}

	private void ArmaFiltros() {
		layoutFiltros.setVisible(false);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(6);
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label label = new Label("Nombre:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("50px");
		formFiltro.addComponent(label);
		filtroNombre = new TextField();
		filtroNombre.setNullRepresentation("");
		filtroNombre.setNullSettingAllowed(false);
		filtroNombre.addStyleName(R.Style.EDITABLE);
		filtroNombre.addStyleName(R.Style.LABEL_BOLD);
		filtroNombre.setWidth("200px");
		formFiltro.addComponent(filtroNombre);
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		imageIcono.addStyleName(R.Style.MARGIN_LARGO);
		formFiltro.addComponent(imageIcono);
		label = new Label("Activo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("50px");
		formFiltro.addComponent(label);
		filtroActivo = new CheckBox();
		filtroActivo.setValue(true);
		formFiltro.addComponent(filtroActivo);
		layoutFiltros.addComponent(formFiltro);
		
	}

	private void ArmaTabla() {
		table.setHeight("550px");
		table.setWidth("985px");
		table.setSelectable(true);
		table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Materia materia = (Materia)itemId;
				Button botonVer = new Button(materia.getNombre());
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = CreaWindowsABM();
						MateriaABM materiaABM = new MateriaABM(materia ,Dominios.AccesoADatos.SELECT ,ui ,windowABM);
						windowABM.setContent(materiaABM);
						ui.addWindow(windowABM);
					}
				});
				return botonVer;
			}

		});
		CreaDataSource();
	}

	private void CreaDataSource() {
		table.setContainerDataSource(MateriaContenedor.LeeContainerXNombre(filtroNombre.getValue() ,filtroActivo.getValue()));
		table.setVisibleColumns((Object[]) new String[] {"id",LINK_VER});
		table.setColumnHeaders("Id" ,"Nombre");
		table.setColumnWidth("id", 70);
	}

	public static Window CreaWindowsABM(){
		Window windowABM = new Window(EtiquetasLabel.getMaterias());
		windowABM.setModal(true);
		windowABM.setResizable(false);
		windowABM.setDraggable(false);
		windowABM.setWidth("500px");
		windowABM.setHeight("220px");
		windowABM.center();
		windowABM.setCloseShortcut(KeyCode.ESCAPE, null);
		return windowABM;
	}
	
	public static void CreaMateriasDefecto(){
		if (!MateriaSelect.ControlaExiste("MATEMATICAS"))
			MateriaRN.Save(MateriaRN.Inizializate("MATEMATICAS"));
		if (!MateriaSelect.ControlaExiste("MUSICA"))
			MateriaRN.Save(MateriaRN.Inizializate("MUSICA"));
		if (!MateriaSelect.ControlaExiste("LENGUA Y LITERATURA"))
			MateriaRN.Save(MateriaRN.Inizializate("LENGUA Y LITERATURA"));
		if (!MateriaSelect.ControlaExiste("INGLES"))
			MateriaRN.Save(MateriaRN.Inizializate("INGLES"));
		if (!MateriaSelect.ControlaExiste("FRANCES"))
			MateriaRN.Save(MateriaRN.Inizializate("FRANCES"));
		if (!MateriaSelect.ControlaExiste("CIENCIAS SOCIALES"));
			MateriaRN.Save(MateriaRN.Inizializate("CIENCIAS SOCIALES"));
		if (!MateriaSelect.ControlaExiste("GEOGRAFIA"))
			MateriaRN.Save(MateriaRN.Inizializate("GEOGRAFIA"));
		if (!MateriaSelect.ControlaExiste("EDUCACIÓN FISICA"))
			MateriaRN.Save(MateriaRN.Inizializate("EDUCACIÓN FISICA"));
		
	}
}
