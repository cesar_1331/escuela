package ar.com.trisquel.escuela.componentes;

import ar.com.trisquel.escuela.EscuelaUI;
import ar.com.trisquel.escuela.componentes.otros.ArticuloView;
import ar.com.trisquel.escuela.componentes.otros.ContactoView;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.server.ExternalResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class Footer extends VerticalLayout implements ClickListener {

	private Button ButtonInicio;
	private Button ButtonAutoridades;
	private Button ButtonOfertaAcademica;
	private Button ButtonContacto;
	private Button ButtonHistoria;
	public Footer() {
	}
	public Footer(Component... children) {
		super(children);
	}
	
	
	public void onDraw()
	{
		removeAllComponents();
		/**
		 * Arma el menu del footer
		 */
		VerticalLayout LayoutFooterMenu = new VerticalLayout();
		LayoutFooterMenu.setPrimaryStyleName(R.StyleFooter.LAYOUTMENU);
		addComponent(LayoutFooterMenu);
		
		VerticalLayout footerMenu = new VerticalLayout();
		footerMenu.setWidth("800px");
		LayoutFooterMenu.addComponent(footerMenu);
		LayoutFooterMenu.setComponentAlignment(footerMenu ,Alignment.TOP_CENTER);
		
		/**
		 * Columna 1
		 */
		HorizontalLayout menuFooter = new HorizontalLayout();
		menuFooter.setSizeFull();
		VerticalLayout Columna1 = new VerticalLayout();
		ButtonInicio = new Button(EtiquetasLabel.getInicio() ,this);
		ButtonInicio.setPrimaryStyleName(R.StyleFooter.LINK);
		ButtonOfertaAcademica = new Button(EtiquetasLabel.getOfertaAcademica() ,this);
		ButtonOfertaAcademica.setPrimaryStyleName(R.StyleFooter.LINK);
		Columna1.addComponent(ButtonInicio);
		Columna1.addComponent(ButtonOfertaAcademica);
		menuFooter.addComponent(Columna1);
		/**
		 * Columna 2
		 */
		VerticalLayout Columna2 = new VerticalLayout();
		ButtonAutoridades = new Button(EtiquetasLabel.getAutoridades() ,this);
		ButtonAutoridades.setPrimaryStyleName(R.StyleFooter.LINK);
		ButtonHistoria = new Button(EtiquetasLabel.getHistoria() ,this);
		ButtonHistoria.setPrimaryStyleName(R.StyleFooter.LINK);
		Columna2.addComponent(ButtonAutoridades);
		Columna2.addComponent(ButtonHistoria);
		menuFooter.addComponent(Columna2);
		/**
		 * Columna 3
		 */
		VerticalLayout Columna3 = new VerticalLayout();
		ButtonContacto = new Button(EtiquetasLabel.getContacto() ,this);
		ButtonContacto.setPrimaryStyleName(R.StyleFooter.LINK);
		Columna3.addComponent(ButtonContacto);
		menuFooter.addComponent(Columna3);
		footerMenu.addComponent(menuFooter);
		footerMenu.setComponentAlignment(menuFooter, Alignment.BOTTOM_CENTER);
		
		VerticalLayout LayoutFooterFooter = new VerticalLayout();
		LayoutFooterFooter.setPrimaryStyleName(R.StyleFooter.LAYOUTFOOTER);
		addComponent(LayoutFooterFooter);
		/**
		 * Arma el footer Copyright
		 */
		Label separator = new Label();
		separator.addStyleName(R.Style.PADDING_MEDIO);
		LayoutFooterFooter.addComponent(separator);
		ExternalResource trisquel = new ExternalResource("http://trisquel-software.com.ar");
		Link BottonTrisquel = new Link("Copyright � 2013 - Trisquel Software" ,trisquel);
		BottonTrisquel.setPrimaryStyleName(R.StyleFooter.TEXT);
		BottonTrisquel.setWidth(null);
		LayoutFooterFooter.addComponent(BottonTrisquel);
		LayoutFooterFooter.setComponentAlignment(BottonTrisquel, Alignment.BOTTOM_CENTER);
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		Sesion sesion = (Sesion)getUI().getSession().getAttribute(R.Session.SESSION);
		if (event.getButton() == ButtonInicio){
			sesion.setNavegarA(R.Paginas.INICIO);
			sesion.setArticuloSeleccionado(R.Paginas.INICIO);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getButton() == ButtonOfertaAcademica){
			sesion.setNavegarA(ArticuloView.VIEW);
			sesion.setArticuloSeleccionado(R.Paginas.OFERTA_ACADEMICA);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getButton() == ButtonAutoridades){
			sesion.setNavegarA(ArticuloView.VIEW);
			sesion.setArticuloSeleccionado(R.Paginas.AUTORIDADES);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getButton() == ButtonHistoria){
			sesion.setNavegarA(ArticuloView.VIEW);
			sesion.setArticuloSeleccionado(R.Paginas.HISTORIA);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
		else if (event.getButton() == ButtonContacto){
			sesion.setNavegarA(ContactoView.VIEW);
			getUI().getSession().setAttribute(R.Session.SESSION , sesion);
			((EscuelaUI)getUI()).getBody().onDraw();
		}
	}

}
