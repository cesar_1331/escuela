package ar.com.trisquel.escuela.componentes.user;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Blob;

import org.hibernate.Hibernate;

import ar.com.trisquel.escuela.EscuelaUI;
import ar.com.trisquel.escuela.componentes.personas.alumno.AlumnoABM;
import ar.com.trisquel.escuela.componentes.personas.profesor.ProfesorABM;
import ar.com.trisquel.escuela.data.ImagenData;
import ar.com.trisquel.escuela.data.reglasdenegocio.AlumnoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ProfesorRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.UsuarioRN;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.ImagenResize;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FileResource;
import com.vaadin.server.StreamResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class UserPreferencesView extends VerticalLayout implements ClickListener,View, BlurListener {

	public static final String VIEW = "PreferenciasUsuario"; 
	private TextField Nombre;
	private Label NombreError = new Label("" ,ContentMode.HTML);
	private Image ImageUser;
	private TextField Mail;
	private Label MailError = new Label("" ,ContentMode.HTML);
	private TextField MailAlternativo;
	private Label MailAlternativoError = new Label("" ,ContentMode.HTML);
	private Label UsuarioError = new Label("" ,ContentMode.HTML);
	private ButtonIcon BottonHelp;
	private ButtonIcon BottonHelpIcono;
	private ButtonIcon BottonRegister;
	private ButtonIcon BottonDescartarDatos;
	private ButtonIcon BottonCambiarClave;
	private ButtonIcon BottonRegisterIcon;
	private ButtonIcon BottonDescartarDatosIcon;
	private ButtonIcon BottonCambiarClaveIcon;
	private Usuario user ;
	private UI Ui;
	private Sesion sesion;
	private boolean EditarDatos = false;
	
	public UserPreferencesView() {
	}

	public UserPreferencesView(Component... children) {
		super(children);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		Ui = event.getNavigator().getUI();
		sesion = (Sesion)Ui.getSession().getAttribute(R.Session.SESSION);
		ControlAcceso control = ControlAcceso.Control(UserPreferencesView.class ,Ui); 
		if (!control.isAcceso()){
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		}
		else{
			onDraw();
		}
	}

	public void onDraw(){
		removeAllComponents();
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		user = UsuarioRN.Read(sesion.getUsuarioId());
		Label labelTitulo = new Label(EtiquetasLabel.getMiCuenta());
		labelTitulo.addStyleName(R.Style.TITLE);
		labelTitulo.addStyleName(R.Style.PADDING_MINIMO);
		addComponent(labelTitulo);
		Label labelSeparator = new Label("<hr>" ,ContentMode.HTML);
		labelSeparator.addStyleName(R.Style.EXPAND_WIDTH_ALL);
		addComponent(labelSeparator);
		HorizontalLayout LayoutUserManager = new HorizontalLayout();
		
		VerticalLayout layoutImage = new VerticalLayout();
		layoutImage.setWidth("150px");
		LayoutUserManager.addComponent(layoutImage);
		ImageUser = new Image();
		
		if (user.getImagen() != null){
			ImagenData blob = new ImagenData(user.getImagen()); 
			ImageUser.setSource(new StreamResource(blob ,user.toString()+".png"));
		}
		else{
			ImageUser.setSource(new ThemeResource("img/user-big.png"));
		}
		ImageUser.setStyleName(R.Style.IMAGEN_SCALE_150);
		layoutImage.addComponent(ImageUser);
		Upload upload = new Upload();
		upload.setButtonCaption(EtiquetasLabel.getSubirImagen());
		upload.setImmediate(true);
		layoutImage.addComponent(upload);
		ImageUploader uploader = new ImageUploader(); 
		upload.setReceiver(uploader);
		upload.addSucceededListener(uploader);
		
		VerticalLayout LayoutUserAcciones = new VerticalLayout();
		LayoutUserAcciones.addStyleName(R.Style.MARGIN_MEDIO);
		
		Label Usuario = new Label(user.getId());
		Usuario.setStyleName(R.Style.TITLE);
		Usuario.setWidth("100%");
		LayoutUserAcciones.addComponent(Usuario);
		Label labelTitle = new Label(EtiquetasLabel.getMiCuenta());
		labelTitle.addStyleName(R.Style.EXPAND_WIDTH_ALL);
		labelTitle.addStyleName(R.Style.PADDING_MINIMO);
		labelTitle.setPrimaryStyleName(R.Style.TEXTO_SUBTITULO_COLOR);
		LayoutUserAcciones.addComponent(labelTitle);
		LayoutUserManager.addComponent(LayoutUserAcciones);
		addComponent(LayoutUserManager);
		/*
		 * Datos personales del usuario
		 */
		HorizontalLayout layoutUser = new HorizontalLayout();
		LayoutUserAcciones.addComponent(layoutUser);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout FormDatos = new GridLayout();
		FormDatos.setWidth("680px");
		FormDatos.setColumns(3);
		// Nombre
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		FormDatos.addComponent(imageIcono);
		Label labelNombre = new Label(EtiquetasLabel.getNombre());
		labelNombre.addStyleName(R.Style.EDITABLE_LABEL);
		labelNombre.setWidth("100px");
		FormDatos.addComponent(labelNombre);
		HorizontalLayout layoutNombre = new HorizontalLayout();
		layoutNombre.setWidth("550px");
		Nombre = new TextField();
		Nombre.setValue(user.getNombre());
		Nombre.setNullRepresentation("");
		Nombre.setWidth("250px");
		Nombre.setMaxLength(60);
		Nombre.addBlurListener(this);
		Nombre.addStyleName(R.Style.EDITABLE);
		Nombre.addStyleName(R.Style.LABEL_BOLD);
		Nombre.addStyleName(R.Style.TEXTO_MAYUSCULA);
		layoutNombre.addComponent(Nombre);
		NombreError.setStyleName(R.Style.ERROR_VIEW);
		layoutNombre.addComponent(NombreError);
		FormDatos.addComponent(layoutNombre);;
		// Mail
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		FormDatos.addComponent(imageIcono);
		Label labelMail = new Label(EtiquetasLabel.getMail());
		labelMail.addStyleName(R.Style.EDITABLE_LABEL);
		FormDatos.addComponent(labelMail);
		HorizontalLayout layoutMail = new HorizontalLayout();
		Mail = new TextField();
		Mail.setValue(user.getMailPrincipal());
		Mail.setWidth("400px");
		Mail.addStyleName(R.Style.LABEL_BOLD);
		Mail.addStyleName(R.Style.EDITABLE);
		layoutMail.addComponent(Mail);
		MailError.setStyleName(R.Style.ERROR_VIEW);
		layoutMail.addComponent(MailError);
		FormDatos.addComponent(layoutMail);
		// Mail alternativo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		FormDatos.addComponent(imageIcono);
		Label labelMailAlter = new Label(EtiquetasLabel.getMailAlter());
		labelMailAlter.addStyleName(R.Style.EDITABLE_LABEL);
		FormDatos.addComponent(labelMailAlter);
		HorizontalLayout layoutMailAlternativo = new HorizontalLayout();
		MailAlternativo = new TextField();
		MailAlternativo.setValue(user.getMailSecundario());
		MailAlternativo.setWidth("400px");
		MailAlternativo.addBlurListener(this);
		MailAlternativo.addStyleName(R.Style.LABEL_BOLD);
		MailAlternativo.addStyleName(R.Style.EDITABLE);
		layoutMailAlternativo.addComponent(MailAlternativo);
		MailAlternativoError.setStyleName(R.Style.ERROR_VIEW);
		layoutMailAlternativo.addComponent(MailAlternativoError);
		FormDatos.addComponent(layoutMailAlternativo);
		FormDatos.addComponent(new Label());
		FormDatos.addComponent(new Label());
		UsuarioError.setStyleName(R.Style.ERROR_VIEW);
		FormDatos.addComponent(UsuarioError);
		FormDatos.addComponent(new Label());
		FormDatos.addComponent(new Label());
		//Botones
		VerticalLayout LayoutBotonesDatos = new VerticalLayout();
		LayoutBotonesDatos.setWidth("130px");
		HorizontalLayout layoutBoton1 = new HorizontalLayout();
		BottonRegisterIcon = new ButtonIcon(ButtonIcon.MODIFICAR ,true ,this);
		layoutBoton1.addComponent(BottonRegisterIcon);
		BottonRegister = new ButtonIcon(ButtonIcon.MODIFICAR ,false ,this);
		layoutBoton1.addComponent(BottonRegister);
		LayoutBotonesDatos.addComponent(layoutBoton1);
		HorizontalLayout layoutBoton2 = new HorizontalLayout();
		BottonDescartarDatosIcon = new ButtonIcon(ButtonIcon.DESCARTAR ,true ,this);
		layoutBoton2.addComponent(BottonDescartarDatosIcon);
		BottonDescartarDatos = new ButtonIcon(ButtonIcon.DESCARTAR ,false ,this);
		layoutBoton2.addComponent(BottonDescartarDatos);
		LayoutBotonesDatos.addComponent(layoutBoton2);
		HorizontalLayout layoutBoton3 = new HorizontalLayout();
		BottonCambiarClaveIcon = new ButtonIcon(ButtonIcon.CAMBIARCLAVE ,true ,this);
		layoutBoton3.addComponent(BottonCambiarClaveIcon);
		BottonCambiarClave = new ButtonIcon(ButtonIcon.CAMBIARCLAVE ,false ,this);
		layoutBoton3.addComponent(BottonCambiarClave);
		LayoutBotonesDatos.addComponent(layoutBoton3);
		HorizontalLayout layoutBotonAyuda = new HorizontalLayout();
		BottonHelpIcono = new ButtonIcon(ButtonIcon.AYUDA ,true ,this);
		BottonHelp = new ButtonIcon(ButtonIcon.AYUDA ,false ,this);
		BottonHelpIcono.setVisible(false);
		BottonHelp.setVisible(false);
//		layoutBotonAyuda.addComponent(BottonHelpIcono);
//		layoutBotonAyuda.addComponent(BottonHelp);
		LayoutBotonesDatos.addComponent(layoutBotonAyuda);
		
		//Esta editando o no
		if (!EditarDatos){
			Nombre.setReadOnly(true);
			Mail.setReadOnly(true);
			MailAlternativo.setReadOnly(true);
			BottonDescartarDatos.setVisible(false);
			BottonDescartarDatosIcon.setVisible(false);
		}
		layoutUser.addComponent(FormDatos);
		layoutUser.addComponent(LayoutBotonesDatos);
		HorizontalLayout layoutVacio = new HorizontalLayout();
		if (user.getTipoUsuario() == Dominios.TipoUsuario.ALUMNO && user.getAlumnoId() > 0){
			Label labelTituloAlumno = new Label(EtiquetasLabel.getAlumno());
			labelTituloAlumno.addStyleName(R.Style.EXPAND_WIDTH_ALL);
			labelTituloAlumno.addStyleName(R.Style.PADDING_MINIMO);
			labelTituloAlumno.setPrimaryStyleName(R.Style.TEXTO_SUBTITULO_COLOR);
			LayoutUserAcciones.addComponent(labelTituloAlumno);
			Alumno alumno = AlumnoRN.Read(user.getAlumnoId());
			LayoutUserAcciones.addComponent(new AlumnoABM(alumno, Dominios.AccesoADatos.SELECT, Ui, null));
			layoutVacio.setHeight("130px");
		}
		if (user.getTipoUsuario() == Dominios.TipoUsuario.PROFESOR && user.getProfesorId() > 0){
			Label labelTituloProfesor = new Label(EtiquetasLabel.getProfesor());
			labelTituloProfesor.addStyleName(R.Style.EXPAND_WIDTH_ALL);
			labelTituloProfesor.addStyleName(R.Style.PADDING_MINIMO);
			labelTituloProfesor.setPrimaryStyleName(R.Style.TEXTO_SUBTITULO_COLOR);
			LayoutUserAcciones.addComponent(labelTituloProfesor);
			Profesor profesor = ProfesorRN.Read(user.getProfesorId());
			LayoutUserAcciones.addComponent(new ProfesorABM(profesor, Dominios.AccesoADatos.SELECT, Ui, null));
			layoutVacio.setHeight("130px");
		}
		if (user.getTipoUsuario() != Dominios.TipoUsuario.PROFESOR && user.getTipoUsuario() != Dominios.TipoUsuario.ALUMNO){
			layoutVacio.setHeight("390px");
		}
		addComponent(layoutVacio);
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		UsuarioError.setValue("");
		if (event.getButton() == BottonCambiarClave || event.getButton() == BottonCambiarClaveIcon) {
			CambiarClaveLayout userChangePass = new CambiarClaveLayout();
			Window WinCambiarClave = new Window(EtiquetasLabel.getCambiarClave());
			WinCambiarClave.setModal(true);
			WinCambiarClave.setResizable(false);
			WinCambiarClave.setDraggable(false);
			WinCambiarClave.setWidth("420px");
			WinCambiarClave.setHeight("240px");
			WinCambiarClave.center();
			WinCambiarClave.setCloseShortcut(KeyCode.ESCAPE, null);
			WinCambiarClave.setContent(userChangePass);
			WinCambiarClave.addCloseListener(new CloseListener() {
				@Override
				public void windowClose(CloseEvent e) {
					onDraw();
				}
			});
			Ui.addWindow(WinCambiarClave);
			userChangePass.onDraw(WinCambiarClave , sesion.getUsuarioId());
		} else if ((event.getButton() == BottonRegister || event.getButton() == BottonRegisterIcon) && !EditarDatos){
			Nombre.setReadOnly(false);
			Mail.setReadOnly(false);
			MailAlternativo.setReadOnly(false);
			EditarDatos = true;
			BottonDescartarDatos.setVisible(true);
			BottonDescartarDatosIcon.setVisible(true);
			BottonCambiarClave.setVisible(false);
			BottonCambiarClaveIcon.setVisible(false);
			BottonRegisterIcon.setIcon(new ThemeResource("img/fila_save.png"));
			BottonRegister.setCaption(EtiquetasLabel.getGrabar());
			Nombre.focus();
		} else if ((event.getButton() == BottonRegister || event.getButton() == BottonRegisterIcon) && EditarDatos){
			user.setNombre(Nombre.getValue());
			user.setMailSecundario(MailAlternativo.getValue());
			RespuestaEntidad usuarioValido = UsuarioRN.Validate(user ,Dominios.AccesoADatos.UPDATE);
			if (!usuarioValido.isError()){
				UsuarioRN.Save(user);
				UsuarioRN.AfterSave(user ,Dominios.AccesoADatos.UPDATE);
				((Sesion)Ui.getSession().getAttribute(R.Session.SESSION)).setUsuarioNombre(Nombre.getValue());
				EditarDatos = false;
				onDraw();
			}
			else{
				UsuarioError.setValue(usuarioValido.getMsgError());
			}
		} else if (event.getButton() == BottonDescartarDatos || event.getButton() == BottonDescartarDatosIcon){
			EditarDatos = false;
			NombreError.setValue("");
			MailAlternativoError.setValue("");
			UsuarioError.setValue("");
			onDraw();
		}
	}

	@Override
	public void blur(BlurEvent event) {
		if (!EditarDatos){
		}
		else{
			if (event.getSource() == Nombre){
				NombreError.setValue("");
				RespuestaEntidad respuesta = UsuarioRN.ValidateNombre(Nombre.getValue());
				if (respuesta.isError()){
					NombreError.setValue(respuesta.getMsgError());
				}
			}
		}
	}
	
	class ImageUploader implements Receiver, SucceededListener {
	    public File file;
	    public OutputStream receiveUpload(String filename,String mimeType) {
	        FileOutputStream fos = null;
	        try {
	            file = new File(VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/VAADIN/temp/" + filename);
	            fos = new FileOutputStream(file);
	        } catch (final java.io.FileNotFoundException e) {
	        	MessageBox.showHTML(Icon.ERROR, "", "Could not open the file<br/>"+ e.getLocalizedMessage(), ButtonId.OK);
	            return null;
	        }
	        return fos;
	    }

	    public void uploadSucceeded(SucceededEvent event) {
	    	ImageUser.setSource(new FileResource(file));
			try {
				Blob thunbailnBlob = null;
				try{
					String nuevaImagen = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/VAADIN/temp/thunbainl" + file.getName();
					ImagenResize iamgenResize = new ImagenResize(file.getAbsolutePath(), nuevaImagen, 100, 0, 64);
					iamgenResize.resize();
					iamgenResize.getImgResult();
					File fileThunbailn = new File(nuevaImagen);
					thunbailnBlob = Hibernate.createBlob(new FileInputStream(fileThunbailn));
				}catch (Exception e){
					System.out.println(e.toString());
				}
				Blob imagenBlob = Hibernate.createBlob(new FileInputStream(file));
				user.setImagen(imagenBlob);
				user.setThumbnail(thunbailnBlob);
				UsuarioRN.Save(user);
				user = UsuarioRN.Read(user.getId());
				Ui.getSession().setAttribute(R.Session.SESSION ,sesion);
				((EscuelaUI)Ui).getHeader().DrawLogin();
			} catch (FileNotFoundException e) {
				MessageBox.showHTML(Icon.ERROR, "", "Could not save the file<br/>"+ e.getLocalizedMessage(), ButtonId.OK);
			} catch (IOException e) {
				MessageBox.showHTML(Icon.ERROR, "", "Could not save the file<br/>"+ e.getLocalizedMessage(), ButtonId.OK);
			}
	    	
	    }
	} 
}
