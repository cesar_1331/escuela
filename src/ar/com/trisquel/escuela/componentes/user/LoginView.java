package ar.com.trisquel.escuela.componentes.user;

import java.util.Date;

import javax.servlet.http.Cookie;

import ar.com.trisquel.escuela.EscuelaUI;
import ar.com.trisquel.escuela.componentes.otros.MiMenuView;
import ar.com.trisquel.escuela.componentes.personas.alumno.MisAsignaturasView;
import ar.com.trisquel.escuela.componentes.personas.profesor.MisAlumnosView;
import ar.com.trisquel.escuela.componentes.personas.profesor.MisCursosView;
import ar.com.trisquel.escuela.data.reglasdenegocio.ProfesorRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.UsuarioRN;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.seguridad.HerramientasSeguridad;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.sun.jersey.core.util.Base64;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class LoginView extends VerticalLayout implements View, ClickListener {

	public static final String VIEW = "Login"; 
	private TextField User;
	private PasswordField Pass;
	private CheckBox RecordarMe;
	private Button ButtonLogin;
	private Button ButtonForgotPass;
	private Sesion sesion;
	private UI ui;
	public LoginView() {
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		sesion = (Sesion)ui.getSession().getAttribute(R.Session.SESSION);
		ControlAcceso control = ControlAcceso.Control(LoginView.class ,ui ); 
		if (!control.isAcceso()){
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		}
		else{
			onDraw();
		}
	}
	public void onDraw()
	{
		removeAllComponents();
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		Label labelTitulo = new Label("Login");
		labelTitulo.setStyleName(R.Style.TITLE);
		labelTitulo.addStyleName(R.Style.MARGIN_MEDIO);
		addComponent(labelTitulo);
		Label labelSeparator = new Label("<hr>" ,ContentMode.HTML );
		labelSeparator.setStyleName(R.Style.EXPAND_WIDTH_ALL);
		addComponent(labelSeparator);
		
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout FormLogin = new GridLayout();
		FormLogin.setColumns(3);
		FormLogin.addStyleName(R.Style.MARGIN_MEDIO);
		//User
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		FormLogin.addComponent(imageIcono);
		Label labelUser = new Label(EtiquetasLabel.getUser());
		labelUser.addStyleName(R.Style.EDITABLE_LABEL);
		labelUser.setWidth("60px");
		FormLogin.addComponent(labelUser);
		User = new TextField();
		User.setNullRepresentation("");
		User.focus();
		FormLogin.addComponent(User);
		//User
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		FormLogin.addComponent(imageIcono);
		Label labelPass = new Label(EtiquetasLabel.getPass());
		labelPass.addStyleName(R.Style.EDITABLE_LABEL);
		FormLogin.addComponent(labelPass);
		Pass = new PasswordField();
		Pass.setNullRepresentation("");
		FormLogin.addComponent(Pass);
		FormLogin.addComponent(new Label());
		FormLogin.addComponent(new Label());
		RecordarMe = new CheckBox(EtiquetasLabel.getRecordarMe() ,false);
		FormLogin.addComponent(RecordarMe);
		//Botones
		FormLogin.addComponent(new Label());
		FormLogin.addComponent(new Label());
		HorizontalLayout LayoutBotones = new HorizontalLayout();
		ButtonLogin = new Button(EtiquetasLabel.getLogin());
		ButtonLogin.setPrimaryStyleName(R.Style.BUTTON_COLOR);
		LayoutBotones.addComponent(ButtonLogin);
		ButtonLogin.addClickListener(this);
		ButtonLogin.setClickShortcut(KeyCode.ENTER);
		FormLogin.addComponent(LayoutBotones);
		//Perdio Clave
		FormLogin.addComponent(new Label());
		FormLogin.addComponent(new Label());
		ButtonForgotPass = new Button(EtiquetasLabel.getNoPuedeIngresar());
		ButtonForgotPass.setPrimaryStyleName(R.Style.LINK_BLACK);
		FormLogin.addComponent(ButtonForgotPass);
		ButtonForgotPass.addClickListener(this);
		addComponent(FormLogin);
		VerticalLayout layoutVacio = new VerticalLayout();
		layoutVacio.setHeight("380px");
		addComponent(layoutVacio);
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == ButtonLogin){
			ButtonLogin.removeClickShortcut();
			Usuario user = UsuarioRN.Read(User.getValue());
			String errorMensaje = "";
			if (user != null){
				String passDesencriptada;
				try {
					passDesencriptada = HerramientasSeguridad.desencripta(user.getClave());
					if (passDesencriptada.equals(Pass.getValue())){
						if (user.getEstadoActual() == Dominios.EstadoUsuario.ACTIVO){
							sesion.setUsuarioId(user.getId());
							sesion.setUsuarioNombre(user.getNombre());
							sesion.setNavegarA(MiMenuView.VIEW);
							if (user.getTipoUsuario() == Dominios.TipoUsuario.ALUMNO && user.getAlumnoId() > 0 ){
								sesion.setNavegarA(MisAsignaturasView.VIEW);
							}
							else if (user.getTipoUsuario() == Dominios.TipoUsuario.PROFESOR && user.getProfesorId() > 0 ){
								Profesor profesor = ProfesorRN.Read(user.getProfesorId());
								if (profesor.isEsPreceptor()){
									sesion.setNavegarA(MisCursosView.VIEW);
								}
								else{
									sesion.setNavegarA(MisAlumnosView.VIEW);
								}
							}
							ui.getSession().setAttribute(R.Session.SESSION, sesion);
							((EscuelaUI)ui).getHeader().DrawLogin();
							((EscuelaUI)ui).getBody().onDraw();
							try{
								if (RecordarMe.getValue()){
									String stringCookie = HerramientasSeguridad.encripta(user.getId() + Dominios.RECORDARME.trim() + passDesencriptada);
									byte[] byteCookie = Base64.encode(stringCookie);
									Cookie cookie = new Cookie(Dominios.RECORDARME ,new String(byteCookie));
							        cookie.setPath(VaadinService.getCurrentRequest().getContextPath());
							        cookie.setMaxAge(640800); // 7 dias
							        VaadinService.getCurrentResponse().addCookie(cookie);
								}
								else{
									Cookie cookie = new Cookie(Dominios.RECORDARME ,"");
							        cookie.setPath(VaadinService.getCurrentRequest().getContextPath());
							        cookie.setMaxAge(1);
							        VaadinService.getCurrentResponse().addCookie(cookie);
								}
							}catch(Exception e){
								System.out.print(e.toString());
							}
						} else if (user.getEstadoActual() == Dominios.EstadoUsuario.DADO_DE_BAJA){
							errorMensaje = MensajeLabel.getUsuarioDadoDeBaja();
						} else if (user.getEstadoActual() == Dominios.EstadoUsuario.SUSPENDIDO){
							Date FechaSuspension = user.getFechaFinEstado();
							errorMensaje = MensajeLabel.getUsuarioSuspendido() + FechaSuspension.toString();
						}
						
					}else{
						errorMensaje = MensajeLabel.getUsuarioClaveNoValidos();
					}
				} catch (Exception e) {
					errorMensaje = MensajeLabel.getUsuarioClaveNoValidos();
				}
			}else{
				errorMensaje = MensajeLabel.getUsuarioClaveNoValidos();
			}
			if (!errorMensaje.isEmpty()){
				MessageBox.showHTML(Icon.ERROR, "", errorMensaje ,new MessageBoxListener(){
					@Override
					public void buttonClicked(ButtonId buttonType) {
						ButtonLogin.setClickShortcut(KeyCode.ENTER);
						User.focus();
					}
					
				} ,ButtonId.OK );
				
			}
			
		} else if (event.getButton() == ButtonForgotPass){
			sesion.setNavegarA(ForgotPasswordView.VIEW);
			ui.getSession().setAttribute(R.Session.SESSION, sesion);
			((EscuelaUI)ui).getBody().onDraw();
		}
	}
}
