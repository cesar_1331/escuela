package ar.com.trisquel.escuela.componentes.user;

import ar.com.trisquel.escuela.data.reglasdenegocio.UsuarioRN;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.seguridad.HerramientasSeguridad;
import ar.com.trisquel.escuela.utiles.ImageCaptcha;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

import com.vaadin.server.StreamResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
@SuppressWarnings("serial")
public class CambiarClaveLayout extends VerticalLayout implements ClickListener {
	
	private PasswordField Clave;
	private PasswordField ClaveRep;
	private TextField Captcha;
	private Button BottonChangePass;
	private Button BottonDescartar;
	private Usuario user ;
	private String StringCapcha;
	private Window win;
	public CambiarClaveLayout(){
	}

	public void onDraw(Window win ,String usuarioId){
		this.win = win;
		user = UsuarioRN.Read(usuarioId);

		Label vacio = new Label();
		vacio.addStyleName(R.Style.PADDING_MEDIO);
		addComponent(vacio);
		/*
		 * Cambiar Clave
		 */
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout FormClave = new GridLayout();
		FormClave.setColumns(3);
		// Clave
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		FormClave.addComponent(imageIcono);
		Label labelClave = new Label(EtiquetasLabel.getPass());
		labelClave.addStyleName(R.Style.EDITABLE_LABEL);
		labelClave.setWidth("150px");
		FormClave.addComponent(labelClave);
		Clave = new PasswordField();
		Clave.addStyleName(R.Style.EDITABLE);
		Clave.addStyleName(R.Style.LABEL_BOLD);
		Clave.focus();
		FormClave.addComponent(Clave);
		// Clave
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		FormClave.addComponent(imageIcono);
		Label labelClaveRep = new Label(EtiquetasLabel.getPass_Rep());
		labelClaveRep.addStyleName(R.Style.EDITABLE_LABEL);
		FormClave.addComponent(labelClaveRep);
		ClaveRep = new PasswordField();
		ClaveRep.addStyleName(R.Style.EDITABLE);
		ClaveRep.addStyleName(R.Style.LABEL_BOLD);
		FormClave.addComponent(ClaveRep); 
		// Captcha
		FormClave.addComponent(new Label());
		FormClave.addComponent(new Label());
		ImageCaptcha imageCaptcha = new ImageCaptcha();
		StreamResource.StreamSource imagesource = imageCaptcha;
		StringCapcha = imageCaptcha.getTextoCaptcha();
		StreamResource resource = new StreamResource(imagesource, imageCaptcha.getFileCaptcha());
		FormClave.addComponent(new Image(null, resource));
		//Ingrese el texto
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		FormClave.addComponent(imageIcono);
		Label labelCaptcha = new Label(EtiquetasLabel.getIngreseElTextoDeLaImagen());
		labelCaptcha.addStyleName(R.Style.EDITABLE_LABEL);
		FormClave.addComponent(labelCaptcha);
		Captcha = new TextField();
		FormClave.addComponent(Captcha);
		//Botones
		FormClave.addComponent(new Label());
		FormClave.addComponent(new Label());
		HorizontalLayout layoutBotones = new HorizontalLayout();
		BottonChangePass = new Button(EtiquetasLabel.getCambiarClave() ,this);
		BottonChangePass.setPrimaryStyleName(R.Style.BUTTON_COLOR);
		layoutBotones.addComponent(BottonChangePass);
		BottonDescartar = new Button(EtiquetasLabel.getDescartar() ,this);
		BottonDescartar.setPrimaryStyleName(R.Style.BUTTON_COLOR);
		layoutBotones.addComponent(BottonDescartar);
		FormClave.addComponent(layoutBotones);
		addComponent(FormClave);
	}
	
	
	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == BottonChangePass){
			if (!Clave.getValue().equals(ClaveRep.getValue())){
				MessageBox.showHTML(Icon.ERROR, "", MensajeLabel.getClavesDistintas(), ButtonId.OK);
			}
			else if(!Captcha.getValue().trim().equals(StringCapcha))
			{
				MessageBox.showHTML(Icon.ERROR,"",MensajeLabel.getElTextoIngresadoNoEsCorrecto() ,ButtonId.OK);
				Captcha.focus();
			}
			else{
				RespuestaEntidad respuesta = UsuarioRN.ValidateClave(Clave.getValue());
				if (!respuesta.isError()){
					MessageBox.showHTML(Icon.INFO, "", MensajeLabel.getConfirmaElCambio() ,new MessageBoxListener(){
						@Override
						public void buttonClicked(ButtonId button) {
							if (button.equals(ButtonId.OK)){
								try {
									user.setClave(HerramientasSeguridad.encripta(Clave.getValue()));
									UsuarioRN.Save(user);
									MessageBox.showHTML(Icon.INFO, "", MensajeLabel.getDatosGuardados(), ButtonId.OK);
									win.getUI().removeWindow(win);
								} catch (Exception e) {
									MessageBox.showHTML(Icon.ERROR, "", "Error<hr>" + e.toString(), ButtonId.OK);
								}
							}
						}
					}
					, ButtonId.OK
					,ButtonId.CANCEL);
				}else{
					MessageBox.showHTML(Icon.ERROR, "", respuesta.getMsgError(), ButtonId.OK);
				}
			}
		}
		else if (event.getButton() == BottonDescartar){
			win.getUI().removeWindow(win);
		}
	}
}
