package ar.com.trisquel.escuela.componentes.user;

import java.sql.Date;

import org.vaadin.hene.popupbutton.PopupButton;

import ar.com.trisquel.escuela.data.ImagenData;
import ar.com.trisquel.escuela.data.contenedor.UsuarioContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.UsuarioEstadoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.UsuarioRN;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.data.tablas.UsuarioEstado;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.language.UsuarioLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.StreamResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class UsuarioWWView extends VerticalLayout implements View ,ClickListener {
	public static final String VIEW = "UsuarioWW";
	private static final String LINK_VER = "LINK_VER";
	private static final String COLUMNA_IMAGEN = "COLUMNA_IMAGEN";
	private static final String COLUMNA_TIPO = "COLUMNA_TIPO";
	private static final String COLUMNA_ESTADO = "COLUMNA_ESTADO";
	private static final String COLUMNA_ACCIONES = "COLUMNA_ACCIONES";
	private ButtonIcon botonFiltros;
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonEliminar;
	private ButtonIcon botonRefrescar;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private TextField filtroNombre;
	private ComboBox filtroTipoUsuario;
	private ComboBox filtroEstado;
	private Table table;
	private VerticalLayout layoutFiltros;
	private UI ui;
	
	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass() ,ui); 
		if (!control.isAcceso()){
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		}
		else{
			onDraw();
		}
	}
	
	public void onDraw(){
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(EtiquetasLabel.getUsuarios().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>" ,ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true ,this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonFiltros = new ButtonIcon(ButtonIcon.BUSCAR, true ,this);
		botonFiltros.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonFiltros);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true ,this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		//botonAgregar.setEnabled(false);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true ,this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		//botonModificar.setEnabled(false);
		layoutBotones.addComponent(botonModificar);
		botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true ,this);
		botonEliminar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonEliminar);
		botonEliminar.setEnabled(false);
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true ,this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonExcel);
		botonExcel.setEnabled(false);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true ,this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonPDF);
		botonPDF.setEnabled(false);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		table = new Table();
        ArmaTabla();
        layout.addComponent(table);
        Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonFiltros){
			layoutFiltros.setVisible(!layoutFiltros.isVisible());
		}
		else if (event.getButton() == botonRefrescar){
			CreaDataSource();
		}
		else if (event.getButton() == botonModificar){
			Usuario usuario = (Usuario)table.getValue();
			if (usuario != null){
				Window windowABM = CreaWindowsABM();
				UsuarioABM usuarioABM = new UsuarioABM(usuario ,Dominios.AccesoADatos.UPDATE ,ui ,windowABM ,usuario.getAlumnoId() ,usuario.getProfesorId());
				windowABM.setContent(usuarioABM);
				windowABM.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						CreaDataSource();
					}
				});
				ui.addWindow(windowABM);
			}
		}
		else if(event.getButton() == botonAgregar){
			Window windowABM = CreaWindowsABM();
			UsuarioABM usuarioABM = new UsuarioABM(null ,Dominios.AccesoADatos.INSERT ,ui ,windowABM ,0 ,0);
			windowABM.setContent(usuarioABM);
			windowABM.addCloseListener(new CloseListener() {
				@Override
				public void windowClose(CloseEvent e) {
					CreaDataSource();
				}
			});
			ui.addWindow(windowABM);
		}
		else if (event.getButton() == botonEliminar ||
				event.getButton() == botonExcel ||
				event.getButton() == botonPDF ){
			/**
			 * No hace nada
			 */
		}
		
	}
	
	private void ArmaFiltros(){
		layoutFiltros.setVisible(false);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(6);
		// Tipo de Usuario
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label label = new Label(UsuarioLabel.getTipoUsuario());
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("90px");
		formFiltro.addComponent(label);
		filtroTipoUsuario = Dominios.TipoUsuario.CreaComboBox(true);
		filtroTipoUsuario.setNullSelectionAllowed(false);
		filtroTipoUsuario.addStyleName(R.Style.EDITABLE);
		filtroTipoUsuario.addStyleName(R.Style.LABEL_BOLD);
		filtroTipoUsuario.setWidth("200px");
		formFiltro.addComponent(filtroTipoUsuario); 
		// Estado
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label(UsuarioLabel.getEstadoTitulo());
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		filtroEstado = Dominios.EstadoUsuario.CreaComboBox(true);
		filtroEstado.setNullSelectionAllowed(false);
		filtroEstado.addStyleName(R.Style.EDITABLE);
		filtroEstado.addStyleName(R.Style.LABEL_BOLD);
		filtroEstado.setWidth("200px");
		formFiltro.addComponent(filtroEstado);
		// Nombre
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label(UsuarioLabel.getNombre());
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("90px");
		formFiltro.addComponent(label);
		filtroNombre = new TextField();
		filtroNombre.setNullRepresentation("");
		filtroNombre.setNullSettingAllowed(false);
		filtroNombre.addStyleName(R.Style.EDITABLE);
		filtroNombre.addStyleName(R.Style.LABEL_BOLD);
		filtroNombre.setWidth("300px");
		formFiltro.addComponent(filtroNombre);
		layoutFiltros.addComponent(formFiltro);
	}
	
	private void ArmaTabla(){
        table.setHeight("550px");
        table.setWidth("985px");
        table.setSelectable(true);
        table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
        	public Component generateCell(Table source, Object itemId, Object columnId) {
        		final Usuario usuario = (Usuario)itemId;
        		Button botonVer = new Button(usuario.getNombre());
        		botonVer.setPrimaryStyleName(R.Style.LINK);
        		botonVer.addStyleName(R.Style.LABEL_BOLD);
        		botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						Window windowABM = CreaWindowsABM();
						UsuarioABM usuarioABM = new UsuarioABM(usuario ,Dominios.AccesoADatos.SELECT ,ui ,windowABM ,0 ,0);
						windowABM.setContent(usuarioABM);
						ui.addWindow(windowABM);
					}
				});
        		return botonVer; 
        	}
        	
        });
        table.addGeneratedColumn(COLUMNA_ACCIONES, new Table.ColumnGenerator() {
        	public Component generateCell(Table source, Object itemId, Object columnId) {
        		final Usuario usuario = (Usuario)itemId;
        		HorizontalLayout layoutBotonAcciones = new HorizontalLayout();
        		final PopupButton botonAcciones = new PopupButton(EtiquetasLabel.getAccion());
        		botonAcciones.setPrimaryStyleName(R.Style.LINK_COLOR);
        		layoutBotonAcciones.addComponent(botonAcciones);
        		VerticalLayout layoutAcciones = new VerticalLayout();
        		botonAcciones.setContent(layoutAcciones);
        		
        		Button botonPermisos = new Button(EtiquetasLabel.getPermisos() ,new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {	
						Window window = CreaWindowsABM();
						window.setCaption(EtiquetasLabel.getPermisos());
						window.setWidth("500px");
						window.setHeight("500px");
						UsuarioPermisos usuarioPermisos = new UsuarioPermisos(usuario);
						window.setContent(usuarioPermisos);
						ui.addWindow(window);
						botonAcciones.setPopupVisible(false);
					}	
				});
        		botonPermisos.setPrimaryStyleName(R.Style.LINK_COLOR);
        		if (usuario.getTipoUsuario() == Dominios.TipoUsuario.ADMINISTRATIVO || usuario.getTipoUsuario() == Dominios.TipoUsuario.ADMINISTRADOR){
	        		layoutAcciones.addComponent(botonPermisos);
	        		Label separator = new Label();
	    			separator.addStyleName(R.Style.EXPAND_WIDTH_ALL);
	    			separator.addStyleName(R.Style.BORDER_MINIMO_ABAJO);
	    			layoutAcciones.addComponent(separator);
        		}
        		Button botonActivar = new Button(EtiquetasLabel.getActivar() ,new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {	
						MessageBox.showHTML(Icon.QUESTION, "", MensajeLabel.getConfirmaActivaUsuario() ,new MessageBoxListener(){
        					@Override
        					public void buttonClicked(ButtonId button) {
        						if (button.equals(ButtonId.OK)){
        							usuario.setEstadoActual(Dominios.EstadoUsuario.ACTIVO);
        							usuario.setFechaIniEstado(new Date(System.currentTimeMillis()));
        							usuario.setFechaFinEstado(null);
        							UsuarioRN.Save(usuario);
        							UsuarioEstado userEstado = UsuarioEstadoRN.Inizializate(usuario.getId());
        							userEstado.setFechaIni(usuario.getFechaIniEstado());
        							userEstado.setFechaFin(usuario.getFechaFinEstado());
        							userEstado.setMotivo(Dominios.EstadoUsuarioMotivo.ACTIVADO_POR_ADMINISTRADOR);
        							userEstado.setObservacion("");
        							userEstado.setResponsable(usuario.getId());
        							UsuarioEstadoRN.Save(userEstado);
        							onDraw();
        						}
        					}
        				} 
        				,ButtonId.OK
        				,ButtonId.CANCEL);
					}
				});
    			botonActivar.setPrimaryStyleName(R.Style.LINK_COLOR);
        		Button botonSuspender = new Button(EtiquetasLabel.getSuspender() ,new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						MessageBox.showHTML(Icon.QUESTION, "", MensajeLabel.getConfirmaSuspendeUsuario() ,new MessageBoxListener(){
        					@Override
        					public void buttonClicked(ButtonId button) {
        						if (button.equals(ButtonId.OK)){
        							usuario.setEstadoActual(Dominios.EstadoUsuario.SUSPENDIDO);
        							usuario.setFechaIniEstado(new Date(System.currentTimeMillis()));
        							usuario.setFechaFinEstado(null);
        							UsuarioRN.Save(usuario);
        							UsuarioEstado userEstado = UsuarioEstadoRN.Inizializate(usuario.getId());
        							userEstado.setFechaIni(usuario.getFechaIniEstado());
        							userEstado.setFechaFin(usuario.getFechaFinEstado());
        							userEstado.setMotivo(Dominios.EstadoUsuarioMotivo.SUSPENDIDO_POR_ADMINISTRADOR);
        							userEstado.setObservacion("");
        							userEstado.setResponsable(usuario.getId());
        							UsuarioEstadoRN.Save(userEstado);
        							onDraw();
        						}
        					}
        				} 
        				,ButtonId.OK
        				,ButtonId.CANCEL);
					}
				});
        		botonSuspender.setPrimaryStyleName(R.Style.LINK_COLOR);
        		Button botonDarDeBaja = new Button(EtiquetasLabel.getDarDeBaja() ,new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {	
						MessageBox.showHTML(Icon.QUESTION, "", MensajeLabel.getConfirmaDarDeBajaUsuario() ,new MessageBoxListener(){
        					@Override
        					public void buttonClicked(ButtonId button) {
        						if (button.equals(ButtonId.OK)){
        							usuario.setEstadoActual(Dominios.EstadoUsuario.DADO_DE_BAJA);
        							usuario.setFechaIniEstado(new Date(System.currentTimeMillis()));
        							usuario.setFechaFinEstado(null);
        							UsuarioRN.Save(usuario);
        							UsuarioEstado userEstado = UsuarioEstadoRN.Inizializate(usuario.getId());
        							userEstado.setFechaIni(usuario.getFechaIniEstado());
        							userEstado.setFechaFin(usuario.getFechaFinEstado());
        							userEstado.setMotivo(Dominios.EstadoUsuarioMotivo.DADO_DE_BAJA_POR_ADMINISTRADOR);
        							userEstado.setObservacion("");
        							userEstado.setResponsable(usuario.getId());
        							UsuarioEstadoRN.Save(userEstado);
        							onDraw();
        						}
        					}
        				} 
        				,ButtonId.OK
        				,ButtonId.CANCEL);
					}
				});
        		botonDarDeBaja.setPrimaryStyleName(R.Style.LINK_COLOR);
        		Button botonCambiarClave = new Button(EtiquetasLabel.getCambiarClave() ,new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						CambiarClaveLayout userChangePass = new CambiarClaveLayout();
        				Window WinCambiarClave = new Window(EtiquetasLabel.getCambiarClave());
        				WinCambiarClave.setModal(true);
        				WinCambiarClave.setResizable(false);
        				WinCambiarClave.setDraggable(false);
        				WinCambiarClave.setWidth("420px");
        				WinCambiarClave.setHeight("240px");
        				WinCambiarClave.center();
        				WinCambiarClave.setCloseShortcut(KeyCode.ESCAPE, null);
        				WinCambiarClave.setContent(userChangePass);
        				ui.addWindow(WinCambiarClave);
        				userChangePass.onDraw(WinCambiarClave , usuario.getId());
					}
				});
        		botonCambiarClave.setPrimaryStyleName(R.Style.LINK_COLOR);
        		if (usuario.getEstadoActual() == Dominios.EstadoUsuario.ACTIVO){
        			layoutAcciones.addComponent(botonSuspender);
        			Label separator = new Label();
        			separator.addStyleName(R.Style.EXPAND_WIDTH_ALL);
        			separator.addStyleName(R.Style.BORDER_MINIMO_ABAJO);
        			layoutAcciones.addComponent(separator);
        			layoutAcciones.addComponent(botonDarDeBaja);
        			separator = new Label();
        			separator.addStyleName(R.Style.EXPAND_WIDTH_ALL);
        			separator.addStyleName(R.Style.BORDER_MINIMO_ABAJO);
        			layoutAcciones.addComponent(separator);
        			layoutAcciones.addComponent(botonCambiarClave);
        		}
        		if (usuario.getEstadoActual() == Dominios.EstadoUsuario.DADO_DE_BAJA){
        			layoutAcciones.addComponent(botonActivar);
        		}
        		if (usuario.getEstadoActual() == Dominios.EstadoUsuario.SUSPENDIDO){
        			layoutAcciones.addComponent(botonActivar);
        			Label separator = new Label();
        			separator.addStyleName(R.Style.EXPAND_WIDTH_ALL);
        			separator.addStyleName(R.Style.BORDER_MINIMO_ABAJO);
        			layoutAcciones.addComponent(separator);
        			layoutAcciones.addComponent(botonDarDeBaja);
        			separator = new Label();
        			separator.addStyleName(R.Style.EXPAND_WIDTH_ALL);
        			separator.addStyleName(R.Style.BORDER_MINIMO_ABAJO);
        			layoutAcciones.addComponent(separator);
        			layoutAcciones.addComponent(botonCambiarClave);
        		}
        		return layoutBotonAcciones; 
        	}
        	
        });
        table.addGeneratedColumn(COLUMNA_TIPO, new Table.ColumnGenerator() {
        	public Component generateCell(Table source, Object itemId, Object columnId) {
        		final Usuario usuario = (Usuario)itemId;
        		return new Label(Dominios.TipoUsuario.ARRAY[usuario.getTipoUsuario()]);
        	}
        });
        table.addGeneratedColumn(COLUMNA_ESTADO, new Table.ColumnGenerator() {
        	public Component generateCell(Table source, Object itemId, Object columnId) {
        		final Usuario usuario = (Usuario)itemId;
        		return new Label(Dominios.EstadoUsuario.ARRAY[usuario.getEstadoActual()]);
        	}
        });
        table.addGeneratedColumn(COLUMNA_IMAGEN, new Table.ColumnGenerator() {
        	public Component generateCell(Table source, Object itemId, Object columnId) {
        		final Usuario usuario = (Usuario)itemId;
        		Image imagen = new Image();
        		imagen.setWidth("32px");
        		imagen.setWidth("32px");
    			if (usuario.getThumbnail() != null){
    				ImagenData blob = new ImagenData(usuario.getThumbnail()); 
    				imagen.setSource(new StreamResource(blob ,usuario.getThumbnail().toString()+".png"));
    			}
    			else{
    				imagen.setSource(new ThemeResource("img/user.png"));
    			}
        		return imagen;
        	}
        });
        CreaDataSource();
	}
	
	private void CreaDataSource(){
		int estado = 0;
		int tipoUsuario = 0;
		try{
			if (filtroEstado.getValue() != null)
				estado = (Integer) filtroEstado.getValue();
			if (filtroTipoUsuario.getValue() != null)
				tipoUsuario = (Integer) filtroTipoUsuario.getValue();
		}catch (Exception e){
		}
		table.setContainerDataSource(UsuarioContenedor.LeeContainerXNombreXEstado(filtroNombre.getValue() ,estado ,tipoUsuario));
		table.setVisibleColumns((Object[]) new String[] {COLUMNA_ACCIONES ,COLUMNA_IMAGEN , "id",LINK_VER,"mailPrincipal" , COLUMNA_TIPO ,COLUMNA_ESTADO});
        table.setColumnHeaders("" ,"" ,UsuarioLabel.getIdTitulo(),UsuarioLabel.getNombreTitulo(),UsuarioLabel.getMailTitulo() , UsuarioLabel.getTipoUsuarioTitulo() ,UsuarioLabel.getEstadoTitulo());
	}
	
	public static Window CreaWindowsABM(){
		Window windowABM = new Window(EtiquetasLabel.getUserTitle());
		windowABM.setModal(true);
		windowABM.setResizable(false);
		windowABM.setDraggable(false);
		windowABM.setWidth("600px");
		windowABM.setHeight("220px");
		windowABM.center();
		windowABM.setCloseShortcut(KeyCode.ESCAPE, null);
		return windowABM;
	}
}
