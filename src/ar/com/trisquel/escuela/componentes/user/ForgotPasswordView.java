package ar.com.trisquel.escuela.componentes.user;

import java.util.ArrayList;

import ar.com.trisquel.escuela.EscuelaUI;
import ar.com.trisquel.escuela.data.reglasdenegocio.ArticuloRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.UsuarioRN;
import ar.com.trisquel.escuela.data.tablas.Articulo;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.seguridad.HerramientasSeguridad;
import ar.com.trisquel.escuela.utiles.EnvioMail;
import ar.com.trisquel.escuela.utiles.ImageCaptcha;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class ForgotPasswordView extends VerticalLayout implements ClickListener, View {
	
	public static final String VIEW = "RecuperarPassword";
	private Button ButtonRestablecer;
	private TextField CaptchaClave;
	private TextField User;
	private TextField MailClave;
	private FormLayout FormClave; 
	private Label labelTitulo;
	private Usuario usuario;
	private Sesion sesion;
	private UI Ui;
	private String StringCapcha;
	
	public ForgotPasswordView() {
	}

	public ForgotPasswordView(Component... children) {
		super(children);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		Ui = event.getNavigator().getUI();
		sesion = (Sesion)Ui.getSession().getAttribute(R.Session.SESSION);
		ControlAcceso control = ControlAcceso.Control(ForgotPasswordView.class ,Ui); 
		if (!control.isAcceso()){
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		}
		else{
			onDraw();
		}	
	}

	public void onDraw(){
		removeAllComponents();
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		labelTitulo = new Label(EtiquetasLabel.getForgotPass());
		labelTitulo.addStyleName(R.Style.TITLE);
		labelTitulo.addStyleName(R.Style.MARGIN_MEDIO);
		addComponent(labelTitulo);
		Label labelSeparator = new Label("<hr>" ,ContentMode.HTML);
		labelSeparator.addStyleName(R.Style.EXPAND_WIDTH_ALL);
		addComponent(labelSeparator);
		
		ImageCaptcha imageCaptcha = new ImageCaptcha();
		StreamResource.StreamSource imagesource = imageCaptcha;
		StringCapcha = imageCaptcha.getTextoCaptcha();
		StreamResource resource = new StreamResource(imagesource, imageCaptcha.getFileCaptcha());
		FormClave = new FormLayout();
		User = new TextField(EtiquetasLabel.getUser());
		User.setWidth("200px");
		User.setNullRepresentation("");
		FormClave.addComponent(User);
		MailClave = new TextField(EtiquetasLabel.getMail());
		MailClave.setWidth("200px");
		MailClave.setNullRepresentation("");
		FormClave.addComponent(MailClave);
		FormClave.addComponent(new Image(null, resource));
		CaptchaClave = new TextField(EtiquetasLabel.getIngreseElTextoDeLaImagen());
		FormClave.addComponent(CaptchaClave);
		ButtonRestablecer = new Button(EtiquetasLabel.getEnviar(), this);
		ButtonRestablecer.setPrimaryStyleName(R.Style.BUTTON_COLOR);
		FormClave.addComponent(ButtonRestablecer);
		addComponent(FormClave);
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == ButtonRestablecer){
			if(!CaptchaClave.getValue().trim().equals(StringCapcha))
			{
				MessageBox.showHTML(Icon.ERROR, "",MensajeLabel.getElTextoIngresadoNoEsCorrecto(), ButtonId.OK);
				CaptchaClave.focus();
			}
			else{
				usuario = UsuarioRN.Read(User.getValue());
				if (usuario == null){
					MessageBox.showHTML(Icon.ERROR, "", MensajeLabel.getMensajeUsuarioNoExiste() , ButtonId.OK);
				}
				else if(usuario.getMailPrincipal().trim().isEmpty() && usuario.getMailSecundario().trim().isEmpty()){
					MessageBox.showHTML(Icon.ERROR, "", MensajeLabel.getErrorErrorUsuarioSinMail() , ButtonId.OK);
				}
				else {
					if (usuario.getMailPrincipal().equals(MailClave.getValue()) || 
						usuario.getMailSecundario().equals(MailClave.getValue()))
						EnviarMail(R.Paginas.MAILREESTABLECERCLAVE ,EtiquetasLabel.getForgotPass() ,MensajeLabel.getRestaurarClave() ,MailClave.getValue());
					else{
						MessageBox.showHTML(Icon.ERROR, "", MensajeLabel.getMensajeMailNoCorrespondeUsuario(), ButtonId.OK);
					}
				}
			}
		}
	}
	
	private void EnviarMail(String pagina ,String asunto ,String mensaje ,String mail){
		try {
			Articulo articulo = ArticuloRN.Read(pagina);
			String MensajeMail = "Nombre: [NOMBRE]<br>" +
					"Usuario: [USUARIO]<br>" +
					"Clave: [CLAVE]";
			if (articulo != null){
				MensajeMail = articulo.getContenido(); 
			}
			 
			MensajeMail = MensajeMail.replace("[NOMBRE]", usuario.getNombre());
			MensajeMail = MensajeMail.replace("[USUARIO]", usuario.getId());
			MensajeMail = MensajeMail.replace("[CLAVE]", HerramientasSeguridad.desencripta(usuario.getClave())); 
			RespuestaEntidad respuesta = EnvioMail.Enviar(mail, asunto, MensajeMail ,new ArrayList<String>());
			if (!respuesta.isError()){
				MessageBox.showHTML(Icon.INFO, "",mensaje, ButtonId.OK);
				sesion.setNavegarA(R.Paginas.INICIO);
				sesion.setArticuloSeleccionado(R.Paginas.INICIO);
				Ui.getSession().setAttribute(R.Session.SESSION, sesion);
				((EscuelaUI)Ui).getBody().onDraw();
			}
			else{
				MessageBox.showHTML(Icon.ERROR," ",MensajeLabel.getErrorEnviarMail() + "<hr>" + respuesta.getMsgError() , ButtonId.OK);
			}
			
		} catch (Exception e) {
			MessageBox.showHTML(Icon.ERROR, "",MensajeLabel.getErrorEnviarMail()+ "<hr>"+e.getLocalizedMessage() , ButtonId.OK);
		}
	}
}
