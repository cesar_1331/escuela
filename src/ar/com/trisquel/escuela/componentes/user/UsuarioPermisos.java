package ar.com.trisquel.escuela.componentes.user;

import ar.com.trisquel.escuela.componentes.asignatura.AsignaturaWWView;
import ar.com.trisquel.escuela.componentes.asignatura.CursoWWView;
import ar.com.trisquel.escuela.componentes.comprobante.ComprobanteBalanceView;
import ar.com.trisquel.escuela.componentes.comprobante.ComprobanteBloqueoView;
import ar.com.trisquel.escuela.componentes.comprobante.ComprobantesView;
import ar.com.trisquel.escuela.componentes.materias.MateriaWWView;
import ar.com.trisquel.escuela.componentes.otros.ArticuloWWView;
import ar.com.trisquel.escuela.componentes.otros.ErrorLogWWView;
import ar.com.trisquel.escuela.componentes.otros.MensajeWWView;
import ar.com.trisquel.escuela.componentes.otros.NotificacionWWView;
import ar.com.trisquel.escuela.componentes.parametro.EscuelaView;
import ar.com.trisquel.escuela.componentes.personas.alumno.AlumnoWWView;
import ar.com.trisquel.escuela.componentes.personas.profesor.ProfesorWWView;
import ar.com.trisquel.escuela.componentes.personas.provedorcliente.PersonasWWView;
import ar.com.trisquel.escuela.componentes.tipocomprobante.TipoComprobanteWWView;
import ar.com.trisquel.escuela.data.contenedor.PermisoContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.PermisoRN;
import ar.com.trisquel.escuela.data.tablas.Permiso;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class UsuarioPermisos extends VerticalLayout {

	private String COLUMNA_ACCESO = "COLUMNA_ACCESO";
	private Table table;
	private Usuario usuario;
	public UsuarioPermisos(Usuario usuario){
		this.usuario = usuario;
		CreaPermisos(usuario);
		onDraw();
	}
	
	private void onDraw(){
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(usuario.getNombre().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>" ,ContentMode.HTML);
		layout.addComponent(Separator);
		
		table = new Table();
		layout.addComponent(table);
		table.setHeight("390px");
        table.setWidth("480px");
        table.setSelectable(true);
        table.addGeneratedColumn(COLUMNA_ACCESO, new Table.ColumnGenerator() {
        	public Component generateCell(Table source, Object itemId, Object columnId) {
        		final Permiso permiso = (Permiso)itemId;
        		CheckBox acceso = new CheckBox();
        		acceso.setValue(permiso.isAcceso());
        		acceso.addValueChangeListener(new ValueChangeListener() {
					@Override
					public void valueChange(ValueChangeEvent event) {
						permiso.setAcceso(!permiso.isAcceso());
						PermisoRN.Save(permiso);
					}
				});
        		return acceso; 
        	}
        });
		CreaDataSource();
	}
	
	private void CreaDataSource(){
		table.setContainerDataSource(PermisoContenedor.LeeContainerXUsuario(usuario.getId()));
		table.setVisibleColumns((Object[]) new String[] {"nombre" , COLUMNA_ACCESO});
        table.setColumnHeaders("Opcion" ,"Acceso");
        table.setColumnWidth("nombre", 340);
	}
	
	public static void CreaPermisos(Usuario usuario){
		if (usuario.getTipoUsuario() == Dominios.TipoUsuario.ADMINISTRATIVO || usuario.getTipoUsuario() == Dominios.TipoUsuario.ADMINISTRADOR){
			PermisoRN.Add(PermisoRN.Inizializate(usuario.getId(), MateriaWWView.VIEW, EtiquetasLabel.getMaterias()));
			PermisoRN.Add(PermisoRN.Inizializate(usuario.getId(), ProfesorWWView.VIEW, EtiquetasLabel.getProfesoresPreceptores()));
			PermisoRN.Add(PermisoRN.Inizializate(usuario.getId(), AlumnoWWView.VIEW, EtiquetasLabel.getAlumnos()));
			PermisoRN.Add(PermisoRN.Inizializate(usuario.getId(), AsignaturaWWView.VIEW, EtiquetasLabel.getAsignaturas()));
			PermisoRN.Add(PermisoRN.Inizializate(usuario.getId(), CursoWWView.VIEW, EtiquetasLabel.getCursos()));
			PermisoRN.Add(PermisoRN.Inizializate(usuario.getId(), EscuelaView.VIEW, EtiquetasLabel.getDatosEscuela()));
			PermisoRN.Add(PermisoRN.Inizializate(usuario.getId(), NotificacionWWView.VIEW, EtiquetasLabel.getNotificaciones()));
			PermisoRN.Add(PermisoRN.Inizializate(usuario.getId(), MensajeWWView.VIEW, EtiquetasLabel.getMensajes()));
			PermisoRN.Add(PermisoRN.Inizializate(usuario.getId(), PersonasWWView.VIEW, EtiquetasLabel.getPersonas()));
			PermisoRN.Add(PermisoRN.Inizializate(usuario.getId(), TipoComprobanteWWView.VIEW, EtiquetasLabel.getTipoComprobantes()));
			PermisoRN.Add(PermisoRN.Inizializate(usuario.getId(), ComprobantesView.VIEW, EtiquetasLabel.getComprobantes()));
			PermisoRN.Add(PermisoRN.Inizializate(usuario.getId(), ComprobanteBloqueoView.VIEW, EtiquetasLabel.getBloqueosComprobantes()));
			PermisoRN.Add(PermisoRN.Inizializate(usuario.getId(), ComprobanteBalanceView.VIEW, EtiquetasLabel.getBalance()));
		}
		if (usuario.getTipoUsuario() == Dominios.TipoUsuario.ADMINISTRADOR){
			PermisoRN.Add(PermisoRN.Inizializate(usuario.getId(), UsuarioWWView.VIEW, EtiquetasLabel.getUsuarios()));
			PermisoRN.Add(PermisoRN.Inizializate(usuario.getId(), ArticuloWWView.VIEW, EtiquetasLabel.getPaginas()));
			PermisoRN.Add(PermisoRN.Inizializate(usuario.getId(), ErrorLogWWView.VIEW, EtiquetasLabel.getErrores()));
		}
	}
}
