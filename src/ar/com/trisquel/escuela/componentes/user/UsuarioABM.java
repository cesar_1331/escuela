package ar.com.trisquel.escuela.componentes.user;

import ar.com.trisquel.escuela.data.reglasdenegocio.AlumnoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ProfesorRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.UsuarioRN;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.language.UsuarioLabel;
import ar.com.trisquel.escuela.seguridad.HerramientasSeguridad;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class UsuarioABM extends VerticalLayout implements ClickListener{
	private TextField id;
	private TextField nombre;
	private TextField mailPrincipal;
	private TextField mailSecundario;
	private ComboBox tipoUsuario;
	private ButtonIcon ButtonGuardar;
	private ButtonIcon ButtonDescartar;
	private Label usuarioError;
	private UI ui;
	private String modo;
	private Usuario usuario;
	private Window window;
	private long alumnoId;
	private long profesorId;

	public UsuarioABM(Usuario usuario, String modo, UI ui, Window window ,long alumnoId ,long profesorId) {
		this.ui = ui;
		this.usuario = usuario;
		this.modo = modo;
		this.window = window;
		this.alumnoId = alumnoId; 
		this.profesorId = profesorId;
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setWidth("100%");
		layout.setColumns(3);
		addComponent(layout);
		//Id
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelId = new Label(UsuarioLabel.getId());
		labelId.addStyleName(R.Style.EDITABLE_LABEL);
		labelId.setWidth("90px");
		layout.addComponent(labelId);
		HorizontalLayout layoutId = new HorizontalLayout();
		layoutId.setWidth("450px");
		id = new TextField();
		id.setMaxLength(20);
		id.addStyleName(R.Style.EDITABLE);
		id.addStyleName(R.Style.LABEL_BOLD);
		layoutId.addComponent(id);
		//Botones
		HorizontalLayout layoutBotones = new HorizontalLayout();
		ButtonGuardar = new ButtonIcon(ButtonIcon.GUARDAR, true, this);
		ButtonGuardar.addStyleName(R.Style.PADDING_MINIMO);
		ButtonGuardar.setTabIndex(101);
		layoutBotones.addComponent(ButtonGuardar);
		ButtonDescartar = new ButtonIcon(ButtonIcon.DESCARTAR, true, this);
		ButtonDescartar.addStyleName(R.Style.PADDING_MINIMO);
		ButtonDescartar.setTabIndex(102);
		layoutBotones.addComponent(ButtonDescartar);
		layoutId.addComponent(layoutBotones);
		layoutId.setComponentAlignment(layoutBotones, Alignment.TOP_RIGHT);
		layout.addComponent(layoutId);
		//Nombre
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelTitulo = new Label(UsuarioLabel.getNombre());
		labelTitulo.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(labelTitulo);
		nombre = new TextField();
		nombre.setWidth("300px");
		nombre.setMaxLength(60);
		nombre.addStyleName(R.Style.TEXTO_MAYUSCULA);
		nombre.addStyleName(R.Style.EDITABLE);
		nombre.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(nombre);
		//Mail Principal
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelMailPrincipal = new Label(UsuarioLabel.getMailPrincipal());
		labelMailPrincipal.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(labelMailPrincipal);
		mailPrincipal = new TextField();
		mailPrincipal.setWidth("400px");
		mailPrincipal.setMaxLength(300);
		mailPrincipal.addStyleName(R.Style.EDITABLE);
		mailPrincipal.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(mailPrincipal);
		//Mail Secundario
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelMailSecundario = new Label(UsuarioLabel.getMailSecundario());
		labelMailSecundario.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(labelMailSecundario);
		mailSecundario = new TextField();
		mailSecundario.setWidth("400px");
		mailSecundario.setMaxLength(300);
		mailSecundario.addStyleName(R.Style.EDITABLE);
		mailSecundario.addStyleName(R.Style.LABEL_BOLD);
		layout.addComponent(mailSecundario);
		//Tipo de Usuario
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label labelTipoUsuario = new Label(UsuarioLabel.getTipoUsuario());
		labelTipoUsuario.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(labelTipoUsuario);
		tipoUsuario = new ComboBox();
		tipoUsuario.setNullSelectionAllowed(false);
		tipoUsuario.addItem(Dominios.TipoUsuario.ADMINISTRATIVO);
		tipoUsuario.setItemCaption(Dominios.TipoUsuario.ADMINISTRATIVO, Dominios.TipoUsuario.ARRAY[Dominios.TipoUsuario.ADMINISTRATIVO]);
		tipoUsuario.addItem(Dominios.TipoUsuario.ADMINISTRADOR);
		tipoUsuario.setItemCaption(Dominios.TipoUsuario.ADMINISTRADOR, Dominios.TipoUsuario.ARRAY[Dominios.TipoUsuario.ADMINISTRADOR]);
		layout.addComponent(tipoUsuario);
		if (alumnoId > 0){
			Alumno alumno = AlumnoRN.Read(alumnoId);
			nombre.setValue(alumno.getApellido().trim() + ", " + alumno.getNombre().trim());
			tipoUsuario.addItem(Dominios.TipoUsuario.ALUMNO);
			tipoUsuario.setItemCaption(Dominios.TipoUsuario.ALUMNO, Dominios.TipoUsuario.ARRAY[Dominios.TipoUsuario.ALUMNO]);
			tipoUsuario.setValue(Dominios.TipoUsuario.ALUMNO);
		}
		else if (profesorId > 0){
			Profesor profesor = ProfesorRN.Read(profesorId);
			nombre.setValue(profesor.getApellido().trim() + ", " + profesor.getNombre().trim());
			tipoUsuario.addItem(Dominios.TipoUsuario.PROFESOR);
			tipoUsuario.setItemCaption(Dominios.TipoUsuario.PROFESOR, Dominios.TipoUsuario.ARRAY[Dominios.TipoUsuario.PROFESOR]);
			tipoUsuario.setValue(Dominios.TipoUsuario.PROFESOR);
		}
		else{
			tipoUsuario.setValue(Dominios.TipoUsuario.ADMINISTRATIVO);
		}
		layout.addComponent(new Label());
		layout.addComponent(new Label());
		usuarioError = new Label();
		usuarioError.addStyleName(R.Style.ERROR_VIEW);
		layout.addComponent(usuarioError);
		if (modo.equals(Dominios.AccesoADatos.INSERT)){
			if (alumnoId > 0 || profesorId > 0){
				nombre.setReadOnly(true);
				tipoUsuario.setReadOnly(true);
			}
			ButtonGuardar.setVisible(true);
			ButtonDescartar.setVisible(true);
			id.focus();
		}
		else{
			id.setValue(usuario.getId());
			id.setReadOnly(true);
			mailPrincipal.setValue(usuario.getMailPrincipal());
			mailSecundario.setValue(usuario.getMailSecundario());
			tipoUsuario.setValue(usuario.getTipoUsuario());
			if (alumnoId > 0 || profesorId > 0){
				nombre.setReadOnly(true);
				tipoUsuario.setReadOnly(true);
				mailPrincipal.focus();
			}
			else{
				nombre.setValue(usuario.getNombre());
				nombre.focus();
			}
			if (modo.equals(Dominios.AccesoADatos.SELECT)) {
				id.setReadOnly(true);
				nombre.setReadOnly(true);
				mailPrincipal.setReadOnly(true);
				mailSecundario.setReadOnly(true);
				tipoUsuario.setReadOnly(true);
				ButtonGuardar.setVisible(false);
				ButtonDescartar.setVisible(false);
			} else if (modo.equals(Dominios.AccesoADatos.UPDATE)) {
				ButtonGuardar.setVisible(true);
				ButtonDescartar.setVisible(true);
			}
		}
		
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == ButtonDescartar) {
			if (modo.equals(Dominios.AccesoADatos.INSERT) || modo.equals(Dominios.AccesoADatos.UPDATE)) {
				ui.removeWindow(window);
			}
			else{
				onDraw();
			}
		} else if (event.getButton() == ButtonGuardar) {
			if (usuario == null){
				usuario = UsuarioRN.Inizializate();
			}
			usuario.setId(id.getValue());
			usuario.setNombre(nombre.getValue());
			usuario.setMailPrincipal(mailPrincipal.getValue());
			usuario.setMailSecundario(mailSecundario.getValue());
			usuario.setTipoUsuario(Integer.parseInt(tipoUsuario.getValue().toString()));
			if (alumnoId > 0){
				usuario.setAlumnoId(alumnoId);
			}
			if (profesorId > 0){
				usuario.setProfesorId(profesorId);
			}
			RespuestaEntidad respuesta = UsuarioRN.Validate(usuario ,modo);
			if (respuesta.isError()) {
				MessageBox.showHTML(Icon.ERROR, "", respuesta.getMsgError(),ButtonId.OK);
			} else {
				MessageBox.showHTML(Icon.QUESTION, "",MensajeLabel.getConfirmaAccion(),new MessageBoxListener() {
					@Override
					public void buttonClicked(ButtonId buttonType) {
						if (buttonType == ButtonId.SAVE) {
							RespuestaEntidad respuesta = new RespuestaEntidad();
							try {
								if (modo.equals(Dominios.AccesoADatos.INSERT)){
									usuario.setClave(HerramientasSeguridad.encripta(id.getValue().trim().toLowerCase()));
								}
								respuesta = UsuarioRN.Save(usuario);
							} catch (Exception e) {
								respuesta = new RespuestaEntidad(true, e.toString()); 
							}
							if (respuesta.isError()) {
								MessageBox.showHTML(Icon.ERROR, "",respuesta.getMsgError(),ButtonId.OK);
							} else {
								if (modo.equals(Dominios.AccesoADatos.INSERT) || modo.equals(Dominios.AccesoADatos.UPDATE)) {
									if (modo.equals(Dominios.AccesoADatos.INSERT)){
										String mensaje = MensajeLabel.getUsuarioCreado() + id.getValue().trim().toLowerCase();
										MessageBox.showHTML(Icon.QUESTION, "", mensaje ,ButtonId.OK);
										UsuarioPermisos.CreaPermisos(usuario);
									}
									ui.removeWindow(window);
								} else {
									onDraw();
								}
							}
						}
					}
				}, ButtonId.SAVE, ButtonId.CANCEL);
			}
		}
	}

}
