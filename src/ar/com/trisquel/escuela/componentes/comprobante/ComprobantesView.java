package ar.com.trisquel.escuela.componentes.comprobante;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import org.vaadin.hene.popupbutton.PopupButton;

import ar.com.trisquel.escuela.data.contenedor.ComprobanteContenedor;
import ar.com.trisquel.escuela.data.contenedor.PersonasContenedor;
import ar.com.trisquel.escuela.data.contenedor.TipoComprobanteContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.ComprobanteBloqueoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.PersonasRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.TipoComprobanteRN;
import ar.com.trisquel.escuela.data.tablas.Comprobante;
import ar.com.trisquel.escuela.data.tablas.ComprobanteBloqueo;
import ar.com.trisquel.escuela.data.tablas.Personas;
import ar.com.trisquel.escuela.data.tablas.TipoComprobante;
import ar.com.trisquel.escuela.data.tablas.identificadores.AnioMesId;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.Funciones;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class ComprobantesView extends VerticalLayout implements ClickListener,View {

	public static final String VIEW = "ComprobanteWW";
	private static final String LINK_VER = "LINK_VER";
	private static final String COLUMNA_ACCIONES = "COLUMNA_ACCIONES";
	private static final String COLUMNA_FECHA = "COLUMNA_FECHA";
	private static final String COLUMNA_TIPOCOMPROBANTE = "COLUMNA_TIPOCOMPROBANTE";
	private static final String COLUMNA_NUMERO = "COLUMNA_NUMERO";
	private static final String COLUMNA_TOTAL = "COLUMNA_TOTAL";
	private ButtonIcon botonFiltros;
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonEliminar;
	private ButtonIcon botonRefrescar;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private TextField filtroNumero;
	private DateField filtroFechaDesde;
	private DateField filtroFechaHasta;
	private ComboBox filtroPersona;
	private ComboBox filtroTipoComprobante;
	private CheckBox filtroAnulado;
	private Table table;
	private VerticalLayout layoutFiltros;
	private UI ui;

	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass(), ui);
		if (!control.isAcceso()) {
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		} else {
			onDraw();
		}
	}

	public void onDraw() {
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(EtiquetasLabel.getComprobantes().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true, this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonFiltros = new ButtonIcon(ButtonIcon.BUSCAR, true, this);
		botonFiltros.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonFiltros);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		//botonAgregar.setEnabled(false);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		//botonModificar.setEnabled(false);
		layoutBotones.addComponent(botonModificar);
		botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true, this);
		botonEliminar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonEliminar);
		//botonEliminar.setEnabled(false);
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true, this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonExcel);
		botonExcel.setEnabled(false);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true, this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonPDF);
		botonPDF.setEnabled(false);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		table = new Table();
		ArmaTabla();
		layout.addComponent(table);
		Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonFiltros) {
			layoutFiltros.setVisible(!layoutFiltros.isVisible());
		} else if (event.getButton() == botonRefrescar) {
			CreaDataSource();
		} else if (event.getButton() == botonModificar) {
			Comprobante comprobante = (Comprobante) table.getValue();
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(comprobante.getFecha());
			ComprobanteBloqueo comprobanteBloqueo = ComprobanteBloqueoRN.Read(new AnioMesId(calendar.get(GregorianCalendar.YEAR), calendar.get(GregorianCalendar.MONTH)+1));
			if (comprobante != null) {
				if (comprobante.isAnulado()){
					MessageBox.showHTML(Icon.ERROR, "", "El comprobante esta anulado", ButtonId.OK);
				}
				else if (comprobante.isBloqueado()){
					MessageBox.showHTML(Icon.ERROR, "", "El comprobante esta bloqueado", ButtonId.OK);
				}
				else if (comprobanteBloqueo != null && comprobanteBloqueo.isBloqueado()){
					MessageBox.showHTML(Icon.ERROR, "", "La fecha del comprobante esta dentro de un periodo bloqueado", ButtonId.OK);
				}
				else{
					TipoComprobante tipoComprobante = TipoComprobanteRN.Read(comprobante.getTipoComprobanteId());
					Window windowABM = CreaWindowABM();
					windowABM.setCaption(tipoComprobante.getNombre().toUpperCase());
					ComprobanteABM comprobanteABM = new ComprobanteABM(comprobante,tipoComprobante ,Dominios.AccesoADatos.UPDATE , ui, windowABM); 
					windowABM.setContent(comprobanteABM);
					windowABM.addCloseListener(new CloseListener() {
						@Override
						public void windowClose(CloseEvent e) {
							CreaDataSource();
						}
					});
					ui.addWindow(windowABM);
				}
			}
		} else if (event.getButton() == botonAgregar) {
			Window windowABM = CreaWindowABM();
			windowABM.setWidth("620px");
			windowABM.setHeight("470px");
			final ComprobanteTipoBuscar comprobanteTipoBuscar = new ComprobanteTipoBuscar(ui, windowABM);
			windowABM.setContent(comprobanteTipoBuscar);
			windowABM.addCloseListener(new CloseListener() {
				@Override
				public void windowClose(CloseEvent e) {
					TipoComprobante tipoComprobante = comprobanteTipoBuscar.getTipoComprobante();
					if (tipoComprobante != null){
						Window windowABM = CreaWindowABM();
						windowABM.setCaption(tipoComprobante.getNombre().toUpperCase());
						ComprobanteABM comprobanteABM = new ComprobanteABM(null,tipoComprobante ,Dominios.AccesoADatos.INSERT , ui, windowABM); 
						windowABM.setContent(comprobanteABM);
						windowABM.addCloseListener(new CloseListener() {
							@Override
							public void windowClose(CloseEvent e) {
								CreaDataSource();
							}
						});
						ui.addWindow(windowABM);
					}
				}
			});
			ui.addWindow(windowABM);
		} else if (event.getButton() == botonEliminar
				|| event.getButton() == botonExcel
				|| event.getButton() == botonPDF) {
			/**
			 * No hace nada
			 */
		}

	}

	private void ArmaFiltros() {
		GregorianCalendar calendarHoy = new GregorianCalendar();
		calendarHoy.setTime(new Date(System.currentTimeMillis()));
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		layoutFiltros.setVisible(false);
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(9);
		//TipoComprobate
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label label = new Label("Tipo Comprobante:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("100px");
		formFiltro.addComponent(label);
		VerticalLayout layout = new VerticalLayout();
		layout.setWidth("400px");
		filtroTipoComprobante = new ComboBox();
		filtroTipoComprobante.setInputPrompt(EtiquetasLabel.getTodos());
		filtroTipoComprobante.setWidth("300px");
		filtroTipoComprobante.setContainerDataSource(TipoComprobanteContenedor.LeeContainerXNombreXActivo("", true));
		layout.addComponent(filtroTipoComprobante);
		formFiltro.addComponent(layout);
		//Numero
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("N�mero:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("50px");
		formFiltro.addComponent(label);
		filtroNumero = new TextField();
		filtroNumero.setNullRepresentation("0");
		filtroNumero.setNullSettingAllowed(false);
		filtroNumero.addStyleName(R.Style.EDITABLE);
		filtroNumero.addStyleName(R.Style.LABEL_BOLD);
		filtroNumero.setWidth("100px");
		formFiltro.addComponent(filtroNumero);
		//Anulado
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Anulado:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		filtroAnulado = new CheckBox();
		filtroAnulado.setValue(false);
		formFiltro.addComponent(filtroAnulado);
		//Persona
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Persona:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		filtroPersona = new ComboBox();
		filtroPersona.setContainerDataSource(PersonasContenedor.LeeContainerXNombreXActivo("", false, false, true));
		filtroPersona.setInputPrompt(EtiquetasLabel.getTodos());
		filtroPersona.setWidth("300px");
		formFiltro.addComponent(filtroPersona);
		//Periodo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		label = new Label("Periodo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formFiltro.addComponent(label);
		HorizontalLayout layoutPeriodo = new HorizontalLayout();
		filtroFechaDesde = new DateField();
		filtroFechaDesde.setWidth("150px");
		filtroFechaDesde.setResolution(Resolution.DAY);
		layoutPeriodo.addComponent(filtroFechaDesde);
		filtroFechaHasta = new DateField();
		filtroFechaHasta.setWidth("150px");
		filtroFechaHasta.setResolution(Resolution.DAY);
		layoutPeriodo.addComponent(filtroFechaHasta);
		formFiltro.addComponent(layoutPeriodo);		
		layoutFiltros.addComponent(formFiltro);
		
		GregorianCalendar calendar = new GregorianCalendar(
				calendarHoy.get(GregorianCalendar.YEAR) ,
				calendarHoy.get(GregorianCalendar.MONTH) ,
				calendarHoy.get(GregorianCalendar.DAY_OF_MONTH));
		calendar.add(GregorianCalendar.DAY_OF_YEAR, -7);
		filtroFechaDesde.setValue(calendar.getTime());
		calendar.add(GregorianCalendar.DAY_OF_YEAR, 7);
		calendar.add(GregorianCalendar.HOUR_OF_DAY, 23);
		calendar.add(GregorianCalendar.MINUTE, 59);
		filtroFechaHasta.setValue(calendar.getTime());
	}

	private void ArmaTabla() {
		table.setHeight("550px");
		table.setWidth("985px");
		table.setSelectable(true);
		table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Comprobante comprobante = (Comprobante) itemId;
				Button botonVer = new Button(Funciones.substring(comprobante.getObservacion(), 0, 50, "..").toUpperCase());
				if (comprobante.getPersonaId() > 0){
					Personas persona = PersonasRN.Read(comprobante.getPersonaId());
					botonVer.setCaption(persona.getNombre());
				}
				botonVer.setWidth("200px");
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						TipoComprobante tipoComprobante = TipoComprobanteRN.Read(comprobante.getTipoComprobanteId());
						Window windowABM = CreaWindowABM();
						windowABM.setCaption(tipoComprobante.getNombre().toUpperCase());
						ComprobanteVer comprobanteVer = new ComprobanteVer(comprobante);
						windowABM.setContent(comprobanteVer);
						ui.addWindow(windowABM);
					}
				});
				return botonVer;
			}
		});
		table.addGeneratedColumn(COLUMNA_FECHA, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Comprobante comprobante = (Comprobante) itemId;
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
				Label fecha = new Label(df.format(comprobante.getFecha()));
				return fecha;
			}
		});
		table.addGeneratedColumn(COLUMNA_TIPOCOMPROBANTE, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Comprobante comprobante = (Comprobante) itemId;
				TipoComprobante tipoComprobante = TipoComprobanteRN.Read(comprobante.getTipoComprobanteId());
				Label labelTipo = new Label(tipoComprobante.getNombre());
				return labelTipo;
			}
		});
		table.addGeneratedColumn(COLUMNA_NUMERO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Comprobante comprobante = (Comprobante) itemId;
				Label labelTipo = new Label(String.valueOf(comprobante.getNumero()));
				return labelTipo;
			}
		});
		table.addGeneratedColumn(COLUMNA_TOTAL, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Comprobante comprobante = (Comprobante) itemId;
				DecimalFormat df = new DecimalFormat("##,###,##0.00");
				Label labelTipo = new Label(df.format(comprobante.getTotal()));
				return labelTipo;
			}
		});
		table.addGeneratedColumn(COLUMNA_ACCIONES, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Comprobante comprobante = (Comprobante) itemId;
				final PopupButton botonAcciones = new PopupButton(EtiquetasLabel.getAccion());
				botonAcciones.setPrimaryStyleName(R.Style.LINK_COLOR);
				VerticalLayout layoutAcciones = new VerticalLayout();
				botonAcciones.setContent(layoutAcciones);
				Button botonAnular = new Button("Anular", new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						GregorianCalendar calendar = new GregorianCalendar();
						calendar.setTime(comprobante.getFecha());
						ComprobanteBloqueo comprobanteBloqueo = ComprobanteBloqueoRN.Read(new AnioMesId(calendar.get(GregorianCalendar.YEAR), calendar.get(GregorianCalendar.MONTH)+1));
						if (comprobante.isAnulado()){
							MessageBox.showHTML(Icon.ERROR, "", "El comprobante esta anulado", ButtonId.OK);
						}
						else if (comprobante.isBloqueado()){
							MessageBox.showHTML(Icon.ERROR, "", "El comprobante esta bloqueado", ButtonId.OK);
						}
						else if (comprobanteBloqueo != null && comprobanteBloqueo.isBloqueado()){
							MessageBox.showHTML(Icon.ERROR, "", "La fecha del comprobante esta dentro de un periodo bloqueado", ButtonId.OK);
						}
						else{
							MessageBox.showHTML(Icon.QUESTION, ""
									,"�Realmente desea anular el comprobante?"
									,new MessageBoxListener() {
										@Override
										public void buttonClicked(ButtonId buttonType) {
											Sesion sesion = (Sesion)ui.getSession().getAttribute(R.Session.SESSION);
											comprobante.setAnulado(true);
											comprobante.setAnuladoFecha(new Date(System.currentTimeMillis()));
											comprobante.setAnuladoResponsable(sesion.getUsuarioId());
											CreaDataSource();
										}
									}
									, ButtonId.YES ,ButtonId.NO);
						}
					}
				});
				botonAnular.setPrimaryStyleName(R.Style.LINK_COLOR);
				layoutAcciones.addComponent(botonAnular);
				return botonAcciones;
			}
		});
		CreaDataSource();
	}

	private void CreaDataSource() {
		int tipoComprobanteId = 0;
		long personaId = 0;
		long numero = 0;
		try {
			numero = Long.parseLong(filtroNumero.getValue());
		} catch (NumberFormatException e) {
			filtroNumero.setValue("0");
		}
		if (filtroPersona.getValue() != null) {
			personaId = ((Personas)filtroPersona.getValue()).getId();
		}
		if (filtroTipoComprobante.getValue() != null) {
			tipoComprobanteId = ((TipoComprobante)filtroTipoComprobante.getValue()).getId();
		}
		table.setContainerDataSource(ComprobanteContenedor.LeeContainerXPeriodoXTipoXNumeroXPersona(filtroFechaDesde.getValue(), filtroFechaHasta.getValue(),tipoComprobanteId ,numero, personaId, filtroAnulado.getValue()));
		table.setVisibleColumns((Object[]) new String[] {COLUMNA_ACCIONES, COLUMNA_FECHA ,LINK_VER ,COLUMNA_TIPOCOMPROBANTE ,"letra" ,COLUMNA_NUMERO ,COLUMNA_TOTAL ,"id" });
		table.setColumnHeaders("" ,"Fecha","Persona/Detalle" , "Tipo Comprobante" ,"Letra" ,"N�mero" ,"Total" ,"Id");
		table.setColumnWidth(COLUMNA_ACCIONES, 80);
		table.setColumnWidth(COLUMNA_FECHA, 50);
		table.setColumnWidth(LINK_VER, 250);
		table.setColumnWidth(COLUMNA_TIPOCOMPROBANTE , 200);
		table.setColumnWidth("letra" , 40);
		table.setColumnWidth(COLUMNA_NUMERO , 100);
		table.setColumnWidth(COLUMNA_TOTAL ,100);
		table.setColumnWidth("id", 50);
	}

	public static Window CreaWindowABM() {
		Window windowABM = new Window(EtiquetasLabel.getComprobantes());
		windowABM.setModal(true);
		windowABM.setResizable(false);
		windowABM.setDraggable(false);
		windowABM.setWidth("900px");
		windowABM.setHeight("530px");
		windowABM.center();
		windowABM.setCloseShortcut(KeyCode.ESCAPE, null);
		return windowABM;
	}

}
