package ar.com.trisquel.escuela.componentes.comprobante;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import ar.com.trisquel.escuela.componentes.concepto.ConceptoABM;
import ar.com.trisquel.escuela.componentes.concepto.ConceptoBuscar;
import ar.com.trisquel.escuela.componentes.concepto.ConceptoWWView;
import ar.com.trisquel.escuela.componentes.personas.provedorcliente.PersonasABM;
import ar.com.trisquel.escuela.componentes.personas.provedorcliente.PersonasBuscar;
import ar.com.trisquel.escuela.componentes.personas.provedorcliente.PersonasWWView;
import ar.com.trisquel.escuela.data.contenedor.ConceptoContenedor;
import ar.com.trisquel.escuela.data.contenedor.PersonasContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.ComprobanteConceptoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ComprobanteRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ConceptoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.PersonasRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.UsuarioRN;
import ar.com.trisquel.escuela.data.tablas.Comprobante;
import ar.com.trisquel.escuela.data.tablas.ComprobanteConcepto;
import ar.com.trisquel.escuela.data.tablas.Concepto;
import ar.com.trisquel.escuela.data.tablas.Personas;
import ar.com.trisquel.escuela.data.tablas.TipoComprobante;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.language.MensajeLabel;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.MessageBox;
import ar.com.trisquel.escuela.utiles.MessageBox.ButtonId;
import ar.com.trisquel.escuela.utiles.MessageBox.Icon;
import ar.com.trisquel.escuela.utiles.MessageBox.MessageBoxListener;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;
import ar.com.trisquel.escuela.utiles.Sesion;
import ar.com.trisquel.escuela.utiles.TextFieldNumber;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;
import com.vaadin.ui.themes.BaseTheme;

@SuppressWarnings("serial")
public class ComprobanteABM extends VerticalLayout implements ClickListener {

	private TextField id;
	private ComboBox personaId;
	private DateField fecha;
	private DateField fechaRegistro;
	private ComboBox letra;
	private TextFieldNumber numero;
	private ComboBox formaPago;
	private TextArea observacion;
	private Image imageIconoPersona;
	private Label labelPersona; 
	private Label labelCuit;
	private Label labelTotal; 
	private VerticalLayout layoutConceptos;
	private HorizontalLayout layoutPersonaCompleto;
	private ButtonIcon ButtonGuardar;
	private ButtonIcon ButtonDescartar;
	private ButtonIcon ButtonBuscarPersona;
	private ButtonIcon ButtonAgregarPersona;
	private ButtonIcon ButtonModificarPersona;
	private UI ui;
	private String modo;
	private Window window;
	private Comprobante comprobante;
	private TipoComprobante tipoComprobante;
	private GregorianCalendar calendarHoy;
	private Usuario usuario;
	private boolean backgroundFilaUno = true;
	private List<ComprobanteConcepto> listComprobanteConcepto = new ArrayList<ComprobanteConcepto>();
	
	public ComprobanteABM(Comprobante comprobante,TipoComprobante tipoComprobante , String modo, UI ui, Window window) {
		this.ui = ui;
		this.modo = modo;
		this.window = window;
		this.comprobante = comprobante;
		this.tipoComprobante = tipoComprobante;
		
		Sesion sesion  = (Sesion)ui.getSession().getAttribute(R.Session.SESSION);
		this.usuario = UsuarioRN.Read(sesion.getUsuarioId());
		GregorianCalendar calendarAux = new GregorianCalendar();
		calendarAux.setTime(new Date(System.currentTimeMillis()));
		this.calendarHoy = new GregorianCalendar(
				calendarAux.get(GregorianCalendar.YEAR),
				calendarAux.get(GregorianCalendar.MONTH),
				calendarAux.get(GregorianCalendar.DAY_OF_MONTH));
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setWidth("100%");
		layout.setColumns(3);
		addComponent(layout);
		//Id
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Id:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("80px");
		layout.addComponent(label);
		HorizontalLayout layoutId = new HorizontalLayout();
		id = new TextField();
		id.addStyleName(R.Style.EDITABLE);
		id.addStyleName(R.Style.LABEL_BOLD);
		id.setWidth("350px");
		layoutId.addComponent(id);
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layoutId.addComponent(imageIcono);
		label = new Label("Fecha de Alta:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("90px");
		layoutId.addComponent(label);
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm");
		Label labelAlta = new Label(df.format(new Date(System.currentTimeMillis())) +
				R.CaracteresEsteciales.SPACIO+R.CaracteresEsteciales.SPACIO+
				usuario.getId() ,ContentMode.HTML);
		labelAlta.addStyleName(R.Style.EDITABLE);
		labelAlta.addStyleName(R.Style.LABEL_BOLD);
		labelAlta.setWidth("280px");
		layoutId.addComponent(labelAlta);
		//Botones
		HorizontalLayout layoutBotones = new HorizontalLayout();
		ButtonGuardar = new ButtonIcon(ButtonIcon.GUARDAR, true, this);
		ButtonGuardar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonGuardar);
		ButtonDescartar = new ButtonIcon(ButtonIcon.DESCARTAR, true, this);
		ButtonDescartar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(ButtonDescartar);
		layoutId.addComponent(layoutBotones);
		layoutId.setComponentAlignment(layoutBotones, Alignment.TOP_RIGHT);
		layout.addComponent(layoutId);
		//Persona
		imageIconoPersona = new Image();
		imageIconoPersona.setSource(resourceIcono);
		layout.addComponent(imageIconoPersona);
		labelPersona = new Label("Persona:");
		labelPersona.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(labelPersona);
		layoutPersonaCompleto = new HorizontalLayout();
		layout.addComponent(layoutPersonaCompleto);
		personaId = new ComboBox();
		ArmaComboPersona();
		personaId.setWidth("280px");
		personaId.setNullSelectionAllowed(true);
		personaId.setImmediate(true);
		personaId.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (personaId.getValue() != null){
					Personas persona = (Personas)personaId.getValue(); 
					labelCuit.setValue(persona.getCuit());
					ButtonModificarPersona.setEnabled(true);
				}
				else{
					labelCuit.setValue("");
					ButtonModificarPersona.setEnabled(false);
				}
			}
		});
		layoutPersonaCompleto.addComponent(personaId);
		HorizontalLayout layoutBotonesPersona = new HorizontalLayout();
		layoutBotonesPersona.setWidth("70px");
		layoutPersonaCompleto.addComponent(layoutBotonesPersona);
		ButtonBuscarPersona = new ButtonIcon(ButtonIcon.BUSCAR, true ,this);
		ButtonBuscarPersona.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotonesPersona.addComponent(ButtonBuscarPersona);
		ButtonAgregarPersona = new ButtonIcon(ButtonIcon.AGREGAR, true,this);
		layoutBotonesPersona.addComponent(ButtonAgregarPersona);
		ButtonModificarPersona = new ButtonIcon(ButtonIcon.MODIFICAR, true ,this);
		layoutBotonesPersona.addComponent(ButtonModificarPersona);
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layoutPersonaCompleto.addComponent(imageIcono);
		label = new Label("CUIT/CUIL:");
		label.setWidth("90px");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layoutPersonaCompleto.addComponent(label);
		labelCuit = new Label();
		labelCuit.addStyleName(R.Style.EDITABLE);
		labelCuit.addStyleName(R.Style.LABEL_BOLD);
		layoutPersonaCompleto.addComponent(labelCuit);	
		//Fechas
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Fecha:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		HorizontalLayout layoutFechas = new HorizontalLayout();
		layout.addComponent(layoutFechas);
		VerticalLayout layoutFecha = new VerticalLayout();
		layoutFecha.setWidth("350px");
		layoutFechas.addComponent(layoutFecha);
		fecha = new DateField();
		fecha.setResolution(Resolution.DAY);
		layoutFecha.addComponent(fecha);				
		//Fechas
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layoutFechas.addComponent(imageIcono);
		label = new Label("Fecha Registro:");
		label.setWidth("90px");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layoutFechas.addComponent(label);
		fechaRegistro = new DateField();
		fechaRegistro.setResolution(Resolution.DAY);
		layoutFechas.addComponent(fechaRegistro);		
		//Letra
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Numero:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		HorizontalLayout layoutLetraNumeroFormaPago = new HorizontalLayout();
		layout.addComponent(layoutLetraNumeroFormaPago);
		VerticalLayout layoutAuxiliar = new VerticalLayout();
		layoutAuxiliar.setWidth("350px");
		layoutLetraNumeroFormaPago.addComponent(layoutAuxiliar);
		HorizontalLayout layoutLetraNumero = new HorizontalLayout();
		layoutAuxiliar.addComponent(layoutLetraNumero);
		letra = Dominios.ComprobanteLetra.CreaComboBox(false);
		letra.setWidth("40px");
		letra.setNullSelectionAllowed(false);
		layoutLetraNumero.addComponent(letra);		
		//N�mero
		numero = new TextFieldNumber();
		numero.addStyleName(R.Style.EDITABLE);
		numero.addStyleName(R.Style.LABEL_BOLD);
		numero.setWidth("100px");
		numero.setConDecimales(false);
		numero.setMaxLength(15);
		numero.setValue("0");
		layoutLetraNumero.addComponent(numero);		
		//Forma de Pago
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layoutLetraNumeroFormaPago.addComponent(imageIcono);
		label = new Label("Forma de Pago:");
		label.setWidth("90px");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layoutLetraNumeroFormaPago.addComponent(label);
		formaPago = Dominios.ComprobanteFormaPago.CreaComboBox(false);
		formaPago.setNullSelectionAllowed(false);
		layoutLetraNumeroFormaPago.addComponent(formaPago);		
		//Observaciones
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Observaciones:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		observacion = new TextArea();
		observacion.addStyleName(R.Style.EDITABLE);
		observacion.addStyleName(R.Style.LABEL_BOLD);
		observacion.setWidth("700px");
		observacion.setHeight("70px");
		observacion.setMaxLength(2048);
		layout.addComponent(observacion);
		//Conceptos
		layout.addComponent(new Label());
		layout.addComponent(new Label());
		Panel panel = new Panel();
		panel.setHeight("250px");
		panel.setWidth("770px");
		layoutConceptos = new VerticalLayout();
		layoutConceptos.setWidth("100%");
		panel.setContent(layoutConceptos);
		layout.addComponent(panel);
		layout.addComponent(new Label());
		layout.addComponent(new Label());
		Label separator = new Label();
		separator.setHeight("20px");
		layout.addComponent(separator);
		//Total
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Total:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.addStyleName(R.Style.TEXTO_BIG);
		layout.addComponent(label);
		labelTotal = new Label("0.00");
		labelTotal.addStyleName(R.Style.EDITABLE);
		labelTotal.addStyleName(R.Style.LABEL_BOLD);
		labelTotal.addStyleName(R.Style.TEXTO_ALINEACION_DERECHA);
		labelTotal.addStyleName(R.Style.TEXTO_BIG);
		labelTotal.setWidth("130px");
		layout.addComponent(labelTotal);
		
		ArmaListaConceptos();
		personaId.focus();
		if (modo.equals(Dominios.AccesoADatos.INSERT)) {
			id.setReadOnly(true);
			fecha.setValue(calendarHoy.getTime());
			fechaRegistro.setValue(calendarHoy.getTime());
			if (tipoComprobante.getTipo().equals(Dominios.TipoComprobante.Tipo.COMPROBANTE_CAJA)){
				letra.setValue("X");
				letra.setReadOnly(true);
				formaPago.setValue(Dominios.ComprobanteFormaPago.CONTADO);
				formaPago.setReadOnly(true);
				layoutPersonaCompleto.setVisible(false);
				labelPersona.setVisible(false);
				imageIconoPersona.setVisible(false);
				fecha.focus();
			}
			else{
				letra.setValue("B");
				letra.removeItem("X");
				formaPago.setValue(Dominios.ComprobanteFormaPago.CONTADO);
			}
			ButtonModificarPersona.setEnabled(false);
			ButtonGuardar.setVisible(true);
			ButtonDescartar.setVisible(true);
		}
		else{
			id.setValue(String.valueOf(comprobante.getId()));
			if (comprobante.getPersonaId() > 0){
				personaId.setValue(PersonasRN.Read(comprobante.getPersonaId()));
			}
			fecha.setValue(comprobante.getFecha());
			fechaRegistro.setValue(comprobante.getFechaRegistro());
			letra.setValue(comprobante.getLetra());
			numero.setValue(String.valueOf(comprobante.getNumero()));
			formaPago.setValue(comprobante.getFormaPago());
			observacion.setValue(comprobante.getObservacion());
			labelAlta.setValue(df.format(comprobante.getAltaFecha()) +
					R.CaracteresEsteciales.SPACIO+R.CaracteresEsteciales.SPACIO+
					comprobante.getAltaResponsable());
			if (tipoComprobante.getTipo().equals(Dominios.TipoComprobante.Tipo.COMPROBANTE_CAJA)){
				letra.setReadOnly(true);
				formaPago.setReadOnly(true);
				layoutPersonaCompleto.setVisible(false);
				labelPersona.setVisible(false);
				imageIconoPersona.setVisible(false);
				fecha.focus();
			}
			if (modo.equals(Dominios.AccesoADatos.SELECT)) {
				id.setReadOnly(true);
				personaId.setReadOnly(true);
				fecha.setReadOnly(true);
				fechaRegistro.setReadOnly(true);
				letra.setReadOnly(true);
				numero.setReadOnly(true);
				formaPago.setReadOnly(true);
				observacion.setReadOnly(true);
				ButtonGuardar.setVisible(false);
				ButtonDescartar.setVisible(false);
			} else if (modo.equals(Dominios.AccesoADatos.UPDATE)) {
				id.setReadOnly(true);
				ButtonGuardar.setVisible(true);
				ButtonDescartar.setVisible(true);
			}
		}
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == ButtonDescartar) {
			ui.removeWindow(window);
		} else if (event.getButton() == ButtonGuardar) {
			if (comprobante == null){
				comprobante = ComprobanteRN.Inizializate(usuario ,tipoComprobante);
			}
			Personas persona = (Personas)personaId.getValue();
			long nro = 0;
			try{
				nro = Long.parseLong(numero.getValue());
			}catch (NumberFormatException e){
				numero.setValue("0");
			}
			comprobante.setFecha(fecha.getValue());
			comprobante.setFechaRegistro(fechaRegistro.getValue());
			comprobante.setFormaPago(String.valueOf(formaPago.getValue()));
			comprobante.setLetra(String.valueOf(letra.getValue()));
			comprobante.setNumero(nro);
			if (persona != null)
				comprobante.setPersonaId(persona.getId());
			comprobante.setObservacion(observacion.getValue());
			RespuestaEntidad respuesta = ComprobanteRN.Validate(comprobante ,modo);
			if (respuesta.isError()) {
				MessageBox.showHTML(Icon.ERROR, "", respuesta.getMsgError(),ButtonId.OK);
			} else {
				MessageBox.showHTML(Icon.QUESTION, "",MensajeLabel.getConfirmaAccion(), new MessageBoxListener() {
					@Override
					public void buttonClicked(ButtonId buttonType) {
						if (buttonType == ButtonId.SAVE) {
							RespuestaEntidad respuesta = ComprobanteRN.Save(comprobante);
							if (respuesta.isError()) {
								MessageBox.showHTML(Icon.ERROR, "",respuesta.getMsgError(),ButtonId.OK);
							} else {
								ComprobanteRN.AfterSave(comprobante ,modo);
								if (modo.equals(Dominios.AccesoADatos.INSERT)){
									for (ComprobanteConcepto comprobanteConcepto : listComprobanteConcepto){
										ComprobanteConcepto comprobanteConceptoSave = ComprobanteConceptoRN.Inizializate(comprobante.getId());
										comprobanteConceptoSave.setConceptoId(comprobanteConcepto.getConceptoId());
										comprobanteConceptoSave.setDescripcion(comprobanteConcepto.getDescripcion());
										comprobanteConceptoSave.setImporte(comprobanteConcepto.getImporte());
										ComprobanteConceptoRN.Save(comprobanteConceptoSave);
									}
								}
								ui.removeWindow(window);
							}
						}
					}
				}, ButtonId.SAVE, ButtonId.CANCEL);
			}
		} else if (event.getButton() == ButtonBuscarPersona) {
			Window windowABM = PersonasWWView.CreaWindowABM();
			final PersonasBuscar personasBuscar = new PersonasBuscar(ui, windowABM);
			windowABM.setCaption(EtiquetasLabel.getBuscar() + " " + windowABM.getCaption());
			windowABM.setWidth("900px");
			windowABM.setHeight("600px");
			windowABM.setContent(personasBuscar);
			windowABM.addCloseListener(new CloseListener() {
				@Override
				public void windowClose(CloseEvent e) {
					if (personasBuscar.getPersonas() != null){
						ArmaComboPersona();
						personaId.setValue(personasBuscar.getPersonas());
					}
				}
			});
			ui.addWindow(windowABM);
		} else if (event.getButton() == ButtonAgregarPersona) {
			Window windowABM = PersonasWWView.CreaWindowABM();
			final PersonasABM personaABM = new PersonasABM(null,Dominios.AccesoADatos.INSERT, ui, windowABM);
			windowABM.setContent(personaABM);
			windowABM.addCloseListener(new CloseListener() {
				@Override
				public void windowClose(CloseEvent e) {
					if (personaABM.getPersonas() != null){
						ArmaComboPersona();
						personaId.setValue(personaABM.getPersonas());
					}
				}
			});
			ui.addWindow(windowABM);
		} else if (event.getButton() == ButtonModificarPersona) {
			if (personaId.getValue() != null){
				Personas personas = (Personas)personaId.getValue();
				Window windowABM = PersonasWWView.CreaWindowABM();
				final PersonasABM personaABM = new PersonasABM(personas,Dominios.AccesoADatos.UPDATE, ui, windowABM);
				windowABM.setContent(personaABM);
				windowABM.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						ArmaComboPersona();
						personaId.setValue(personaABM.getPersonas());
					}
				});
				ui.addWindow(windowABM);
			}
		}
		
	}
	
	private void ArmaComboPersona(){
		if (tipoComprobante.getOperacion().equals(Dominios.TipoComprobante.Operacion.COMPRA)){
			personaId.setContainerDataSource(PersonasContenedor.LeeContainerXNombreXActivo("", false, true, true));
		}
		else if (tipoComprobante.getOperacion().equals(Dominios.TipoComprobante.Operacion.VENTA)){
			personaId.setContainerDataSource(PersonasContenedor.LeeContainerXNombreXActivo("", true, false, true));
		}
	}
	
	private void ArmaListaConceptos(){
		layoutConceptos.removeAllComponents();
		HorizontalLayout layoutTitulo = new HorizontalLayout();
		layoutTitulo.setPrimaryStyleName(R.Style.TEXTO_SUBTITULO_COLOR);
		Label label = new Label("Concepto");
		label.setWidth("340px");
		layoutTitulo.addComponent(label);
		label = new Label("Descripci�n");
		label.setWidth("295px");
		layoutTitulo.addComponent(label);
		label = new Label("Importe");
		label.setWidth("115px");
		layoutTitulo.addComponent(label);
		layoutConceptos.addComponent(layoutTitulo);
		if (comprobante != null){
			listComprobanteConcepto = comprobante.getConceptos(); 
		}
		double total = 0;
		for (ComprobanteConcepto comprobanteConcepto : listComprobanteConcepto){
			ArmaFila(comprobanteConcepto);
			total = total + comprobanteConcepto.getImporte();
		}
		DecimalFormat df = new DecimalFormat("##,###,##0.00");
		labelTotal.setValue(df.format(total));
		HorizontalLayout layoutAgregar = new HorizontalLayout();
		layoutAgregar.setWidth("750px");
		if (backgroundFilaUno){
			layoutAgregar.addStyleName(R.Style.BACKGROUND_FILA_1);
		}
		else{
			layoutAgregar.addStyleName(R.Style.BACKGROUND_FILA_2);
		}
		final Button ButtonMostrarAgregar = new Button(EtiquetasLabel.getAgregar());
		ButtonMostrarAgregar.setPrimaryStyleName(BaseTheme.BUTTON_LINK);
		ButtonMostrarAgregar.addStyleName(R.Style.LABEL_COLOR_AZUL);
		ButtonMostrarAgregar.addStyleName(R.Style.LABEL_UNDERLINE);
		ButtonMostrarAgregar.setIcon(new ThemeResource("img/fila_add.png"));
		ButtonMostrarAgregar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				ButtonMostrarAgregar.setVisible(false);
				ArmaFila(null);
			}
		});
		layoutAgregar.addComponent(ButtonMostrarAgregar);
		layoutConceptos.addComponent(layoutAgregar);
	}
	
	private void ArmaFila(final ComprobanteConcepto comprobanteConcepto){
		HorizontalLayout layoutFila = new HorizontalLayout();
		layoutFila.setSpacing(true);
		if (backgroundFilaUno){
			layoutFila.addStyleName(R.Style.BACKGROUND_FILA_1);
		}
		else{
			layoutFila.addStyleName(R.Style.BACKGROUND_FILA_2);
		}
		layoutConceptos.addComponent(layoutFila);
		backgroundFilaUno = !backgroundFilaUno;
		//Concepto
		final ComboBox filaConcepto = new ComboBox();
		filaConcepto.setInputPrompt("Seleccione el Concepto");
		filaConcepto.setWidth("290px");
		filaConcepto.setNullSelectionAllowed(false);
		filaConcepto.setContainerDataSource(ConceptoContenedor.LeeContainerXNombreXActivo("",true));
		layoutFila.addComponent(filaConcepto);
		//Botones
		HorizontalLayout layoutBotonConcepto = new HorizontalLayout();
		layoutBotonConcepto.setWidth("37px");
		layoutFila.addComponent(layoutBotonConcepto);
		final ButtonIcon buttonBuscarConcepto = new ButtonIcon(ButtonIcon.BUSCAR ,true ,new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				Window windowABM = ConceptoWWView.CreaWindowABM();
				windowABM.setCaption(EtiquetasLabel.getBuscar() + " " + windowABM.getCaption());
				windowABM.setWidth("400px");
				windowABM.setHeight("600px");
				final ConceptoBuscar conceptoBuscar = new ConceptoBuscar(ui, windowABM);
				windowABM.setContent(conceptoBuscar);
				windowABM.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						if (conceptoBuscar.getConcepto() != null){
							filaConcepto.setContainerDataSource(ConceptoContenedor.LeeContainerXNombreXActivo("",true));
							filaConcepto.setValue(conceptoBuscar.getConcepto());
						}
					}
				});
				ui.addWindow(windowABM);
			}
		});
		layoutBotonConcepto.addComponent(buttonBuscarConcepto);
		final ButtonIcon buttonAgregarConcepto = new ButtonIcon(ButtonIcon.AGREGAR ,true ,new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				Window windowABM = ConceptoWWView.CreaWindowABM();
				final ConceptoABM conceptoABM = new ConceptoABM(null,Dominios.AccesoADatos.INSERT, ui, windowABM);
				windowABM.setContent(conceptoABM);
				windowABM.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						if (conceptoABM.getConcepto() != null){
							filaConcepto.setContainerDataSource(ConceptoContenedor.LeeContainerXNombreXActivo("",true));
							filaConcepto.setValue(conceptoABM.getConcepto());
						}
					}
				});
				ui.addWindow(windowABM);
			}
		});
		layoutBotonConcepto.addComponent(buttonAgregarConcepto);
		//Descripcion
		final TextField filaDescripcion = new TextField();
		filaDescripcion.setWidth("290px");
		filaDescripcion.addStyleName(R.Style.EDITABLE);
		filaDescripcion.addStyleName(R.Style.LABEL_BOLD);
		filaDescripcion.addStyleName(R.Style.TEXTO_MAYUSCULA);
		layoutFila.addComponent(filaDescripcion);
		//Importe
		final TextFieldNumber filaImporte = new TextFieldNumber();
		filaImporte.setInputPrompt("0.00");
		filaImporte.setWidth("80px");
		filaImporte.addStyleName(R.Style.EDITABLE);
		filaImporte.addStyleName(R.Style.LABEL_BOLD);
		filaImporte.addStyleName(R.Style.TEXTO_ALINEACION_DERECHA);
		filaImporte.setConDecimales(true);
		layoutFila.addComponent(filaImporte);
		HorizontalLayout layoutBotones = new HorizontalLayout();
		final ButtonIcon filaGuardar = new ButtonIcon(ButtonIcon.GUARDAR, true, new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				long conceptoId = 0;
				double importe = 0;
				if (filaConcepto.getValue() != null){
					conceptoId = ((Concepto)filaConcepto.getValue()).getId();
				}
				if (filaImporte.getValue() != null && filaImporte.getValue().trim().length() > 0){
					try{
						importe = Double.parseDouble(filaImporte.getValue().trim());
					}catch(Exception e){}
				}
				String modo = Dominios.AccesoADatos.UPDATE;
				if (comprobante != null){
					ComprobanteConcepto comprobanteConceptoSave = comprobanteConcepto;
					if (comprobanteConceptoSave == null){
						comprobanteConceptoSave = ComprobanteConceptoRN.Inizializate(comprobante.getId());
						modo = Dominios.AccesoADatos.INSERT;
					}
					comprobanteConceptoSave.setConceptoId(conceptoId);
					comprobanteConceptoSave.setDescripcion(filaDescripcion.getValue());
					comprobanteConceptoSave.setImporte(importe);
					RespuestaEntidad res = ComprobanteConceptoRN.Validate(comprobanteConceptoSave ,modo);
					if (res.isError()){
						MessageBox.showHTML(Icon.ERROR, "", res.getMsgError(), ButtonId.OK);
					}
					else{
						ComprobanteConceptoRN.Save(comprobanteConceptoSave);
						ArmaListaConceptos();
					}
				}
				else{
					ComprobanteConcepto comprobanteConceptoSave = new ComprobanteConcepto();
					if (comprobanteConcepto == null){
						modo = Dominios.AccesoADatos.INSERT;
					}
					comprobanteConceptoSave.setConceptoId(conceptoId);
					comprobanteConceptoSave.setDescripcion(filaDescripcion.getValue());
					comprobanteConceptoSave.setImporte(importe);
					RespuestaEntidad res = ComprobanteConceptoRN.Validate(comprobanteConceptoSave ,modo);
					if (res.isError()){
						MessageBox.showHTML(Icon.ERROR, "", res.getMsgError(), ButtonId.OK);
					}
					else{
						if (comprobanteConcepto!= null)
							comprobanteConceptoSave = comprobanteConcepto;
						comprobanteConceptoSave.setConceptoId(conceptoId);
						comprobanteConceptoSave.setDescripcion(filaDescripcion.getValue());
						comprobanteConceptoSave.setImporte(importe);
						if (modo.equals(Dominios.AccesoADatos.INSERT) ){
							listComprobanteConcepto.add(comprobanteConceptoSave);
						}
						ArmaListaConceptos();
					}
				}
			}
		});
		final ButtonIcon filaEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true ,new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				MessageBox.showHTML(Icon.QUESTION , "", MensajeLabel.getConfirmaBorrarDato(), new MessageBoxListener(){
					@Override
					public void buttonClicked(ButtonId buttonType) {
						if (buttonType == ButtonId.YES){
							if (comprobante != null){
								ComprobanteConceptoRN.Delete(comprobanteConcepto);
								ArmaListaConceptos();
							}
							else{
								listComprobanteConcepto.remove(comprobanteConcepto);
								ArmaListaConceptos();
							}
						}
					}
					
				}, ButtonId.YES ,ButtonId.NO );
			}
		});
		final ButtonIcon filaDescartar = new ButtonIcon(ButtonIcon.DESCARTAR, true ,new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				ArmaListaConceptos();
			}
		});
		final ButtonIcon filaModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true ,new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				filaConcepto.setReadOnly(false);
				filaDescripcion.setReadOnly(false);
				filaImporte.setReadOnly(false);
				filaGuardar.setVisible(true);
				filaDescartar.setVisible(true);
				filaEliminar.setVisible(false);
				buttonBuscarConcepto.setVisible(true);
				buttonAgregarConcepto.setVisible(true);
				event.getButton().setVisible(false);
			}
		});
		layoutBotones.addComponent(filaGuardar);
		layoutBotones.addComponent(filaModificar);
		layoutBotones.addComponent(filaDescartar);
		layoutBotones.addComponent(filaEliminar);
		layoutFila.addComponent(layoutBotones);
		if (comprobanteConcepto != null){
			filaConcepto.setValue(ConceptoRN.Read(comprobanteConcepto.getConceptoId()));
			filaDescripcion.setValue(comprobanteConcepto.getDescripcion());
			filaImporte.setValue(String.valueOf(comprobanteConcepto.getImporte()));
			filaConcepto.setReadOnly(true);
			filaDescripcion.setReadOnly(true);
			filaImporte.setReadOnly(true);
			filaGuardar.setVisible(false);
			filaDescartar.setVisible(false);
			filaModificar.setVisible(true);
			filaEliminar.setVisible(true);
			buttonBuscarConcepto.setVisible(false);
			buttonAgregarConcepto.setVisible(false);
		}
		else{
			filaGuardar.setVisible(true);
			filaDescartar.setVisible(true);
			filaModificar.setVisible(false);
			filaEliminar.setVisible(false);
			buttonBuscarConcepto.setVisible(true);
			buttonAgregarConcepto.setVisible(true);
		}
	}
}
