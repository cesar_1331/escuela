package ar.com.trisquel.escuela.componentes.comprobante;

import ar.com.trisquel.escuela.data.contenedor.TipoComprobanteContenedor;
import ar.com.trisquel.escuela.data.tablas.TipoComprobante;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class ComprobanteTipoBuscar extends VerticalLayout {
	private static final String LINK_VER = "LINK_VER";
	private static final String COLUMNA_TIPO = "COLUMNA_TIPO";
	private static final String COLUMNA_SALDO = "COLUMNA_SALDO";
	private static final String COLUMNA_OPERACION = "COLUMNA_OPERACION";
	private Table table;
	private Window window;
	private UI ui;
	private TipoComprobante tipoComprobante = null; 
	
	public ComprobanteTipoBuscar(UI ui ,Window window){
		this.ui = ui;
		this.window = window;
		onDraw();
	}
	
	private void onDraw(){
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		table = new Table();
		table.setHeight("400px");
		table.setWidth("600px");
		table.setSelectable(true);
		addComponent(table);
		table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final TipoComprobante tipoComprobante = (TipoComprobante) itemId;
				Button botonVer = new Button(tipoComprobante.getNombre());
				botonVer.setWidth("200px");
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						setTipoComprobante(tipoComprobante); 
						ui.removeWindow(window);
					}
				});
				return botonVer;
			}
		});
		table.addGeneratedColumn(COLUMNA_OPERACION, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				TipoComprobante tipoComprobante = (TipoComprobante) itemId;
				ComboBox comboOperacion = Dominios.TipoComprobante.Operacion.CreaComboBox(false);
				comboOperacion.setWidth("90px");
				comboOperacion.setValue(tipoComprobante.getOperacion());
				comboOperacion.setReadOnly(true);
				return comboOperacion;
			}
		});
		table.addGeneratedColumn(COLUMNA_SALDO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				TipoComprobante tipoComprobante = (TipoComprobante) itemId;
				ComboBox comboSaldo = Dominios.TipoComprobante.Saldo.CreaComboBox(false);
				comboSaldo.setWidth("90px");
				comboSaldo.setValue(tipoComprobante.getSaldo());
				comboSaldo.setReadOnly(true);
				return comboSaldo;
			}
		});
		table.addGeneratedColumn(COLUMNA_TIPO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				TipoComprobante tipoComprobante = (TipoComprobante) itemId;
				ComboBox comboTipo = Dominios.TipoComprobante.Tipo.CreaComboBox(false);
				comboTipo.setWidth("140px");
				comboTipo.setValue(tipoComprobante.getTipo());
				comboTipo.setReadOnly(true);
				return comboTipo;
			}
		});
		CreaDataSource();
	}
	
	private void CreaDataSource() {
		table.setContainerDataSource(TipoComprobanteContenedor.LeeContainerXNombreXActivo("", true));
		table.setVisibleColumns((Object[]) new String[] { LINK_VER, COLUMNA_TIPO,COLUMNA_SALDO ,COLUMNA_OPERACION});
		table.setColumnHeaders("Tipo de Comprobante", "Tipo", "Saldo","Operación");
	}

	public TipoComprobante getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(TipoComprobante tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}
}
