package ar.com.trisquel.escuela.componentes.comprobante;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ar.com.trisquel.escuela.data.reglasdenegocio.ConceptoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.PersonasRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.TipoComprobanteRN;
import ar.com.trisquel.escuela.data.tablas.Comprobante;
import ar.com.trisquel.escuela.data.tablas.ComprobanteConcepto;
import ar.com.trisquel.escuela.data.tablas.Personas;
import ar.com.trisquel.escuela.data.tablas.TipoComprobante;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class ComprobanteVer extends VerticalLayout implements ClickListener {
	private Image imageIconoPersona;
	private Label labelPersona; 
	private VerticalLayout layoutConceptos;
	private HorizontalLayout layoutPersonaCompleto;
	private Comprobante comprobante;
	private TipoComprobante tipoComprobante;
	private boolean backgroundFilaUno = true;
	private List<ComprobanteConcepto> listComprobanteConcepto = new ArrayList<ComprobanteConcepto>();
	
	public ComprobanteVer(Comprobante comprobante) {
		this.comprobante = comprobante;
		this.tipoComprobante = TipoComprobanteRN.Read(comprobante.getTipoComprobanteId());
		onDraw();
	}

	public void onDraw() {
		removeAllComponents();
		addStyleName(R.Style.PADDING_MEDIO);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout layout = new GridLayout();
		layout.setWidth("100%");
		layout.setColumns(3);
		addComponent(layout);
		//Id
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		Label label = new Label("Id:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("80px");
		layout.addComponent(label);
		HorizontalLayout layoutId = new HorizontalLayout();
		layout.addComponent(layoutId);
		TextField id = new TextField();
		id.addStyleName(R.Style.EDITABLE);
		id.addStyleName(R.Style.LABEL_BOLD);
		id.setWidth("280px");
		id.focus();
		layoutId.addComponent(id);
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layoutId.addComponent(imageIcono);
		label = new Label("Fecha de Alta:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("80px");
		layoutId.addComponent(label);
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm");
		Label labelAlta = new Label(df.format(comprobante.getAltaFecha()) +
				R.CaracteresEsteciales.SPACIO+R.CaracteresEsteciales.SPACIO+
				comprobante.getAltaResponsable() ,ContentMode.HTML);
		labelAlta.addStyleName(R.Style.EDITABLE);
		labelAlta.addStyleName(R.Style.LABEL_BOLD);
		labelAlta.setWidth("185px");
		layoutId.addComponent(labelAlta);
		
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layoutId.addComponent(imageIcono);
		label = new Label("Anulado:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("50px");
		layoutId.addComponent(label);
		Label labelAnulado = new Label("N",ContentMode.HTML);
		labelAnulado.addStyleName(R.Style.EDITABLE);
		labelAnulado.addStyleName(R.Style.LABEL_BOLD);
		labelAnulado.setWidth("190px");
		if (comprobante.isAnulado()){
			labelAnulado.setValue("S" +
					R.CaracteresEsteciales.SPACIO+R.CaracteresEsteciales.SPACIO+
					df.format(comprobante.getAltaFecha()) +
					R.CaracteresEsteciales.SPACIO+R.CaracteresEsteciales.SPACIO+
					comprobante.getAltaResponsable());
		}
		layoutId.addComponent(labelAnulado);
		//Persona
		imageIconoPersona = new Image();
		imageIconoPersona.setSource(resourceIcono);
		layout.addComponent(imageIconoPersona);
		labelPersona = new Label("Persona:");
		labelPersona.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(labelPersona);
		layoutPersonaCompleto = new HorizontalLayout();
		layout.addComponent(layoutPersonaCompleto);
		Label persona = new Label();
		persona.addStyleName(R.Style.EDITABLE);
		persona.addStyleName(R.Style.LABEL_BOLD);
		persona.setWidth("280px");
		layoutPersonaCompleto.addComponent(persona);
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layoutPersonaCompleto.addComponent(imageIcono);
		label = new Label("CUIT/CUIL:");
		label.setWidth("80px");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layoutPersonaCompleto.addComponent(label);
		Label labelCuit = new Label();
		labelCuit.addStyleName(R.Style.EDITABLE);
		labelCuit.addStyleName(R.Style.LABEL_BOLD);
		layoutPersonaCompleto.addComponent(labelCuit);	
		//Fechas
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Fecha:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		HorizontalLayout layoutFechas = new HorizontalLayout();
		layout.addComponent(layoutFechas);
		VerticalLayout layoutFecha = new VerticalLayout();
		layoutFecha.setWidth("280px");
		layoutFechas.addComponent(layoutFecha);
		Label fecha = new Label();
		fecha.addStyleName(R.Style.EDITABLE);
		fecha.addStyleName(R.Style.LABEL_BOLD);
		layoutFecha.addComponent(fecha);				
		//Fechas
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layoutFechas.addComponent(imageIcono);
		label = new Label("Fecha Registro:");
		label.setWidth("80px");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layoutFechas.addComponent(label);
		Label fechaRegistro = new Label();
		fechaRegistro.addStyleName(R.Style.EDITABLE);
		fechaRegistro.addStyleName(R.Style.LABEL_BOLD);
		layoutFechas.addComponent(fechaRegistro);		
		//Letra
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Numero:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		HorizontalLayout layoutLetraNumeroFormaPago = new HorizontalLayout();
		layout.addComponent(layoutLetraNumeroFormaPago);
		VerticalLayout layoutAuxiliar = new VerticalLayout();
		layoutAuxiliar.setWidth("280px");
		layoutLetraNumeroFormaPago.addComponent(layoutAuxiliar);
		HorizontalLayout layoutLetraNumero = new HorizontalLayout();
		layoutAuxiliar.addComponent(layoutLetraNumero);
		Label letra = new Label();
		letra.setWidth("30px");
		letra.addStyleName(R.Style.EDITABLE);
		letra.addStyleName(R.Style.LABEL_BOLD);
		layoutLetraNumero.addComponent(letra);		
		//N�mero
		Label numero = new Label();
		numero.addStyleName(R.Style.EDITABLE);
		numero.addStyleName(R.Style.LABEL_BOLD);
		numero.setWidth("100px");
		layoutLetraNumero.addComponent(numero);		
		//Forma de Pago
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layoutLetraNumeroFormaPago.addComponent(imageIcono);
		label = new Label("Forma de Pago:");
		label.setWidth("80px");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layoutLetraNumeroFormaPago.addComponent(label);
		ComboBox formaPago = Dominios.ComprobanteFormaPago.CreaComboBox(false);
		layoutLetraNumeroFormaPago.addComponent(formaPago);		
		//Observaciones
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Observaciones:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		layout.addComponent(label);
		Label observacion = new Label();
		observacion.addStyleName(R.Style.EDITABLE);
		observacion.addStyleName(R.Style.LABEL_BOLD);
		observacion.setWidth("700px");
		layout.addComponent(observacion);
		//Conceptos
		layout.addComponent(new Label());
		layout.addComponent(new Label());
		Panel panel = new Panel();
		panel.setHeight("250px");
		panel.setWidth("740px");
		layoutConceptos = new VerticalLayout();
		layoutConceptos.setWidth("100%");
		panel.setContent(layoutConceptos);
		layout.addComponent(panel);
		layout.addComponent(new Label());
		layout.addComponent(new Label());
		Label separator = new Label();
		separator.setHeight("20px");
		layout.addComponent(separator);
		//Total
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		layout.addComponent(imageIcono);
		label = new Label("Total:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.addStyleName(R.Style.TEXTO_BIG);
		layout.addComponent(label);
		Label labelTotal = new Label();
		labelTotal.addStyleName(R.Style.EDITABLE);
		labelTotal.addStyleName(R.Style.LABEL_BOLD);
		labelTotal.addStyleName(R.Style.TEXTO_ALINEACION_DERECHA);
		labelTotal.addStyleName(R.Style.TEXTO_BIG);
		labelTotal.setWidth("130px");
		layout.addComponent(labelTotal);
	
		df = new SimpleDateFormat("dd/MM/yy");
		id.setValue(String.valueOf(comprobante.getId()));
		if (comprobante.getPersonaId() > 0){
			Personas personas = PersonasRN.Read(comprobante.getPersonaId()); 
			persona.setValue(personas.getNombre());
			labelCuit.setValue(personas.getCuit());
		}
		fecha.setValue(df.format(comprobante.getFecha()));
		fechaRegistro.setValue(df.format(comprobante.getFechaRegistro()));
		letra.setValue(comprobante.getLetra());
		numero.setValue(String.valueOf(comprobante.getNumero()));
		formaPago.setValue(comprobante.getFormaPago());
		observacion.setValue(comprobante.getObservacion());
		formaPago.setReadOnly(true);
		if (tipoComprobante.getTipo().equals(Dominios.TipoComprobante.Tipo.COMPROBANTE_CAJA)){
			layoutPersonaCompleto.setVisible(false);
			labelPersona.setVisible(false);
			imageIconoPersona.setVisible(false);
		}
		/*
		 * Conceptos
		 */
		listComprobanteConcepto = comprobante.getConceptos();
		layoutConceptos.removeAllComponents();
		HorizontalLayout layoutTitulo = new HorizontalLayout();
		layoutTitulo.setPrimaryStyleName(R.Style.TEXTO_SUBTITULO_COLOR);
		label = new Label("Concepto");
		label.setWidth("340px");
		layoutTitulo.addComponent(label);
		label = new Label("Descripci�n");
		label.setWidth("300px");
		layoutTitulo.addComponent(label);
		label = new Label("Importe");
		label.setWidth("78px");
		layoutTitulo.addComponent(label);
		layoutConceptos.addComponent(layoutTitulo);
		double total = 0;
		for (ComprobanteConcepto comprobanteConcepto : listComprobanteConcepto){
			ArmaFila(comprobanteConcepto);
			total = total + comprobanteConcepto.getImporte();
		}
		DecimalFormat decimalf = new DecimalFormat("###,###,##0.00");
		labelTotal.setValue(decimalf.format(total));
		id.setReadOnly(true);
	}

	@Override
	public void buttonClick(ClickEvent event) {
	}
	
	
	private void ArmaFila(final ComprobanteConcepto comprobanteConcepto){
		HorizontalLayout layoutFila = new HorizontalLayout();
		layoutFila.setSpacing(true);
		if (backgroundFilaUno){
			layoutFila.addStyleName(R.Style.BACKGROUND_FILA_1);
		}
		else{
			layoutFila.addStyleName(R.Style.BACKGROUND_FILA_2);
		}
		layoutConceptos.addComponent(layoutFila);
		backgroundFilaUno = !backgroundFilaUno;
		//Concepto
		Label filaConcepto = new Label();
		filaConcepto.addStyleName(R.Style.EDITABLE);
		filaConcepto.addStyleName(R.Style.LABEL_BOLD);
		filaConcepto.addStyleName(R.Style.TEXTO_MAYUSCULA);
		filaConcepto.setWidth("338px");
		layoutFila.addComponent(filaConcepto);
		//Descripcion
		Label filaDescripcion = new Label();
		filaDescripcion.setWidth("290px");
		filaDescripcion.addStyleName(R.Style.EDITABLE);
		filaDescripcion.addStyleName(R.Style.LABEL_BOLD);
		filaDescripcion.addStyleName(R.Style.TEXTO_MAYUSCULA);
		layoutFila.addComponent(filaDescripcion);
		//Importe
		DecimalFormat df = new DecimalFormat("###,###,##0.00");
		Label filaImporte = new Label();
		filaImporte.setWidth("80px");
		filaImporte.addStyleName(R.Style.EDITABLE);
		filaImporte.addStyleName(R.Style.LABEL_BOLD);
		filaImporte.addStyleName(R.Style.TEXTO_ALINEACION_DERECHA);
		layoutFila.addComponent(filaImporte);

		filaConcepto.setValue(ConceptoRN.Read(comprobanteConcepto.getConceptoId()).getNombre());
		filaDescripcion.setValue(comprobanteConcepto.getDescripcion());
		filaImporte.setValue(df.format(comprobanteConcepto.getImporte()));
	}
}
