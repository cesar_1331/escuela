package ar.com.trisquel.escuela.componentes.comprobante;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import ar.com.trisquel.escuela.data.reglasdenegocio.ParametroRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.PersonasRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.TipoComprobanteRN;
import ar.com.trisquel.escuela.data.select.ComprobanteSelect;
import ar.com.trisquel.escuela.data.tablas.Comprobante;
import ar.com.trisquel.escuela.data.tablas.Personas;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.Funciones;
import ar.com.trisquel.escuela.utiles.ImageCaptcha;
import ar.com.trisquel.escuela.utiles.Recursos;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.vaadin.server.VaadinService;

public class ComprobanteBalanceExportar {
	
	private static final int EXCEL = 1;
	private static final int PDF = 2;
	
	public static String ExportarExcel(Date fechaDesde ,Date fechaHasta ){
		Workbook libro = new HSSFWorkbook();
		short fontHeigt = 14;
		CellStyle styleTitle = libro.createCellStyle();
		Font fuente = libro.createFont();
		fuente.setFontHeightInPoints(fontHeigt);
		fuente.setBoldweight(Font.BOLDWEIGHT_BOLD);
		styleTitle.setFont(fuente);
		
		CellStyle styleTituloColumna = libro.createCellStyle();
		fuente = libro.createFont();
		fuente.setBoldweight(Font.BOLDWEIGHT_BOLD);
		styleTituloColumna.setFont(fuente);
		styleTituloColumna.setFillBackgroundColor(IndexedColors.AQUA.getIndex());
		styleTituloColumna.setBottomBorderColor(IndexedColors.BLUE_GREY.getIndex());
		styleTituloColumna.setTopBorderColor(IndexedColors.BLUE_GREY.getIndex());
		styleTituloColumna.setBorderBottom(CellStyle.BORDER_MEDIUM);
		styleTituloColumna.setBorderTop(CellStyle.BORDER_MEDIUM);
		//A�adir una hoja al libro:
		Sheet hoja = libro.createSheet("Export");
		//Crear una fila:
		Row fila = hoja.createRow(0);
		//Se accede a la celda 2 de la fila 1:
		Cell celda = fila.createCell(0);
		// Generamos el contenido de la celda.
		HSSFRichTextString miContenido= new HSSFRichTextString(ParametroRN.LeeEscuelaNombre());
		celda.setCellValue(miContenido);
		celda.setCellStyle(styleTitle);

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		fila = hoja.createRow(2);
		celda = fila.createCell(0);
		celda.setCellValue("Periodo:");
		celda.setCellStyle(styleTitle);
		celda = fila.createCell(1);
		celda.setCellValue(df.format(fechaDesde));
		celda.setCellStyle(styleTitle);
		celda = fila.createCell(2);
		celda.setCellValue(df.format(fechaHasta));
		celda.setCellStyle(styleTitle);
		
		fila = hoja.createRow(4);
		celda = fila.createCell(0);
		celda.setCellValue("Fecha");
		celda.setCellStyle(styleTituloColumna);
		celda = fila.createCell(1);
		celda.setCellValue("Persona/Detalle");
		celda.setCellStyle(styleTituloColumna);
		celda = fila.createCell(2);
		celda.setCellValue("Tipo de Comprobante");
		celda.setCellStyle(styleTituloColumna);
		celda = fila.createCell(3);
		celda.setCellValue("Letra");
		celda.setCellStyle(styleTituloColumna);
		celda = fila.createCell(4);
		celda.setCellValue("N�mero");
		celda.setCellStyle(styleTituloColumna);
		celda = fila.createCell(5);
		celda.setCellValue("Contado");
		celda.setCellStyle(styleTituloColumna);
		celda = fila.createCell(6);
		celda.setCellValue("Cta. Cte.");
		celda.setCellStyle(styleTituloColumna);
		celda = fila.createCell(7);
		celda.setCellValue("Id");
		celda.setCellStyle(styleTituloColumna);
		celda = fila.createCell(8);
		celda.setCellValue("Observaciones");
		celda.setCellStyle(styleTituloColumna);
		hoja.createFreezePane(0, 5);
		
		ArmaBalance(fechaDesde ,fechaHasta,hoja ,null ,EXCEL);
		
		hoja.autoSizeColumn(1);
		hoja.autoSizeColumn(2);
		hoja.autoSizeColumn(3);
		hoja.autoSizeColumn(4);
		hoja.autoSizeColumn(5);
		hoja.autoSizeColumn(6);
		hoja.autoSizeColumn(7);
		hoja.autoSizeColumn(8);
		
		String fileNombre = new String(ImageCaptcha.generarRamdom(20));
		String fileGuardar = VaadinService.getCurrent().getBaseDirectory()+ "/VAADIN/temp/"+fileNombre+".xls";
		OutputStream output;
		try {
			output = new FileOutputStream(fileGuardar);
			libro.write(output);
			output.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "./VAADIN/temp/"+fileNombre+".xls";
	}

	public static String  ExportarPDF(Date fechaDesde ,Date fechaHasta ){
		String fileNombre = new String(ImageCaptcha.generarRamdom(20));
		String fileGuardar = VaadinService.getCurrent().getBaseDirectory()+ "/VAADIN/temp/"+fileNombre+".pdf";
		try{
			Document document = new Document(PageSize.A4.rotate() );
			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fileGuardar));
			document.open();
			document.addAuthor(Recursos.Parametros.getAppTituloSistema());
			document.addCreationDate();
			document.addTitle(EtiquetasLabel.getMisAlumnos());
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
			String str = 
				"<table><tbody>"+
					"<tr><td><span style=\"font-size:16px\"><strong>"+ParametroRN.LeeEscuelaNombre()+ "</strong></span></td></tr>" +
					"<tr><td><span style=\"font-size:16px\"><strong>Periodo: "+df.format(fechaDesde) + " - " + df.format(fechaHasta) + "</strong></span></td></tr>" +
				"</tbody></table>" +
				"<table border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%\"><tbody>"+ 
					"<tr>" +
						"<th align=\"center\" style=\"width: 70px;\">Fecha</th>" +
						"<th align=\"center\" style=\"width: 280px;\">Persona/Detalle</th>" +
						"<th align=\"center\" style=\"width: 200px;\">Tipo de Comprobante</th>" +
						"<th align=\"center\" style=\"width: 40px;\">Letra</th>" +
						"<th align=\"center\" style=\"width: 100px;\">N�mero</th>" +
						"<th align=\"center\" style=\"width: 100px;\">Contado</th>" +
						"<th align=\"center\" style=\"width: 100px;\">Cta. Cte</th>" +
						"<th align=\"center\" style=\"width: 90px;\">Id</th>" +
					"</tr>";
			str = ArmaBalance(fechaDesde ,fechaHasta ,null ,str ,PDF);
			str = str + "</tbody></table>";
			worker.parseXHtml(pdfWriter, document, new StringReader(str));
			document.close();
		}catch(Exception e){
			System.out.print(e.toString());
		}
		return "./VAADIN/temp/"+fileNombre+".pdf";
	}

	private static String ArmaBalance(Date fechaDesde ,Date fechaHasta ,Sheet hoja ,String html ,int mode){
		
		DecimalFormat decimalf = new DecimalFormat("##,###,##0.00");
		SimpleDateFormat datef = new SimpleDateFormat("dd/MM/yy");
		double saldoIni = 0;
		double saldo = 0;
		int row = 5;
		short fontHeigt = 12;
		CellStyle styleTitle = null;
		if (mode == EXCEL){
			styleTitle = hoja.getWorkbook().createCellStyle();
			Font fuente = hoja.getWorkbook().createFont();
			fuente.setFontHeightInPoints(fontHeigt);
			fuente.setBoldweight(Font.BOLDWEIGHT_BOLD);
			styleTitle.setFont(fuente);
		}
		List<Comprobante> listComprobante = ComprobanteSelect.SelectXPeriodo(null, fechaDesde);
		for (Comprobante comprobante : listComprobante){
			if (comprobante.getFormaPago().equals(Dominios.ComprobanteFormaPago.CONTADO)){
				if (comprobante.getTipo().equals(Dominios.TipoComprobante.Tipo.COMPROBANTE_CAJA)){
					saldoIni = saldoIni + (-comprobante.getSaldo() * comprobante.getTotal()); 
				}
				else{
					saldoIni = saldoIni + (comprobante.getSaldo() * comprobante.getTotal());
				}
			}
		}
		if (saldoIni != 0){
			if (mode == EXCEL){
				Row fila = hoja.createRow(row);
				Cell celda = fila.createCell(1);
				celda.setCellValue("SALDO INICIAL");
				celda = fila.createCell(5);
				celda.setCellValue(saldoIni);
				row++;
			}
			else if (mode == PDF){
				html = html + 
						"<tr>" +
							"<td></td>" +
							"<td>SALDO INICIAL</td>" +
							"<td></td>" +
							"<td></td>" +
							"<td style=\"text-align: right\"></td>" +
							"<td style=\"text-align: right\">"+decimalf.format(saldoIni)+"</td>" +
							"<td style=\"text-align: right\"></td>" +
							"<td style=\"text-align: right\"></td>" +
							"</tr>";
			}
		}
		listComprobante = ComprobanteSelect.SelectXPeriodo(fechaDesde, fechaHasta);
		for (Comprobante comprobante : listComprobante){
			if (mode == EXCEL){
				Row fila = hoja.createRow(row);
				Cell celda = fila.createCell(0);
				celda.setCellValue(datef.format(comprobante.getFecha()));
				celda = fila.createCell(1);
				celda.setCellValue(Funciones.substring(comprobante.getObservacion(), 0, 50, "..").toUpperCase().trim());
				if (comprobante.getPersonaId() > 0){
					Personas persona = PersonasRN.Read(comprobante.getPersonaId());
					celda.setCellValue(persona.getNombre().trim());
				}
				celda = fila.createCell(2);
				celda.setCellValue(TipoComprobanteRN.Read(comprobante.getTipoComprobanteId()).getNombre().trim());
				celda = fila.createCell(3);
				celda.setCellValue(comprobante.getLetra());
				celda = fila.createCell(4);
				celda.setCellValue(comprobante.getNumero());
				double contado = 0, ctacte = 0;
				if (comprobante.getFormaPago().equals(Dominios.ComprobanteFormaPago.CONTADO)){
					if (comprobante.getTipo().equals(Dominios.TipoComprobante.Tipo.COMPROBANTE_CAJA)){
						saldo = saldo + (-comprobante.getSaldo() * comprobante.getTotal());
						contado = (-comprobante.getSaldo() * comprobante.getTotal());
					}
					else{
						saldo = saldo + (comprobante.getSaldo() * comprobante.getTotal());
						contado = (comprobante.getSaldo() * comprobante.getTotal());
					}
				}
				else{
					ctacte = (comprobante.getSaldo() * comprobante.getTotal());
				}
				celda = fila.createCell(5);
				celda.setCellValue(contado);
				celda = fila.createCell(6);
				celda.setCellValue(ctacte);
				celda = fila.createCell(7);
				celda.setCellValue(comprobante.getId());
				celda = fila.createCell(8);
				celda.setCellValue(comprobante.getObservacion().trim());
				row++;
			}
			else if (mode == PDF){
				String detalle = Funciones.substring(comprobante.getObservacion(), 0, 50, "..").toUpperCase();
				if (comprobante.getPersonaId() > 0){
					Personas persona = PersonasRN.Read(comprobante.getPersonaId());
					detalle = persona.getNombre();
				}
				double contado = 0, ctacte = 0;
				if (comprobante.getFormaPago().equals(Dominios.ComprobanteFormaPago.CONTADO)){
					if (comprobante.getTipo().equals(Dominios.TipoComprobante.Tipo.COMPROBANTE_CAJA)){
						saldo = saldo + (-comprobante.getSaldo() * comprobante.getTotal());
						contado = (-comprobante.getSaldo() * comprobante.getTotal());
					}
					else{
						saldo = saldo + (comprobante.getSaldo() * comprobante.getTotal());
						contado = (comprobante.getSaldo() * comprobante.getTotal());
					}
				}
				else{
					ctacte = (comprobante.getSaldo() * comprobante.getTotal());
				}
				html = html + 
						"<tr>" +
							"<td>"+datef.format(comprobante.getFecha())+"</td>" +
							"<td>"+detalle+"</td>" +
							"<td>"+TipoComprobanteRN.Read(comprobante.getTipoComprobanteId()).getNombre()+"</td>" +
							"<td>"+comprobante.getLetra()+"</td>" +
							"<td style=\"text-align: right\">"+String.valueOf(comprobante.getNumero())+"</td>" +
							"<td style=\"text-align: right\">"+decimalf.format(contado)+"</td>" +
							"<td style=\"text-align: right\">"+decimalf.format(ctacte)+"</td>" +
							"<td style=\"text-align: right\">"+String.valueOf(comprobante.getId())+"</td>" +
							"</tr>";
			}		
		}
		if (mode == EXCEL){
			Row fila = hoja.createRow(row);
			Cell celda = fila.createCell(1);
			celda.setCellValue("SALDO PERIODO");
			celda = fila.createCell(5);
			celda.setCellValue(saldo);
			row++;
			fila = hoja.createRow(row);
			celda = fila.createCell(1);
			celda.setCellValue("SALDO FINAL");
			celda = fila.createCell(5);
			celda.setCellValue(saldoIni + saldo);
			row++;
		}
		else if (mode == PDF){
			html = html + 
					"<tr>" +
						"<td></td>" +
						"<td>SALDO PERIODO</td>" +
						"<td></td>" +
						"<td></td>" +
						"<td></td>" +
						"<td style=\"text-align: right\">"+decimalf.format(saldo)+"</td>" +
						"<td></td>" +
						"<td></td>" +
					"</tr>" +
					"<tr>" +
						"<td></td>" +
						"<td>SALDO FINAL</td>" +
						"<td></td>" +
						"<td></td>" +
						"<td></td>" +
						"<td style=\"text-align: right\">"+decimalf.format(saldoIni + saldo)+"</td>" +
						"<td></td>" +
						"<td></td>" +
					"</tr>";
		}
		return html;
	}
}
