package ar.com.trisquel.escuela.componentes.comprobante;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import ar.com.trisquel.escuela.data.reglasdenegocio.PersonasRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.TipoComprobanteRN;
import ar.com.trisquel.escuela.data.select.ComprobanteSelect;
import ar.com.trisquel.escuela.data.tablas.Comprobante;
import ar.com.trisquel.escuela.data.tablas.Personas;
import ar.com.trisquel.escuela.data.tablas.TipoComprobante;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.Funciones;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;


@SuppressWarnings("serial")
public class ComprobanteBalanceView extends VerticalLayout implements ClickListener, View {

	public static final String VIEW = "ComprobanteBalanceWW";
	private static final String LINK_VER = "LINK_VER";
	private static final String COLUMNA_FECHA = "COLUMNA_FECHA";
	private static final String COLUMNA_TIPOCOMPROBANTE = "COLUMNA_TIPOCOMPROBANTE";
	private static final String COLUMNA_NUMERO = "COLUMNA_NUMERO";
	private static final String COLUMNA_TOTAL = "COLUMNA_TOTAL";
	private static final String COLUMNA_CUENTACORRIENTE = "COLUMNA_CUENTACORRIENTE";
	private ButtonIcon botonFiltros;
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonEliminar;
	private ButtonIcon botonRefrescar;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private Label saldoInicial;
	private Label saldoPeriodo;
	private Label saldoFinal;
	private DateField filtroFechaDesde;
	private DateField filtroFechaHasta;
	private Table table;
	private VerticalLayout layoutFiltros;
	private UI ui;

	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass(), ui);
		if (!control.isAcceso()) {
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		} else {
			onDraw();
		}
	}

	public void onDraw() {
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(EtiquetasLabel.getBalance().toUpperCase());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true, this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonFiltros = new ButtonIcon(ButtonIcon.BUSCAR, true, this);
		botonFiltros.addStyleName(R.Style.PADDING_MINIMO);
		botonFiltros.setEnabled(false);
		layoutBotones.addComponent(botonFiltros);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		botonAgregar.setEnabled(false);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		botonModificar.setEnabled(false);
		layoutBotones.addComponent(botonModificar);
		botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true, this);
		botonEliminar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonEliminar);
		botonEliminar.setEnabled(false);
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true, this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonExcel);
		//botonExcel.setEnabled(false);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true, this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonPDF);
		//botonPDF.setEnabled(false);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		table = new Table();
		layout.addComponent(table);
		/*
		 * Totales
		 */
		HorizontalLayout layoutTotalesEstacio = new HorizontalLayout();
		layout.addComponent(layoutTotalesEstacio);
		Separator = new Label();
		Separator.setWidth("605px");
		layoutTotalesEstacio.addComponent(Separator);
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout formTotales = new GridLayout();
		formTotales.setSpacing(true);
		formTotales.setColumns(3);
		layoutTotalesEstacio.addComponent(formTotales);
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formTotales.addComponent(imageIcono);
		Label label = new Label("Saldo Inicial:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("90px");
		formTotales.addComponent(label);
		saldoInicial = new Label();
		saldoInicial.addStyleName(R.Style.EDITABLE);
		saldoInicial.addStyleName(R.Style.LABEL_BOLD);
		saldoInicial.addStyleName(R.Style.TEXTO_ALINEACION_DERECHA);
		saldoInicial.setWidth("100px");
		formTotales.addComponent(saldoInicial);
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formTotales.addComponent(imageIcono);
		label = new Label("Saldo del Periodo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formTotales.addComponent(label);
		saldoPeriodo = new Label("" ,ContentMode.HTML);
		saldoPeriodo.addStyleName(R.Style.EDITABLE);
		saldoPeriodo.addStyleName(R.Style.LABEL_BOLD);
		saldoPeriodo.addStyleName(R.Style.TEXTO_ALINEACION_DERECHA);
		saldoPeriodo.setWidth("100px");
		formTotales.addComponent(saldoPeriodo);
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formTotales.addComponent(imageIcono);
		label = new Label("Saldo Final:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		formTotales.addComponent(label);
		saldoFinal = new Label();
		saldoFinal.addStyleName(R.Style.EDITABLE);
		saldoFinal.addStyleName(R.Style.LABEL_BOLD);
		saldoFinal.addStyleName(R.Style.TEXTO_ALINEACION_DERECHA);
		saldoFinal.setWidth("100px");
		formTotales.addComponent(saldoFinal);		
		
		Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
		
		ArmaTabla();
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonFiltros) {
			layoutFiltros.setVisible(!layoutFiltros.isVisible()); 
		} else if (event.getButton() == botonRefrescar) {
			CreaDataSource();
		} else if (event.getButton() == botonExcel){
			String fileName = ComprobanteBalanceExportar.ExportarExcel(filtroFechaDesde.getValue() ,filtroFechaHasta.getValue());
			ui.getPage().open(fileName ,"_blank", false);
		} else if (event.getButton() == botonPDF) {
			String fileName = ComprobanteBalanceExportar.ExportarPDF(filtroFechaDesde.getValue() ,filtroFechaHasta.getValue());
			ui.getPage().open(fileName ,"_blank", false);
		} else if (event.getButton() == botonModificar) {
		} else if (event.getButton() == botonAgregar) {
		} else if (event.getButton() == botonEliminar) {
			/**
			 * No hace nada
			 */
		}

	}

	private void ArmaFiltros() {
		GregorianCalendar calendarHoy = new GregorianCalendar();
		calendarHoy.setTime(new Date(System.currentTimeMillis()));
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout formFiltro = new GridLayout();
		formFiltro.setSpacing(true);
		formFiltro.setColumns(3);
		//Periodo
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label label = new Label("Periodo:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("55px");
		formFiltro.addComponent(label);
		HorizontalLayout layoutPeriodo = new HorizontalLayout();
		filtroFechaDesde = new DateField();
		filtroFechaDesde.setResolution(Resolution.DAY);
		layoutPeriodo.addComponent(filtroFechaDesde);
		filtroFechaHasta = new DateField();
		filtroFechaHasta.setResolution(Resolution.DAY);
		layoutPeriodo.addComponent(filtroFechaHasta);
		formFiltro.addComponent(layoutPeriodo);		
		layoutFiltros.addComponent(formFiltro);
		
		GregorianCalendar calendar = new GregorianCalendar(
				calendarHoy.get(GregorianCalendar.YEAR) ,
				calendarHoy.get(GregorianCalendar.MONTH) ,
				calendarHoy.get(GregorianCalendar.DAY_OF_MONTH));
		calendar.add(GregorianCalendar.MONTH, -1);
		filtroFechaDesde.setValue(calendar.getTime());
		calendar.add(GregorianCalendar.MONTH, 1);
		calendar.add(GregorianCalendar.HOUR_OF_DAY, 23);
		calendar.add(GregorianCalendar.MINUTE, 59);
		filtroFechaHasta.setValue(calendar.getTime());
	}

	private void ArmaTabla() {
		table.setHeight("470px");
		table.setWidth("985px");
		table.setSelectable(true);
		table.setColumnReorderingAllowed(false);
		table.setSortEnabled(false);
		table.addGeneratedColumn(LINK_VER, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final Comprobante comprobante = (Comprobante) itemId;
				Button botonVer = new Button(Funciones.substring(comprobante.getObservacion(), 0, 50, "..").toUpperCase());
				if (comprobante.getPersonaId() > 0){
					Personas persona = PersonasRN.Read(comprobante.getPersonaId());
					botonVer.setCaption(persona.getNombre());
				}
				botonVer.setWidth("200px");
				botonVer.setPrimaryStyleName(R.Style.LINK);
				botonVer.addStyleName(R.Style.LABEL_BOLD);
				botonVer.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						TipoComprobante tipoComprobante = TipoComprobanteRN.Read(comprobante.getTipoComprobanteId());
						Window windowABM = ComprobantesView.CreaWindowABM();
						windowABM.setCaption(tipoComprobante.getNombre().toUpperCase());
						ComprobanteVer comprobanteVer = new ComprobanteVer(comprobante);
						windowABM.setContent(comprobanteVer);
						ui.addWindow(windowABM);
					}
				});
				return botonVer;
			}
		});
		table.addGeneratedColumn(COLUMNA_FECHA, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Comprobante comprobante = (Comprobante) itemId;
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
				Label fecha = new Label(df.format(comprobante.getFecha()));
				return fecha;
			}
		});
		table.addGeneratedColumn(COLUMNA_TIPOCOMPROBANTE, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Comprobante comprobante = (Comprobante) itemId;
				TipoComprobante tipoComprobante = TipoComprobanteRN.Read(comprobante.getTipoComprobanteId());
				Label labelTipo = new Label(tipoComprobante.getNombre());
				return labelTipo;
			}
		});
		table.addGeneratedColumn(COLUMNA_NUMERO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Comprobante comprobante = (Comprobante) itemId;
				Label labelTipo = new Label(String.valueOf(comprobante.getNumero()));
				return labelTipo;
			}
		});
		table.addGeneratedColumn(COLUMNA_TOTAL, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Comprobante comprobante = (Comprobante) itemId;
				DecimalFormat df = new DecimalFormat("##,###,##0.00");
				double total = 0;
				if (comprobante.getFormaPago().equals(Dominios.ComprobanteFormaPago.CONTADO)){
					if (comprobante.getTipo().equals(Dominios.TipoComprobante.Tipo.COMPROBANTE_CAJA)){
						total = -comprobante.getSaldo() * comprobante.getTotal(); 
					}
					else{
						total = comprobante.getSaldo() * comprobante.getTotal();
					}
				}
				Label labelTipo = new Label(df.format(total));
				labelTipo.addStyleName(R.Style.TEXTO_ALINEACION_DERECHA);
				return labelTipo;
			}
		});
		table.addGeneratedColumn(COLUMNA_CUENTACORRIENTE, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				Comprobante comprobante = (Comprobante) itemId;
				DecimalFormat df = new DecimalFormat("##,###,###0.00");
				double total = 0;
				if (comprobante.getFormaPago().equals(Dominios.ComprobanteFormaPago.CUENTA_CORRIENTE)){
					if (comprobante.getTipo().equals(Dominios.TipoComprobante.Tipo.COMPROBANTE_CAJA)){
						total = -comprobante.getSaldo() * comprobante.getTotal(); 
					}
					else{
						total = comprobante.getSaldo() * comprobante.getTotal();
					}
				}
				Label labelTipo = new Label(df.format(total));
				labelTipo.addStyleName(R.Style.TEXTO_ALINEACION_DERECHA);
				return labelTipo;
			}
		});
		CreaDataSource();
	}

	private void CreaDataSource() {
		DecimalFormat df = new DecimalFormat("##,###,##0.00");
		double saldoIni = 0;
		double saldo = 0;
		List<Comprobante> listComprobante = ComprobanteSelect.SelectXPeriodo(null, filtroFechaDesde.getValue());
		for (Comprobante comprobante : listComprobante){
			if (comprobante.getFormaPago().equals(Dominios.ComprobanteFormaPago.CONTADO)){
				if (comprobante.getTipo().equals(Dominios.TipoComprobante.Tipo.COMPROBANTE_CAJA)){
					saldoIni = saldoIni + (-comprobante.getSaldo() * comprobante.getTotal()); 
				}
				else{
					saldoIni = saldoIni + (comprobante.getSaldo() * comprobante.getTotal());
				}
			}
		}
		listComprobante = ComprobanteSelect.SelectXPeriodo(filtroFechaDesde.getValue(), filtroFechaHasta.getValue());
		for (Comprobante comprobante : listComprobante){
			if (comprobante.getFormaPago().equals(Dominios.ComprobanteFormaPago.CONTADO)){
				if (comprobante.getTipo().equals(Dominios.TipoComprobante.Tipo.COMPROBANTE_CAJA)){
					saldo = saldo + (-comprobante.getSaldo() * comprobante.getTotal()); 
				}
				else{
					saldo = saldo + (comprobante.getSaldo() * comprobante.getTotal());
				}
			}
		}
		saldoInicial.setValue(df.format(saldoIni));
		saldoPeriodo.setValue(df.format(saldo) + "<hr />");
		saldoFinal.setValue(df.format(saldoIni + saldo));
		BeanItemContainer<Comprobante> container = new BeanItemContainer<Comprobante>(Comprobante.class);
		container.addAll(listComprobante);
		table.setContainerDataSource(container);
		table.setVisibleColumns((Object[]) new String[] {COLUMNA_FECHA ,LINK_VER ,COLUMNA_TIPOCOMPROBANTE ,"letra" ,COLUMNA_NUMERO ,COLUMNA_TOTAL ,COLUMNA_CUENTACORRIENTE ,"id" });
		table.setColumnHeaders("Fecha","Persona/Detalle" , "Tipo Comprobante" ,"Letra" ,"N�mero" ,"Contado" ,"Cta. Cte." ,"Id"); 
		table.setColumnWidth(COLUMNA_FECHA, 50);
		table.setColumnWidth(LINK_VER, 250);
		table.setColumnWidth(COLUMNA_TIPOCOMPROBANTE , 200);
		table.setColumnWidth("letra" , 40);
		table.setColumnWidth(COLUMNA_NUMERO , 100);
		table.setColumnWidth(COLUMNA_TOTAL ,100);
		table.setColumnWidth(COLUMNA_CUENTACORRIENTE ,100);
		table.setColumnWidth("id", 45);
	}
}

