package ar.com.trisquel.escuela.componentes.comprobante;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import ar.com.trisquel.escuela.data.contenedor.ComprobanteBloqueoContenedor;
import ar.com.trisquel.escuela.data.reglasdenegocio.ComprobanteBloqueoRN;
import ar.com.trisquel.escuela.data.tablas.ComprobanteBloqueo;
import ar.com.trisquel.escuela.language.EtiquetasLabel;
import ar.com.trisquel.escuela.seguridad.AccesoDenegadoView;
import ar.com.trisquel.escuela.seguridad.ControlAcceso;
import ar.com.trisquel.escuela.utiles.ButtonIcon;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class ComprobanteBloqueoView extends VerticalLayout implements ClickListener, View {

	public static final String VIEW = "ComprobanteBloqueoWW";
	private static final String COLUMNA_MES = "COLUMNA_MES";
	private static final String COLUMNA_BLOQUEADO = "COLUMNA_BLOQUEADO";
	private static final String COLUMNA_FECHA = "COLUMNA_FECHA";
	private ButtonIcon botonFiltros;
	private ButtonIcon botonAgregar;
	private ButtonIcon botonModificar;
	private ButtonIcon botonEliminar;
	private ButtonIcon botonRefrescar;
	private ButtonIcon botonExcel;
	private ButtonIcon botonPDF;
	private InlineDateField filtroAnio;
	private Table table;
	private VerticalLayout layoutFiltros;
	private UI ui;

	@Override
	public void enter(ViewChangeEvent event) {
		ui = event.getNavigator().getUI();
		ControlAcceso control = ControlAcceso.Control(this.getClass(), ui);
		if (!control.isAcceso()) {
			AccesoDenegadoView accesoDenegado = new AccesoDenegadoView();
			addComponent(accesoDenegado);
			accesoDenegado.onDraw(control);
		} else {
			onDraw();
		}
	}

	public void onDraw() {
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MEDIO);
		addStyleName(R.Style.EXPAND_HEIGHT_ALL);
		addStyleName(R.Style.EXPAND_WIDTH_ALL);
		VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setWidth("100%");
		Label Titulo = new Label(EtiquetasLabel.getBloqueosComprobantes());
		Titulo.setStyleName(R.Style.TITLE);
		LayoutTitle.addComponent(Titulo);
		layout.addComponent(LayoutTitle);
		Label Separator = new Label("<hr>", ContentMode.HTML);
		layout.addComponent(Separator);
		/*
		 * Botones
		 */
		HorizontalLayout layoutBotones = new HorizontalLayout();
		layout.addComponent(layoutBotones);
		botonRefrescar = new ButtonIcon(ButtonIcon.REFRESCAR, true, this);
		botonRefrescar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonRefrescar);
		botonFiltros = new ButtonIcon(ButtonIcon.BUSCAR, true, this);
		botonFiltros.addStyleName(R.Style.PADDING_MINIMO);
		botonFiltros.setEnabled(false);
		layoutBotones.addComponent(botonFiltros);
		botonAgregar = new ButtonIcon(ButtonIcon.AGREGAR, true, this);
		botonAgregar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonAgregar);
		botonAgregar.setEnabled(false);
		botonModificar = new ButtonIcon(ButtonIcon.MODIFICAR, true, this);
		botonModificar.addStyleName(R.Style.PADDING_MINIMO);
		botonModificar.setEnabled(false);
		layoutBotones.addComponent(botonModificar);
		botonEliminar = new ButtonIcon(ButtonIcon.ELIMINAR, true, this);
		botonEliminar.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonEliminar);
		botonEliminar.setEnabled(false);
		botonExcel = new ButtonIcon(ButtonIcon.EXPORTAR_EXCEL, true, this);
		botonExcel.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonExcel);
		botonExcel.setEnabled(false);
		botonPDF = new ButtonIcon(ButtonIcon.EXPORTAR_PDF, true, this);
		botonPDF.addStyleName(R.Style.PADDING_MINIMO);
		layoutBotones.addComponent(botonPDF);
		botonPDF.setEnabled(false);
		/*
		 * Filtro 
		 */
		layoutFiltros = new VerticalLayout();
		ArmaFiltros();
		layout.addComponent(layoutFiltros);
		/*
		 * Tabla
		 */
		table = new Table();
		ArmaTabla();
		layout.addComponent(table);
		Separator = new Label();
		Separator.addStyleName(R.Style.PADDING_MINIMO);
		layout.addComponent(Separator);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == botonRefrescar){
			CreaDataSource();
		}
	}

	private void ArmaFiltros() {
		ThemeResource resourceIcono = new ThemeResource("img/icono.png");
		Image imageIcono;
		GridLayout formFiltro = new GridLayout();
		formFiltro.setColumns(3);
		//Anio
		imageIcono = new Image();
		imageIcono.setSource(resourceIcono);
		formFiltro.addComponent(imageIcono);
		Label label = new Label("A�o:");
		label.addStyleName(R.Style.EDITABLE_LABEL);
		label.setWidth("40px");
		formFiltro.addComponent(label);
		VerticalLayout layout = new VerticalLayout();
		layout.setWidth("400px");
		filtroAnio = new InlineDateField();
		filtroAnio.setResolution(Resolution.YEAR);
		filtroAnio.setValue(new Date(System.currentTimeMillis()));
		filtroAnio.setImmediate(true);
		layout.addComponent(filtroAnio);
		filtroAnio.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				GregorianCalendar calendar = new GregorianCalendar();
				calendar.setTime(filtroAnio.getValue());
				for (int i = 1; i <= 12 ;i++){
					ComprobanteBloqueoRN.Add(ComprobanteBloqueoRN.Inizializate(calendar.get(GregorianCalendar.YEAR), i));
				}
				CreaDataSource();
			}
		});
		formFiltro.addComponent(layout);
		layoutFiltros.addComponent(formFiltro);

	}

	private void ArmaTabla() {
		table.setHeight("550px");
		table.setWidth("985px");
		table.setSelectable(true);
		table.addGeneratedColumn(COLUMNA_MES, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,	Object columnId) {
				final ComprobanteBloqueo comprobanteBloqueo = (ComprobanteBloqueo) itemId;
				return new Label(Dominios.MesesDelAnio.ARRAY[comprobanteBloqueo.getId().getMes()]);
			}
		});
		table.addGeneratedColumn(COLUMNA_BLOQUEADO, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final ComprobanteBloqueo comprobanteBloqueo = (ComprobanteBloqueo) itemId;
				CheckBox bloqueado = new CheckBox();
				bloqueado.setValue(comprobanteBloqueo.isBloqueado());
				bloqueado.setImmediate(true);
				bloqueado.addValueChangeListener(new ValueChangeListener() {
					@Override
					public void valueChange(ValueChangeEvent event) {
						Sesion sesion = (Sesion)ui.getSession().getAttribute(R.Session.SESSION);
						comprobanteBloqueo.setBloqueado(!comprobanteBloqueo.isBloqueado());
						comprobanteBloqueo.setFecha(new Date(System.currentTimeMillis()));
						comprobanteBloqueo.setResponsable(sesion.getUsuarioId());
						ComprobanteBloqueoRN.Save(comprobanteBloqueo);
						CreaDataSource();
					}
				});
				return bloqueado;
			}
		});
		table.addGeneratedColumn(COLUMNA_FECHA, new Table.ColumnGenerator() {
			public Component generateCell(Table source, Object itemId,Object columnId) {
				final ComprobanteBloqueo comprobanteBloqueo = (ComprobanteBloqueo) itemId;
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm");
				Label labelFecha = new Label();
				if (comprobanteBloqueo.getFecha() != null) {
					labelFecha.setValue(df.format(comprobanteBloqueo.getFecha()));
				}
				return labelFecha;
			}
		});
		table.setCellStyleGenerator(new Table.CellStyleGenerator() {
			@Override
			public String getStyle(Table source, Object itemId,Object propertyId) {
		        if (propertyId != null &&propertyId.equals(COLUMNA_BLOQUEADO))
		        	return R.Style.TABLE_CHECKBOX_EDITABLE;
		        else
		        	return null;
			}
		});
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(filtroAnio.getValue());
		for (int i = 1; i <= 12 ;i++){
			ComprobanteBloqueoRN.Add(ComprobanteBloqueoRN.Inizializate(calendar.get(GregorianCalendar.YEAR), i));
		}
		CreaDataSource();
	}

	private void CreaDataSource() {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(filtroAnio.getValue());
		table.setContainerDataSource(ComprobanteBloqueoContenedor.LeeContainerXAnio(calendar.get(GregorianCalendar.YEAR)));
		table.setVisibleColumns((Object[]) new String[] { COLUMNA_MES,COLUMNA_BLOQUEADO,COLUMNA_FECHA ,"responsable" });
		table.setColumnHeaders("Mes", "Bloqueado" ,"Fecha" ,"Responsable" );
		table.setColumnWidth(COLUMNA_MES, 200);
		table.setColumnWidth(COLUMNA_BLOQUEADO, 100);
		table.setColumnWidth(COLUMNA_FECHA , 200);
		table.setColumnWidth("responsable", 200);
	}

}
