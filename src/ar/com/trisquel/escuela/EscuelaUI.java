package ar.com.trisquel.escuela;

import java.util.Date;

import javax.servlet.http.Cookie;

import ar.com.trisquel.escuela.componentes.Body;
import ar.com.trisquel.escuela.componentes.Footer;
import ar.com.trisquel.escuela.componentes.Header;
import ar.com.trisquel.escuela.componentes.materias.MateriaWWView;
import ar.com.trisquel.escuela.componentes.otros.ArticuloWWView;
import ar.com.trisquel.escuela.componentes.tipocomprobante.TipoComprobanteWWView;
import ar.com.trisquel.escuela.data.reglasdenegocio.UsuarioRN;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.seguridad.HerramientasSeguridad;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.sun.jersey.core.util.Base64;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Title("College of")
@SuppressWarnings("serial")
@Theme("reindeer")
public class EscuelaUI extends UI {

	private Header header = new Header();
	private Body body = new Body();
	private Footer footer = new Footer();
	
	@Override
	protected void init(VaadinRequest request) {
		Object sesion = getSession().getAttribute(R.Session.SESSION);
		if (sesion == null){
			Sesion sesionActual = new Sesion();
			getSession().setAttribute(R.Session.SESSION, sesionActual);
		}
		ControlaCookies();
		// Crea los layouts principales, header, body  y footer
		setSizeFull();
		VerticalLayout layout = new VerticalLayout();
		layout.setWidth("100%");
		layout.addComponent(header);
		
		body.setHeight("100%");
		layout.addComponent(body);

		layout.addComponent(footer);

		setContent(layout);
		
		/**
		 * Inicializa la aplicacion
		 */
		Usuario usuario = UsuarioRN.Read("admin");
		if (usuario == null){
			usuario = UsuarioRN.Inizializate();
			usuario.setId("admin");
			try {
				usuario.setClave(HerramientasSeguridad.encripta("admin"));
			} catch (Exception e) {
			}
			usuario.setEstadoActual(Dominios.EstadoUsuario.ACTIVO);
			usuario.setFechaIniEstado(new Date(System.currentTimeMillis()));
			usuario.setNombre("Administrador");
			usuario.setTipoUsuario(Dominios.TipoUsuario.ADMINISTRADOR);
			UsuarioRN.Add(usuario);
			// Inicializa los articulos
			ArticuloWWView.inicializa();
			// Tipos de Comprobantes
			TipoComprobanteWWView.CreaTipoComprobantesDefecto();
			// Materias 
			MateriaWWView.CreaMateriasDefecto();
		}
		header.onDraw();
		body.onDraw();
		footer.onDraw();
	}

	private void ControlaCookies(){
		Sesion sesion = (Sesion)getSession().getAttribute(R.Session.SESSION);
		Cookie[] cookies =  VaadinService.getCurrentRequest().getCookies();
		if (sesion.getUsuarioId() == null || sesion.getUsuarioId().isEmpty()) {
            for (int i=0; i<cookies.length; i++) {
                if (Dominios.RECORDARME.equals(cookies[i].getName())){
                	try {
                		byte[] byteCookie = Base64.decode(cookies[i].getValue());
						String stringCookie = HerramientasSeguridad.desencripta(new String(byteCookie));
						String[] cookiesValues = stringCookie.split(Dominios.RECORDARME);
						if (cookiesValues.length == 2){
							Usuario usuario = UsuarioRN.Read(cookiesValues[0]);
							String passDesencriptada = HerramientasSeguridad.desencripta(usuario.getClave());
							if (passDesencriptada.equals(cookiesValues[1])){
								sesion.setUsuarioId(usuario.getId());
								sesion.setUsuarioNombre(usuario.getNombre());
								getSession().setAttribute(R.Session.SESSION ,sesion);
								stringCookie = HerramientasSeguridad.encripta(usuario.getId() + Dominios.RECORDARME.trim() + passDesencriptada);
								byteCookie = Base64.encode(stringCookie);
								Cookie cookie = new Cookie(Dominios.RECORDARME ,new String(byteCookie));
						        cookie.setPath(VaadinService.getCurrentRequest().getContextPath());
						        cookie.setMaxAge(640800); // 7 dias
						        VaadinService.getCurrentResponse().addCookie(cookie);
							}	
						}
					} catch (Exception e) {
					}
                }
            }
        }
	}
	
	public Header getHeader() {
		return header;
	}
	public Body getBody() {
		return body;
	}
	public Footer getFooter() {
		return footer;
	}
}