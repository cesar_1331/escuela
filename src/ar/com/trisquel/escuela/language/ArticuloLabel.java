package ar.com.trisquel.escuela.language;

import java.util.Locale;
import java.util.ResourceBundle;

public class ArticuloLabel {

	private static ResourceBundle resource = ResourceBundle.getBundle("ar/com/trisquel/escuela/language/Articulo");
	public static void inicializar(Locale locale){
		resource = ResourceBundle.getBundle("ar/com/trisquel/escuela/language/Articulo" ,locale);
	}
	public static String getTituloId(){
		return resource.getString("TituloId");
	}
	public static String getTituloTitulo(){
		return resource.getString("TituloTitulo");
	}
	public static String getTituloModificado(){
		return resource.getString("TituloModificado");
	}
	public static String getTituloResponsable(){
		return resource.getString("TituloResponsable");
	}
	public static String getTituloContenido(){
		return resource.getString("TituloContenido");
	}
	public static String getTitulo(){
		return resource.getString("Titulo");
	}
	public static String getId(){
		return resource.getString("Id");
	}
	public static String getContenido(){
		return resource.getString("Contenido");
	}
	public static String getResponsable(){
		return resource.getString("Responsable");
	}
	public static String getModificado(){
		return resource.getString("Modificado");
	}
}
