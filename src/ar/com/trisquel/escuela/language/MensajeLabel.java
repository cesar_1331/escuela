package ar.com.trisquel.escuela.language;

import java.util.Locale;
import java.util.ResourceBundle;

public class MensajeLabel {

	private static ResourceBundle resource  = ResourceBundle.getBundle("ar/com/trisquel/escuela/language/Mensajes");
	public static void inicializar(Locale locale){
		resource = ResourceBundle.getBundle("ar/com/trisquel/escuela/language/Mensajes" ,locale);
	}
	public static String getErrorEnviarMail(){
		return resource.getString("ErrorEnviarMail");
	}
	public static String getElTextoIngresadoNoEsCorrecto(){
		return resource.getString("ElTextoIngresadoNoEsCorrecto");
	}
	public static String getErrorErrorUsuarioSinMail(){
		return resource.getString("ErrorUsuarioSinMail");
	}
	public static String getErrorConfirmarMail(){
		return resource.getString("ConfirmarMail");
	}
	public static String getDebeIngresarElNombre(){
		return resource.getString("DebeIngresarElNombre");
	}
	public static String getDebeIngresarElAsunto(){
		return resource.getString("DebeIngresarElAsunto");
	}
	public static String getDebeIngresarElMail(){
		return resource.getString("DebeIngresarElMail");
	}
	public static String getDebeIngresarElMensaje(){
		return resource.getString("DebeIngresarElMensaje");
	}
	public static String getMensajeEnviado(){
		return resource.getString("MensajeEnviado");
	}
	public static String getMailInvalido(){
		return resource.getString("MailInvalido");
	}
	public static String getUsuarioClaveNoValidos(){
		return resource.getString("UsuarioClaveNoValidos");
	}
	public static String getUsuarioSuspendido(){
		return resource.getString("UsuarioSuspendido");
	}
	public static String getUsuarioDadoDeBaja(){
		return resource.getString("UsuarioDadoDeBaja");
	}
	public static String getDatosGuardados(){
		return resource.getString("DatosGuardados");
	}
	public static String getClavesDistintas(){
		return resource.getString("ClavesDistintas");
	}
	public static String getRestaurarClave(){
		return resource.getString("RestaurarClave");
	}
	public static String getRestaurarUsuario(){
		return resource.getString("RestaurarUsuario");
	}
	public static String getConfirmaElCambio(){
		return resource.getString("ConfirmaElCambio");
	}
	public static String getMensajeErrorParametros(){
		return resource.getString("MensajeErrorParametros");
	}
	public static String getConfirmaBorrarTelefono(){
		return resource.getString("ConfirmaBorrarTelefono");
	}
	public static String getConfirmaAccion(){
		return resource.getString("ConfirmaAccion");
	}
	public static String getUsuarioCreado(){
		return resource.getString("UsuarioCreado");
	}
	public static String getConfirmaEnvioMensaje(){
		return resource.getString("ConfirmaEnvioMensaje");
	}
	public static String getMensajeMailNoExiste(){
		return resource.getString("MensajeMailNoExiste");
	}
	public static String getMensajeMailNoCorrespondeUsuario(){
		return resource.getString("MensajeMailNoCorrespondeUsuario");
	}
	public static String getMensajeUsuarioNoExiste(){
		return resource.getString("MensajeUsuarioNoExiste");
	}
	public static String getMensajeRespuesNoEsCorrecta(){
		return resource.getString("MensajeRespuesNoEsCorrecta");
	}
	public static String getErrorTamanioImagen(){
		return resource.getString("ErrorTamanioImagen");
	}
	public static String getUsuarioRegistrado(){
		return resource.getString("UsuarioRegistrado");
	}
	public static String getMensajeErrorPeriodo(){
		return resource.getString("MensajeErrorPeriodo");
	}
	public static String getMensajeNumeroCelular(){
		return resource.getString("MensajeNumeroCelular");
	}
	public static String getErrorDebeIngresarCelular(){
		return resource.getString("ErrorDebeIngresarCelular");
	}
	public static String getErrorNumeroCelular(){
		return resource.getString("ErrorNumeroCelular");
	}
	public static String getConfirmaSuspendeUsuario(){
		return resource.getString("ConfirmaSuspendeUsuario");
	}
	public static String getConfirmaActivaUsuario(){
		return resource.getString("ConfirmaActivaUsuario");
	}
	public static String getConfirmaDarDeBajaUsuario(){
		return resource.getString("ConfirmaDarDeBajaUsuario");
	}
	public static String getConfirmaBorrarDato(){
		return resource.getString("ConfirmaBorrarDato");
	}
	
}
