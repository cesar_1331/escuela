package ar.com.trisquel.escuela.language;

import java.util.Locale;
import java.util.ResourceBundle;

public class EtiquetasLabel {
	
	private static ResourceBundle resource = ResourceBundle.getBundle("ar/com/trisquel/escuela/language/Etiquetas");
	public static void inicializar(Locale locale){
		resource = ResourceBundle.getBundle("ar/com/trisquel/escuela/language/Etiquetas" ,locale);
	}
	public static String getInicio(){
		return resource.getString("Inicio");
	}
	public static String getAutoridades(){
		return resource.getString("Autoridades");
	}
	public static String getOfertaAcademica(){
		return resource.getString("OfertaAcademica");
	}
	public static String getContacto(){
		return resource.getString("Contacto");
	}
	public static String getHistoria(){
		return resource.getString("Historia");
	}
	public static String getMiCuenta(){
		return resource.getString("MiCuenta");
	}
	public static String getMisAsignaturas(){
		return resource.getString("MisAsignaturas");
	}
	public static String getMisAlumnos(){
		return resource.getString("MisAlumnos");
	}
	public static String getMisCursos(){
		return resource.getString("MisCursos");
	}
	public static String getAlumnos(){
		return resource.getString("Alumnos");
	}
	public static String getProfesores(){
		return resource.getString("Profesores");
	}
	public static String getProfesoresPreceptores(){
		return resource.getString("ProfesoresPreceptores");
	}
	
	public static String getMaterias(){
		return resource.getString("Materias");
	}
	public static String getAsignaturas(){
		return resource.getString("Asignaturas");
	}
	public static String getCursos(){
		return resource.getString("Cursos");
	}
	public static String getPaginas(){
		return resource.getString("Paginas");
	}
	public static String getNotificaciones(){
		return resource.getString("Notificaciones");
	}
	public static String getMensajes(){
		return resource.getString("Mensajes");
	}
	public static String getErrores(){
		return resource.getString("Errores");
	}
	public static String getUsuarios(){
		return resource.getString("Usuarios");
	}
	public static String getLogin(){
		return resource.getString("Login");
	}
	public static String getAlumno(){
		return resource.getString("Alumno");
	}
	public static String getProfesor(){
		return resource.getString("Profesor");
	}
	public static String getDatosEscuela(){
		return resource.getString("DatosEscuela");
	}
	public static String getLogout(){
		return resource.getString("Logout");
	}
	public static String getUsersTitle(){
		return resource.getString("UsersTitle");
	}
	public static String getNameField(){
		return resource.getString("NameField");
	}
	public static String getUserTitle(){
		return resource.getString("UserTitle");
	}
	public static String getUser(){
		return resource.getString("User");
	}
	public static String getName(){
		return resource.getString("Name");
	}
	public static String getMail(){
		return resource.getString("Mail");
	}
	public static String getMailTitle(){
		return resource.getString("MailTitle");
	}
	public static String getPass(){
		return resource.getString("Pass");
	}
	public static String getPass_Rep(){
		return resource.getString("Pass_Rep");
	}
	public static String getProvincia(){
		return resource.getString("Provincia");
	}
	public static String getCiudad(){
		return resource.getString("Ciudad");
	}
	public static String getTelefono(){
		return resource.getString("Telefono");
	}
	public static String getCelular(){
		return resource.getString("Celular");
	}
	public static String getMailAlter(){
		return resource.getString("MailAlter");
	}
	public static String getDomicilio(){
		return resource.getString("Domicilio");
	}
	public static String getAsunto(){
		return resource.getString("Asunto");
	}
	public static String getMensaje(){
		return resource.getString("Mensaje");
	}
	public static String getTelefonoTitulo(){
		return resource.getString("TelefonoTitulo");
	}
	public static String getCelularTitulo(){
		return resource.getString("CelularTitulo");
	}
	public static String getRecordarMe(){
		return resource.getString("RecordarMe");
	}
	public static String getNoPuedeIngresar(){
		return resource.getString("NoPuedeIngresar");
	}
	public static String getForgotPass(){
		return resource.getString("ForgotPass");
	}
	public static String getForgotUser(){
		return resource.getString("ForgotUser");
	}
	public static String getContinuar(){
		return resource.getString("Continuar");
	}
	public static String getModificar(){
		return resource.getString("Modificar");
	}
	public static String getBorrar(){
		return resource.getString("Borrar");
	}
	public static String getDatos_del_Usuario(){
		return resource.getString("Datos_del_Usuario");
	}
	public static String getCambiarClave(){
		return resource.getString("CambiarClave");
	}
	public static String getIngreseElTextoDeLaImagen(){
		return resource.getString("IngreseElTextoDeLaImagen");
	}
	public static String getEnviar(){
		return resource.getString("Enviar");
	}
	public static String getCancelar(){
		return resource.getString("Cancelar");
	}
	public static String getGrabar(){
		return resource.getString("Grabar");
	}
	public static String getTodosLosCamposSonObligatorios(){
		return resource.getString("TodosLosCamposSonObligatorios");
	} 
	public static String getOtro(){
		return resource.getString("Otro");
	} 
	public static String getDescartar(){
		return resource.getString("Descartar");
	} 
	public static String getBuscar(){
		return resource.getString("Buscar");
	} 
	public static String getBusquedaAvanzada(){
		return resource.getString("BusquedaAvanzada");
	} 
	public static String getEstado(){
		return resource.getString("Estado");
	} 
	public static String getSeleccionar(){
		return resource.getString("Seleccionar");
	}
	public static String getDesde(){
		return resource.getString("Desde");
	}
	public static String getHasta(){
		return resource.getString("Hasta");
	}
	public static String getNuevo(){
		return resource.getString("Nuevo");
	}
	public static String getObservacion(){
		return resource.getString("Observacion");
	}
	public static String getObligatorio(){
		return resource.getString("Obligatorio");
	}
	public static String getNombre(){
		return resource.getString("Nombre");
	}
	public static String getHora(){
		return resource.getString("Hora");
	}
	public static String getMinutos(){
		return resource.getString("Minutos");
	}
	public static String getImagenes(){
		return resource.getString("Imagenes");
	}
	public static String getSiguiente(){
		return resource.getString("Siguiente");
	}
	public static String getPeriodo(){
		return resource.getString("Periodo");
	}
	public static String getCancelado(){
		return resource.getString("Cancelado");
	}
	public static String getConfirmar(){
		return resource.getString("Confirmar");
	}
	public static String getCumplir(){
		return resource.getString("Cumplir");
	}
	public static String getFecha(){
		return resource.getString("Fecha");
	}
	public static String getVerMas(){
		return resource.getString("VerMas");
	}
	public static String getAgregar(){
		return resource.getString("Agregar");
	}
	public static String getDe(){
		return resource.getString("De");
	}
	public static String getHoy(){
		return resource.getString("Hoy");
	}
	public static String getSemanaAnterior(){
		return resource.getString("SemanaAnterior");
	}
	public static String getMesAnterior(){
		return resource.getString("MesAnterior");
	}
	public static String getDesdeUnaFecha(){
		return resource.getString("DesdeUnaFecha");
	}
	public static String getActivo(){
		return resource.getString("Activo");
	}
	public static String getSuspendido(){
		return resource.getString("Suspendido");
	}
	public static String getDadoDeBaja(){
		return resource.getString("DadoDeBaja");
	}
	public static String getSuspender(){
		return resource.getString("Suspender");
	}
	public static String getActivar(){
		return resource.getString("Activar");
	}
	public static String getAsociar(){
		return resource.getString("Asociar");
	}
	public static String getImprimir(){
		return resource.getString("Imprimir");
	}
	public static String getSubirImagen(){
		return resource.getString("SubirImagen");
	}
	public static String getTodos(){
		return resource.getString("Todos");
	}
	public static String getDatosMostrados(){
		return resource.getString("DatosMostrados");
	}
	public static String getA(){
		return resource.getString("A");
	}
	public static String getMasculino(){
		return resource.getString("Masculino");
	}
	public static String getFemenino(){
		return resource.getString("Femenino");
	}
	public static String getAyuda(){
		return resource.getString("Ayuda");
	}
	public static String getExportar(){
		return resource.getString("Exportar");
	}
	public static String getExportarAPDF(){
		return resource.getString("ExportarAPDF");
	}
	public static String getExportarAExcel(){
		return resource.getString("ExportarAExcel");
	}
	public static String getFiltros(){
		return resource.getString("Filtros");
	}
	public static String getRefrescar(){
		return resource.getString("Refrescar");
	}
	public static String getAccion(){
		return resource.getString("Accion");
	}
	public static String getDarDeBaja(){
		return resource.getString("DarDeBaja");
	}
	public static String getParientes(){
		return resource.getString("Parientes");
	}
	public static String getSeleccioneMateria(){
		return resource.getString("SeleccioneMateria");
	}
	public static String getSeleccioneProfesor(){
		return resource.getString("SeleccioneProfesor");
	}
	public static String getAsistente(){
		return resource.getString("Asistente");
	}
	public static String getMateriaNoAsociadoAlCurso(){
		return resource.getString("MateriaNoAsociadoAlCurso");
	}
	public static String getAsociarAlumnoAsignatura(){
		return resource.getString("AsociarAlumnoAsignatura");
	}
	public static String getPlanilla(){
		return resource.getString("Planilla");
	}
	public static String getBuscarPlanilla(){
		return resource.getString("BuscarPlanilla");
	}
	public static String getCambiarEstado(){
		return resource.getString("CambiarEstado");
	}
	public static String getCopiar(){
		return resource.getString("Copiar");
	}
	public static String getCambiarEstadoAlumnos(){
		return resource.getString("CambiarEstadoAlumnos");
	}
	public static String getPermisos(){
		return resource.getString("Permisos");
	}
	public static String getHistorico(){
		return resource.getString("Historico");
	}
	public static String getTipoComprobantes(){
		return resource.getString("TipoComprobantes");
	} 
	public static String getComprobantes(){
		return resource.getString("Comprobantes");
	} 
	public static String getAdministrarMiEscuela(){
		return resource.getString("AdministrarMiEscuela");
	} 
	public static String getAdministrarCuentas(){
		return resource.getString("AdministrarCuentas");
	} 
	public static String getAdministrarSitio(){
		return resource.getString("AdministrarSitio");
	}
	public static String getMiMenu(){
		return resource.getString("MiMenu");
	} 	
	public static String getConceptos(){
		return resource.getString("Conceptos");
	} 	
	public static String getPersonas(){
		return resource.getString("Personas");
	}
	public static String getBloqueosComprobantes(){
		return resource.getString("BloqueosComprobantes");
	}
	public static String getBalance(){
		return resource.getString("Balance");
	}
	public static String getCalendario(){
		return resource.getString("Calendario");
	} 
	
}
