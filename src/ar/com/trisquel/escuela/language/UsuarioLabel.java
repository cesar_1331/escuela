package ar.com.trisquel.escuela.language;

import java.util.Locale;
import java.util.ResourceBundle;

public class UsuarioLabel {

	private static ResourceBundle resource = ResourceBundle.getBundle("ar/com/trisquel/escuela/language/Usuario");
	public static void inicializar(Locale locale){
		resource = ResourceBundle.getBundle("ar/com/trisquel/escuela/language/Etiquetas" ,locale);
	}
	
	public static String getDebeIngresarElUsuario(){
		return resource.getString("Debe_ingresar_el_usuario");
	}
	public static String getIdExcedio20Caracteres(){
		return resource.getString("Id_Excedio_20_Caracteres");
	}
	public static String getIDNoDebeTenesEspaciosEnBlanco(){
		return resource.getString("IDNoDebeTenesEspaciosEnBlanco");
	}
	public static String getExisteId(){
		return resource.getString("Existe_Id");
	}
	public static String getDebeIngresarNombre(){
		return resource.getString("Debe_Ingresar_Nombre");
	}
	public static String getNombreExcedio60Caracteres(){
		return resource.getString("Nombre_Excedio_60_Caracteres");
	}
	public static String getMailNoValido(){
		return resource.getString("Mail_No_Valido");
	}
	public static String getClaveValidacion(){
		return resource.getString("Clave_Validacion");
	}
	public static String getClaveConfirmadaDistinta(){
		return resource.getString("Clave_Confirmada_Distinta");
	}
	public static String getDebeIngresarMail(){
		return resource.getString("Debe_Ingresar_Mail");
	}
	public static String getDebeIngresarClave(){
		return resource.getString("DebeIngresarClave");
	}
	public static String getIdTitulo(){
		return resource.getString("IdTitulo");
	}
	public static String getNombreTitulo(){
		return resource.getString("NombreTitulo");
	}
	public static String getMailTitulo(){
		return resource.getString("MailTitulo");
	}
	public static String getTipoUsuarioTitulo(){
		return resource.getString("TipoUsuarioTitulo");
	}
	public static String getEstadoTitulo(){
		return resource.getString("EstadoTitulo");
	}
	public static String getId(){
		return resource.getString("Id");
	}
	public static String getNombre(){
		return resource.getString("Nombre");
	}
	public static String getMailPrincipal(){
		return resource.getString("MailPrincipal");
	}
	public static String getMailSecundario(){
		return resource.getString("MailSecundario");
	}
	public static String getTipoUsuario(){
		return resource.getString("TipoUsuario");
	}
	public static String getFoto(){
		return resource.getString("Foto");
	}
}
