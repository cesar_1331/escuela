package ar.com.trisquel.escuela.utiles;




public class Funciones {

	public static String substring(String cadena ,int beginIndex ,int endIndex ,String sufijo){
		int length = endIndex - beginIndex ;
		if (cadena == null){
			cadena = "";
		}else{
			if (cadena.length() > length){
				endIndex = endIndex - sufijo.length();
				cadena = cadena.substring(beginIndex, endIndex) + sufijo;
			}
		}
		return cadena;
	}

}
