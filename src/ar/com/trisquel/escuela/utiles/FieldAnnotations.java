package ar.com.trisquel.escuela.utiles;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value={ElementType.FIELD ,ElementType.METHOD ,ElementType.TYPE })
@Retention(RetentionPolicy.SOURCE)
public @interface FieldAnnotations  {
    public String Descripcion();
}