package ar.com.trisquel.escuela.utiles;
import java.awt.Container;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

public class ImagenResize {
    private String imgResult;
    private int calidad;
    private int width;
    private int height;
    private Image image;
    public ImagenResize(){
    }
    public ImagenResize(String imgOriginal ,String imgResult,int calidad ,int width ,int height){
        this.imgResult = imgResult;
        this.calidad = calidad;
        this.width = width;
        this.height = height;
        image = Toolkit.getDefaultToolkit().getImage(imgOriginal);
    }
    
    public ImagenResize(InputStream stream ,String imgResult,int calidad ,int width ,int height){
        this.imgResult = imgResult;
        this.calidad = calidad;
        this.width = width;
        this.height = height;
        try {
			image = ImageIO.read(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public void resize()throws Exception{
        //Cargamos la imagen y la preparamos para manipularla
        MediaTracker mediaTracker = new MediaTracker(new Container());
        mediaTracker.addImage(image, 0);
        mediaTracker.waitForID(0);
        //Calculamos la proporcion de la imagen original
        int imgWidth = image.getWidth(null);
        int imgHeight = image.getHeight(null);
        // Controla ancho de la imagen
        if (width == 0){
        	double factor = (double)height/(double)imgHeight;
        	width =  (int)(factor * imgWidth);
        }
        //Sacamos la proporcion de las nuevas medidas
        double tnsPro = (double)width/(double)height;
        
        double imgPro = (double)imgWidth / (double)imgHeight;
        //comprobamos cual de las dos medidas introducidas esta desproporcionada
        //para asi corregirla
        if (tnsPro < imgPro) {
            //asignamos una nueva height a la imagen generada
            //para mantener la proporcion con respecto a la original
            height = (int)(width / imgPro);
        } else {
            //lo mismo pero con el width
            width = (int)(height * imgPro);
        }
        
        //Creamos la imagen a generar, con los atributos requeridos, ancho, algo, y
        //el ultimo parametro especifica que vamos a guardar la imagen en formato "8-bit RGB"
        //que es un formato compatible con sistemas basados en Solaris o Windows
        //hay mas formatos, ver http://java.sun.com/j2se/1.4.2/docs/api/java/awt/image/BufferedImage.html
        BufferedImage tnsImg = new BufferedImage(width,height, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = tnsImg.createGraphics();
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.drawImage(image, 0, 0, width, height, null);
        
        //Preparamos para escribir la imagen generada en el disco
        BufferedOutputStream out = new BufferedOutputStream(new    FileOutputStream(imgResult));
        //creamos el "parseador" para guardar la imagen en formato JPG
        JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
        JPEGEncodeParam param = encoder.getDefaultJPEGEncodeParam(tnsImg);

        //Asignamos la calidad con la que se va a guardar la imagen de 0-100
        calidad = Math.max(0, Math.min(calidad, 100));
        param.setQuality((float)calidad / 100.0f, false);
        encoder.setJPEGEncodeParam(param);
        encoder.encode(tnsImg);
        out.close(); 
    }
    
    
    /**
     * @return Returns the calidad.
     */
    public int getCalidad() {
        return calidad;
    }
    /**
     * @param calidad The calidad to set.
     */
    public void setCalidad(int calidad) {
        this.calidad = calidad;
    }
    /**
     * @return Returns the height.
     */
    public int getHeight() {
        return height;
    }
    /**
     * @param height The height to set.
     */
    public void setHeight(int height) {
        this.height = height;
    }
    /**
     * @return Returns the imgResult.
     */
    public String getImgResult() {
        return imgResult;
    }
    /**
     * @param imgResult The imgResult to set.
     */
    public void setImgResult(String imgResult) {
        this.imgResult = imgResult;
    }
    /**
     * @return Returns the width.
     */
    public int getWidth() {
        return width;
    }
    /**
     * @param width The width to set.
     */
    public void setWidth(int width) {
        this.width = width;
    }
}  