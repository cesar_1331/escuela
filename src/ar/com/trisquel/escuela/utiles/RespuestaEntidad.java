package ar.com.trisquel.escuela.utiles;

public class RespuestaEntidad {
	private boolean isError;
	private String msgError;
	
	public RespuestaEntidad(){
		isError = false;
		msgError = "";
	}
	
	public RespuestaEntidad(boolean isError,String msgError){
		this.isError = isError;
		this.msgError = msgError;
	}
	
	public boolean isError() {
		return isError;
	}
	public void setError(boolean isError) {
		this.isError = isError;
	}
	public String getMsgError() {
		return msgError;
	}
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	
}
