package ar.com.trisquel.escuela.utiles;

import com.vaadin.server.ErrorMessage;

@SuppressWarnings("serial")
public class ErrorFieldMessage implements ErrorMessage{

	private String ErrorMensaje;
	private ErrorLevel errorLevel;
	public ErrorFieldMessage(String ErrorMensaje ,ErrorLevel errorLevel) {
		this.ErrorMensaje = ErrorMensaje;
		this.errorLevel = errorLevel;
	}
	@Override
	public ErrorLevel getErrorLevel() {
		return errorLevel;
	}

	@Override
	public String getFormattedHtmlMessage() {
		return ErrorMensaje;
	}
}