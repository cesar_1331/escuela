package ar.com.trisquel.escuela.utiles;

import org.vaadin.hene.popupbutton.PopupButton;

import ar.com.trisquel.escuela.language.EtiquetasLabel;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.themes.BaseTheme;

@SuppressWarnings("serial")
public class ButtonIcon extends Button {
	
	public static final int GUARDAR = 1;
	public static final int MODIFICAR = 2;
	public static final int ELIMINAR = 3;
	public static final int DESCARTAR = 4;
	public static final int AGREGAR = 5;
	public static final int SELECCIONAR = 6;
	public static final int ACTIVAR = 7;
	public static final int SUSPENDER = 8;
	public static final int CAMBIARCLAVE = 9;
	public static final int CONFIRMARASOCIACION = 10;
	public static final int ASOCIAR = 11;
	public static final int CANCELAR = 12;
	public static final int IMPRIMIR = 13;
	public static final int AYUDA = 14;
	public static final int AYUDA_BLANCO = 15;
	public static final int BUSCAR = 16;
	public static final int EXPORTAR_PDF = 17;
	public static final int EXPORTAR_EXCEL = 18;
	public static final int REFRESCAR = 19;
	public static final int ACCIONES = 20;
	public static final int DARDEBAJA = 21;
	public static final int USUARIO = 22;
	public static final int WIZARD = 23;
	public static final int CONFIRMAR = 24;
	public static final int COPIAR = 25;
	public static final int CALENDARIO = 26;
	
	
	public ButtonIcon(int boton ,boolean ShowIcon) {
		super();
		Config(boton ,ShowIcon );
	}
	
	public ButtonIcon(int boton ,boolean ShowIcon ,ClickListener listener) {
		super();
		addClickListener(listener);
		Config(boton ,ShowIcon );
	}
	
	private void Config(int boton ,boolean ShowIcon ){
		setPrimaryStyleName(BaseTheme.BUTTON_LINK);
		addStyleName(R.Style.CURSOR_POINTER);
		switch (boton) {
		case GUARDAR:
			setDescription(EtiquetasLabel.getGrabar());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_save.png"));
			}
			else{
				setCaption(EtiquetasLabel.getGrabar());
			}
			break;
		case MODIFICAR:
			setDescription(EtiquetasLabel.getModificar());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_edit.png"));
			}
			else{
				setCaption(EtiquetasLabel.getModificar());
			}
			break;
		case ELIMINAR:
			setDescription(EtiquetasLabel.getBorrar());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_delete.png"));
			}
			else{
				setCaption(EtiquetasLabel.getBorrar());
			}
			break;
		case DESCARTAR:
			setDescription(EtiquetasLabel.getDescartar());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_cancel.png"));
			}
			else{
				setCaption(EtiquetasLabel.getDescartar());
			}
			break;
		case AGREGAR:
			setDescription(EtiquetasLabel.getAgregar());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_add.png"));
			}
			else{
				setCaption(EtiquetasLabel.getAgregar());
			}
			break;
		case SELECCIONAR:
			setDescription(EtiquetasLabel.getSeleccionar());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_select.png"));
			}
			else{
				setCaption(EtiquetasLabel.getSeleccionar());
			}
			break;
		case ACTIVAR:
			setDescription(EtiquetasLabel.getActivar());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_active.png"));
			}
			else{
				setCaption(EtiquetasLabel.getActivar());
			}
			break;
		case SUSPENDER:
			setDescription(EtiquetasLabel.getSuspender());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_suspend.png"));
			}
			else{
				setCaption(EtiquetasLabel.getSuspender());
			}
			break;
		case CAMBIARCLAVE:
			setDescription(EtiquetasLabel.getCambiarClave());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_key.png"));
			}
			else{
				setCaption(EtiquetasLabel.getCambiarClave());
			}
			break;
		case CONFIRMARASOCIACION:
			setDescription(EtiquetasLabel.getConfirmar());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_confirm.png"));
			}
			else{
				setCaption(EtiquetasLabel.getConfirmar());
			}
			break;
		case ASOCIAR:
			setDescription(EtiquetasLabel.getAsociar());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_asociar.png"));
			}
			else{
				setCaption(EtiquetasLabel.getAsociar());
			}
			break;
	
		case CANCELAR:
			setDescription(EtiquetasLabel.getCancelar());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_cancel.png"));
			}
			else{
				setCaption(EtiquetasLabel.getCancelar());
			}
			break;
			
		case IMPRIMIR:
			setDescription(EtiquetasLabel.getImprimir());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_imprimir.png"));
			}
			else{
				setCaption(EtiquetasLabel.getImprimir());
			}
			break;
			
		case AYUDA:
			setDescription(EtiquetasLabel.getAyuda());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_help.png"));
			}
			else{
				setCaption(EtiquetasLabel.getAyuda());
			}
			break;
		case AYUDA_BLANCO:
			setDescription(EtiquetasLabel.getAyuda());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_help_blanco.png"));
			}
			else{
				setCaption(EtiquetasLabel.getAyuda());
			}
			break;
		case BUSCAR:
			setDescription(EtiquetasLabel.getBuscar());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_buscar.png"));
			}
			else{
				setCaption(EtiquetasLabel.getBuscar());
			}
			break;
		case EXPORTAR_PDF:
			setDescription(EtiquetasLabel.getExportarAPDF());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_pdf.png"));
			}
			else{
				setCaption(EtiquetasLabel.getExportarAPDF());
			}
			break;
		case EXPORTAR_EXCEL:
			setDescription(EtiquetasLabel.getExportarAExcel());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_excel.png"));
			}
			else{
				setCaption(EtiquetasLabel.getExportarAExcel());
			}
			break;
		case REFRESCAR:
			setDescription(EtiquetasLabel.getRefrescar());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_refresh.png"));
			}
			else{
				setCaption(EtiquetasLabel.getRefrescar());
			}
			break;
		case ACCIONES:
			setDescription(EtiquetasLabel.getAccion());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_accion.png"));
			}
			else{
				setCaption(EtiquetasLabel.getAccion());
			}
			break;
		case DARDEBAJA:
			setDescription(EtiquetasLabel.getDarDeBaja());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_accion.png"));
			}
			else{
				setCaption(EtiquetasLabel.getDarDeBaja());
			}
			break;
		case USUARIO:
			setDescription(EtiquetasLabel.getUser());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_user.png"));
			}
			else{
				setCaption(EtiquetasLabel.getUser());
			}
			break;
		case WIZARD:
			setDescription(EtiquetasLabel.getAsistente());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_wizard.png"));
			}
			else{
				setCaption(EtiquetasLabel.getAsistente());
			}
			break;
		case CONFIRMAR:
			setDescription(EtiquetasLabel.getConfirmar());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_confirm.png"));
			}
			else{
				setCaption(EtiquetasLabel.getConfirmar());
			}
			break;
		case COPIAR:
			setDescription(EtiquetasLabel.getCopiar());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_copiar.png"));
			}
			else{
				setCaption(EtiquetasLabel.getCopiar());
			}
			break;
		case CALENDARIO:
			setDescription(EtiquetasLabel.getCalendario());
			if (ShowIcon){
				setIcon(new ThemeResource("img/fila_calendario.png"));
			}
			else{
				setCaption(EtiquetasLabel.getCalendario());
			}
			break;	
		default:
			break;
		}
		if (!ShowIcon){
			addStyleName(R.Style.LABEL_COLOR_AZUL);
			addStyleName(R.Style.LABEL_UNDERLINE);
			addStyleName(R.Style.MARGIN_MINIMO);
		}
	}
	
	public static PopupButton PopupButton(int boton ,boolean ShowIcon){
		PopupButton buton = new PopupButton();
		buton.setPrimaryStyleName(BaseTheme.BUTTON_LINK);
		buton.addStyleName(R.Style.CURSOR_POINTER);
		switch (boton) {
		case AYUDA:
			buton.setDescription(EtiquetasLabel.getAyuda());
			if (ShowIcon){
				buton.setIcon(new ThemeResource("img/fila_help.png"));
			}
			else{
				buton.setCaption(EtiquetasLabel.getAyuda());
				buton.addStyleName(R.Style.LABEL_COLOR_AZUL);
				buton.addStyleName(R.Style.LABEL_UNDERLINE);
				buton.addStyleName(R.Style.MARGIN_MINIMO);
			}
			break;
		case ACCIONES:
			buton.setDescription(EtiquetasLabel.getAccion());
			if (ShowIcon){
				buton.setIcon(new ThemeResource("img/fila_accion.png"));
			}
			else{
				buton.setCaption(EtiquetasLabel.getAccion());
				buton.addStyleName(R.Style.LABEL_COLOR_AZUL);
				buton.addStyleName(R.Style.LABEL_UNDERLINE);
				buton.addStyleName(R.Style.MARGIN_MINIMO);
			}
			break;
		}
		return buton;
	}
}
