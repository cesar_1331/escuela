package ar.com.trisquel.escuela.utiles;


import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.ui.TextField;

@SuppressWarnings("serial")
public class TextFieldNumber extends com.vaadin.ui.TextField implements TextChangeListener {

	private boolean conDecimales = false;
	private String lastValue = "";
	public TextFieldNumber() {
        super();
        Config();
    }
	
	public boolean isConDecimales() {
		return conDecimales;
	}

	public void setConDecimales(boolean conDecimales) {
		this.conDecimales = conDecimales;
	}

	public TextFieldNumber(String caption){
		super(caption );
		Config();
	}
	
	public TextFieldNumber(String caption ,String Value){
		super(caption ,Value);
		Config();
	}
	@SuppressWarnings("rawtypes")
	public TextFieldNumber(com.vaadin.data.Property Datasource){
		super(Datasource);
		Config();
	}
	
	@SuppressWarnings("rawtypes")
	public TextFieldNumber(String caption ,com.vaadin.data.Property Datasource){
		super(caption ,Datasource);
		Config();
	}
	
    private void Config()
    {
    	
    	//addValidator(new com.vaadin.data.validator.DoubleRangeValidator("El valor no es valido", 0d,9999999999999999d));
    	setImmediate(true);
        setTextChangeEventMode(TextChangeEventMode.EAGER);
        addTextChangeListener(this);
    }

	@Override
	public void textChange(TextChangeEvent event) {
		String text = event.getText();
		if (conDecimales){
			text = text.replace(",", "");
		    try {
		    	if (text.trim().length() > 0){
		    		Double.parseDouble(text);
		    		lastValue = text;
		    	}
		    	else{
		    		lastValue = "";
		    		((TextField)event.getSource()).setValue("");
		    	}
		    } catch (NumberFormatException e) {
		    	String numero = lastValue.replace(",", "").replace("-", "");
		    	setValue(numero);
		    }
		}
		else{
			text = text.replace(".", "").replace(",", "");
		    try {
		    	if (text.trim().length() > 0){
		    		Long.parseLong(text);
		    		lastValue = text;
		    	}
		    	else{
		    		lastValue = "";
		    		((TextField)event.getSource()).setValue("");
		    	}
		    } catch (NumberFormatException e) {
		    	
		    	String numero = lastValue.replace(".", "").replace(",", "").replace("-", "");
		    	setValue(numero);
		    }
		}
	}

}
