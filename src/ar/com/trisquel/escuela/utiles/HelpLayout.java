package ar.com.trisquel.escuela.utiles;

import ar.com.trisquel.escuela.language.EtiquetasLabel;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class HelpLayout extends VerticalLayout {
	public static final int MICUENTA = 1;
	public static final int ALUMNO_PARIENTES = 2;
	public static final int PROFESOR_MATERIAS = 3;
	public HelpLayout(int opcion){
		addStyleName(R.Style.BACKGROUND_BLANCO);
		addStyleName(R.Style.BORDER_REDONDEADO);
		addStyleName(R.Style.PADDING_MINIMO);
		Label labelTitulo = new Label(EtiquetasLabel.getAyuda());
		labelTitulo.addStyleName(R.Style.LABEL_COLOR);
		labelTitulo.addStyleName(R.Style.LABEL_BOLD);
		labelTitulo.addStyleName(R.Style.PADDING_MINIMO);
		addComponent(labelTitulo);
		Label labelSeparator = new Label("<hr>" ,ContentMode.HTML);
		labelSeparator.addStyleName(R.Style.EXPAND_WIDTH_ALL);
		addComponent(labelSeparator);
		Label labelHelp = new Label("" ,ContentMode.HTML);
//		Articulo articulo = ArticuloRN.Inizializate();
//		switch (opcion) {
//		case MICUENTA:
//			articulo = ArticuloRN.Read(R.Paginas.HELP_MICUENTA);
//			labelHelp.setWidth("500px");
//			break;
//		case ALUMNO_PARIENTES:
//			articulo = ArticuloRN.Read(R.Paginas.HELP_ALUMNO_PARIENTES);
//			labelHelp.setWidth("500px");
//			break;
//		case PROFESOR_MATERIAS:
//			articulo = ArticuloRN.Read(R.Paginas.HELP_PROFESOR_MATERIA);
//			labelHelp.setWidth("500px");
//			break;
//		}
//		if (articulo != null){
//			labelHelp.setValue(articulo.getContenido());
//		}
		addComponent(labelHelp);
		addComponent(new Label("<br>" ,ContentMode.HTML));
	}
}
