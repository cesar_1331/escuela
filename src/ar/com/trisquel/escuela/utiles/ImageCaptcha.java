package ar.com.trisquel.escuela.utiles;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;

import javax.imageio.ImageIO;

import com.vaadin.server.StreamResource.StreamSource;

@SuppressWarnings("serial")
public class ImageCaptcha implements StreamSource {
    ByteArrayOutputStream imagebuffer = null;
    String TextoCaptcha;
    String FileCaptcha;
    private static final char[] symbols = new char[36];
    static {
      for (int idx = 0; idx < 10; ++idx)
        symbols[idx] = (char) ('0' + idx);
      for (int idx = 10; idx < 36; ++idx)
        symbols[idx] = (char) ('a' + idx - 10);
    }
    
	public ImageCaptcha() {
		TextoCaptcha = (new String(generarRamdom(10))).toUpperCase();
		FileCaptcha = (new String(generarRamdom(10))).toLowerCase() + ".png";
	}
	
	public static char[] generarRamdom(int len){
		char[] buf = new char[len];
		SecureRandom random = new SecureRandom();
		for (int idx = 0; idx < buf.length; ++idx) 
		      buf[idx] = symbols[random.nextInt(symbols.length)];
		return buf;
	}
	
    public InputStream getStream () {
    	BufferedImage image = new BufferedImage (200, 50,BufferedImage.TYPE_INT_RGB);
		Graphics drawable = image.getGraphics();
        drawable.setColor(Color.lightGray);
        drawable.fillRect(0,0,200,50);
        drawable.setColor(Color.black);
        Font fuente = new Font("Verdana,Tahoma,Arial,Helvetica" , Font.BOLD , 15);
        drawable.setFont(fuente);
        drawable.drawString(TextoCaptcha, 40, 30);
        try {
            imagebuffer = new ByteArrayOutputStream();
            ImageIO.write(image, "png", imagebuffer);
            
            return new ByteArrayInputStream(
                         imagebuffer.toByteArray());
        } catch (IOException e) {
            return null;
        }
    }
    
   
    
	public String getTextoCaptcha() {
		return TextoCaptcha;
	}


	public String getFileCaptcha() {
		return FileCaptcha;
	}

}
