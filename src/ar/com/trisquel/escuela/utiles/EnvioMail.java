package ar.com.trisquel.escuela.utiles;

import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class EnvioMail {
    final static String miCorreo = "misturnosya@gmail.com";
    final static String miContraseņa = "cobY3319";
    final static String servidorSMTP = "smtp.gmail.com";
    final static String puertoEnvio = "465";

    public static RespuestaEntidad Enviar(String mailReceptor, String asunto,String cuerpo ,List<String> listAttach) {
    	RespuestaEntidad respuesta = new RespuestaEntidad();
        Properties props = new Properties();
        props.put("mail.smtp.user", miCorreo);
        props.put("mail.smtp.host", servidorSMTP);
        props.put("mail.smtp.port", puertoEnvio);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.socketFactory.port", puertoEnvio);
        props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");

        @SuppressWarnings("unused")
		SecurityManager security = System.getSecurityManager();
        try {
            Authenticator auth = new autentificadorSMTP();
            Session session = Session.getInstance(props, auth);
            // session.setDebug(true);
            MimeMessage msg = new MimeMessage(session);
            msg.setContent(cuerpo, "text/html; charset=utf-8");
            msg.setSubject(asunto);
            msg.setFrom(new InternetAddress(miCorreo));
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(mailReceptor));
            
            if (listAttach.size() > 0){
	            Multipart multipart = new MimeMultipart("mixed");
	            for (String attach : listAttach) {
	                MimeBodyPart messageBodyPart = new MimeBodyPart();
	                DataSource source = new FileDataSource(attach);
	                messageBodyPart.setDataHandler(new DataHandler(source));
	                messageBodyPart.setFileName(source.getName());
	                multipart.addBodyPart(messageBodyPart);
	            }
	            msg.setContent(multipart);
            }
            Transport.send(msg);
            return respuesta;
        } catch (Exception e) {
        	respuesta.setError(true);
        	respuesta.setMsgError(e.toString());
            return respuesta;
        }
    }

    private static class autentificadorSMTP extends javax.mail.Authenticator {
        public PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(miCorreo, miContraseņa);
        }
    }

}