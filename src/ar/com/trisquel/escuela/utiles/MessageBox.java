package ar.com.trisquel.escuela.utiles;

import java.io.Serializable;
import java.util.ResourceBundle;

import ar.com.trisquel.escuela.utiles.MessageBoxResources.IconResource;
import ar.com.trisquel.escuela.utiles.Recursos.Parametros;

import com.vaadin.server.Resource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.BaseTheme;

@SuppressWarnings("serial")
public class MessageBox extends Window {

	public static String DIALOG_DEFAULT_WIDTH = "365px";
	public static String DIALOG_DEFAULT_HEIGHT = "200px";
	public static String BUTTON_DEFAULT_WIDTH = "100px";
	public static Alignment BUTTON_DEFAULT_ALIGNMENT = Alignment.MIDDLE_LEFT;
	public static String ICON_DEFAULT_SIZE = "48px";
	public static MessageBoxResources RESOURCE_FACTORY = new MessageBoxResources();
	private MessageBoxListener listener;

	public static enum Icon {
		NONE,
		QUESTION,
		INFO,
		WARN,
		ERROR
	}

	public static enum ButtonId {
		OK,
		ABORT,
		CANCEL,
		YES,
		NO,
		CLOSE,
		SAVE,
		RETRY,
		IGNORE,
		/**
		 * The message box is not closed on using this value. You may have to explicitly close the message box.
		 */
		HELP,
		NONE,
		CUSTOM1,
		CUSTOM2,
		CUSTOM3,
		CUSTOM4,
		CUSTOM5
	}

	public static class ButtonConfig implements Serializable {

		private static final long serialVersionUID = 1L;

		private Resource optionalResource;
		private ButtonId buttonType;
		private String caption;
		private String width;

		/**
		 * Creates an instance of this class for defining the appearance and 
		 * caption of a button displayed inside the message box.
		 * 
		 * @param buttonType 			which button type (see {@link ButtonId})
		 * @param caption				caption of the button
		 */
		public ButtonConfig(ButtonId buttonType, String caption) {
			this(buttonType, caption, BUTTON_DEFAULT_WIDTH);
		}
		/**
		 * Equals to {@link #MessageBox.ButtonConfig(de.steinwedel.vaadin.MessageBox.ButtonId, java.lang.String) ButtonConfig(ButtonId, String)}, 
		 * but the button width is set explicitly instead of using {@code BUTTON_DEFAULT_WIDTH}.
		 * 
		 * @param width button width
		 */
		public ButtonConfig(ButtonId buttonType,
				String caption, 
				String width) {
			this(buttonType, caption, width, null);
		}

		/**
		 * Equals to {@link #MessageBox.ButtonConfig(de.steinwedel.vaadin.MessageBox.ButtonId, java.lang.String, java.lang.String) ButtonConfig(ButtonId, String, String)}, 
		 * but the button  icon resource is set explicitly.
		 * 
		 * @param optionalResource		an none default resource, that should be displayed as icon
		 */
		public ButtonConfig(ButtonId buttonType,
				String caption, 
				String width,
				Resource optionalResource) {
			super();
			
			ResourceBundle mensajes =  ResourceBundle.getBundle("ar/com/trisquel/escuela/language/MessageBox" ,UI.getCurrent().getLocale());
			if (buttonType == null) {
				throw new IllegalArgumentException("The field buttonType cannot be null");
			}
			if (caption == null){
				switch (buttonType) {
				case ABORT:
					caption = mensajes.getString("Abortar");
					break;
				case CANCEL:
					caption = mensajes.getString("Cancelar");
					break;
				case CLOSE:
					caption = mensajes.getString("Cerrar");
					break;
				case HELP:
					caption = mensajes.getString("Ayuda");
					break;
				case OK:
					caption = mensajes.getString("Aceptar");
					break;
				case YES:
					caption = mensajes.getString("Si");
					break;
				case NO:
					caption = mensajes.getString("No");
					break;
				case SAVE:
					caption = mensajes.getString("Guardar");
					break;
				case RETRY:
					caption = mensajes.getString("Reintentar");
					break;
				case IGNORE:
					caption = mensajes.getString("Ignorar");
					break;
				default:
					break;
				}
			}
			this.optionalResource = optionalResource;
			this.buttonType = buttonType;
			this.caption = caption;
			this.width = width;
		}

		/**
		 * Returns the optional resource, if set.
		 * 
		 * @return optional resource.
		 */
		public Resource getOptionalResource() {
			return optionalResource;
		}

		/**
		 * Returns the button type.
		 * 
		 * @return button type
		 */
		public ButtonId getButtonId() {
			return buttonType;
		}

		/**
		 * Returns the button caption.
		 * 
		 * @return button caption
		 */
		public String getCaption() {
			return caption;
		}

		/**
		 * Returns the button width.
		 * 
		 * @return button width
		 */
		public String getWidth() {
			return width;
		}

	}

	public interface MessageBoxListener extends Serializable {

		/**
		 * The method is triggered when a button of the message box is pressed. The
		 * parameter describes, which configured button was pressed.
		 * 
		 * @param buttonType pressed button
		 */
		public void buttonClicked(ButtonId buttonType);

	}
	public class ButtonClickListener implements ClickListener {

		private static final long serialVersionUID = 1L;

		private ButtonId buttonType;
		/**
		 * The constructor.
		 */
		public ButtonClickListener(ButtonId buttonType) {
			this.buttonType = buttonType;
		}

		/**
		 * @see com.vaadin.ui.Button.ClickListener#buttonClick(com.vaadin.ui.Button.ClickEvent)
		 */
		@Override
		public void buttonClick(ClickEvent event) {
			if (!buttonType.equals(ButtonId.HELP)) {
				close();
			}
			if (listener != null) {
				listener.buttonClicked(buttonType);
			}
		}
	}
	

	public static void showHTML(Icon dialogIcon, 
			String dialogCaption,
			String message, 
			ButtonId... buttonIds) {
		
		ButtonConfig[] buttonConfigs = new ButtonConfig[buttonIds.length];
		int i = 0;
		for (ButtonId buttonId : buttonIds) {
			ButtonConfig buttonConfig = new ButtonConfig(buttonId, null ,null);
			buttonConfigs[i] = buttonConfig;
			i++;
		}
		MessageBox messageBox = new MessageBox(DIALOG_DEFAULT_WIDTH, 
				DIALOG_DEFAULT_HEIGHT,
				dialogCaption, 
				dialogIcon, 
				new Label(message ,ContentMode.HTML),
				null,
				BUTTON_DEFAULT_ALIGNMENT, 
				buttonConfigs);
		
		UI.getCurrent().addWindow(messageBox);
		messageBox.focus();
	}
	
	
	public static void showHTML(
			Icon dialogIcon, 
			String dialogCaption, 
			String message,
			MessageBoxListener listener,
			ButtonId... buttonIds){
		
		ButtonConfig[] buttonConfigs = new ButtonConfig[buttonIds.length];
		int i = 0;
		for (ButtonId buttonId : buttonIds) {
			ButtonConfig buttonConfig = new ButtonConfig(buttonId, null ,null);
			buttonConfigs[i] = buttonConfig;
			i++;
		}
		MessageBox messageBox = new MessageBox(DIALOG_DEFAULT_WIDTH, 
				DIALOG_DEFAULT_HEIGHT,
				dialogCaption, 
				dialogIcon, 
				new Label(message ,ContentMode.HTML),
				listener ,
				BUTTON_DEFAULT_ALIGNMENT, 
				buttonConfigs);
		UI.getCurrent().addWindow(messageBox);
		messageBox.focus();
	}
	
	public MessageBox(String dialogWidth, 
			String dialogHeight,
			String dialogCaption, 
			Icon dialogIcon, 
			Component messageComponent,
			MessageBoxListener listener,
			Alignment buttonsAlignment, 
			ButtonConfig... buttonConfigs) {
		super();
		this.listener = listener;
		setWidth(dialogWidth);
		setHeight(dialogHeight);
		setResizable(false);
		//setClosable(false);
		setModal(true);
		center();
		if (dialogCaption.isEmpty()){
			dialogCaption = Parametros.getAppTituloSistema();
		}
		setCaption(dialogCaption);

		GridLayout mainLayout = new GridLayout(2, 2);
		mainLayout.setMargin(true);
		mainLayout.setSpacing(true);

		// Add Content
		if (dialogIcon == null || Icon.NONE.equals(dialogIcon)) {
			mainLayout.addComponent(new Label());
		} else {
			Embedded icon = null;
			switch (dialogIcon) {
			case QUESTION:
				icon = new Embedded(null, RESOURCE_FACTORY.loadResource(IconResource.QUESTION));
				break;
			case INFO:
				icon = new Embedded(null, RESOURCE_FACTORY.loadResource(IconResource.INFO));
				break;
			case WARN:
				icon = new Embedded(null, RESOURCE_FACTORY.loadResource(IconResource.WARN));
				break;
			case ERROR:
				icon = new Embedded(null, RESOURCE_FACTORY.loadResource(IconResource.ERROR));
				break;
			default:
				break;
			}
			mainLayout.addComponent(icon);
			icon.setWidth(ICON_DEFAULT_SIZE);
			icon.setHeight(ICON_DEFAULT_SIZE);
		}
		messageComponent.setWidth("280px");
		mainLayout.addComponent(messageComponent);

		// Add Buttons
		mainLayout.addComponent(new Label());
		HorizontalLayout buttonLayout = new HorizontalLayout();
		//buttonLayout.setSpacing(true);
		mainLayout.addComponent(buttonLayout);
		mainLayout.setComponentAlignment(buttonLayout, buttonsAlignment);
		for (ButtonConfig buttonConfig : buttonConfigs) {
			Button buttonIcon = new Button("", new ButtonClickListener(buttonConfig.getButtonId()));
			Button button = new Button(buttonConfig.getCaption(), new ButtonClickListener(buttonConfig.getButtonId()));
			buttonIcon.setPrimaryStyleName(BaseTheme.BUTTON_LINK);
			buttonIcon.addStyleName(R.Style.CURSOR_POINTER);
			button.setPrimaryStyleName(BaseTheme.BUTTON_LINK);
			button.addStyleName(R.Style.LABEL_COLOR_AZUL);
			button.addStyleName(R.Style.LABEL_UNDERLINE);
			button.addStyleName(R.Style.CURSOR_POINTER);
			button.addStyleName(R.Style.MARGIN_MINIMO);
			
			if (buttonConfig.getWidth() != null) {
				buttonIcon.setWidth(buttonConfig.getWidth());
			}
			if (buttonConfig.getOptionalResource() != null) {
				buttonIcon.setIcon(buttonConfig.getOptionalResource());
			} else {
				Resource icon = null;
				switch (buttonConfig.getButtonId()) {
				case ABORT:
					icon = RESOURCE_FACTORY.loadResource(IconResource.ABORT);
					break;
				case CANCEL:
					icon = RESOURCE_FACTORY.loadResource(IconResource.CANCEL);
					break;
				case CLOSE:
					icon = RESOURCE_FACTORY.loadResource(IconResource.CLOSE);
					break;
				case HELP:
					icon = RESOURCE_FACTORY.loadResource(IconResource.HELP);
					break;
				case OK:
					icon = RESOURCE_FACTORY.loadResource(IconResource.OK);
					break;
				case YES:
					icon = RESOURCE_FACTORY.loadResource(IconResource.YES);
					break;
				case NO:
					icon = RESOURCE_FACTORY.loadResource(IconResource.NO);
					break;
				case SAVE:
					icon = RESOURCE_FACTORY.loadResource(IconResource.SAVE);
					break;
				case RETRY:
					icon = RESOURCE_FACTORY.loadResource(IconResource.RETRY);
					break;
				case IGNORE:
					icon = RESOURCE_FACTORY.loadResource(IconResource.IGNORE);
					break;
				default:
					break;
				}
				buttonIcon.setIcon(icon);
			}
			buttonLayout.addComponent(buttonIcon);
			buttonLayout.addComponent(button);
			buttonLayout.addComponent(new Label(R.CaracteresEsteciales.SPACIO+R.CaracteresEsteciales.SPACIO+R.CaracteresEsteciales.SPACIO ,ContentMode.HTML));
		}

		setContent(mainLayout);
	}


}
