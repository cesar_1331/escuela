package ar.com.trisquel.escuela.utiles;

import java.util.ResourceBundle;

public final class Recursos {
	
	public static final class Parametros{
		public static String getAppHost(){
			ResourceBundle resource = ResourceBundle.getBundle("Parametros" );
			return resource.getString("Host");
		}
		public static String getAppFolderBase(){
			ResourceBundle resource = ResourceBundle.getBundle("Parametros" );
			return resource.getString("FolderBase");
		}
		public static String getAppTituloSistema(){
			ResourceBundle resource = ResourceBundle.getBundle("Parametros" );
			return resource.getString("TituloSistema");
		}
		public static String getCarpetaTemporal(){
			ResourceBundle resource = ResourceBundle.getBundle("Parametros" );
			return resource.getString("CarpetaTemporal");
		}
		public static String getIdioma(){
			ResourceBundle resource = ResourceBundle.getBundle("Parametros" );
			return resource.getString("Idioma");
		}
		public static boolean isModoProduction() {
			ResourceBundle resource = ResourceBundle.getBundle("Parametros" );
			boolean production = false;
			if (resource.getString("Production").toLowerCase().equals("true"))
				production = true;
			return production;
		}
	}
}
