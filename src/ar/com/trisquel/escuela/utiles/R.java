package ar.com.trisquel.escuela.utiles;

public final class R {

	
	public R() {
	}
	public static final class CaracteresEsteciales{
		public static final String SPACIO = "&nbsp;";
	}
	
	
	public static final class Paginas{
		public static final String INICIO = "INICIO";
		public static final String ERROR = "ERROR";
		public static final String OFERTA_ACADEMICA = "OFERTAACADEMICA";
		public static final String AUTORIDADES = "AUTORIDADES";
		public static final String HISTORIA = "HISTORIA";
		public static final String MAILREESTABLECERCLAVE = "MAILREESTABLECER";
		public static final String ACCESO_DENEGADO = "ACCESO_DENEGADO";
//		public static final String ACCESODENEGADO_DEBEESTARLOGUEADO = "DebeEstarLogueado";
//		public static final String ACCESODENEGADO_NOPOSEEPERMISOS = "NoPoseePermisos";
	}
	
	public static final class Session{
		public static final String SESSION = "SESSION";
		public static final String NAVIGATORHISTORY = "NAVIGATORHISTORY";
	}
	
	public static final class StyleHeader{
		public static final String LAYOUT = "header-layout";
		public static final String LINK = "button-link-header";
	}
	
	public static final class StyleFooter{
		public static final String TEXT = "footer-text";
		public static final String LAYOUTMENU = "footer-menu-layout";
		public static final String LAYOUTFOOTER = "footer-layout";
		public static final String LINK = "button-link-footer";
	}
	
	public static final class Style{
		public static final String HORIZONTAL_GROUP = "horizontal";
		public static final String TEXTO_MAYUSCULA = "text-uppercase";
		public static final String LAYOUT_CATEGIRIA_SELECCIONADA = "categoria-seleccionada-layout";
		public static final String BUTTON_HEADER = "button-Header";
		public static final String BUTTON_COLOR = "button-Color";
		public static final String BUTTON_IMAGE = "button-image";
		public static final String LINK = "button-link";
		public static final String LINK_BLACK = "button-link-black";
		public static final String LINK_COLOR = "button-link-color";
		public static final String LINK_TITLE = "button-Title-link";
		public static final String TITLE = "title-text";
		public static final String SUBTITLE_DATE = "Subtitle-inlineDate";
		public static final String TITLE_COLOR = "title-text-color";
		public static final String TITLE_ERROR = "title-text-error";
		public static final String TEXTO_ERROR = "text-error";
		public static final String LISTA1 = "PanelGrid";
		public static final String LISTA2 = "GridPrestadores";
		public static final String ERROR_VIEW = "text-message-error";
		public static final String LABEL = "label";
		public static final String EDITABLE = "label-editable";
		public static final String EDITABLE_LABEL = "label-editable-label";
		public static final String EDITABLE_LABEL_MINIMO = "label-editable-label-minimo";
		public static final String LABEL_HEIGTH_MINIMO = "Label-Heigth-Minimo";
		public static final String LABEL_TREE = "label-tree";
		public static final String LABEL_BOLD = "label-bold";
		public static final String LABEL_COLOR = "label-color";
		public static final String LABEL_COLOR_BLANCO = "label-color-blanco";
		public static final String LABEL_COLOR_NARANJA = "label-color-naranja";
		public static final String LABEL_COLOR_AZUL = "label-color-azul";
		public static final String LABEL_UNDERLINE = "label-underline";
		public static final String EXPAND_WIDTH_ALL = "ExpandWidthAll";
		public static final String EXPAND_HEIGHT_ALL = "ExpanHeightAll";
		public static final String BORDER_MINIMO = "BorderThin";
		public static final String BORDER_CLARO = "Border-Claro";
		public static final String BORDER_MINIMO_LATERAL = "BorderThinLateral";
		public static final String BORDER_MINIMO_LATERAL_COLOR = "BorderThinLateralColor";
		public static final String BORDER_MINIMO_DERECHA = "BorderThinRight";
		public static final String BORDER_MINIMO_IZQUIERDA = "BorderThinLeft";
		public static final String BORDER_MINIMO_ARRIBA = "BorderThinTop";
		public static final String BORDER_MINIMO_ABAJO = "BorderThinBooton";
		public static final String BORDER_REDONDEADO = "BorderRedondeado";
		public static final String BORDER_SEPARATOR = "Border-Separator";
		public static final String BORDER_SEPARATOR_SERVICIO = "Border-Separator-Servicio";
		public static final String BORDER_TITULO_GRIS = "BorderTituloGris";
		public static final String BORDER_AGREGAR_COLOR = "Border-Agregar-Color";
		public static final String TEXTO_SUBTITULO_COLOR = "labelSubTitleColor";
		public static final String TEXTO_SUBTITULO_COLOR_GRIS = "labelSubTitleColor-Gris";
		public static final String PADDING_MINIMO = "PaddingThin";
		public static final String PADDING_MEDIO = "PaddingMedium";
		public static final String MARGIN_MINIMO = "MarginThin";
		public static final String MARGIN_MEDIO = "MarginMedium";
		public static final String MARGIN_LARGO = "MarginLarge";
		public static final String MARGIN_DERECHA_LARGO = "MarginRigthLarge";
		public static final String TEXTO_SMALL = "label-size-small";
		public static final String TEXTO_MEDIUM_SMALL = "label-size-medium-small";
		public static final String TEXTO_MEDIUM_BIG = "label-medium-big";
		public static final String TEXTO_BIG = "label-big";
		public static final String TEXTO_COLOR_GRANDE = "text-color-grande";		
		public static final String TEXTO_ALINEACION_DERECHA = "Text-Align-Rigth";
		public static final String TEXTO_ALINEACION_CENTRO = "Text-Align-Center";
		public static final String TOKEN_FIELD = "tokenfield";
		public static final String TOKEN_SELECT = "tokenSelect";
		public static final String SUBTITULO_FILTROS = "subtitulo-filtros";
		public static final String ALINEACION_TOP = "Vertical-Align-Top";
		public static final String ALINEACION_VERTICAL_CENTRADO = "Vertical-Align-Center";		
		public static final String CURSOR_POINTER = "cursor-pointer";
		public static final String BACKGROUND_OCUPADO = "Background-Ocupado";
		public static final String BACKGROUND_DISPONIBLE = "Background-Disponible";
		public static final String BACKGROUND_NOCORRESPONDE = "Background-NoCorresponde";
		public static final String BACKGROUND_TITULO_COLOR = "Background-Titulo-Color";
		public static final String BACKGROUND_FILA_1 = "Background-Fila-1";
		public static final String BACKGROUND_FILA_2 = "Background-Fila-2";
		public static final String BACKGROUND_SEPARADOR_CALENDARIO = "Background-Separador-Calendario";
		public static final String BACKGROUND_GRIS = "layout-Color-Gris";
		public static final String BACKGROUND_TRANSPARENTE = "background-transparente";
		public static final String BACKGROUND_BLANCO = "background-blanco";
		public static final String LABEL_PENDIENTE = "label-Pendiente";
		public static final String LABEL_CONFIRMADO = "label-Confirmado";
		public static final String LABEL_CUMPLIDO = "label-Cumplido";
		public static final String LABEL_CANCELADO = "label-Cancelado";
		public static final String LABEL_SERVICIOSUSPENDIDO = "label-ServicioSuspendido";
		public static final String LABEL_COLOR_ROJO = "label-color-rojo";
		public static final String LABEL_COLOR_ROJO_OSCURO = "label-color-rojo-oscuro";
		public static final String LABEL_PREGUNTA = "text-pregunta";
		public static final String LABEL_PREGUNTA_OBLIGATORIO = "text-pregunta-obligatorio";
		public static final String LABEL_SUBTITULO_MENU = "SubTitulo-MenuDesplegable";
		public static final String IMAGEN_SCALE_150 = "image-scale-150";
		public static final String IMAGEN_SCALE_30 = "image-scale-30";
		public static final String TABLE_CHECKBOX_EDITABLE = "editable";
			
	}
}