package ar.com.trisquel.escuela.utiles;


import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.server.Resource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;

@SuppressWarnings("serial")
public class ButtonImage extends GridLayout {
	
	public ButtonImage(String texto ,Resource resource ) {
		super();
		Config(texto ,resource);
	}
	
	public ButtonImage(String texto ,Resource resource ,LayoutClickListener listener) {
		super();
		this.addLayoutClickListener(listener);
		Config(texto ,resource);
	}
	
	private void Config(String texto ,Resource resource ){
		addStyleName(R.Style.BUTTON_IMAGE);
		setWidth("120px");
		setHeight("120px");
		Image image = new Image();
		image.setSource(resource);
		image.setWidth("64px");
		image.setHeight("64px");
		addComponent(image);
		setComponentAlignment(image, Alignment.TOP_CENTER);
		Label label = new Label(texto);
		label.addStyleName(R.Style.TEXTO_ALINEACION_CENTRO);
		label.setWidth("100px");
		label.setHeight("40px");
		addComponent(label);
		setComponentAlignment(label, Alignment.TOP_CENTER);
	}
}
