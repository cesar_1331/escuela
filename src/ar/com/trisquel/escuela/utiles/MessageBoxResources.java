package ar.com.trisquel.escuela.utiles;

import java.io.Serializable;

import com.vaadin.server.ClassResource;
import com.vaadin.server.Resource;

public class MessageBoxResources implements Serializable {
	
	private static final long serialVersionUID = 1L;

	/**
	 * A enumeration with named resources.
	 * 
	 * @author Dieter Steinwedel
	 */
	public static enum IconResource {
		QUESTION,
		INFO,
		WARN,
		ERROR,
		OK,
		ABORT,
		CANCEL,
		YES,
		NO,
		CLOSE,
		SAVE,
		RETRY,
		IGNORE,
		HELP,
		CUSTOM1,
		CUSTOM2,
		CUSTOM3,
		CUSTOM4,
		CUSTOM5
	}
	
	/**
	 * Loads the specified icon for the application.
	 * @param icon				The requested icon
	 * @param application		The application, where the icon is used
	 * @return					The loaded icon
	 */
	public Resource loadResource(IconResource icon) {
		switch (icon) {
		case QUESTION:
			return new ClassResource(MessageBoxResources.class, "images/question.png");
		case INFO:
			return new ClassResource(MessageBoxResources.class, "images/info.png");
		case WARN:
			return new ClassResource(MessageBoxResources.class, "images/warn.png");
		case ERROR:
			return new ClassResource(MessageBoxResources.class, "images/error.png");
		case OK:
			return new ClassResource(MessageBoxResources.class, "images/tick.png");
		case ABORT:
			return new ClassResource(MessageBoxResources.class, "images/cross.png");
		case CANCEL:
			return new ClassResource(MessageBoxResources.class, "images/cross.png");
		case YES:
			return new ClassResource(MessageBoxResources.class, "images/tick.png");
		case NO:
			return new ClassResource(MessageBoxResources.class, "images/cross.png");
		case CLOSE:
			return new ClassResource(MessageBoxResources.class, "images/door.png");
		case SAVE:
			return new ClassResource(MessageBoxResources.class, "images/disk.png");
		case RETRY:
			return new ClassResource(MessageBoxResources.class, "images/arrow_refresh.png");
		case IGNORE:
			return new ClassResource(MessageBoxResources.class, "images/lightning_go.png");
		case HELP:
			return new ClassResource(MessageBoxResources.class, "images/lightbulb.png");
		default:
			return null;
		}
	}

}
