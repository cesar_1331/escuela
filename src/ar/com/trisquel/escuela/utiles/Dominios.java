package ar.com.trisquel.escuela.utiles;

import ar.com.trisquel.escuela.language.EtiquetasLabel;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.OptionGroup;

public final class Dominios {
	
	public static final String RECORDARME = "95D0D55DC8F243C99BE585638EA77B2C";
	public static final String TODOS_STRING_COMBO = "TOD";
	public static final int TODOS_NUMERO_COMBO = 0;
	public static final class TipoUsuario{
		public static final String ARRAY[] = {"" ,"Alumno" ,"Padre" ,"Profesor" ,"Administrativo" ,"" ,"" ,"" ,"" ,"Administrador"};
		public static final int ALUMNO = 1;
		public static final int PADRE = 2;
		public static final int PROFESOR = 3;
		public static final int ADMINISTRATIVO = 4;
		public static final int ADMINISTRADOR = 9;
		public static ComboBox CreaComboBox(boolean agregarTodos){
			ComboBox tipoUsuario = new ComboBox();
			tipoUsuario.setTextInputAllowed(false);
			if (agregarTodos){
				tipoUsuario.addItem(TODOS_NUMERO_COMBO);
				tipoUsuario.setItemCaption(TODOS_NUMERO_COMBO,EtiquetasLabel.getTodos());
			}
			tipoUsuario.addItem(ALUMNO);
			tipoUsuario.setItemCaption(ALUMNO ,ARRAY[ALUMNO]);
			tipoUsuario.addItem(PROFESOR);
			tipoUsuario.setItemCaption(PROFESOR ,ARRAY[PROFESOR]);
			tipoUsuario.addItem(ADMINISTRATIVO);
			tipoUsuario.setItemCaption(ADMINISTRATIVO ,ARRAY[ADMINISTRATIVO]);
			tipoUsuario.addItem(ADMINISTRADOR);
			tipoUsuario.setItemCaption(ADMINISTRADOR ,ARRAY[ADMINISTRADOR]);
			return tipoUsuario;
		}
	}
	public static final class TipoMensajeEnviado{
		public static final int MENSAJE = 1;
		public static final int SUGERENCIA = 2;
		public static final int QUEJA = 3;
	}
	
	public static final class TipoPersonaNotificacion{
		public static final String ALUMNO = "ALU";
		public static final String PROFESOR = "PRO";
	}
	
	public static final class Prioridad{
		public static final String[] ARRAY = {"" ,"URGENTE" ,"NORMAL" ,"BAJA"};
		public static final int URGENTE = 1;
		public static final int NORMAL = 2;
		public static final int BAJA = 3;
		public static ComboBox CreaComboBox(boolean agregarTodos){
			ComboBox prioridad = new ComboBox();
			prioridad.setTextInputAllowed(false);
			if (agregarTodos){
				prioridad.addItem(TODOS_NUMERO_COMBO);
				prioridad.setItemCaption(TODOS_NUMERO_COMBO,EtiquetasLabel.getTodos());
			}
			prioridad.addItem(URGENTE);
			prioridad.setItemCaption(URGENTE,ARRAY[URGENTE]);
			prioridad.addItem(NORMAL);
			prioridad.setItemCaption(NORMAL,ARRAY[NORMAL]);
			prioridad.addItem(BAJA);
			prioridad.setItemCaption(BAJA,ARRAY[BAJA]);
			return prioridad;
		}
	}
	
	
	public static final class EstadoUsuario{
		public static final String[] ARRAY = {"" ,"Activo" ,"Suspendido" ,"DadoDeBaja"};
		public static final int ACTIVO = 1;
		public static final int SUSPENDIDO = 2;
		public static final int DADO_DE_BAJA = 3;
		public static ComboBox CreaComboBox(boolean agregarTodos){
			ComboBox estadoUsuario = new ComboBox();
			estadoUsuario.setTextInputAllowed(false);
			if (agregarTodos){
				estadoUsuario.addItem(TODOS_NUMERO_COMBO);
				estadoUsuario.setItemCaption(TODOS_NUMERO_COMBO,EtiquetasLabel.getTodos());
			}
			estadoUsuario.addItem(ACTIVO);
			estadoUsuario.setItemCaption(ACTIVO,ARRAY[ACTIVO]);
			estadoUsuario.addItem(SUSPENDIDO);
			estadoUsuario.setItemCaption(SUSPENDIDO,ARRAY[SUSPENDIDO]);
			estadoUsuario.addItem(DADO_DE_BAJA);
			estadoUsuario.setItemCaption(DADO_DE_BAJA,ARRAY[DADO_DE_BAJA]);
			return estadoUsuario;
		}
	}
	
	public static final class EstadoAlumno{
		public static final String[] ARRAY = {"Cursando","Egresado","Transferido","Abandono"};
		public static final String CURSANDO = "CUR";
		public static final String EGRESADO = "EGR";
		public static final String TRANSFERIDO = "TRN";
		public static final String ABANDONO = "ABA";
		public static ComboBox CreaComboBox(boolean agregarTodos){
			ComboBox estado = new ComboBox();
			estado.setTextInputAllowed(false);
			if (agregarTodos){
				estado.addItem(TODOS_STRING_COMBO);
				estado.setItemCaption(TODOS_STRING_COMBO,EtiquetasLabel.getTodos());
			}
			estado.addItem(CURSANDO);
			estado.setItemCaption(CURSANDO ,ARRAY[0]);
			estado.addItem(EGRESADO);
			estado.setItemCaption(EGRESADO ,ARRAY[1]);
			estado.addItem(TRANSFERIDO);
			estado.setItemCaption(TRANSFERIDO ,ARRAY[2]);
			estado.addItem(ABANDONO);
			estado.setItemCaption(ABANDONO ,ARRAY[3]);
			return estado;
		}
	}
	
	public static final class EstadoUsuarioMotivo{
		public static final int ALTA = 1;
		public static final int SUSPENDIDO_POR_ADMINISTRADOR = 2;
		public static final int ACTIVADO_POR_ADMINISTRADOR = 3;
		public static final int DADO_DE_BAJA_POR_ADMINISTRADOR = 3;
	}
	
	public static final class AccesoADatos{
		public static final String INSERT = "INSERT";
		public static final String UPDATE = "UPDATE"; 
		public static final String DELETE = "DELETE";
		public static final String SELECT = "SELECT";
	}
	
	
	public static final class ComboPeriodoMisReservas{
		public static final String[] ARRAY = {"Hoy" ,"SemanaAnterior" ,"MesAnterior" ,"DesdeUnaFecha"};
		public static final String HOY = "Hoy";
		public static final String SEMANA_ANTERIOR = "Semana Anterior" ; 
		public static final String MES_ANTERIOR = "Mes Anterior";
		public static final String DESDE_UNA_FECHA = "Desde una Fecha";
	}
	
	public static final class DiasDeLaSemana{
		public static final int DOMINGO = 1;
		public static final int LUNES = 2;
		public static final int MARTES = 3;
		public static final int MIERCOLES = 4;
		public static final int JUEVES = 5;
		public static final int VIERNES = 6;
		public static final int SABADO = 7;
	}
	public static final class MesesDelAnio{
		public static final String[] ARRAY = {"" ,"Enero" ,"Febrero" ,"Marzo" ,"Abril" ,"Mayo" ,"Junio" ,"Julio" ,"Agosto" ,"Setiembre" ,"Octubre" ,"Noviembre" ,"Diciembre"};
		public static final int ENERO = 1;
		public static final int FEBRERO = 2;
		public static final int MARZO = 3;
		public static final int ABRIL = 4;
		public static final int MAYO = 5;
		public static final int JUNIO = 6;
		public static final int JULIO = 7;
		public static final int AGOSTO = 8;
		public static final int SETIEMBRE = 9;
		public static final int OCTUBRE = 10;
		public static final int NOVIEMBRE = 11;
		public static final int DICIEMBRE = 12;
	}
	
	public static final class TiposDeDatoParametro{
		public static final String TEXTO = "TEXTO";
		public static final String NUMERO = "NUMERO";
		public static final String VERDADERO_FALSO = "V/F";
	}
	
	public static final class Entidades{
		public static final String[] ARRAY = {"" ,"Art�culo" ,"Ciudad" ,"Provincia" ,"Pais" ,"Clasificaci�n de Socio" ,"Socio Estrategico", "Categoria del Servicio" ,"Servicio"};
		public static final int ARTICULO = 1;
		public static final int CIUDAD = 2;
		public static final int PROVINCIA = 3;
		public static final int PAIS = 4;
		public static final int CLASIFICACIONSOCIO = 5;
		public static final int SOCIOESTRATEGICO = 6;
		public static final int CATEGORIASERVICO = 7;
		public static final int SERVICIO = 8;
	}
	
	public static final class Sexo{
		public static final String MASCULINO = "M";
		public static final String FEMENINO = "F";
		public static OptionGroup CreaOptionGroup(){
			OptionGroup sexo = new OptionGroup();
			sexo.addItem(MASCULINO);
			sexo.setItemCaption(MASCULINO ,"Masculino");
			sexo.addItem(FEMENINO);
			sexo.setItemCaption(FEMENINO ,"Femenino");
			return sexo;
		}
	}
	
	public static final class Grado{
		public static ComboBox CreaComboBox(){
			ComboBox sexo = new ComboBox();
			sexo.setPageLength(16);
			sexo.addItem(1);
			sexo.addItem(2);
			sexo.addItem(3);
			sexo.addItem(4);
			sexo.addItem(5);
			sexo.addItem(6);
			sexo.addItem(7);
			sexo.addItem(8);
			sexo.addItem(9);
			sexo.addItem(10);
			sexo.addItem(11);
			sexo.addItem(12);
			sexo.addItem(13);
			sexo.addItem(14);
			sexo.addItem(15);
			return sexo;
		}
	}
	public static final class Nota{
		public static ComboBox CreaComboBox(){
			ComboBox nota = new ComboBox();
			nota.setPageLength(11);
			nota.addItem(0);
			nota.addItem(1);
			nota.addItem(2);
			nota.addItem(3);
			nota.addItem(4);
			nota.addItem(5);
			nota.addItem(6);
			nota.addItem(7);
			nota.addItem(8);
			nota.addItem(9);
			nota.addItem(10);
			return nota;
		}
	}
	public static final class Turno{
		public static final String MANIANA = "Ma�ana";
		public static final String TARDE = "Tarde";
		public static final String NOCHE = "Noche";
		public static ComboBox CreaComboBox(boolean agregarTodos){
			ComboBox turno = new ComboBox();
			turno.setTextInputAllowed(false);
			if (agregarTodos){
				turno.addItem(TODOS_STRING_COMBO);
				turno.setItemCaption(TODOS_STRING_COMBO,EtiquetasLabel.getTodos());
			}
			turno.addItem(MANIANA);
			turno.addItem(TARDE);
			turno.addItem(NOCHE);
			return turno;
		}
	}
	
	public static final class Periodos{
		public static final String ARRAY[] = {"" ,"" ,"Semestre" ,"Trimestre" ,"Bimestres" };
		public static final int SEMESTRE = 2;
		public static final int TRIMESTRE = 3;
		public static final int BIMESTRE = 4;
		public static final int FINAL = 10;
		public static ComboBox CreaComboBox(boolean agregarTodos){
			ComboBox periodos = new ComboBox();
			periodos.setTextInputAllowed(false);
			if (agregarTodos){
				periodos.addItem(TODOS_STRING_COMBO);
				periodos.setItemCaption(TODOS_STRING_COMBO,EtiquetasLabel.getTodos());
			}
			periodos.addItem(SEMESTRE);
			periodos.setItemCaption(SEMESTRE, ARRAY[SEMESTRE]);
			periodos.addItem(TRIMESTRE);
			periodos.setItemCaption(TRIMESTRE, ARRAY[TRIMESTRE]);
			periodos.addItem(BIMESTRE);
			periodos.setItemCaption(BIMESTRE, ARRAY[BIMESTRE]);
			return periodos;
		}
		
		public static ComboBox CreaComboBoxSegunPeriodo(int periodo ,boolean agregarTodos){
			ComboBox periodos = new ComboBox();
			periodos.setTextInputAllowed(false);
			if (agregarTodos){
				periodos.addItem(TODOS_STRING_COMBO);
				periodos.setItemCaption(TODOS_STRING_COMBO,EtiquetasLabel.getTodos());
			}
			if (periodo == SEMESTRE){
				periodos.addItem(1);
				periodos.setItemCaption(1, "1� Semestre");
				periodos.addItem(2);
				periodos.setItemCaption(2, "2� Semestre");
			}
			else if (periodo == TRIMESTRE){
				periodos.addItem(1);
				periodos.setItemCaption(1, "1� Trimestre");
				periodos.addItem(2);
				periodos.setItemCaption(2, "2� Trimestre");
				periodos.addItem(3);
				periodos.setItemCaption(3, "3� Trimestre");
			}
			else if (periodo == BIMESTRE){
				periodos.addItem(1);
				periodos.setItemCaption(1, "1� Bimestre");
				periodos.addItem(2);
				periodos.setItemCaption(2, "2� Bimestre");
				periodos.addItem(3);
				periodos.setItemCaption(3, "3� Bimestre");
				periodos.addItem(4);
				periodos.setItemCaption(4, "4� Bimestre");
			} 
			periodos.addItem(FINAL);
			periodos.setItemCaption(FINAL, "Final");
			return periodos;
		}
		
		public static String NombrePeriodo(int cantidadPeriodo ,int periodo){
			if (periodo == FINAL)
				return "Final";
			else if (cantidadPeriodo == SEMESTRE){
				if (periodo == 1)
					return "1� Semestre";
				if (periodo == 2)
					return "2� Semestre";
			}
			else if (cantidadPeriodo == TRIMESTRE){
				if (periodo == 1)
					return "1� Trimestre";
				if (periodo == 2)
					return "2� Trimestre";
				if (periodo == 3)
					return "3� Trimestre";
			}
			else if (cantidadPeriodo == BIMESTRE){
				if (periodo == 1)
					return "1� Bimestre";
				if (periodo == 2)
					return "2� Bimestre";
				if (periodo == 3)
					return "3� Bimestre";
				if (periodo == 4)
					return "4� Bimestre";
			} 
			return "";
		}
	}
	
	public static final class TipoComprobante{
		
		public static final class Tipo{
			public static final String FACTURA = "FAC";
			public static final String NOTA_DE_DEBITO = "N/D";
			public static final String NOTA_DE_CREDITO = "N/C";
			public static final String COMPROBANTE_CAJA = "CAJ";
			
			public static ComboBox CreaComboBox(boolean agregarTodos){
				ComboBox tipo = new ComboBox();
				tipo.setTextInputAllowed(false);
				if (agregarTodos){
					tipo.addItem(TODOS_NUMERO_COMBO);
					tipo.setItemCaption(TODOS_NUMERO_COMBO,EtiquetasLabel.getTodos());
				}
				tipo.addItem(FACTURA);
				tipo.setItemCaption(FACTURA,"Factura");
				tipo.addItem(NOTA_DE_CREDITO);
				tipo.setItemCaption(NOTA_DE_CREDITO,"Nota de Credito");
				tipo.addItem(NOTA_DE_DEBITO);
				tipo.setItemCaption(NOTA_DE_DEBITO,"Nota de Debito");
				tipo.addItem(COMPROBANTE_CAJA);
				tipo.setItemCaption(COMPROBANTE_CAJA,"Comprobante de Caja");
				return tipo;
			}
		}
		
		public static final class Saldo{
			public static final int DEUDOR = 1;
			public static final int ACREEDOR = -1;
			
			public static ComboBox CreaComboBox(boolean agregarTodos){
				ComboBox saldo = new ComboBox();
				saldo.setTextInputAllowed(false);
				if (agregarTodos){
					saldo.addItem(TODOS_STRING_COMBO);
					saldo.setItemCaption(TODOS_STRING_COMBO,EtiquetasLabel.getTodos());
				}
				saldo.addItem(DEUDOR);
				saldo.setItemCaption(DEUDOR,"Deudor");
				saldo.addItem(ACREEDOR);
				saldo.setItemCaption(ACREEDOR,"Acreedor");
				return saldo;
			}
		}
		
		
		public static final class Operacion{
			public static final String COMPRA = "CPR";
			public static final String VENTA = "VTA";
			public static final String CAJA = "CAJ";
			
			public static ComboBox CreaComboBox(boolean agregarTodos){
				ComboBox operacion = new ComboBox();
				operacion.setTextInputAllowed(false);
				if (agregarTodos){
					operacion.addItem(TODOS_STRING_COMBO);
					operacion.setItemCaption(TODOS_STRING_COMBO,EtiquetasLabel.getTodos());
				}
				operacion.addItem(COMPRA);
				operacion.setItemCaption(COMPRA,"Compra");
				operacion.addItem(VENTA);
				operacion.setItemCaption(VENTA,"Venta");
				operacion.addItem(CAJA);
				operacion.setItemCaption(CAJA,"Caja");
				return operacion;
			}
		}
		
	}
	public static class CondicionIVA{
		public static final String[] ARRAY = {"" ,"Responsable Inscripto" ,"Responsable No Inscripto" ,"No Responsable" ,"Exento" ,"Consumidor Final" ,"Responsable Monotributo"};
		public static final short RESPONSABLE_INSCRIPTO = 1;
		public static final short RESPONSABLE_NO_INSCRIPTO = 2;
		public static final short NO_RESPONSABLE = 3;
		public static final short EXENTO = 4;
		public static final short CONSUMIDOR_FINAL = 5;
		public static final short RESPONSABLE_MONOTRIBUTO = 6;
		
		public static ComboBox CreaComboBox(boolean agregarTodos){
			ComboBox condicionIVA = new ComboBox();
			condicionIVA.setTextInputAllowed(false);
			if (agregarTodos){
				condicionIVA.addItem(TODOS_NUMERO_COMBO);
				condicionIVA.setItemCaption(TODOS_NUMERO_COMBO,EtiquetasLabel.getTodos());
			}
			condicionIVA.addItem(RESPONSABLE_INSCRIPTO);
			condicionIVA.setItemCaption(RESPONSABLE_INSCRIPTO,ARRAY[RESPONSABLE_INSCRIPTO]);
			condicionIVA.addItem(RESPONSABLE_NO_INSCRIPTO);
			condicionIVA.setItemCaption(RESPONSABLE_NO_INSCRIPTO,ARRAY[RESPONSABLE_NO_INSCRIPTO]);
			condicionIVA.addItem(NO_RESPONSABLE);
			condicionIVA.setItemCaption(NO_RESPONSABLE,ARRAY[NO_RESPONSABLE]);
			condicionIVA.addItem(EXENTO);
			condicionIVA.setItemCaption(EXENTO,ARRAY[EXENTO]);
			condicionIVA.addItem(CONSUMIDOR_FINAL);
			condicionIVA.setItemCaption(CONSUMIDOR_FINAL,ARRAY[CONSUMIDOR_FINAL]);
			condicionIVA.addItem(RESPONSABLE_MONOTRIBUTO);
			condicionIVA.setItemCaption(RESPONSABLE_MONOTRIBUTO,ARRAY[RESPONSABLE_MONOTRIBUTO]);
			return condicionIVA;
		}
	}
	
	public static class ComprobanteLetra{
		public static ComboBox CreaComboBox(boolean agregarTodos){
			ComboBox letra = new ComboBox();
			letra.setTextInputAllowed(false);
			if (agregarTodos){
				letra.addItem(TODOS_STRING_COMBO);
				letra.setItemCaption(TODOS_STRING_COMBO,EtiquetasLabel.getTodos());
			}
			letra.addItem("A");
			letra.addItem("B");
			letra.addItem("C");
			letra.addItem("X");
			return letra;
		}
	}
	
	public static class ComprobanteFormaPago{
		public static final String CONTADO = "CON";
		public static final String CUENTA_CORRIENTE = "CTA";
		
		public static ComboBox CreaComboBox(boolean agregarTodos){
			ComboBox formaPago = new ComboBox();
			formaPago.setTextInputAllowed(false);
			if (agregarTodos){
				formaPago.addItem(TODOS_STRING_COMBO);
				formaPago.setItemCaption(TODOS_STRING_COMBO,EtiquetasLabel.getTodos());
			}
			formaPago.addItem(CONTADO);
			formaPago.setItemCaption(CONTADO,"Contado");
			formaPago.addItem(CUENTA_CORRIENTE);
			formaPago.setItemCaption(CUENTA_CORRIENTE,"Cuenta Corriente ");
			return formaPago;
		}
	}
	public static class Asistencia{
		public static final String PRESENTE = "PRE";
		public static final String AUSENTE = "AUS";
		public static final String JUSTIFICADA = "JUS";
		
		public static ComboBox CreaComboBox(boolean agregarTodos){
			ComboBox asistencia = new ComboBox();
			asistencia.setTextInputAllowed(false);
			if (agregarTodos){
				asistencia.addItem(TODOS_STRING_COMBO);
				asistencia.setItemCaption(TODOS_STRING_COMBO,EtiquetasLabel.getTodos());
			}
			asistencia.addItem(PRESENTE);
			asistencia.setItemCaption(PRESENTE,"Presente");
			asistencia.addItem(AUSENTE);
			asistencia.setItemCaption(AUSENTE,"Ausente");
			asistencia.addItem(JUSTIFICADA);
			asistencia.setItemCaption(JUSTIFICADA,"Justificada");
			return asistencia;
		}
	}
}
