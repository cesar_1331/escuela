package ar.com.trisquel.escuela.utiles;

import java.io.Serializable;
import java.util.Locale;

@SuppressWarnings("serial")
public class Sesion implements Serializable{
	private String usuarioId = null;
	private String usuarioNombre = null;
	private String articuloSeleccionado = "";
	private String NavegarA = "";
	private Locale locale;
	public String getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}
	public String getUsuarioNombre() {
		return usuarioNombre;
	}
	public void setUsuarioNombre(String usuarioNombre) {
		this.usuarioNombre = usuarioNombre;
	}
	public String getArticuloSeleccionado() {
		return articuloSeleccionado;
	}
	public void setArticuloSeleccionado(String articuloSeleccionado) {
		this.articuloSeleccionado = articuloSeleccionado;
	}
	public String getNavegarA() {
		return NavegarA;
	}
	public void setNavegarA(String navegarA) {
		NavegarA = navegarA;
	}
	public Locale getLocale() {
		return locale;
	}
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((NavegarA == null) ? 0 : NavegarA.hashCode());
		result = prime
				* result
				+ ((articuloSeleccionado == null) ? 0 : articuloSeleccionado
						.hashCode());
		result = prime * result + ((locale == null) ? 0 : locale.hashCode());
		result = prime * result
				+ ((usuarioId == null) ? 0 : usuarioId.hashCode());
		result = prime * result
				+ ((usuarioNombre == null) ? 0 : usuarioNombre.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sesion other = (Sesion) obj;
		if (NavegarA == null) {
			if (other.NavegarA != null)
				return false;
		} else if (!NavegarA.equals(other.NavegarA))
			return false;
		if (articuloSeleccionado == null) {
			if (other.articuloSeleccionado != null)
				return false;
		} else if (!articuloSeleccionado.equals(other.articuloSeleccionado))
			return false;
		if (locale == null) {
			if (other.locale != null)
				return false;
		} else if (!locale.equals(other.locale))
			return false;
		if (usuarioId == null) {
			if (other.usuarioId != null)
				return false;
		} else if (!usuarioId.equals(other.usuarioId))
			return false;
		if (usuarioNombre == null) {
			if (other.usuarioNombre != null)
				return false;
		} else if (!usuarioNombre.equals(other.usuarioNombre))
			return false;
		return true;
	}
	
	
}
