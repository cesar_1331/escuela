package ar.com.trisquel.escuela.seguridad;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import ar.com.trisquel.escuela.utiles.FieldAnnotations;

import com.sun.xml.internal.messaging.saaj.packaging.mime.internet.MimeUtility;

public class HerramientasSeguridad {

	private final static String keyBuffer = "nphateu."; //8 caracteres 
	//algoritmos
    public static String MD2 = "MD2";
    public static String MD5 = "MD5";
    public static String SHA1 = "SHA-1";
    public static String SHA256 = "SHA-256";
    public static String SHA384 = "SHA-384";
    public static String SHA512 = "SHA-512";
	
	private static byte[] encode(byte[] b) throws Exception {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		OutputStream b64os = MimeUtility.encode(baos,"base64" );
		b64os.write(b);
		b64os.close();
		return baos.toByteArray();

	}

	private static byte[] decode(byte[] b) throws Exception {

		ByteArrayInputStream bais = new ByteArrayInputStream(b);
		InputStream b64is = MimeUtility.decode(bais,"base64");
		byte[] tmp = new byte[b.length];
		int n = b64is.read(tmp);
		byte[] res = new byte[n];
		System.arraycopy(tmp, 0, res, 0, n);
		return res;

	}

	private static SecretKeySpec getKey() {
		//keyBuffer = keyBuffer.substring(0, 8);
		SecretKeySpec key = new SecretKeySpec(keyBuffer.getBytes(), "DES");
		return key;

	}

	public static String desencripta(String s) throws Exception {
		s = desencriptaCadena(s);
		s = desencriptaCadena(s);
		return s;
	}
	
	private static String desencriptaCadena(String s) throws Exception {
		String s1 = null;
		s = "{DES}" + s;
		if (s.indexOf("{DES}") != -1) {
			String s2 = s.substring("{DES}".length());
			Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
			cipher.init(2, getKey());
			byte abyte0[] = cipher.doFinal(decode(s2.getBytes()));
			s1 = new String(abyte0);
		} else {
			s1 = s;
		}

		return s1;

	}

	public static String encripta(String s) throws Exception {
		s = encriptaCadena(s);
		s = encriptaCadena(s);
		return s;
	}
	
	private static String encriptaCadena(String s) throws Exception {

		byte abyte0[];
		SecureRandom securerandom = new SecureRandom();
		securerandom.nextBytes(keyBuffer.getBytes());
		Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
		cipher.init(1, getKey());
		abyte0 = encode(cipher.doFinal(s.getBytes())); 
		//return "{DES}" + new String(abyte0);
		return new String(abyte0);
	}
	
	

    /***
     * Convierte un arreglo de bytes a String usando valores hexadecimales
     * @param digest arreglo de bytes a convertir
     * @return String creado a partir de <code>digest</code>
     */
    private static String toHexadecimal(byte[] digest){
        String hash = "";
        for(byte aux : digest) {
            int b = aux & 0xff;
            if (Integer.toHexString(b).length() == 1) hash += "0";
            hash += Integer.toHexString(b);
        }
        return hash;
    }

    /***
     * Encripta un mensaje de texto mediante algoritmo de resumen de mensaje.
     * @param message texto a encriptar
     * @param algorithm algoritmo de encriptacion, puede ser: MD2, MD5, SHA-1, SHA-256, SHA-384, SHA-512
     * @return mensaje encriptado
     * @throws NoSuchAlgorithmException 
     */
    public static String getStringMessageDigest(String message) throws NoSuchAlgorithmException{
        byte[] digest = null;
        byte[] buffer = message.getBytes();
        MessageDigest messageDigest = MessageDigest.getInstance(SHA512);
        messageDigest.reset();
        messageDigest.update(buffer);
        digest = messageDigest.digest();
        return toHexadecimal(digest);
    } 
	
	
	@FieldAnnotations(Descripcion="Devuelve un valor del 0 al 100, donde 100 es una contraseņa mas fuerte")
	public static int checkPasswordStrength(String password) {
		int strengthPercentage=0;
		String[] partialRegexChecks = { 
				".*[a-z]+.*", // lower
				".*[A-Z]+.*", // upper
				".*[\\d]+.*", // digits
				".*[@#$%]+.*" // symbols
		};
		if(password.length() < 6){
			strengthPercentage=0;
		}else if (password.contains(" ")){
			strengthPercentage=0; 
		}
		else{
			if (password.matches(partialRegexChecks[0])) {
				strengthPercentage+=25;
			}
			if (password.matches(partialRegexChecks[1])) {
				strengthPercentage+=25;
			}
			if (password.matches(partialRegexChecks[2])) {
				strengthPercentage+=25;
			}
			if (password.matches(partialRegexChecks[3])) {
				strengthPercentage+=25;
			}
		}
		return strengthPercentage;
	}

}