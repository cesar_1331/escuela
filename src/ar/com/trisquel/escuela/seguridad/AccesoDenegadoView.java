package ar.com.trisquel.escuela.seguridad;

import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.reglasdenegocio.ArticuloRN;
import ar.com.trisquel.escuela.data.tablas.Articulo;
import ar.com.trisquel.escuela.utiles.R;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class AccesoDenegadoView extends VerticalLayout implements ClickListener {
	
	private Articulo articulo;
	public void onDraw(ControlAcceso control){
		removeAllComponents();
		setWidth("100%");
		setHeight("100%");
		
		/*
		 *  Titulo
		 */
		HorizontalLayout LayoutTitle = new HorizontalLayout();
		LayoutTitle.setSizeFull();
		Label Titulo = new Label();
		Titulo.setStyleName(R.Style.TITLE);
		Titulo.setWidth("100%");
		addComponent(Titulo);
		Label Separator = new Label("<hr>");
		Separator.setContentMode(ContentMode.HTML);
		addComponent(Separator);
		/*
		 * Lee Datos 
		 */
		
		Session HibernateSession = HibernateUtil.getSession();
		HibernateSession.beginTransaction();
		HibernateSession.clear();
		if(!control.getMensaje().trim().isEmpty())
		{
			String Page = String.valueOf(control.getMensaje());
			articulo = ArticuloRN.Read(Page);
		}
		if (articulo != null)
		{
			Titulo.setValue(articulo.getTitulo());
			Label Content = new Label(articulo.getContenido());
			Content.setContentMode(ContentMode.HTML);
			addComponent(Content);
		}
		else{
			articulo = ArticuloRN.Read(R.Paginas.ERROR);
			if (articulo != null){
				Titulo.setValue(articulo.getTitulo());
				Label Content = new Label(articulo.getContenido());
				Content.setContentMode(ContentMode.HTML);
				addComponent(Content);
			}
			else{
				Label Content = new Label("<h1>Error page not foud</h1>");
				Content.setContentMode(ContentMode.HTML);
				addComponent(Content);
			}
		}
	}

	@Override
	public void buttonClick(ClickEvent event) {

	}
}
