package ar.com.trisquel.escuela.seguridad;

import ar.com.trisquel.escuela.componentes.asignatura.AsignaturaWWView;
import ar.com.trisquel.escuela.componentes.asignatura.CursoWWView;
import ar.com.trisquel.escuela.componentes.comprobante.ComprobanteBalanceView;
import ar.com.trisquel.escuela.componentes.comprobante.ComprobanteBloqueoView;
import ar.com.trisquel.escuela.componentes.comprobante.ComprobantesView;
import ar.com.trisquel.escuela.componentes.concepto.ConceptoWWView;
import ar.com.trisquel.escuela.componentes.materias.MateriaWWView;
import ar.com.trisquel.escuela.componentes.otros.ArticuloView;
import ar.com.trisquel.escuela.componentes.otros.ArticuloWWView;
import ar.com.trisquel.escuela.componentes.otros.ContactoView;
import ar.com.trisquel.escuela.componentes.otros.ErrorLogWWView;
import ar.com.trisquel.escuela.componentes.otros.ErrorView;
import ar.com.trisquel.escuela.componentes.otros.MensajeWWView;
import ar.com.trisquel.escuela.componentes.otros.MiMenuView;
import ar.com.trisquel.escuela.componentes.otros.NotificacionView;
import ar.com.trisquel.escuela.componentes.otros.NotificacionWWView;
import ar.com.trisquel.escuela.componentes.parametro.EscuelaView;
import ar.com.trisquel.escuela.componentes.personas.alumno.AlumnoWWView;
import ar.com.trisquel.escuela.componentes.personas.alumno.MisAsignaturasView;
import ar.com.trisquel.escuela.componentes.personas.profesor.MisAlumnosView;
import ar.com.trisquel.escuela.componentes.personas.profesor.MisCursosView;
import ar.com.trisquel.escuela.componentes.personas.profesor.ProfesorWWView;
import ar.com.trisquel.escuela.componentes.personas.provedorcliente.PersonasWWView;
import ar.com.trisquel.escuela.componentes.tipocomprobante.TipoComprobanteWWView;
import ar.com.trisquel.escuela.componentes.user.ForgotPasswordView;
import ar.com.trisquel.escuela.componentes.user.LoginView;
import ar.com.trisquel.escuela.componentes.user.UserPreferencesView;
import ar.com.trisquel.escuela.componentes.user.UsuarioWWView;
import ar.com.trisquel.escuela.data.reglasdenegocio.PermisoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.UsuarioRN;
import ar.com.trisquel.escuela.data.tablas.Permiso;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.data.tablas.identificadores.PermisoId;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.R;
import ar.com.trisquel.escuela.utiles.Sesion;

import com.vaadin.ui.UI;


public class ControlAcceso {

	private boolean Acceso;
	private String Title;
	private String Mensaje;
	
	public ControlAcceso(){
		Acceso = true;
		Mensaje = "";
		Title = "";
	}
	public boolean isAcceso() {
		return Acceso;
	}
	public void setAcceso(boolean acceso) {
		Acceso = acceso;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getMensaje() {
		return Mensaje;
	}
	public void setMensaje(String mensaje) {
		Mensaje = mensaje;
	}
	
	
	@SuppressWarnings("rawtypes")
	public static ControlAcceso Control(Class View  ,UI ui){
		ControlAcceso control = new ControlAcceso();
		Sesion sesion = (Sesion)ui.getSession().getAttribute(R.Session.SESSION);
		Usuario usuario = null;
		if (sesion.getUsuarioId() != null)
			usuario = UsuarioRN.Read(sesion.getUsuarioId());
		/**
		 * Paginas que no nesecitan controles 
		 */
		if (View == ArticuloView.class       || View == ContactoView.class ||
			View == LoginView.class          || View == ForgotPasswordView.class || 
			View == ErrorView.class){
		}
		/**
		 * Controles en paginas del front end 
		 */
		else if (View == UserPreferencesView.class || View == NotificacionView.class ||  
				 View == MiMenuView.class){
			if (usuario == null){
				control.setAcceso(false);
				control.setMensaje(R.Paginas.ACCESO_DENEGADO);
			}
		}
		
		else if (View == MisAsignaturasView.class){
			if (usuario.getAlumnoId() == 0  || usuario.getTipoUsuario() != Dominios.TipoUsuario.ALUMNO){
				control.setAcceso(false);
				control.setMensaje(R.Paginas.ACCESO_DENEGADO);
			}
		}
		else if (View == MisAlumnosView.class ){
			if (usuario.getProfesorId() == 0 ){
				control.setAcceso(false);
				control.setMensaje(R.Paginas.ACCESO_DENEGADO);
			}
		}
		else if (View == MisCursosView.class ){
			if (usuario.getProfesorId() == 0 ){
				control.setAcceso(false);
				control.setMensaje(R.Paginas.ACCESO_DENEGADO);
			}
		}
		else{
			String page = "";
			if (View == ArticuloWWView.class){
				page = ArticuloWWView.VIEW;
			}
			else if (View == UsuarioWWView.class){
				page = UsuarioWWView.VIEW;
			}
			else if (View == MateriaWWView.class){
				page = MateriaWWView.VIEW;
			}
			else if (View == ProfesorWWView.class){
				page = ProfesorWWView.VIEW;
			}
			else if (View == AlumnoWWView.class){
				page = AlumnoWWView.VIEW;
			}
			else if (View == AsignaturaWWView.class){
				page = AsignaturaWWView.VIEW;
			}
			else if (View == CursoWWView.class){
				page = CursoWWView.VIEW;
			}
			else if (View == EscuelaView.class){
				page = EscuelaView.VIEW;
			}
			else if (View == NotificacionWWView.class){
				page = NotificacionWWView.VIEW;
			}
			else if (View == ErrorLogWWView.class){
				page = ErrorLogWWView.VIEW;
			}
			else if (View == MensajeWWView.class){
				page = MensajeWWView.VIEW;
			}
			else if (View == TipoComprobanteWWView.class){
				page = TipoComprobanteWWView.VIEW;
			}
			else if (View == ConceptoWWView.class){
				page = ConceptoWWView.VIEW;
			}
			else if (View == PersonasWWView.class){
				page = PersonasWWView.VIEW;
			}
			else if (View == ComprobantesView.class){
				page = ComprobantesView.VIEW ;
			}
			else if (View == ComprobanteBloqueoView.class){
				page = ComprobanteBloqueoView.VIEW ;
			}
			else if (View == ComprobanteBalanceView.class){
				page = ComprobanteBalanceView.VIEW ;
			}
			Permiso permiso = PermisoRN.Read(new PermisoId(sesion.getUsuarioId(), page));
			if (!(permiso != null && permiso.isAcceso())){
				control.setAcceso(false);
				control.setMensaje(R.Paginas.ACCESO_DENEGADO);
			}
		}
		return control;
	}
}
