package ar.com.trisquel.escuela.data.reglasdenegocio;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.select.AlumnoParienteSelect;
import ar.com.trisquel.escuela.data.tablas.AlumnoPariente;
import ar.com.trisquel.escuela.data.tablas.identificadores.AlumnoLinea;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class AlumnoParienteRN {
	
	public static AlumnoPariente Read(AlumnoLinea id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			AlumnoPariente alumnoPariente = (AlumnoPariente)HibernateSession.get(AlumnoPariente.class , id);
			HibernateSession.getTransaction().rollback();
			return alumnoPariente;
		}
		catch (HibernateException e){
			return null;
		}
	}

	public static RespuestaEntidad Save(AlumnoPariente alumnoPariente) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(alumnoPariente);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(AlumnoPariente alumnoPariente){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(alumnoPariente);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static AlumnoPariente Inizializate(long alumnoId){
		AlumnoPariente alumnoPariente = new AlumnoPariente();
		alumnoPariente.setId(new AlumnoLinea(alumnoId ,AlumnoParienteSelect.MaxLinea(alumnoId)));
		alumnoPariente.setEsTutor(true);
		return alumnoPariente;
	}
	
	public static RespuestaEntidad AfterSave(AlumnoPariente alumnoPariente ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(AlumnoPariente alumnoPariente ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		if (alumnoPariente.getNombre().trim().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe ingresar el nombre del pariente del alumno");
		}
		return response;
	}
}
