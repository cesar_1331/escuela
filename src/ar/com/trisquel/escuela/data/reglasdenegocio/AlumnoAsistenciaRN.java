package ar.com.trisquel.escuela.data.reglasdenegocio;

import java.util.Date;
import java.util.GregorianCalendar;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.AlumnoAsistencia;
import ar.com.trisquel.escuela.data.tablas.identificadores.AlumnoDia;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class AlumnoAsistenciaRN {
	
	public static AlumnoAsistencia Read(AlumnoDia id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			AlumnoAsistencia alumnoAsistencia = (AlumnoAsistencia)HibernateSession.get(AlumnoAsistencia.class , id);
			HibernateSession.getTransaction().rollback();
			return alumnoAsistencia;
		}
		catch (HibernateException e){
			return null;
		}
	}

	public static RespuestaEntidad Save(AlumnoAsistencia alumnoAsistencia) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(alumnoAsistencia);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(AlumnoAsistencia alumnoAsistencia){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(alumnoAsistencia);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static AlumnoAsistencia Inizializate(long alumnoId ,Date fecha){
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(fecha);
		GregorianCalendar calendarFecha = new GregorianCalendar(
				calendar.get(GregorianCalendar.YEAR),
				calendar.get(GregorianCalendar.MONTH),
				calendar.get(GregorianCalendar.DAY_OF_MONTH));
		AlumnoAsistencia alumnoAsistencia = new AlumnoAsistencia();
		alumnoAsistencia.setId(new AlumnoDia(alumnoId ,calendarFecha.getTime()));
		return alumnoAsistencia;
	}
	
	public static RespuestaEntidad AfterSave(AlumnoAsistencia alumnoAsistencia ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(AlumnoAsistencia alumnoAsistencia ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		return response;
	}
}
