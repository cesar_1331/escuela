package ar.com.trisquel.escuela.data.reglasdenegocio;

import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.ProfesorMateria;
import ar.com.trisquel.escuela.data.tablas.identificadores.ProfesorMateriaId;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class ProfesorMateriaRN {
	
	public static ProfesorMateria Read(ProfesorMateriaId id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			ProfesorMateria profesorMateria = (ProfesorMateria)HibernateSession.get(ProfesorMateria.class , id);
			HibernateSession.getTransaction().rollback();
			return profesorMateria;
		}
		catch (HibernateException e){
			return null;
		}
	}

	public static RespuestaEntidad Save(ProfesorMateria profesorMateria) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(profesorMateria);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(ProfesorMateria profesorMateria){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(profesorMateria);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static ProfesorMateria Inizializate(long profesorId ,long materiaId){
		ProfesorMateria profesorMateria = new ProfesorMateria();
		profesorMateria.setId(new ProfesorMateriaId(profesorId ,materiaId));
		profesorMateria.setFechaAsociacion(new Date(System.currentTimeMillis()));
		return profesorMateria;
	}
	
	public static RespuestaEntidad AfterSave(ProfesorMateria profesorMateria ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(ProfesorMateria profesorMateria ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		return response;
	}
}
