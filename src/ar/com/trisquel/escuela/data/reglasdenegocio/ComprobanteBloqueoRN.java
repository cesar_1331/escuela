package ar.com.trisquel.escuela.data.reglasdenegocio;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.ComprobanteBloqueo;
import ar.com.trisquel.escuela.data.tablas.identificadores.AnioMesId;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class ComprobanteBloqueoRN {
	
	public static ComprobanteBloqueo Read(AnioMesId id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			ComprobanteBloqueo comprobanteBloqueo = (ComprobanteBloqueo)HibernateSession.get(ComprobanteBloqueo.class , id);
			HibernateSession.getTransaction().rollback();
			return comprobanteBloqueo;
		}
		catch (HibernateException e){
			return null;
		}
	}
	
	public static RespuestaEntidad Add(ComprobanteBloqueo comprobanteBloqueo) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.save(comprobanteBloqueo);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static RespuestaEntidad Save(ComprobanteBloqueo comprobanteBloqueo) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(comprobanteBloqueo);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(ComprobanteBloqueo comprobanteBloqueo){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(comprobanteBloqueo);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static ComprobanteBloqueo Inizializate(int anio,int mes){
		ComprobanteBloqueo comprobanteBloqueo = new ComprobanteBloqueo();
		comprobanteBloqueo.setId(new AnioMesId(anio, mes));
		comprobanteBloqueo.setBloqueado(false);
		comprobanteBloqueo.setResponsable("");
		return comprobanteBloqueo;
	}
	
	public static RespuestaEntidad AfterSave(ComprobanteBloqueo comprobanteBloqueo ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(ComprobanteBloqueo comprobanteBloqueo ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		return response;
	}
}
