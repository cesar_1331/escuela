package ar.com.trisquel.escuela.data.reglasdenegocio;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.TipoComprobante;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class TipoComprobanteRN {
	
	public static TipoComprobante Read(int id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			TipoComprobante tipoComprobante = (TipoComprobante)HibernateSession.get(TipoComprobante.class , id);
			HibernateSession.getTransaction().rollback();
			return tipoComprobante;
		}
		catch (HibernateException e){
			return null;
		}
	}
	
	public static RespuestaEntidad Add(TipoComprobante tipoComprobante) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.save(tipoComprobante);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static RespuestaEntidad Save(TipoComprobante tipoComprobante) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(tipoComprobante);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(TipoComprobante tipoComprobante){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(tipoComprobante);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static TipoComprobante Inizializate(){
		TipoComprobante tipoComprobante = new TipoComprobante();
		tipoComprobante.setActivo(true);
		return tipoComprobante;
	}
	
	public static TipoComprobante Inizializate(String nombre ,String tipo ,int saldo ,String operacion){
		TipoComprobante tipoComprobante = new TipoComprobante();
		tipoComprobante.setNombre(nombre);
		tipoComprobante.setTipo(tipo);
		tipoComprobante.setSaldo(saldo);
		tipoComprobante.setOperacion(operacion);
		tipoComprobante.setActivo(true);
		return tipoComprobante;
	}
	
	public static RespuestaEntidad AfterSave(TipoComprobante tipoComprobante ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(TipoComprobante tipoComprobante ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		if (tipoComprobante.getNombre().trim().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe ingresar el nombre del tipo de comprobante");
		}
		else if (tipoComprobante.getOperacion().trim().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe seleccionar la operación del tipo de comprobante");
		}  
		else if (tipoComprobante.getTipo().trim().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe seleccionar el tipo del tipo de comprobante");
		}  
		else if (tipoComprobante.getSaldo() == 0){
			response.setError(true);
			response.setMsgError("Debe seleccionar el saldo del tipo de comprobante");
		}
		else if (tipoComprobante.getTipo().equals(Dominios.TipoComprobante.Tipo.COMPROBANTE_CAJA) 
				&& !tipoComprobante.getOperacion().equals(Dominios.TipoComprobante.Operacion.CAJA)){
			response.setError(true);
			response.setMsgError("Si el tipo es comprobante de caja la operación debe ser Caja");
		}
		else if (!tipoComprobante.getTipo().equals(Dominios.TipoComprobante.Tipo.COMPROBANTE_CAJA) 
				&& tipoComprobante.getOperacion().equals(Dominios.TipoComprobante.Operacion.CAJA)){
			response.setError(true);
			response.setMsgError("Si el tipo NO es comprobante de caja la operación NO puede ser Caja");
		}
		return response;
	}
}
