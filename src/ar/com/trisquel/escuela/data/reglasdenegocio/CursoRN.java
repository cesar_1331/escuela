package ar.com.trisquel.escuela.data.reglasdenegocio;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class CursoRN {

	public static Curso Read(long id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			Curso curso = (Curso)HibernateSession.get(Curso.class , id);
			HibernateSession.getTransaction().rollback();
			return curso;
		}
		catch (HibernateException e){
			return null;
		}
	}
	
	public static RespuestaEntidad Add(Curso curso) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.save(curso);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static RespuestaEntidad Save(Curso curso) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(curso);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(Curso curso){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(curso);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static Curso Inizializate(){
		Curso curso = new Curso();
		return curso;
	}
	
	
	public static RespuestaEntidad AfterSave(Curso curso ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(Curso curso ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		if (curso.getGrado() == 0){
			response.setError(true);
			response.setMsgError("Debe ingresar el grado");
		}
		else if (curso.getCantidadPeriodos() == 0){
			response.setError(true);
			response.setMsgError("Debe ingresar la cantidad de periodos");
		}
		else if (curso.getDivision().trim().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe ingresar la divisi�n");
		}
		else if(curso.getTurno().trim().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe el turno");
		}
		return response;
	}
}
