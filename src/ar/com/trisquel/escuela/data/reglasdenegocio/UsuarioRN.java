package ar.com.trisquel.escuela.data.reglasdenegocio;

import java.sql.Date;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.language.UsuarioLabel;
import ar.com.trisquel.escuela.seguridad.HerramientasSeguridad;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

import com.vaadin.data.validator.EmailValidator;

public class UsuarioRN {
	public UsuarioRN(){
	}
	
	/**
	 * Lee el Objeto calificacion de la base de datos
	 * @param id Identificador de del usuario
	 * @return el objeto Usuario
	 */
	public static Usuario Read(String id) {
		if (id == null)
		{
			return null;
		}
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			Usuario usuario = (Usuario)HibernateSession.get(Usuario.class , id);
			HibernateSession.getTransaction().rollback();
			return usuario;
		}
		catch (HibernateException e){
			return null;
		}
	}

	/**
	 * Graba el usuario en la base de datos
	 * @return Respuesta
	 */
	public static RespuestaEntidad Add(Usuario usuario) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.save(usuario);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	/**
	 * Graba el usuario en la base de datos
	 * @return Respuesta
	 */
	public static RespuestaEntidad Save(Usuario usuario) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(usuario);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	/**
	 * Borra un usuario de la base de datos
	 * @return Respuesta
	 */
	public static RespuestaEntidad Delete(Usuario usuario){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(usuario);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	/**
	 * Inicializa el objeto usuario
	 * @return Usuario
	 */
	public static Usuario Inizializate(){
		Usuario usuario = new Usuario();
		usuario.setId("");
		usuario.setNombre("");
		usuario.setClave("");
		usuario.setMailPrincipal("");
		usuario.setMailSecundario("");
		usuario.setAlumnoId(0);
		usuario.setProfesorId(0);
		usuario.setEstadoActual(Dominios.EstadoUsuario.ACTIVO);
		usuario.setFechaIniEstado(new Date(System.currentTimeMillis()));
		usuario.setTipoUsuario(Dominios.TipoUsuario.ALUMNO);
		return usuario;
	}
	
	public static RespuestaEntidad AfterSave(Usuario usuario ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	/**
	 * Valida los datos del objeto usuario
	 * @return Respuesta 
	 */
	public static RespuestaEntidad Validate(Usuario usuario ,String accesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		if (accesoADatos.equals(Dominios.AccesoADatos.INSERT))
			response = ValidateId(true ,usuario.getId());
		else
			response = ValidateId(false ,usuario.getId());
		if (!response.isError()){
			response = ValidateNombre(usuario.getNombre());
			if (!response.isError()){
				response = ValidateMailPrincipal(usuario.getId(), usuario.getMailPrincipal());
				if (!response.isError()){
				}
			}
		}
		return response;
	}
	
	public static RespuestaEntidad ValidateId(boolean validarExiste, String Id){
		RespuestaEntidad response = new RespuestaEntidad();
		Usuario usuario = UsuarioRN.Read(Id);
		if (Id == null){
			response.setMsgError(UsuarioLabel.getDebeIngresarElUsuario());
			response.setError(true);
		}
		else if (Id.isEmpty()){
			response.setMsgError(UsuarioLabel.getDebeIngresarElUsuario());
			response.setError(true);
		}
		else if (Id.length() > 20){
			response.setMsgError(UsuarioLabel.getIdExcedio20Caracteres());
			response.setError(true);
		}
		else if (Id.contains(" ")){
			response.setMsgError(UsuarioLabel.getIDNoDebeTenesEspaciosEnBlanco());
			response.setError(true);
		}
		else if (validarExiste && usuario != null ){
			response.setMsgError(UsuarioLabel.getExisteId());
			response.setError(true);
		}
		return response;
	}
	
	public static RespuestaEntidad ValidateNombre(String nombre){
		RespuestaEntidad response = new RespuestaEntidad();
		if (nombre == null){
			response.setMsgError(UsuarioLabel.getDebeIngresarNombre());
			response.setError(true);
		}
		else if (nombre.isEmpty()){
			response.setMsgError(UsuarioLabel.getDebeIngresarNombre());
			response.setError(true);
		}
		else if (nombre.length() > 60){
			response.setMsgError(UsuarioLabel.getNombreExcedio60Caracteres());
			response.setError(true);
		}
		return response;
	}
	
	public static RespuestaEntidad ValidateMailPrincipal(String id, String mail){
		RespuestaEntidad response = new RespuestaEntidad();
		EmailValidator emailValidator = new EmailValidator(UsuarioLabel.getMailNoValido());
		if (!emailValidator.isValid(mail)){
			response.setMsgError(emailValidator.getErrorMessage());
			response.setError(true);
		}
		return response;
	}

	
	public static RespuestaEntidad ValidateClave(String Clave) {
		RespuestaEntidad response = new RespuestaEntidad();
		if (HerramientasSeguridad.checkPasswordStrength(Clave) < 50){
			response.setError(true);
			response.setMsgError(UsuarioLabel.getClaveValidacion());
		}
		return response;
	}
	
	public static RespuestaEntidad ValidateClaveConfirmar(String clave ,String claveRepetida) {
		RespuestaEntidad response = new RespuestaEntidad();
		if (!clave.equals(claveRepetida)){
			response.setError(true);
			response.setMsgError(UsuarioLabel.getClaveConfirmadaDistinta());
		}
		return response;
	}
	
	public static RespuestaEntidad ValidateConexionGoogle(String mail ,String password){
		RespuestaEntidad response = new RespuestaEntidad();
		EmailValidator emailValidator = new EmailValidator(UsuarioLabel.getMailNoValido());
		if (mail == null){
			response.setMsgError(UsuarioLabel.getDebeIngresarMail());
			response.setError(true);
		}
		else if (mail.isEmpty()){
			response.setMsgError(UsuarioLabel.getDebeIngresarMail());
			response.setError(true);
		}
		else if (!emailValidator.isValid(mail)){
			response.setMsgError(emailValidator.getErrorMessage());
			response.setError(true);
		}
		else if (password == null){
			response.setMsgError(UsuarioLabel.getDebeIngresarClave());
			response.setError(true);
		}
		else if (password.isEmpty()){
			response.setMsgError(UsuarioLabel.getDebeIngresarClave());
			response.setError(true);
		}
		return response;
	}
}
