package ar.com.trisquel.escuela.data.reglasdenegocio;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.PlanillaAlumno;
import ar.com.trisquel.escuela.data.tablas.identificadores.PlanillaAlumnoId;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class PlanillaAlumnoRN {
	
	public static PlanillaAlumno Read(PlanillaAlumnoId id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			PlanillaAlumno planillaNotas = (PlanillaAlumno)HibernateSession.get(PlanillaAlumno.class , id);
			HibernateSession.getTransaction().rollback();
			return planillaNotas;
		}
		catch (HibernateException e){
			return null;
		}
	}

	public static RespuestaEntidad Save(PlanillaAlumno planillaNotas) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(planillaNotas);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(PlanillaAlumno planillaNotas){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(planillaNotas);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static PlanillaAlumno Inizializate(){
		PlanillaAlumno planillaNotas = new PlanillaAlumno();
		return planillaNotas;
	}
	
	public static RespuestaEntidad AfterSave(PlanillaAlumno planillaNotas ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(PlanillaAlumno planillaNotas ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		return response;
	}
}
