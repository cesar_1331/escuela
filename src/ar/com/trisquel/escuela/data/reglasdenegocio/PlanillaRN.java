package ar.com.trisquel.escuela.data.reglasdenegocio;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.select.PlanillaAlumnoSelect;
import ar.com.trisquel.escuela.data.tablas.Planilla;
import ar.com.trisquel.escuela.data.tablas.PlanillaAlumno;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class PlanillaRN {
	
	public static Planilla Read(long id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			Planilla planilla = (Planilla)HibernateSession.get(Planilla.class , id);
			HibernateSession.getTransaction().rollback();
			return planilla;
		}
		catch (HibernateException e){
			return null;
		}
	}

	public static RespuestaEntidad Save(Planilla planilla) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(planilla);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(Planilla planilla){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			for(PlanillaAlumno planillaAlumno : PlanillaAlumnoSelect.SelectXPlanilla(planilla.getId())){
				PlanillaAlumnoRN.Delete(planillaAlumno);
			}
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(planilla);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static Planilla Inizializate(){
		Planilla planilla = new Planilla();
		planilla.setEsNotaFinal(false);
		planilla.setMostrarAlAlumno(true);
		return planilla;
	}
	
	public static RespuestaEntidad AfterSave(Planilla planilla ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(Planilla planilla ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		if (planilla.getAnio() <= 0){
			response.setError(true);
			response.setMsgError("No selecciono el a�o de la planilla");
		}
		else if (planilla.getCursoId() <= 0){
			response.setError(true);
			response.setMsgError("No selecciono el curso de la planilla");
		}
		else if (planilla.getDescripcion().trim().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe ingresar la descripci�n de la planilla");
		}
		else if (planilla.getMateriaId() <= 0){
			response.setError(true);
			response.setMsgError("No selecciono la materia de la planilla");
		}
		else if (planilla.getPeriodo() <= 0){
			response.setError(true);
			response.setMsgError("Debe seleccionar el periodo al que corresponde la planilla");
		}
		return response;
	}
}
