package ar.com.trisquel.escuela.data.reglasdenegocio;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.componentes.parametro.EscuelaView;
import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Parametro;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class ParametroRN {
	
	public static Parametro Read(String id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			Parametro parametro = (Parametro)HibernateSession.get(Parametro.class , id);
			HibernateSession.getTransaction().rollback();
			return parametro;
		}
		catch (HibernateException e){
			return null;
		}
	}

	public static RespuestaEntidad Save(Parametro parametro) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(parametro);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(Parametro parametro){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(parametro);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static Parametro Inizializate(String id ,String tipoDato ,String valor){
		Parametro parametro = new Parametro(id ,tipoDato ,valor );
		return parametro;
	}
	
	public static RespuestaEntidad AfterSave(Parametro parametro ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(Parametro parametro ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		return response;
	}
	
	public static String LeeEscuelaNombre(){
		Parametro parametro = ParametroRN.Read(EscuelaView.ESCUELA_NOMBRE);
		if (parametro != null)
			return parametro.getValor().trim();
		else 
			return "";
	}
	public static String LeeEscuelaDomicilio(){
		Parametro parametro = ParametroRN.Read(EscuelaView.ESCUELA_DOMICILIO);
		if (parametro != null)
			return parametro.getValor().trim();
		else 
			return "";
	}
	public static String LeeEscuelaProvincia(){
		Parametro parametro = ParametroRN.Read(EscuelaView.ESCUELA_PROVINCIA);
		if (parametro != null)
			return parametro.getValor().trim();
		else 
			return "";
	}
	public static String LeeEscuelaCiudad(){
		Parametro parametro = ParametroRN.Read(EscuelaView.ESCUELA_CIUDAD);
		if (parametro != null)
			return parametro.getValor().trim();
		else 
			return "";
	}
	public static int LeeEscuelaCantidadPeriodos(){
		int cantidadPeriodos = 3;
		Parametro parametro = ParametroRN.Read(EscuelaView.ESCUELA_CANTIDADPERIODOS);
		if (parametro != null){
			try{
				cantidadPeriodos = Integer.parseInt(parametro.getValor().trim());
			}catch(Exception e){
			}
		}
		return cantidadPeriodos; 
	}
}
