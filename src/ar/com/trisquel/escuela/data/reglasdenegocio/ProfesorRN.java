package ar.com.trisquel.escuela.data.reglasdenegocio;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class ProfesorRN {
	
	public static Profesor Read(long id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			Profesor profesor = (Profesor)HibernateSession.get(Profesor.class , id);
			HibernateSession.getTransaction().rollback();
			return profesor;
		}
		catch (HibernateException e){
			return null;
		}
	}

	public static RespuestaEntidad Save(Profesor profesor) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(profesor);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(Profesor profesor){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(profesor);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static Profesor Inizializate(){
		Profesor profesor = new Profesor();
		profesor.setActivo(true);
		return profesor;
	}
	
	public static RespuestaEntidad AfterSave(Profesor profesor ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(Profesor profesor ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		if (profesor.getApellido() == null || profesor.getApellido().trim().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe ingresar el apellido del profesor");
		}
		else if (profesor.getNombre() == null || profesor.getNombre().trim().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe ingresar el nombre del profesor");
		}
		else if (profesor.getDomicilio() == null ||profesor.getDomicilio().trim().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe ingresar el domicilio del profesor");
		}
		else if (profesor.getTitulo() == null || profesor.getTitulo().trim().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe ingresar el titulo del profesor");
		}
		return response;
	}
}
