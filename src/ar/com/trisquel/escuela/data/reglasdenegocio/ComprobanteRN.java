package ar.com.trisquel.escuela.data.reglasdenegocio;

import java.util.Date;
import java.util.GregorianCalendar;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Comprobante;
import ar.com.trisquel.escuela.data.tablas.ComprobanteBloqueo;
import ar.com.trisquel.escuela.data.tablas.TipoComprobante;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.data.tablas.identificadores.AnioMesId;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class ComprobanteRN {

	public static Comprobante Read(long id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			Comprobante comprobante = (Comprobante)HibernateSession.get(Comprobante.class , id);
			HibernateSession.getTransaction().rollback();
			return comprobante;
		}
		catch (HibernateException e){
			return null;
		}
	}
	
	public static RespuestaEntidad Add(Comprobante comprobante) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.save(comprobante);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static RespuestaEntidad Save(Comprobante comprobante) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(comprobante);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(Comprobante comprobante){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(comprobante);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static Comprobante Inizializate(Usuario usuario ,TipoComprobante tipoComprobante){
		Comprobante comprobante = new Comprobante();
		comprobante.setAltaFecha(new Date(System.currentTimeMillis()));
		comprobante.setAltaResponsable(usuario.getId());
		comprobante.setAnulado(false);
		comprobante.setBloqueado(false);
		comprobante.setTipoComprobanteId(tipoComprobante.getId());
		comprobante.setTipo(tipoComprobante.getTipo());
		comprobante.setOperacion(tipoComprobante.getOperacion());
		comprobante.setSaldo(tipoComprobante.getSaldo());
		comprobante.setPersonaId(0);
		return comprobante;
	}
	
	public static RespuestaEntidad AfterSave(Comprobante comprobante ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(Comprobante comprobante ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		TipoComprobante tipoComprobante = TipoComprobanteRN.Read(comprobante.getTipoComprobanteId());
		boolean periodoBloqueado = false;
		if (comprobante.getFecha() != null){
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(comprobante.getFecha());
			ComprobanteBloqueo comprobanteBloqueo = ComprobanteBloqueoRN.Read(new AnioMesId(calendar.get(GregorianCalendar.YEAR), calendar.get(GregorianCalendar.MONTH)+1));
			if (comprobanteBloqueo != null && comprobanteBloqueo.isBloqueado()){
				periodoBloqueado = true;
			}
		}
		if (comprobante.getFecha() == null){
			response.setError(true);
			response.setMsgError("Debe ingresar la fecha");
		}
		else if (periodoBloqueado){
			response.setError(true);
			response.setMsgError("La fecha del comprobante pertenece a un periodo bloqueado");
		}
		else if (comprobante.getFechaRegistro() == null){
			response.setError(true);
			response.setMsgError("Debe ingresar la fecha de registro");
		}
		else if (comprobante.getFecha().after(comprobante.getFechaRegistro())){
			response.setError(true);
			response.setMsgError("La fecha del comprobante no puede ser superior a la fecha de registro");
		} 
		else if (comprobante.getFormaPago().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe seleccionar la forma de pago");
		}
		else if (comprobante.getLetra().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe indicar la letra del comprobante");
		}
		else if (comprobante.getNumero() < 0){
			response.setError(true);
			response.setMsgError("Debe ingresar el n�mero del comprobante");
		}
		else if (comprobante.getPersonaId() <= 0 && !tipoComprobante.getTipo().equals(Dominios.TipoComprobante.Tipo.COMPROBANTE_CAJA)){
			response.setError(true);
			response.setMsgError("Debe seleccionar la persona del comprobante");
		}
		else if (comprobante.getTipoComprobanteId() <= 0){
			response.setError(true);
			response.setMsgError("No se selecciono el tipo de comprobante");
		}
		else if (comprobante.getTotal() <= 0){
			response.setError(true);
			response.setMsgError("El total del comprobante no es valido");
		}
		return response;
	}
}
