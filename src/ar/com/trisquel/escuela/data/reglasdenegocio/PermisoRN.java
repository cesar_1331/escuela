package ar.com.trisquel.escuela.data.reglasdenegocio;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.componentes.user.UsuarioPermisos;
import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Permiso;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.data.tablas.identificadores.PermisoId;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class PermisoRN {
	public static Permiso Read(PermisoId id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			Permiso permiso = (Permiso)HibernateSession.get(Permiso.class , id);
			HibernateSession.getTransaction().rollback();
			if (permiso == null){
				Usuario usuario = UsuarioRN.Read(id.getUsuarioId());
				if (usuario.getTipoUsuario() == Dominios.TipoUsuario.ADMINISTRADOR)
					UsuarioPermisos.CreaPermisos(usuario);
			}
			return permiso;
		}
		catch (HibernateException e){
			return null;
		}
	}
	
	public static RespuestaEntidad Add(Permiso permiso) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.save(permiso);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static RespuestaEntidad Save(Permiso permiso) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(permiso);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(Permiso permiso){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(permiso);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static Permiso Inizializate(String usuarioId,String view ,String nombre){
		Permiso permiso = new Permiso(new PermisoId(usuarioId, view));
		permiso.setNombre(nombre);
		permiso.setAcceso(true);
		permiso.setAgregar(true);
		permiso.setBorrar(true);
		permiso.setModificar(true);
		return permiso;
	}
	
	public static RespuestaEntidad AfterSave(Permiso permiso ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(Permiso permiso ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		return response;
	}
}
