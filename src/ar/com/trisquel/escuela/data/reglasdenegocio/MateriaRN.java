package ar.com.trisquel.escuela.data.reglasdenegocio;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Materia;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class MateriaRN {
	public static Materia Read(long id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			Materia materia = (Materia)HibernateSession.get(Materia.class , id);
			HibernateSession.getTransaction().rollback();
			return materia;
		}
		catch (HibernateException e){
			return null;
		}
	}

	public static RespuestaEntidad Save(Materia materia) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(materia);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(Materia materia){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(materia);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static Materia Inizializate(){
		Materia materia = new Materia();
		materia.setId(0);
		materia.setNombre("");
		materia.setActiva(true);
		return materia;
	}
	
	public static Materia Inizializate(String nombre){
		Materia materia = new Materia();
		materia.setId(0);
		materia.setNombre(nombre);
		materia.setActiva(true);
		return materia;
	}
	
	public static RespuestaEntidad AfterSave(Materia materia ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(Materia materia ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		if (materia.getNombre().trim().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe ingresar el nombre de la materia");
		}
		return response;
	}
}
