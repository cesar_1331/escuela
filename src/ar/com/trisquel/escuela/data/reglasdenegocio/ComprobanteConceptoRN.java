package ar.com.trisquel.escuela.data.reglasdenegocio;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.select.ComprobanteConceptoSelect;
import ar.com.trisquel.escuela.data.tablas.ComprobanteConcepto;
import ar.com.trisquel.escuela.data.tablas.identificadores.ComprobanteLinea;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class ComprobanteConceptoRN {
	
	public static ComprobanteConcepto Read(ComprobanteLinea id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			ComprobanteConcepto comprobanteConcepto = (ComprobanteConcepto)HibernateSession.get(ComprobanteConcepto.class , id);
			HibernateSession.getTransaction().rollback();
			return comprobanteConcepto;
		}
		catch (HibernateException e){
			return null;
		}
	}
	
	public static RespuestaEntidad Add(ComprobanteConcepto comprobanteConcepto) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.save(comprobanteConcepto);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static RespuestaEntidad Save(ComprobanteConcepto comprobanteConcepto) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(comprobanteConcepto);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(ComprobanteConcepto comprobanteConcepto){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(comprobanteConcepto);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static ComprobanteConcepto Inizializate(long comprobanteId){
		ComprobanteConcepto comprobanteConcepto = new ComprobanteConcepto();
		comprobanteConcepto.setId(new ComprobanteLinea(comprobanteId ,ComprobanteConceptoSelect.LeeMaxLinea(comprobanteId)));
		return comprobanteConcepto;
	}
	
	public static RespuestaEntidad AfterSave(ComprobanteConcepto comprobanteConcepto ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(ComprobanteConcepto comprobanteConcepto ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		if (comprobanteConcepto.getConceptoId() <= 0){
			response.setError(true);
			response.setMsgError("Debe seleccionar el concepto");
		}
		else if (comprobanteConcepto.getImporte() <= 0){
			response.setError(true);
			response.setMsgError("Debe ingresar el importe");
		}
		
		return response;
	}
}
