package ar.com.trisquel.escuela.data.reglasdenegocio;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.select.NotificacionPersonaSelect;
import ar.com.trisquel.escuela.data.tablas.Notificacion;
import ar.com.trisquel.escuela.data.tablas.NotificacionPersona;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class NotificacionRN {
	
	public static Notificacion Read(long id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			Notificacion notificacion = (Notificacion)HibernateSession.get(Notificacion.class , id);
			HibernateSession.getTransaction().rollback();
			return notificacion;
		}
		catch (HibernateException e){
			return null;
		}
	}

	public static RespuestaEntidad Save(Notificacion notificacion) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(notificacion);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(Notificacion notificacion){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(notificacion);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static Notificacion Inizializate(String usuarioId){
		Notificacion notificacion = new Notificacion();
		notificacion.setFechaAlta(new Date(System.currentTimeMillis()));
		notificacion.setCancelada(false);
		notificacion.setLeido(false);
		notificacion.setResponsableAlta(usuarioId);
		return notificacion;
	}
	
	public static RespuestaEntidad AfterSave(Notificacion notificacion ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(Notificacion notificacion ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		if (notificacion.getDescripcion().trim().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe ingresar el mensaje de la notificación");
		}
		else if (notificacion.getTitulo().trim().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe ingresar el titulo de la notificación");
		}	
		else if (notificacion.getPrioridad() < 0){
			response.setError(true);
			response.setMsgError("Debe ingresar la prioridad de la notificación");
		}
		return response;
	}
	
	public static void Cancelar(Notificacion notificacion ) {
		notificacion.setCancelada(true);
		NotificacionRN.Save(notificacion);
		List<NotificacionPersona> listNotificacionPersona = NotificacionPersonaSelect.SelectXNotificacion(notificacion.getId());
		for (NotificacionPersona notificacionPersona : listNotificacionPersona){
			notificacionPersona.setCancelado(true);
			NotificacionPersonaRN.Save(notificacionPersona);
		}
		
	}
}
