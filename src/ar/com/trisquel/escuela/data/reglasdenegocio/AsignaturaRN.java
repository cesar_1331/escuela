package ar.com.trisquel.escuela.data.reglasdenegocio;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.select.AsignaturaSelect;
import ar.com.trisquel.escuela.data.tablas.Asignatura;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class AsignaturaRN {
	
	public static Asignatura Read(long id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			Asignatura asignatura = (Asignatura)HibernateSession.get(Asignatura.class , id);
			HibernateSession.getTransaction().rollback();
			return asignatura;
		}
		catch (HibernateException e){
			return null;
		}
	}
	
	public static RespuestaEntidad Add(Asignatura asignatura) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.save(asignatura);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static RespuestaEntidad Save(Asignatura asignatura) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(asignatura);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(Asignatura asignatura){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(asignatura);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static Asignatura Inizializate(){
		Asignatura asignatura = new Asignatura();
		return asignatura;
	}
	
	public static RespuestaEntidad AfterSave(Asignatura asignatura ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		return response;
	}
	
	public static RespuestaEntidad Validate(Asignatura asignatura ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		Asignatura asignaturaControl = AsignaturaSelect.LeeAsignaturaXAnioXCursoXMateria(asignatura.getAnio(), asignatura.getCursoId() ,asignatura.getMateriaId());
		if (asignaturaControl !=null )
		{
			if (asignaturaControl.getId() != asignatura.getId()){
				response.setError(true);
				response.setMsgError("Ya existe una asignatura para el a�o, el curso y la materia seleccionadas");
			}
		}
		return response;
	}
}
