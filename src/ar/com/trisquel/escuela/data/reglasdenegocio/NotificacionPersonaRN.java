package ar.com.trisquel.escuela.data.reglasdenegocio;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.select.NotificacionPersonaSelect;
import ar.com.trisquel.escuela.data.tablas.Notificacion;
import ar.com.trisquel.escuela.data.tablas.NotificacionPersona;
import ar.com.trisquel.escuela.data.tablas.identificadores.NotificacionPersonaId;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class NotificacionPersonaRN {
	
	public static NotificacionPersona Read(NotificacionPersonaId id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			NotificacionPersona notificacionPersona = (NotificacionPersona)HibernateSession.get(NotificacionPersona.class , id);
			HibernateSession.getTransaction().rollback();
			return notificacionPersona;
		}
		catch (HibernateException e){
			return null;
		}
	}
	
	public static RespuestaEntidad Add(NotificacionPersona notificacionPersona) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.save(notificacionPersona);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static RespuestaEntidad Save(NotificacionPersona notificacionPersona) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(notificacionPersona);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(NotificacionPersona notificacionPersona){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(notificacionPersona);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static NotificacionPersona Inizializate(long notificacionId,String tipo,long personaId){
		NotificacionPersona notificacionPersona = new NotificacionPersona();
		notificacionPersona.setId(new NotificacionPersonaId(notificacionId, tipo, personaId));
		notificacionPersona.setCancelado(false);
		notificacionPersona.setFechaAlta(new Date(System.currentTimeMillis()));
		notificacionPersona.setLeido(false);
		return notificacionPersona;
	}
	
	public static RespuestaEntidad AfterSave(NotificacionPersona notificacionPersona ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(NotificacionPersona notificacionPersona ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		return response;
	}
	
	public static void MarcarLeido(NotificacionPersona notificacionPersona ) {
		
		notificacionPersona.setLeido(true);
		notificacionPersona.setFechaLeido(new Date(System.currentTimeMillis()));
		NotificacionPersonaRN.Save(notificacionPersona);
		List<NotificacionPersona> listNotificacionPersona = NotificacionPersonaSelect.SelectXNotificacion(notificacionPersona.getId().getNotificacionId());
		boolean marcarComoLeido = true;
		for (NotificacionPersona notificacionPersonaAux : listNotificacionPersona){
			if (!notificacionPersonaAux.isLeido()){
				marcarComoLeido = false;
				break;
			}
		}
		if (marcarComoLeido){
			Notificacion notificacion = NotificacionRN.Read(notificacionPersona.getId().getNotificacionId());
			notificacion.setLeido(true);
			NotificacionRN.Save(notificacion);
		}
	} 
	
}
