package ar.com.trisquel.escuela.data.reglasdenegocio;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.AsignaturaAlumno;
import ar.com.trisquel.escuela.data.tablas.identificadores.AsignaturaAlumnoId;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class AsignaturaAlumnoRN {

	public static AsignaturaAlumno Read(AsignaturaAlumnoId id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			AsignaturaAlumno asignaturaAlumno = (AsignaturaAlumno)HibernateSession.get(AsignaturaAlumno.class , id);
			HibernateSession.getTransaction().rollback();
			return asignaturaAlumno;
		}
		catch (HibernateException e){
			return null;
		}
	}

	public static RespuestaEntidad Add(AsignaturaAlumno asignaturaAlumno) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.save(asignaturaAlumno);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static RespuestaEntidad Save(AsignaturaAlumno asignaturaAlumno) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(asignaturaAlumno);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(AsignaturaAlumno asignaturaAlumno){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(asignaturaAlumno);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static AsignaturaAlumno Inizializate(){
		AsignaturaAlumno asignaturaAlumno = new AsignaturaAlumno();
		asignaturaAlumno.setCursando(true);
		asignaturaAlumno.setOrden(0);
		return asignaturaAlumno;
	}
	
	public static RespuestaEntidad AfterSave(AsignaturaAlumno asignaturaAlumno ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(AsignaturaAlumno asignaturaAlumno ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		return response;
	}
}
