package ar.com.trisquel.escuela.data.reglasdenegocio;

import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Articulo;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class ArticuloRN {
	
	public static Articulo Read(String id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			Articulo articulo = (Articulo)HibernateSession.get(Articulo.class , id);
			HibernateSession.getTransaction().rollback();
			return articulo;
		}
		catch (HibernateException e){
			return null;
		}
	}
	
	public static RespuestaEntidad Add(Articulo articulo) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.save(articulo);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static RespuestaEntidad Save(Articulo articulo) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(articulo);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(Articulo articulo){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(articulo);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static Articulo Inizializate(){
		Articulo articulo = new Articulo();
		articulo.setId("");
		articulo.setTitulo("");
		articulo.setUsuario("");
		articulo.setContenido("");
		articulo.setFechaHora(new Date(System.currentTimeMillis()));
		return articulo;
	}
	
	public static Articulo Inizializate(String id ,String titulo ,String usuario){
		Articulo articulo = new Articulo();
		articulo.setId(id);
		articulo.setTitulo(titulo);
		articulo.setUsuario(usuario);
		articulo.setContenido("");
		articulo.setFechaHora(new Date(System.currentTimeMillis()));
		return articulo;
	}
	
	public static RespuestaEntidad AfterSave(Articulo articulo ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(Articulo articulo ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		return response;
	}
}
