package ar.com.trisquel.escuela.data.reglasdenegocio;

import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.ProfesorCurso;
import ar.com.trisquel.escuela.data.tablas.identificadores.ProfesorCursoId;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class ProfesorCursoRN {
	
	public static ProfesorCurso Read(ProfesorCursoId id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			ProfesorCurso profesorCurso = (ProfesorCurso)HibernateSession.get(ProfesorCurso.class , id);
			HibernateSession.getTransaction().rollback();
			return profesorCurso;
		}
		catch (HibernateException e){
			return null;
		}
	}

	public static RespuestaEntidad Save(ProfesorCurso profesorCurso) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(profesorCurso);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(ProfesorCurso profesorCurso){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(profesorCurso);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static ProfesorCurso Inizializate(long profesorId ,long cursoId){
		ProfesorCurso profesorCurso = new ProfesorCurso();
		profesorCurso.setId(new ProfesorCursoId(profesorId ,cursoId));
		profesorCurso.setFechaAsociacion(new Date(System.currentTimeMillis()));
		return profesorCurso;
	}
	
	public static RespuestaEntidad AfterSave(ProfesorCurso profesorCurso ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(ProfesorCurso profesorCurso ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		return response;
	}
}
