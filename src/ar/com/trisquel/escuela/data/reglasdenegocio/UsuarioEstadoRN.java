package ar.com.trisquel.escuela.data.reglasdenegocio;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.select.UsuarioEstadoSelect;
import ar.com.trisquel.escuela.data.tablas.UsuarioEstado;
import ar.com.trisquel.escuela.data.tablas.identificadores.UsuarioLinea;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class UsuarioEstadoRN {

	public static UsuarioEstado Read(UsuarioLinea id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			UsuarioEstado usuarioEstado = (UsuarioEstado)HibernateSession.get(UsuarioEstado.class , id);
			HibernateSession.getTransaction().rollback();
			return usuarioEstado;
		}
		catch (HibernateException e){
			return null;
		}
	}

	public static RespuestaEntidad Save(UsuarioEstado usuarioEstado) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(usuarioEstado);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(UsuarioEstado usuarioEstado){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(usuarioEstado);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static UsuarioEstado Inizializate(String usuario){
		UsuarioEstado usuarioEstado = new UsuarioEstado();
		long linea = UsuarioEstadoSelect.MaxLinea(usuario); 
		usuarioEstado.setId(new UsuarioLinea(usuario, linea));
		return usuarioEstado;
	}
	
	public static RespuestaEntidad AfterSave(UsuarioEstado usuarioEstado ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(UsuarioEstado usuarioEstado ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		return response;
	}
}
