package ar.com.trisquel.escuela.data.reglasdenegocio;

import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.DatosModificados;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class DatosModificadosRN {
	
	public static DatosModificados Read(long id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			DatosModificados datosModificados = (DatosModificados)HibernateSession.get(DatosModificados.class , id);
			HibernateSession.getTransaction().rollback();
			return datosModificados;
		}
		catch (HibernateException e){
			return null;
		}
	}

	public static RespuestaEntidad Save(DatosModificados datosModificados) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(datosModificados);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(DatosModificados datosModificados){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(datosModificados);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static DatosModificados Inizializate(int entidad ,long entidadId ,String descripcion){
		DatosModificados datosModificados = new DatosModificados();
		datosModificados.setEntidad(entidad);
		datosModificados.setEntidadId(entidadId);
		datosModificados.setDescripcion(descripcion);
		datosModificados.setFechaModificacion(new Date(System.currentTimeMillis()));
		return datosModificados;
	}
	
	public static RespuestaEntidad AfterSave(DatosModificados datosModificados ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(DatosModificados datosModificados ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		return response;
	}
}
