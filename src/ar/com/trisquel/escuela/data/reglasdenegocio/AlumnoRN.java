package ar.com.trisquel.escuela.data.reglasdenegocio;

import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Alumno;
import ar.com.trisquel.escuela.utiles.Dominios;
import ar.com.trisquel.escuela.utiles.RespuestaEntidad;

public class AlumnoRN {
	public static Alumno Read(long id) {
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			Alumno alumno = (Alumno)HibernateSession.get(Alumno.class , id);
			HibernateSession.getTransaction().rollback();
			return alumno;
		}
		catch (HibernateException e){
			return null;
		}
	}

	public static RespuestaEntidad Save(Alumno alumno) {
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.saveOrUpdate(alumno);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}

	public static RespuestaEntidad Delete(Alumno alumno){
		RespuestaEntidad response = new RespuestaEntidad();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			HibernateSession.delete(alumno);
			HibernateSession.getTransaction().commit();
		}
		catch (HibernateException e){
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		} catch (Exception e) {
			response.setError(true);
			response.setMsgError(e.getLocalizedMessage());
		}
		return response;
	}
	
	public static Alumno Inizializate(String usuarioId){
		Alumno alumno = new Alumno();
		alumno.setDni(0);
		alumno.setEstado(Dominios.EstadoAlumno.CURSANDO);
		alumno.setFechaAlta(new Date(System.currentTimeMillis()));
		alumno.setResponsableAlta(usuarioId);
		alumno.setFechaModificacion(new Date(System.currentTimeMillis()));
		alumno.setResponsableModificacion(usuarioId);
		return alumno;
	}
	
	public static RespuestaEntidad AfterSave(Alumno alumno ,String AccesoADatos ){
		RespuestaEntidad response = new RespuestaEntidad();
		
		return response;
	}
	
	public static RespuestaEntidad Validate(Alumno alumno ,String AccesoADatos) {
		RespuestaEntidad response = new RespuestaEntidad();
		if (alumno.getNombre() == null){
			response.setError(true);
			response.setMsgError("Debe ingresar el nombre del alumno");
		}
		else if (alumno.getNombre().trim().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe ingresar el nombre del alumno");
		}
		else if (alumno.getApellido() == null){
			response.setError(true);
			response.setMsgError("Debe ingresar el apellido del alumno");
		}
		else if (alumno.getApellido().trim().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe ingresar el apellido del alumno");
		}
		else if (alumno.getDni() <= 0){
			response.setError(true);
			response.setMsgError("Debe ingresar el n�mero de documento del alumno");
		}
		else if (alumno.getDomicilio() == null){
			response.setError(true);
			response.setMsgError("Debe ingresar el domicilio del alumno");
		}
		else if (alumno.getDomicilio().trim().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe ingresar el domicilio del alumno");
		}
		else if (alumno.getGenero().trim().isEmpty()){
			response.setError(true);
			response.setMsgError("Debe ingresar el sexo del alumno");
		}
		else if (alumno.getFechaNacimiento() == null){
			response.setError(true);
			response.setMsgError("Debe ingresar la fecha de nacimiento del alumno");
		}
		return response;
	}
}
