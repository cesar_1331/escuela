package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Permiso;

public class PermisoSelect {
	public static List<Permiso> SelectXUsuarioId(String usuarioId){
		List<Permiso> listPermiso = new ArrayList<Permiso>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<Permiso> lista  = (List<Permiso>)HibernateSession.createCriteria(Permiso.class)
					.add(Restrictions.eq("id.usuarioId", usuarioId))
					.addOrder(Order.asc("nombre"))
					.list();		
			listPermiso = lista;
			HibernateSession.getTransaction().rollback();
			
		}catch (HibernateException e){
		}
		return listPermiso;
	}
}
