package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Alumno;

public class AlumnoSelect {
	
	public static List<Alumno> SelectXNombreApellidoXDniXEstadoXCurso(String nombre ,long dni ,String estado ,long cursoId){
		List<Alumno> listAlumno = new ArrayList<Alumno>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			Criteria criteria = HibernateSession.createCriteria(Alumno.class)
					.add(Restrictions.or(
							Restrictions.like("nombre", nombre, MatchMode.ANYWHERE),
							Restrictions.like("apellido", nombre, MatchMode.ANYWHERE)))
					.addOrder(Order.asc("apellido"))
					.addOrder(Order.asc("nombre"));
			if (dni > 0){
				criteria.add(Restrictions.eq("dni", dni));
			}
			if (estado != null){
				criteria.add(Restrictions.eq("estado", estado));
			}
			if (cursoId > 0){
				criteria.add(Restrictions.eq("cursoId", cursoId));
			}
			@SuppressWarnings("unchecked")
			List<Alumno> lista  = (List<Alumno>)criteria.list();		
			listAlumno = lista;
			HibernateSession.getTransaction().rollback();
			
		}catch (HibernateException e){
			System.out.println(e.toString());
		}
		return listAlumno;
	}
	
	public static List<Alumno> SelectXCursoId(long cursoId){
		List<Alumno> listAlumno = new ArrayList<Alumno>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<Alumno> lista  = (List<Alumno>)HibernateSession.createCriteria(Alumno.class)
					.add(Restrictions.eq("cursoId", cursoId))
					.addOrder(Order.asc("cursoId"))
					.addOrder(Order.asc("apellido"))
					.addOrder(Order.asc("nombre"))
					.list();		
			listAlumno = lista;
			HibernateSession.getTransaction().rollback();
			
		}catch (HibernateException e){
		}
		return listAlumno;
	}
}
