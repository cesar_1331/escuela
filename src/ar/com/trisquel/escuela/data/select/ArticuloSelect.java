package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Articulo;

public class ArticuloSelect {
	/**
	 * Select Base para armar tomar como ejemplo
	 */
	public static List<Articulo> Select(){
		List<Articulo> listArticulo = new ArrayList<Articulo>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<Articulo> lista  = (List<Articulo>)HibernateSession.createCriteria(Articulo.class).list();
			HibernateSession.getTransaction().rollback();
			listArticulo = lista;
		}catch (HibernateException e){
		}
		return listArticulo;
	}
	
	public static List<Articulo> SelectXTitulo(String titulo){
		List<Articulo> listArticulo = new ArrayList<Articulo>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<Articulo> lista  = (List<Articulo>)HibernateSession.createCriteria(Articulo.class)
					.add(Restrictions.like("titulo", titulo, MatchMode.ANYWHERE))
					.addOrder(Order.asc("titulo"))
				.list();
			HibernateSession.getTransaction().rollback();
			listArticulo = lista;
		}catch (HibernateException e){
		}
		return listArticulo;
	}
}
