package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.AlumnoAsistencia;
import ar.com.trisquel.escuela.utiles.Dominios;

public class AlumnoAsistenciaSelect {

	public static List<AlumnoAsistencia> SelectXAlumno(long alumnoId){
		List<AlumnoAsistencia> listAlumnoAsistencia = new ArrayList<AlumnoAsistencia>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<AlumnoAsistencia> lista  = (List<AlumnoAsistencia>)HibernateSession.createCriteria(AlumnoAsistencia.class)
				.add(Restrictions.eq("id.alumnoId", alumnoId))
				.addOrder(Order.asc("id.alumnoId"))
				.addOrder(Order.asc("id.dia"))
				.list();
			HibernateSession.getTransaction().rollback();
			listAlumnoAsistencia = lista;
		}catch (HibernateException e){
			System.out.print(e.toString());
		}
		return listAlumnoAsistencia;
	}
	
	public static List<AlumnoAsistencia> SelectXAlumnoXAnio(long alumnoId ,int anio){
		GregorianCalendar calendar = new GregorianCalendar(anio, 1, 1);
		Date fechaDesde = calendar.getTime();
		calendar = new GregorianCalendar(anio, 12, 31);
		Date fechaHasta  = calendar.getTime();;
		List<AlumnoAsistencia> listAlumnoAsistencia = new ArrayList<AlumnoAsistencia>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<AlumnoAsistencia> lista  = (List<AlumnoAsistencia>)HibernateSession.createCriteria(AlumnoAsistencia.class)
				.add(Restrictions.eq("id.alumnoId", alumnoId))
				.add(Restrictions.ge("id.dia", fechaDesde)) //Mayor o igual
				.add(Restrictions.lt("id.dia", fechaHasta)) //Menor
				.addOrder(Order.asc("id.alumnoId"))
				.addOrder(Order.asc("id.dia"))
				.list();
			HibernateSession.getTransaction().rollback();
			listAlumnoAsistencia = lista;
		}catch (HibernateException e){
			System.out.print(e.toString());
		}
		return listAlumnoAsistencia;
	}
	
	public static int[]  LeeInasistenciaXAlumnoXAnio(long alumnoId ,int anio){
		GregorianCalendar calendar = new GregorianCalendar(anio, 1, 1);
		Date fechaDesde = calendar.getTime();
		calendar = new GregorianCalendar(anio, 12, 31);
		Date fechaHasta  = calendar.getTime();;
		int[] inasistencias = new int[2];
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<Integer> lista1  = (List<Integer>)HibernateSession.createCriteria(AlumnoAsistencia.class)
				.setProjection(Projections.count("id.alumnoId"))
				.add(Restrictions.eq("id.alumnoId", alumnoId))
				.add(Restrictions.ge("id.dia", fechaDesde)) //Mayor o igual
				.add(Restrictions.lt("id.dia", fechaHasta)) //Menor
				.add(Restrictions.eq("asistencia", Dominios.Asistencia.JUSTIFICADA))
				.addOrder(Order.asc("id.alumnoId"))
				.addOrder(Order.asc("id.dia"))
				.list();
			if (lista1.size() > 0){
				inasistencias[0] = (Integer)lista1.get(0);
			}
			@SuppressWarnings("unchecked")
			List<Integer> lista2 = (List<Integer>)HibernateSession.createCriteria(AlumnoAsistencia.class)
				.setProjection(Projections.count("id.alumnoId"))
				.add(Restrictions.eq("id.alumnoId", alumnoId))
				.add(Restrictions.ge("id.dia", fechaDesde)) //Mayor o igual
				.add(Restrictions.lt("id.dia", fechaHasta)) //Menor
				.add(Restrictions.eq("asistencia", Dominios.Asistencia.AUSENTE))
				.addOrder(Order.asc("id.alumnoId"))
				.addOrder(Order.asc("id.dia"))
				.list();
			if (lista2.size() > 0){
				inasistencias[1] = (Integer)lista2.get(0);
			}
			
			HibernateSession.getTransaction().rollback();
		}catch (HibernateException e){
			System.out.print(e.toString());
		}
		return inasistencias;
	}
	
}
