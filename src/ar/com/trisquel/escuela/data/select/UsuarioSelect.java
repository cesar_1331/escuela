package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Usuario;
import ar.com.trisquel.escuela.utiles.Dominios;

public class UsuarioSelect {
	/**
	 * Select Base para armar tomar como ejemplo
	 */
	public static List<Usuario> Select(){
		List<Usuario> listUsuario = null;
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<Usuario> lista  = (List<Usuario>)HibernateSession.createCriteria(Usuario.class).list();
			HibernateSession.getTransaction().rollback();
			listUsuario = lista;
		}catch (HibernateException e){
			listUsuario = new ArrayList<Usuario>();
		}
		return listUsuario;
	}
	
	public static List<Usuario> SelectXNombre(String nombre){
		List<Usuario> listUsuario = null;
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<Usuario> lista  = (List<Usuario>)HibernateSession.createCriteria(Usuario.class)
					.add(Restrictions.like("nombre", nombre, MatchMode.ANYWHERE))
					.addOrder(Order.asc("nombre"))
					.list();
			HibernateSession.getTransaction().rollback();
			listUsuario = lista;
		}catch (HibernateException e){
			listUsuario = new ArrayList<Usuario>();
		}
		return listUsuario;
	}
	
	public static List<Usuario> SelectXNombreXEstadoXTipo(String nombre ,int estado ,int tipo){
		List<Usuario> listUsuario = null;
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			Criteria criteria = HibernateSession.createCriteria(Usuario.class)
				.add(Restrictions.like("nombre", nombre, MatchMode.ANYWHERE))
				.addOrder(Order.asc("nombre"));
			if (estado > 0){
				criteria.add(Restrictions.eq("estadoActual", estado));
			}
			if (tipo > 0){
				criteria.add(Restrictions.eq("tipoUsuario", tipo));
			}
			@SuppressWarnings("unchecked")
			List<Usuario> lista  = (List<Usuario>)criteria.list();
			HibernateSession.getTransaction().rollback();
			listUsuario = lista;
		}catch (HibernateException e){
			listUsuario = new ArrayList<Usuario>();
		}
		return listUsuario;
	}
	
	public static Usuario LeeUsuarioXAlumno(long alumnoId){
		Usuario usuario = null;
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<Usuario> lista  = (List<Usuario>)HibernateSession.createCriteria(Usuario.class)
					.add(Restrictions.eq("alumnoId", alumnoId))
					.add(Restrictions.eq("tipoUsuario", Dominios.TipoUsuario.ALUMNO))
					.addOrder(Order.asc("alumnoId"))
					.list();
			HibernateSession.getTransaction().rollback();
			if (lista.size() > 0){
				usuario = lista.get(0);
			}
		}catch (HibernateException e){
		}
		return usuario;
	}
	
	public static Usuario LeeUsuarioXProfesor(long profesorId){
		Usuario usuario = null;
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<Usuario> lista  = (List<Usuario>)HibernateSession.createCriteria(Usuario.class)
					.add(Restrictions.eq("profesorId", profesorId))
					.add(Restrictions.eq("tipoUsuario", Dominios.TipoUsuario.PROFESOR))
					.addOrder(Order.asc("profesorId"))
					.list();
			HibernateSession.getTransaction().rollback();
			if (lista.size() > 0){
				usuario = lista.get(0);
			}
		}catch (HibernateException e){
		}
		return usuario;
	}
}

