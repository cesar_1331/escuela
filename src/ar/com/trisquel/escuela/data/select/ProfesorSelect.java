package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Profesor;

public class ProfesorSelect {
	
	public static List<Profesor> SelectXNombreApellidoXActivo(String nombre ,boolean soloPreceptores ,boolean activo){
		List<Profesor> listProfesor = new ArrayList<Profesor>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			Criteria criteria = HibernateSession.createCriteria(Profesor.class)
					.add(Restrictions.eq("activo", activo))
					.add(Restrictions.or(
							Restrictions.like("nombre", nombre, MatchMode.ANYWHERE),
							Restrictions.like("apellido", nombre, MatchMode.ANYWHERE)))
					.addOrder(Order.asc("apellido"))
					.addOrder(Order.asc("nombre"));
			if (soloPreceptores){
				criteria.add(Restrictions.eq("esPreceptor", true));
			}
			@SuppressWarnings("unchecked")
			List<Profesor> lista  = (List<Profesor>)criteria.list();
			HibernateSession.getTransaction().rollback();
			listProfesor = lista;
		}catch (HibernateException e){
		}
		return listProfesor;
	}
}
