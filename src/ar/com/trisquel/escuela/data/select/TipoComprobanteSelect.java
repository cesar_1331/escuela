package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.TipoComprobante;

public class TipoComprobanteSelect {
	
	public static List<TipoComprobante> SelectXNombreXActivo(String nombre,boolean activo){
		List<TipoComprobante> listTipoComprobante = new ArrayList<TipoComprobante>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<TipoComprobante> lista  = (List<TipoComprobante>)HibernateSession.createCriteria(TipoComprobante.class)
				.add(Restrictions.like("nombre", nombre, MatchMode.ANYWHERE))
				.add(Restrictions.eq("activo", activo))
				.addOrder(Order.asc("nombre"))
				.list();
			HibernateSession.getTransaction().rollback();
			listTipoComprobante = lista;
		}catch (HibernateException e){
		}
		return listTipoComprobante;
	}
}
