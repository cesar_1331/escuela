package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Mensajes;

public class MensajesSelect {

	public static List<Mensajes> SelectXNombre(String nombre ,boolean leido){
		List<Mensajes> listUsuarioMsg = null;
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<Mensajes> lista  = (List<Mensajes>)HibernateSession.createCriteria(Mensajes.class)
					.add(Restrictions.like("nombre", nombre ,MatchMode.ANYWHERE))
					.add(Restrictions.eq("leido", leido))
					.addOrder(Order.desc("nombre"))
					.addOrder(Order.desc("leido"))
					.addOrder(Order.desc("fechaHora"))
					.list();
			HibernateSession.getTransaction().rollback();
			listUsuarioMsg = lista;
		}catch (HibernateException e){
			listUsuarioMsg = new ArrayList<Mensajes>();
		}
		return listUsuarioMsg;
	}
}
