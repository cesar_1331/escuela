package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Comprobante;

public class ComprobanteSelect {
	
	public static List<Comprobante> SelectXPeriodoXTipoXNumeroXPersona(Date fechaDesde ,Date fechaHasta ,int tipoId ,long numero ,long personaId ,boolean anulado){
		List<Comprobante> listComprobante = new ArrayList<Comprobante>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			Criteria criteria = HibernateSession.createCriteria(Comprobante.class)
					.add(Restrictions.ge("fecha", fechaDesde)) //Mayor o igual
					.add(Restrictions.lt("fecha", fechaHasta)) //Menor
					.add(Restrictions.eq("anulado", anulado))
					.addOrder(Order.asc("fecha"))
					.addOrder(Order.asc("anulado"));
					
			if (tipoId > 0){
				criteria.add(Restrictions.eq("tipoComprobanteId", tipoId));
			}
			if (numero > 0){
				criteria.add(Restrictions.eq("numero", numero));
			}
			if (personaId > 0){
				criteria.add(Restrictions.eq("tipoComprobanteId", tipoId));
			}
			@SuppressWarnings("unchecked")
			List<Comprobante> lista  = (List<Comprobante>)criteria.list();
			HibernateSession.getTransaction().rollback();
			listComprobante = lista;
		}catch (HibernateException e){
		}
		return listComprobante;
	}
	
	public static List<Comprobante> SelectXPeriodo(Date fechaDesde ,Date fechaHasta){
		List<Comprobante> listComprobante = new ArrayList<Comprobante>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			Criteria criteria = HibernateSession.createCriteria(Comprobante.class)
					.add(Restrictions.lt("fecha", fechaHasta)) //Menor
					.add(Restrictions.eq("anulado", false))
					.addOrder(Order.asc("fecha"))
					.addOrder(Order.asc("anulado"));
			if (fechaDesde != null){
				criteria.add(Restrictions.ge("fecha", fechaDesde)); //Mayor o igual
			}
			@SuppressWarnings("unchecked")
			List<Comprobante> lista  = (List<Comprobante>)criteria.list();
			HibernateSession.getTransaction().rollback();
			listComprobante = lista;
		}catch (HibernateException e){
		}
		return listComprobante;
	}
}
