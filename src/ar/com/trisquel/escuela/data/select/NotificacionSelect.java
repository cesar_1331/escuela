package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Notificacion;

public class NotificacionSelect {
	
	public static List<Notificacion> SelectXPeriodoXTituloXCanceladoXSoloNoLeido(Date fechaDesde ,Date fechaHasta, String titulo ,boolean cancelada ,boolean soloNoLeidas){
		List<Notificacion> listNotificacion = new ArrayList<Notificacion>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			Criteria criteria = HibernateSession.createCriteria(Notificacion.class)
					.add(Restrictions.like("titulo", titulo ,MatchMode.ANYWHERE))
					.add(Restrictions.eq("cancelada", cancelada))
					.add(Restrictions.ge("fechaAlta", fechaDesde)) //Mayor o igual
					.add(Restrictions.lt("fechaAlta", fechaHasta)) //Menor
					.addOrder(Order.asc("fechaAlta"))
					.addOrder(Order.asc("prioridad"));
					
			if (soloNoLeidas){
				criteria.add(Restrictions.eq("leido", false));
			}
			@SuppressWarnings("unchecked")
			List<Notificacion> lista  = criteria.list();
			HibernateSession.getTransaction().rollback();
			listNotificacion = lista;
		}catch (HibernateException e){
		}
		return listNotificacion;
	}
}
