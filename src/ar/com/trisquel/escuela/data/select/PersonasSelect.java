package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Personas;

public class PersonasSelect {
	
	public static List<Personas> SelectXNombreXClienteXProveedorXActivo(String nombre ,boolean soloCliente ,boolean soloProveedor ,boolean activo){
		List<Personas> listPersonas = new ArrayList<Personas>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			Criteria criteria = HibernateSession.createCriteria(Personas.class)
				.add(Restrictions.like("nombre", nombre, MatchMode.ANYWHERE))
				.add(Restrictions.eq("activo", activo))
				.addOrder(Order.asc("nombre"));
			if (soloCliente){
				criteria.add(Restrictions.eq("esCliente", true));
			}
			if (soloProveedor){
				criteria.add(Restrictions.eq("esProveedor", true));
			}
			@SuppressWarnings("unchecked")
			List<Personas> lista  = (List<Personas>)criteria.list();
			HibernateSession.getTransaction().rollback();
			listPersonas = lista;
		}catch (HibernateException e){
			System.out.println(e.toString());
		}
		return listPersonas;
	}
	
}
