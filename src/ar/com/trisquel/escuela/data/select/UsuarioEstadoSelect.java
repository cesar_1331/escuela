package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.UsuarioEstado;

public class UsuarioEstadoSelect {

	public static List<UsuarioEstado> Select(){
		List<UsuarioEstado> listUsuarioEstado = null;
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<UsuarioEstado> lista  = (List<UsuarioEstado>)HibernateSession.createCriteria(UsuarioEstado.class).list();
			HibernateSession.getTransaction().rollback();
			listUsuarioEstado = lista;
		}catch (HibernateException e){
			listUsuarioEstado = new ArrayList<UsuarioEstado>();
		}
		return listUsuarioEstado;
	}
	
	public static long MaxLinea(String usuario){
		long Linea = 0;
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			@SuppressWarnings("unchecked")
			List<UsuarioEstado> lista = HibernateSession.createCriteria(UsuarioEstado.class)
					.setProjection(Projections.max("id.linea"))
					.add(Restrictions.eq("id.usuarioId", usuario)).list();
			
			HibernateSession.getTransaction().rollback();
			Object ObjectLinea = lista.get(0);
			if (ObjectLinea != null){
				Linea = Long.parseLong(String.valueOf(ObjectLinea)) + 1;
			} else{
				Linea = 1;
			}
		}catch (HibernateException e){
			Linea = 1;
		}
		return Linea;
	}
}
