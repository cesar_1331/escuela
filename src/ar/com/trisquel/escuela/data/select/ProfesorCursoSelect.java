package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.reglasdenegocio.CursoRN;
import ar.com.trisquel.escuela.data.tablas.Curso;
import ar.com.trisquel.escuela.data.tablas.ProfesorCurso;

public class ProfesorCursoSelect {
	
	public static List<ProfesorCurso> SelectXProfesor(long profesorId){
		List<ProfesorCurso> listProfesorCurso = new ArrayList<ProfesorCurso>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<ProfesorCurso> lista  = (List<ProfesorCurso>)HibernateSession.createCriteria(ProfesorCurso.class)
					.add(Restrictions.eq("id.profesorId", profesorId))
					.list();
			HibernateSession.getTransaction().rollback();
			listProfesorCurso = lista;
		}catch (HibernateException e){
		}
		return listProfesorCurso;
	}
	
	public static List<Curso> SelectCursoXProfesor(long profesorId){
		List<Curso> listCurso = new ArrayList<Curso>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<ProfesorCurso> lista  = (List<ProfesorCurso>)HibernateSession.createCriteria(ProfesorCurso.class)
					.add(Restrictions.eq("id.profesorId", profesorId))
					.list();
			HibernateSession.getTransaction().rollback();
			for(ProfesorCurso profesorCurso : lista){
				listCurso.add(CursoRN.Read(profesorCurso.getId().getCursoId()));
			}
		}catch (HibernateException e){
		}
		return listCurso;
	}
}
