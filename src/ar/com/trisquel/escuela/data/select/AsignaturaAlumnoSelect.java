package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.reglasdenegocio.AsignaturaRN;
import ar.com.trisquel.escuela.data.tablas.Asignatura;
import ar.com.trisquel.escuela.data.tablas.AsignaturaAlumno;

public class AsignaturaAlumnoSelect {
	
	public static List<AsignaturaAlumno> LeeAsignaturasXAlumno(long alumnoId ,boolean soloCursando){
		List<AsignaturaAlumno> listAsignaturaAlumno = new ArrayList<AsignaturaAlumno>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			Criteria criteria = HibernateSession.createCriteria(AsignaturaAlumno.class)
					.add(Restrictions.eq("id.alumnoId", alumnoId))
					.addOrder(Order.asc("id.alumnoId"))
					.addOrder(Order.asc("orden"));
			if (soloCursando){
				criteria.add(Restrictions.eq("cursando", true));
			}
			@SuppressWarnings("unchecked")
			List<AsignaturaAlumno> lista = (List<AsignaturaAlumno>)criteria.list();
			HibernateSession.getTransaction().rollback();
			listAsignaturaAlumno = lista;
		}catch (HibernateException e){
		}
		return listAsignaturaAlumno;
	}
	
	public static List<AsignaturaAlumno> LeeAlumnosXAsignatura(long asignaturaId ,boolean soloCursando){
		List<AsignaturaAlumno> listAsignaturaAlumno = new ArrayList<AsignaturaAlumno>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			Criteria criteria = HibernateSession.createCriteria(AsignaturaAlumno.class)
					.add(Restrictions.eq("id.asignaturaId", asignaturaId))
					.addOrder(Order.asc("id.asignaturaId"));
			if (soloCursando){
				criteria.add(Restrictions.eq("cursando", true));
			}
			@SuppressWarnings("unchecked")
			List<AsignaturaAlumno> lista = (List<AsignaturaAlumno>)criteria.list();
			HibernateSession.getTransaction().rollback();
			listAsignaturaAlumno = lista;
		}catch (HibernateException e){
		}
		return listAsignaturaAlumno;
	}
	
	public static List<AsignaturaAlumno> LeeAsignaturasXAlumnoXCurso(long alumnaId ,long cursoId ,boolean soloCursando)
	{
		List<AsignaturaAlumno> listAsignaturaAlumno = new ArrayList<AsignaturaAlumno>();
		for (AsignaturaAlumno asignaturaAlumno : AsignaturaAlumnoSelect.LeeAsignaturasXAlumno(alumnaId ,soloCursando)){
			Asignatura asignatura = AsignaturaRN.Read(asignaturaAlumno.getId().getAsignaturaId());
			if (asignatura.getCursoId()== cursoId)
				listAsignaturaAlumno.add(asignaturaAlumno);
		}
		return listAsignaturaAlumno;
	}
}
