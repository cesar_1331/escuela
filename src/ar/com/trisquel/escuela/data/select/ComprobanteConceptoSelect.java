package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.ComprobanteConcepto;

public class ComprobanteConceptoSelect {
	
	public static double LeeTotal(long id){
		double total = 0;
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<ComprobanteConcepto> lista  = (List<ComprobanteConcepto>)HibernateSession.createCriteria(ComprobanteConcepto.class)
				.add(Restrictions.eq("id.comprobanteId", id))
				.addOrder(Order.asc("id.comprobanteId"))
				.list();
			HibernateSession.getTransaction().rollback();
			for (ComprobanteConcepto comprobanteConcepto : lista){
				total = total + comprobanteConcepto.getImporte();
			}
		}catch (HibernateException e){
		}
		return total;
	}
	
	public static long LeeMaxLinea(long id){
		long linea = 1;
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<Long> lista  = (List<Long>)HibernateSession.createCriteria(ComprobanteConcepto.class)
				.setProjection(Projections.max("id.linea"))
				.add(Restrictions.eq("id.comprobanteId", id))
				.addOrder(Order.asc("id.comprobanteId"))
				.list();
			HibernateSession.getTransaction().rollback();
			Object ObjectLinea = lista.get(0);
			if (ObjectLinea != null){
				linea = Long.parseLong(String.valueOf(ObjectLinea)) + 1;
			} else{
				linea = 1;
			}
		}catch (HibernateException e){
		}
		return linea;
	}
	
	public static List<ComprobanteConcepto> LeeConceptos(long id){
		List<ComprobanteConcepto> listComprobanteConcepto = new ArrayList<ComprobanteConcepto>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<ComprobanteConcepto> lista  = (List<ComprobanteConcepto>)HibernateSession.createCriteria(ComprobanteConcepto.class)
				.add(Restrictions.eq("id.comprobanteId", id))
				.addOrder(Order.asc("id.comprobanteId"))
				.list();
			HibernateSession.getTransaction().rollback();
			listComprobanteConcepto = lista;
		}catch (HibernateException e){
		}
		return listComprobanteConcepto;
	}
}
