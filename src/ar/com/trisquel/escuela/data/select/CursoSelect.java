package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Curso;

public class CursoSelect {
	public static List<Curso> SelectXGradoXTurno(int grado ,String turno ,boolean activo){
		List<Curso> listCurso = new ArrayList<Curso>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			Criteria criteria = HibernateSession.createCriteria(Curso.class)
					.add(Restrictions.eq("activo", activo))
					.addOrder(Order.asc("grado"));
			if (grado > 0){
				criteria.add(Restrictions.eq("grado", grado));
			}
			if (!turno.isEmpty()){
				criteria.add(Restrictions.eq("turno", turno));
			}
			@SuppressWarnings("unchecked")
			List<Curso> lista = (List<Curso>)criteria.list();
			HibernateSession.getTransaction().rollback();
			listCurso = lista;
		}catch (HibernateException e){
		}
		return listCurso;
	}
}
