package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Planilla;

public class PlanillaSelect {
	
	public static List<Planilla> SelectXAnioXMateriaXcursoIdXPeriodo(int anio ,long materiaId ,long cursoId ,int periodo ,boolean soloNotaFinal){
		List<Planilla> listPlanilla = new ArrayList<Planilla>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			Criteria criteria = HibernateSession.createCriteria(Planilla.class)
				.add(Restrictions.eq("anio", anio))		
				.add(Restrictions.eq("materiaId", materiaId))
				.add(Restrictions.eq("cursoId", cursoId))
				.addOrder(Order.asc("anio"))
				.addOrder(Order.asc("cursoId"))
				.addOrder(Order.asc("periodo"))
				.addOrder(Order.asc("esNotaFinal"));
			if (periodo > 0){
				criteria.add(Restrictions.eq("periodo", periodo));
			}
			if (soloNotaFinal){
				criteria.add(Restrictions.eq("esNotaFinal" ,true));
			}
			@SuppressWarnings("unchecked")
			List<Planilla> lista = (List<Planilla>)criteria.list();
			HibernateSession.getTransaction().rollback();
			listPlanilla = lista;
		}catch (HibernateException e){
		}
		return listPlanilla;
	}
	
	public static List<Planilla> SelectParaAlumnoXAnioXMateriaXcursoIdXPeriodo(int anio ,long materiaId ,long cursoId ,int periodo ,boolean soloNotaFinal ,boolean SoloMuestranAlumno){
		List<Planilla> listPlanilla = new ArrayList<Planilla>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			Criteria criteria = HibernateSession.createCriteria(Planilla.class)
				.add(Restrictions.eq("anio", anio))		
				.add(Restrictions.eq("materiaId", materiaId))
				.add(Restrictions.eq("cursoId", cursoId))
				.addOrder(Order.asc("anio"))
				.addOrder(Order.asc("cursoId"))
				.addOrder(Order.asc("periodo"))
				.addOrder(Order.asc("esNotaFinal"));
			if (SoloMuestranAlumno){
				criteria.add(Restrictions.eq("mostrarAlAlumno", true));
			}
			if (periodo > 0){
				criteria.add(Restrictions.eq("periodo", periodo));
			}
			if (soloNotaFinal){
				criteria.add(Restrictions.eq("esNotaFinal" ,true));
			}
			@SuppressWarnings("unchecked")
			List<Planilla> lista = (List<Planilla>)criteria.list();
			HibernateSession.getTransaction().rollback();
			listPlanilla = lista;
		}catch (HibernateException e){
			System.out.println(e.toString());
		}
		return listPlanilla;
	}
	
	public static List<Planilla> SelectXAnioXcurso(int anio ,long cursoId){
		List<Planilla> listPlanilla = new ArrayList<Planilla>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			Criteria criteria = HibernateSession.createCriteria(Planilla.class)
				.add(Restrictions.eq("anio", anio))
				.add(Restrictions.eq("cursoId", cursoId))
				.addOrder(Order.asc("anio"))
				.addOrder(Order.asc("cursoId"))
				.addOrder(Order.asc("periodo"))
				.addOrder(Order.asc("esNotaFinal"));
			@SuppressWarnings("unchecked")
			List<Planilla> lista = (List<Planilla>)criteria.list();
			HibernateSession.getTransaction().rollback();
			listPlanilla = lista;
		}catch (HibernateException e){
		}
		return listPlanilla;
	}
	
}
