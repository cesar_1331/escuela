package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.reglasdenegocio.MateriaRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.ProfesorRN;
import ar.com.trisquel.escuela.data.tablas.Materia;
import ar.com.trisquel.escuela.data.tablas.Profesor;
import ar.com.trisquel.escuela.data.tablas.ProfesorMateria;

public class ProfesorMateriaSelect {
	
	public static List<ProfesorMateria> SelectXProfesor(long profesorId){
		List<ProfesorMateria> listProfesorMateria = new ArrayList<ProfesorMateria>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<ProfesorMateria> lista  = (List<ProfesorMateria>)HibernateSession.createCriteria(ProfesorMateria.class)
					.add(Restrictions.eq("id.profesorId", profesorId))
					.list();
			HibernateSession.getTransaction().rollback();
			listProfesorMateria = lista;
		}catch (HibernateException e){
		}
		return listProfesorMateria;
	}
	
	public static List<Materia> SelectMateriasXProfesor(long profesorId){
		List<Materia> listMateria = new ArrayList<Materia>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<ProfesorMateria> lista  = (List<ProfesorMateria>)HibernateSession.createCriteria(ProfesorMateria.class)
					.add(Restrictions.eq("id.profesorId", profesorId))
					.list();
			HibernateSession.getTransaction().rollback();
			for(ProfesorMateria profesorMateria : lista){
				listMateria.add(MateriaRN.Read(profesorMateria.getId().getMateriaId()));
			}
		}catch (HibernateException e){
		}
		return listMateria;
	}
	
	public static List<Profesor> SelectProfesorXMateria(long materiaId){
		List<Profesor> listProfesor = new ArrayList<Profesor>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<ProfesorMateria> lista  = (List<ProfesorMateria>)HibernateSession.createCriteria(ProfesorMateria.class)
					.add(Restrictions.eq("id.materiaId", materiaId))
					.list();
			HibernateSession.getTransaction().rollback();
			for(ProfesorMateria profesorMateria : lista){
				listProfesor.add(ProfesorRN.Read(profesorMateria.getId().getProfesorId()));
			}
		}catch (HibernateException e){
		}
		return listProfesor;
	}
}
