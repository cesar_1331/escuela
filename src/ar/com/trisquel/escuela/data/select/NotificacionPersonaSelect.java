package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.NotificacionPersona;

public class NotificacionPersonaSelect {
	
	public static List<NotificacionPersona> SelectXPersonaXPeriodoXSoloNoLeido(String tipo ,long personaId ,Date fechaDesde ,Date fechaHasta,boolean soloNoLeidas){
		List<NotificacionPersona> listNotificacionPersona = new ArrayList<NotificacionPersona>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			Criteria criteria = HibernateSession.createCriteria(NotificacionPersona.class)
					.add(Restrictions.eq("id.tipo", tipo ))
					.add(Restrictions.eq("id.personaId", personaId))
					.add(Restrictions.eq("cancelado", false))
					.add(Restrictions.ge("fechaAlta", fechaDesde)) //Mayor o igual
					.add(Restrictions.lt("fechaAlta", fechaHasta)) //Menor
					.addOrder(Order.asc("fechaAlta"))
					.addOrder(Order.asc("prioridad"));				
			if (soloNoLeidas){
				criteria.add(Restrictions.eq("leido", false));
			}
			@SuppressWarnings("unchecked")
			List<NotificacionPersona> lista  = criteria.list();
			HibernateSession.getTransaction().rollback();
			listNotificacionPersona = lista;
		}catch (HibernateException e){
		}
		return listNotificacionPersona;
	}
	
	public static List<NotificacionPersona> SelectXNotificacion(long notificacionId ){
		List<NotificacionPersona> listNotificacionPersona = new ArrayList<NotificacionPersona>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<NotificacionPersona> lista  = HibernateSession.createCriteria(NotificacionPersona.class)
					.add(Restrictions.eq("id.notificacionId", notificacionId))
					.add(Restrictions.eq("cancelado", false))
					.addOrder(Order.asc("id.notificacionId"))
					.list();
			HibernateSession.getTransaction().rollback();
			listNotificacionPersona = lista;
		}catch (HibernateException e){
		}
		return listNotificacionPersona;
	}
}
