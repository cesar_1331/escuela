package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.AlumnoPariente;

public class AlumnoParienteSelect {

	public static List<AlumnoPariente> SelectXAlumno(long alumnoId){
		List<AlumnoPariente> listAlumnoPariente = new ArrayList<AlumnoPariente>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<AlumnoPariente> lista  = (List<AlumnoPariente>)HibernateSession.createCriteria(AlumnoPariente.class)
				.add(Restrictions.eq("id.alumnoId", alumnoId))
				.addOrder(Order.asc("id.alumnoId"))
				.list();
			HibernateSession.getTransaction().rollback();
			listAlumnoPariente = lista;
		}catch (HibernateException e){
			System.out.print(e.toString());
		}
		return listAlumnoPariente;
	}
	
	public static long MaxLinea(long alumnoId){
		long Linea = 0;
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			@SuppressWarnings("unchecked")
			List<Object> lista = HibernateSession.createCriteria(AlumnoPariente.class)
					.setProjection(Projections.max("id.linea"))
					.add(Restrictions.eq("id.alumnoId", alumnoId))
					.addOrder(Order.asc("id.alumnoId"))
					.list();
			HibernateSession.getTransaction().rollback();
			Object ObjectLinea = lista.get(0);
			if (ObjectLinea != null){
				Linea = Long.parseLong(String.valueOf(ObjectLinea)) + 1;
			} else{
				Linea = 1;
			}
		}catch (HibernateException e){
			Linea = 1;
		}
		return Linea;
	}
}
