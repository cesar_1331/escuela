package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.ErrorLog;

public class ErrorLogSelect {
	
	public static List<ErrorLog> SelectFiltroVisto(boolean mostrarVistos ,Date fechaDesde ,Date fechaHasta){
		List<ErrorLog> listErrorLog = new ArrayList<ErrorLog>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<ErrorLog> lista  = (List<ErrorLog>)HibernateSession.createCriteria(ErrorLog.class)
					.add(Restrictions.ge("fechaHora", fechaDesde)) //Mayor o igual
					.add(Restrictions.lt("fechaHora", fechaHasta)) //Menor
					.add(Restrictions.eq("visto", mostrarVistos))
					.addOrder(Order.asc("fechaHora"))
					.addOrder(Order.asc("visto"))
					.list();
			HibernateSession.getTransaction().rollback();
			listErrorLog = lista;
		}catch (HibernateException e){
		}
		return listErrorLog;
	}
}
