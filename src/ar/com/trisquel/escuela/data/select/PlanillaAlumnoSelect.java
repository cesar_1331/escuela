package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.reglasdenegocio.PlanillaAlumnoRN;
import ar.com.trisquel.escuela.data.tablas.Planilla;
import ar.com.trisquel.escuela.data.tablas.PlanillaAlumno;
import ar.com.trisquel.escuela.data.tablas.identificadores.PlanillaAlumnoId;

public class PlanillaAlumnoSelect {
	
	public static PlanillaAlumno LeeXPlanillaXAlumno(long planillaId ,long alumnoId ){
		PlanillaAlumno planillaNotas = null;
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			Criteria criteria = HibernateSession.createCriteria(PlanillaAlumno.class)
				.add(Restrictions.eq("id.planillaId", planillaId))		
				.add(Restrictions.eq("id.alumnoId", alumnoId));
			@SuppressWarnings("unchecked")
			List<PlanillaAlumno> lista = (List<PlanillaAlumno>)criteria.list();
			HibernateSession.getTransaction().rollback();
			if (lista.size() > 0){
				planillaNotas = lista.get(0);
			}
		}catch (HibernateException e){
		}
		return planillaNotas;
	}
	
	public static List<PlanillaAlumno> SelectXPlanilla(long planillaId ){
		List<PlanillaAlumno> listPlanillaAlumno = new ArrayList<PlanillaAlumno>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<PlanillaAlumno> lista = (List<PlanillaAlumno>)HibernateSession.createCriteria(PlanillaAlumno.class)
				.add(Restrictions.eq("id.planillaId", planillaId))
				.list();
			HibernateSession.getTransaction().rollback();
			listPlanillaAlumno = lista;
		}catch (HibernateException e){
		}
		return listPlanillaAlumno;
	}
	
	public static PlanillaAlumno LeeXAnioXMateriaXcursoIdXPeriodo(long alumnoId ,int anio ,long materiaId ,long cursoId ,int periodo){
		PlanillaAlumno planillaAlumno = null;
		List<Planilla> listPlanilla = PlanillaSelect.SelectXAnioXMateriaXcursoIdXPeriodo(anio, materiaId, cursoId, periodo, true);
		for (Planilla planilla : listPlanilla){
			planillaAlumno = PlanillaAlumnoRN.Read(new PlanillaAlumnoId(planilla.getId(), alumnoId));
		}
		return planillaAlumno;
	}
}
