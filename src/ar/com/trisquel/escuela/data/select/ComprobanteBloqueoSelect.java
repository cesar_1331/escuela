package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.ComprobanteBloqueo;

public class ComprobanteBloqueoSelect {
	
	public static List<ComprobanteBloqueo> SelectXAnioXMes(int anio){
		List<ComprobanteBloqueo> listComprobanteBloqueo = new ArrayList<ComprobanteBloqueo>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			@SuppressWarnings("unchecked")
			List<ComprobanteBloqueo> lista  = (List<ComprobanteBloqueo>)HibernateSession.createCriteria(ComprobanteBloqueo.class)
				.add(Restrictions.eq("id.anio", anio))
				.addOrder(Order.asc("id.anio"))
				.addOrder(Order.asc("id.mes"))
				.list();
			HibernateSession.getTransaction().rollback();
			listComprobanteBloqueo = lista;
		}catch (HibernateException e){
			System.out.println(e.toString());
		}
		return listComprobanteBloqueo;
	}
}
