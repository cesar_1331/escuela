package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Asignatura;

public class AsignaturaSelect {
	public static List<Asignatura> SelectXAnioXCursoXProfesorXMateria(int anio ,long cursoId ,long profesorId ,long materiaId){
		List<Asignatura> listAsignatura = new ArrayList<Asignatura>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			Criteria criteria = HibernateSession.createCriteria(Asignatura.class)
					.add(Restrictions.eq("anio", anio))
					.addOrder(Order.asc("anio"));
			if (cursoId > 0){
				criteria.add(Restrictions.eq("cursoId", cursoId));
			}
			if (profesorId > 0){
				criteria.add(Restrictions.eq("profesorId", profesorId));
			}
			if (materiaId > 0){
				criteria.add(Restrictions.eq("materiaId", materiaId));
			}
			@SuppressWarnings("unchecked")
			List<Asignatura> lista = (List<Asignatura>)criteria.list();
			HibernateSession.getTransaction().rollback();
			listAsignatura = lista;
		}catch (HibernateException e){
		}
		return listAsignatura;
	}
	
	public static Asignatura LeeAsignaturaXAnioXCursoXMateria(int anio ,long cursoId ,long materiaId){
		Asignatura asignatura = null;
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			@SuppressWarnings("unchecked")
			List<Asignatura> lista = (List<Asignatura>)HibernateSession.createCriteria(Asignatura.class)
					.add(Restrictions.eq("anio", anio))
					.add(Restrictions.eq("cursoId", cursoId))
					.add(Restrictions.eq("materiaId", materiaId))
					.addOrder(Order.asc("anio"))
					.addOrder(Order.asc("cursoId"))
					.addOrder(Order.asc("materiaId"))
					.list();
			HibernateSession.getTransaction().rollback();
			if (lista.size() > 0){
				asignatura = lista.get(0);
			}
		}catch (HibernateException e){
		}
		return asignatura;
	}
	
	public static List<Asignatura> LeeAsignaturaXAnioXCurso(int anio ,long cursoId ){
		List<Asignatura> listAsignatura = new ArrayList<Asignatura>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			@SuppressWarnings("unchecked")
			List<Asignatura> lista = (List<Asignatura>)HibernateSession.createCriteria(Asignatura.class)
					.add(Restrictions.eq("anio", anio))
					.add(Restrictions.eq("cursoId", cursoId))
					.addOrder(Order.asc("anio"))
					.addOrder(Order.asc("cursoId"))
					.list();
			HibernateSession.getTransaction().rollback();
			listAsignatura = lista;
		
		}catch (HibernateException e){
		}
		return listAsignatura;
	}
	
	public static List<Asignatura> LeeAsignaturaXAnio(int anio){
		List<Asignatura> listAsignatura = new ArrayList<Asignatura>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();
			@SuppressWarnings("unchecked")
			List<Asignatura> lista = (List<Asignatura>)HibernateSession.createCriteria(Asignatura.class)
					.add(Restrictions.eq("anio", anio))
					.list();
			HibernateSession.getTransaction().rollback();
			listAsignatura = lista;
		
		}catch (HibernateException e){
		}
		return listAsignatura;
	}
}
