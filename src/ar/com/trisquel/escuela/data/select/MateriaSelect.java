package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Materia;

public class MateriaSelect {
	
	public static List<Materia> Select(boolean activo){
		List<Materia> listMateria = new ArrayList<Materia>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<Materia> lista  = (List<Materia>)HibernateSession.createCriteria(Materia.class)
					.add(Restrictions.eq("activa", activo))
					.addOrder(Order.asc("nombre"))
					.list();
			HibernateSession.getTransaction().rollback();
			listMateria = lista;
		}catch (HibernateException e){
		}
		return listMateria;
	}
	
	public static List<Materia> SelectXNombre(String nombre ,boolean activo){
		List<Materia> listMateria = new ArrayList<Materia>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<Materia> lista  = (List<Materia>)HibernateSession.createCriteria(Materia.class)
				.add(Restrictions.like("nombre", nombre, MatchMode.ANYWHERE))
				.add(Restrictions.eq("activa", activo))
				.addOrder(Order.asc("nombre"))
				.list();
			HibernateSession.getTransaction().rollback();
			listMateria = lista;
		}catch (HibernateException e){
		}
		return listMateria;
	}
	
	public static boolean ControlaExiste(String nombre){
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<Materia> lista  = (List<Materia>)HibernateSession.createCriteria(Materia.class)
				.add(Restrictions.like("nombre", nombre, MatchMode.EXACT))
				.addOrder(Order.asc("nombre"))
				.list();
			HibernateSession.getTransaction().rollback();
			if (lista.size() > 0)
				return true;
		}catch (HibernateException e){
		}
		return false;
	}
}
