package ar.com.trisquel.escuela.data.select;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.trisquel.escuela.data.HibernateUtil;
import ar.com.trisquel.escuela.data.tablas.Concepto;

public class ConceptoSelect {
	public static List<Concepto> SelectXNombreXActivo(String nombre,boolean activo){
		List<Concepto> listConcepto = new ArrayList<Concepto>();
		try{
			Session HibernateSession = HibernateUtil.getSession();
			HibernateSession.beginTransaction();
			HibernateSession.clear();		
			@SuppressWarnings("unchecked")
			List<Concepto> lista  = (List<Concepto>)HibernateSession.createCriteria(Concepto.class)
				.add(Restrictions.like("nombre", nombre, MatchMode.ANYWHERE))
				.add(Restrictions.eq("activo", activo))
				.addOrder(Order.asc("nombre"))
				.list();
			HibernateSession.getTransaction().rollback();
			listConcepto = lista;
		}catch (HibernateException e){
		}
		return listConcepto;
	}
	
}
