package ar.com.trisquel.escuela.data;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import ar.com.trisquel.escuela.utiles.Recursos;

public class HibernateUtil {

	@SuppressWarnings("rawtypes")
	private static final ThreadLocal threadLocal = new ThreadLocal();
	private static final String CONFIG_FILE_LOCATION = "hibernate.cfg.xml";
	private static final String CONFIG_PROTOTIPE_FILE_LOCATION = "hibernate-prototype.cfg.xml";
	private static SessionFactory sessionFactory = buildSessionFactory();
	private static Configuration configuration;
	
	private static SessionFactory buildSessionFactory() {
		configuration = new Configuration();
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			String configFile = CONFIG_FILE_LOCATION;
			if (!Recursos.Parametros.isModoProduction()){
				configFile = CONFIG_PROTOTIPE_FILE_LOCATION;
			}
			configuration.configure(configFile);
			return configuration.buildSessionFactory();
		}
		catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	/**
	* Returns the ThreadLocal Session instance. Lazy initialize
	* the SessionFactory if needed.
	*
	* @return Session
	* @throws HibernateException
	*/
	@SuppressWarnings("unchecked")
	public static Session getSession() throws HibernateException {
		Session session = (Session) threadLocal.get();
		if (session == null || !session.isOpen()) {
			if (sessionFactory == null) {
				rebuildSessionFactory();
			}
			session = (sessionFactory != null) ? sessionFactory.openSession()
					: null;
			threadLocal.set(session);
		}
		return session;
	}

	/**
	* Rebuild hibernate session factory
	*
	*/
	public static void rebuildSessionFactory() {
		try {
			String configFile = CONFIG_FILE_LOCATION;
			if (!Recursos.Parametros.isModoProduction()){
				configFile = CONFIG_PROTOTIPE_FILE_LOCATION;
			}
			configuration.configure(configFile);
			sessionFactory = configuration.buildSessionFactory();
		} catch (Exception e) {
			System.err
			.println("%%%% Error Creating SessionFactory %%%%");
			e.printStackTrace();
		}
	}

	/**
	* Close the single hibernate session instance.
	*
	* @throws HibernateException
	*/
	@SuppressWarnings("unchecked")
	public static void closeSession() throws HibernateException {
		Session session = (Session) threadLocal.get();
		threadLocal.set(null);

		if (session != null) {
			session.close();
		}
	}
	
	
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	public static String getDriver() {
		return new Configuration().configure().getProperty("hibernate.connection.driver_class");
	}
	
	public static String getUrl() {
		return new Configuration().configure().getProperty("hibernate.connection.url");
	}
	
	public static String getPassword() {
		return new Configuration().configure().getProperty("hibernate.connection.password");
	}
	
	public static String getUser() {
		return new Configuration().configure().getProperty("hibernate.connection.username");
	}
	
}

