package ar.com.trisquel.escuela.data.tablas;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@Entity
@Table(name="ErrorLog")
public class ErrorLog {
	@GeneratedValue
	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id Error")
	private long id;
	@Column(name="Causa")
	@FieldAnnotations(Descripcion="Causa")
	private String causa;
	@Column(name="Traza")
	@FieldAnnotations(Descripcion="Traza del error")
	private String traza;
	@Column(name="FechaHora")
	@FieldAnnotations(Descripcion="Fecha de cuando ocurrio el evento")
	private Date fechaHora;
	@Column(name="Visto")
	@FieldAnnotations(Descripcion="Error Visto")
	private boolean visto;
	
	public ErrorLog(){
	}
	public ErrorLog(long Id ,String Causa ,String Traza ,boolean Visto ){
		this.id = Id;
		if (Causa.length() > 254)
			this.causa = Causa.substring(0 ,253);
		else
			this.causa = Causa;
		this.traza = Traza;
		this.visto = Visto; 
		this.fechaHora = new Date(System.currentTimeMillis());
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCausa() {
		return causa;
	}
	public void setCausa(String causa) {
		this.causa = causa;
	}
	public String getTraza() {
		return traza;
	}
	public void setTraza(String traza) {
		this.traza = traza;
	}
	public Date getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}
	public boolean isVisto() {
		return visto;
	}
	public void setVisto(boolean visto) {
		this.visto = visto;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ErrorLog other = (ErrorLog) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
