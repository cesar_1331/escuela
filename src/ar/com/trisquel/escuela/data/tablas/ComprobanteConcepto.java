package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.data.tablas.identificadores.ComprobanteLinea;
import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="COMPROBANTECONCEPTO")
public class ComprobanteConcepto implements Serializable {	
	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id")
	private ComprobanteLinea id;
	@Column(name="conceptoId")
	@FieldAnnotations(Descripcion="Id del concepto")
	private long conceptoId;
	@Column(name="Descripcion")
	@FieldAnnotations(Descripcion="Descripcion")
	private String descripcion;
	@Column(name="importe")
	@FieldAnnotations(Descripcion="importe")
	private double importe;
	public ComprobanteLinea getId() {
		return id;
	}
	public void setId(ComprobanteLinea id) {
		this.id = id;
	}
	public long getConceptoId() {
		return conceptoId;
	}
	public void setConceptoId(long conceptoId) {
		this.conceptoId = conceptoId;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public double getImporte() {
		return importe;
	}
	public void setImporte(double importe) {
		this.importe = importe;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComprobanteConcepto other = (ComprobanteConcepto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
