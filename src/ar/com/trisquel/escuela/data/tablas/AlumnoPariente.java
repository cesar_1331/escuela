package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.data.tablas.identificadores.AlumnoLinea;
import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="ALUMNOPARIENTE")
public class AlumnoPariente implements Serializable {
	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id del alumno con linea")
	private AlumnoLinea id;
	@Column(name="Nombre")
	@FieldAnnotations(Descripcion="Nombre")
	private String nombre;
	@Column(name="Telefono")
	@FieldAnnotations(Descripcion="Telefono")
	private String telefono;
	@Column(name="Observaciones")
	@FieldAnnotations(Descripcion="Observaciones")
	private String observaciones;
	@Column(name="EsTutor")
	@FieldAnnotations(Descripcion="Es el tutor responsable")
	private boolean esTutor;
	public AlumnoLinea getId() {
		return id;
	}
	public void setId(AlumnoLinea id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre.toUpperCase();
	}
	public void setNombre(String nombre) {
		this.nombre = nombre.toUpperCase();
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public boolean getEsTutor() {
		return esTutor;
	}
	public void setEsTutor(boolean esTutor) {
		this.esTutor = esTutor;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlumnoPariente other = (AlumnoPariente) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
