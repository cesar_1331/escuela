package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.data.tablas.identificadores.ProfesorCursoId;
import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="PROFESORCURSO")
public class ProfesorCurso implements Serializable{

	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id del profesor con materia")
	private ProfesorCursoId id;
	@Column(name="Responsable")
	@FieldAnnotations(Descripcion="Responsable")
	private String responsable;
	@Column(name="Fecha de Asociacion")
	@FieldAnnotations(Descripcion="Fecha de asociacion")
	private Date fechaAsociacion;
	public ProfesorCursoId getId() {
		return id;
	}
	public void setId(ProfesorCursoId id) {
		this.id = id;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public Date getFechaAsociacion() {
		return fechaAsociacion;
	}
	public void setFechaAsociacion(Date fechaAsociacion) {
		this.fechaAsociacion = fechaAsociacion;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfesorCurso other = (ProfesorCurso) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
