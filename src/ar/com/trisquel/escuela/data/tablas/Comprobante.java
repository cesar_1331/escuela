package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.data.select.ComprobanteConceptoSelect;
import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="COMPROBANTE")
public class Comprobante implements Serializable {
	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id")
	private long id;
	@Column(name="TipoComprobanteId")
	@FieldAnnotations(Descripcion="Tipo de Comprobante")
	private int tipoComprobanteId;
	@Column(name="tipo")
	@FieldAnnotations(Descripcion="Tipo (factura/comprobante de caja) redundado")
	private String tipo;
	@Column(name="saldo")
	@FieldAnnotations(Descripcion="Saldo deudor/acredor redundado")
	private int saldo;
	@Column(name="operacion")
	@FieldAnnotations(Descripcion="Operacion compra/venta redundado")
	private String operacion;
	@Column(name="anulado")
	@FieldAnnotations(Descripcion="Anulado")
	private boolean anulado;
	@Column(name="AnuladoFecha")
	@FieldAnnotations(Descripcion="Fecha de Anulacion")
	private Date anuladoFecha;
	@Column(name="anuladoResponsable")
	@FieldAnnotations(Descripcion="Responsable de la Anulacion")
	private String anuladoResponsable;
	@Column(name="AltaFecha")
	@FieldAnnotations(Descripcion="Fecha de Alta")
	private Date altaFecha;
	@Column(name="anuladoResponsable")
	@FieldAnnotations(Descripcion="Responsable de Alta")
	private String altaResponsable;
	@Column(name="PersonaId")
	@FieldAnnotations(Descripcion="Id Persona")
	private long personaId;
	@Column(name="fecha")
	@FieldAnnotations(Descripcion="Fecha")
	private Date fecha;
	@Column(name="fechaRegistro")
	@FieldAnnotations(Descripcion="Fecha de Registración")
	private Date fechaRegistro;
	@Column(name="letra")
	@FieldAnnotations(Descripcion="Letra")
	private String letra;
	@Column(name="numero")
	@FieldAnnotations(Descripcion="Numero")
	private long numero;
	@Column(name="FormaPago")
	@FieldAnnotations(Descripcion="Forma de Pago")
	private String formaPago;
	@Column(name="Bloqueado")
	@FieldAnnotations(Descripcion="Bloqueado")
	private boolean bloqueado;
	@Column(name="Observacion")
	@FieldAnnotations(Descripcion="Observacion")
	private String observacion;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getTipoComprobanteId() {
		return tipoComprobanteId;
	}
	public void setTipoComprobanteId(int tipoComprobanteId) {
		this.tipoComprobanteId = tipoComprobanteId;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public int getSaldo() {
		return saldo;
	}
	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public boolean isAnulado() {
		return anulado;
	}
	public void setAnulado(boolean anulado) {
		this.anulado = anulado;
	}
	public Date getAnuladoFecha() {
		return anuladoFecha;
	}
	public void setAnuladoFecha(Date anuladoFecha) {
		this.anuladoFecha = anuladoFecha;
	}
	public String getAnuladoResponsable() {
		return anuladoResponsable;
	}
	public void setAnuladoResponsable(String anuladoResponsable) {
		this.anuladoResponsable = anuladoResponsable;
	}
	public Date getAltaFecha() {
		return altaFecha;
	}
	public void setAltaFecha(Date altaFecha) {
		this.altaFecha = altaFecha;
	}
	public String getAltaResponsable() {
		return altaResponsable;
	}
	public void setAltaResponsable(String altaResponsable) {
		this.altaResponsable = altaResponsable;
	}
	public long getPersonaId() {
		return personaId;
	}
	public void setPersonaId(long personaId) {
		this.personaId = personaId;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public String getLetra() {
		return letra;
	}
	public void setLetra(String letra) {
		this.letra = letra;
	}
	public long getNumero() {
		return numero;
	}
	public void setNumero(long numero) {
		this.numero = numero;
	}
	public String getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	
	public boolean isBloqueado() {
		return bloqueado;
	}
	public void setBloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public double getTotal() {
		return ComprobanteConceptoSelect.LeeTotal(getId());
	}
	
	public List<ComprobanteConcepto> getConceptos(){
		return ComprobanteConceptoSelect.LeeConceptos(getId());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comprobante other = (Comprobante) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
