package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.utiles.FieldAnnotations;
/**
 * Clase para la definicion de los usuarios del sistema
 * @Version 1.0
 */

@SuppressWarnings("serial")
@Entity
@Table(name="Usuario")
public class Usuario implements Serializable{

	@Id
	@Column(name="id",length = 20 ,unique=true)
	@FieldAnnotations(Descripcion="Identificador del usuario")
	private String id;
	@Column(length = 100)
	@FieldAnnotations(Descripcion="Nombre del usuario")
	private String nombre;
	@Column(length = 320)
	@FieldAnnotations(Descripcion="Mail principal del usuario")
	private String mailPrincipal;
	@Column(length = 320)
	@FieldAnnotations(Descripcion="Mail alternativo del usuario")
	private String mailSecundario;
	@Column(length = 255)
	@FieldAnnotations(Descripcion="Clave del usuario")
	private String clave;
	@FieldAnnotations(Descripcion="Estado actual del usuario")
	private int estadoActual;
	@FieldAnnotations(Descripcion="Fecha de inicio del estado actual del usuario")
	private Date fechaIniEstado;
	@FieldAnnotations(Descripcion="Fecha de finalizacion del estado actual del usuario")
	private Date fechaFinEstado;
	@FieldAnnotations(Descripcion="Tipo de usuario(Alumno,Profesor,Administrativo,Administrador)")
	private int tipoUsuario;
	@Column(name="Imagen")
	@FieldAnnotations(Descripcion="Imagen del usuario")
	private Blob imagen;
	@Column(name="Thumbnail")
	@FieldAnnotations(Descripcion="Thumbnail")
	private Blob thumbnail;
	@Column(name="ProfesorId")
	@FieldAnnotations(Descripcion="Id del profesor asociado")
	private long profesorId;
	@Column(name="AlumnoId")
	@FieldAnnotations(Descripcion="Id del alumno asociado")
	private long alumnoId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre.toUpperCase();
	}
	public void setNombre(String nombre) {
		this.nombre = nombre.toUpperCase();
	}
	public String getMailPrincipal() {
		return mailPrincipal;
	}
	public void setMailPrincipal(String mailPrincipal) {
		this.mailPrincipal = mailPrincipal;
	}
	public String getMailSecundario() {
		return mailSecundario;
	}
	public void setMailSecundario(String mailSecundario) {
		this.mailSecundario = mailSecundario;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public int getEstadoActual() {
		return estadoActual;
	}
	public void setEstadoActual(int estadoActual) {
		this.estadoActual = estadoActual;
	}
	public Date getFechaIniEstado() {
		return fechaIniEstado;
	}
	public void setFechaIniEstado(Date fechaIniEstado) {
		this.fechaIniEstado = fechaIniEstado;
	}
	public Date getFechaFinEstado() {
		return fechaFinEstado;
	}
	public void setFechaFinEstado(Date fechaFinEstado) {
		this.fechaFinEstado = fechaFinEstado;
	}
	public int getTipoUsuario() {
		return tipoUsuario;
	}
	public void setTipoUsuario(int tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	public Blob getImagen() {
		return imagen;
	}
	public void setImagen(Blob imagen) {
		this.imagen = imagen;
	}
	public Blob getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(Blob thumbnail) {
		this.thumbnail = thumbnail;
	}
	public long getProfesorId() {
		return profesorId;
	}
	public void setProfesorId(long profesorId) {
		this.profesorId = profesorId;
	}
	public long getAlumnoId() {
		return alumnoId;
	}
	public void setAlumnoId(long alumnoId) {
		this.alumnoId = alumnoId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
	
	
}
