package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="CURSO")
public class Curso implements Serializable {
		@Id
		@Column(name="Id")
		@FieldAnnotations(Descripcion="Id")
		private long id;
		@Column(name="Grado")
		@FieldAnnotations(Descripcion="Grado")
		private int grado;
		@Column(name="Division")
		@FieldAnnotations(Descripcion="division")
		private String division;
		@Column(name="Turno")
		@FieldAnnotations(Descripcion="turno")
		private String turno;
		@Column(name="Activo")
		@FieldAnnotations(Descripcion="Activo")
		private boolean activo;
		@Column(name="CantidadPeriodos")
		@FieldAnnotations(Descripcion="Cantidad de Periodos")
		private int cantidadPeriodos;
		
		
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public int getGrado() {
			return grado;
		}
		public void setGrado(int grado) {
			this.grado = grado;
		}
		public String getDivision() {
			return division.toUpperCase();
		}
		public void setDivision(String division) {
			this.division = division.toUpperCase();
		}
		public String getTurno() {
			return turno.trim();
		}
		public void setTurno(String turno) {
			this.turno = turno.trim();
		}
		public int getCantidadPeriodos() {
			return cantidadPeriodos;
		}
		public void setCantidadPeriodos(int cantidadPeriodos) {
			this.cantidadPeriodos = cantidadPeriodos;
		}
		public boolean isActivo() {
			return activo;
		}
		public void setActivo(boolean activo) {
			this.activo = activo;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int) (id ^ (id >>> 32));
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Curso other = (Curso) obj;
			if (id != other.id)
				return false;
			return true;
		}
		@Override
		public String toString() {
			return String.valueOf(grado) + "� " + division;
		}
		
		
}
