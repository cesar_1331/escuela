package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.data.reglasdenegocio.CursoRN;
import ar.com.trisquel.escuela.data.reglasdenegocio.MateriaRN;
import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="ASIGNATURA")
public class Asignatura implements Serializable{
	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id")
	private long id;
	@Column(name="Anio")
	@FieldAnnotations(Descripcion="A�o")
	private int anio;
	@Column(name="MateriaId")
	@FieldAnnotations(Descripcion="Id la materia")
	private long materiaId;
	@Column(name="ProfesorId")
	@FieldAnnotations(Descripcion="Id del profesor")
	private long profesorId;
	@Column(name="CursoId")
	@FieldAnnotations(Descripcion="Id del Curso")
	private long cursoId;
	@Column(name="Observaciones")
	@FieldAnnotations(Descripcion="Observaciones")
	private String observaciones;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getAnio() {
		return anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
	public long getMateriaId() {
		return materiaId;
	}
	public void setMateriaId(long materiaId) {
		this.materiaId = materiaId;
	}
	public long getProfesorId() {
		return profesorId;
	}
	public void setProfesorId(long profesorId) {
		this.profesorId = profesorId;
	}
	public long getCursoId() {
		return cursoId;
	}
	public void setCursoId(long cursoId) {
		this.cursoId = cursoId;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Asignatura other = (Asignatura) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		Curso curso = CursoRN.Read(cursoId);
		Materia materia = MateriaRN.Read(materiaId);
		String cursoNombre = curso.toString() + "        ";
		return cursoNombre.substring(0, 7) + materia.toString();
	}

}
