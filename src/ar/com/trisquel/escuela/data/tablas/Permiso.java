package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.data.tablas.identificadores.PermisoId;
import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="PERMISO")
public class Permiso implements Serializable {
	
	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Usuario y View")
	private PermisoId id;
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Nombre")
	private String nombre;
	@Column(name="Acceso")
	@FieldAnnotations(Descripcion="Acceso a la opcion")
	private boolean acceso;
	@Column(name="Agregar")
	@FieldAnnotations(Descripcion="Agregar")
	private boolean agregar;
	@Column(name="Modificar")
	@FieldAnnotations(Descripcion="Modificar")
	private boolean modificar;
	@Column(name="Borrar")
	@FieldAnnotations(Descripcion="Borrar")
	private boolean borrar;
	
	public Permiso(){
	}
	public Permiso(PermisoId id){
		this.id = id;
	}
	public PermisoId getId() {
		return id;
	}
	public void setId(PermisoId id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public boolean isAcceso() {
		return acceso;
	}
	public void setAcceso(boolean acceso) {
		this.acceso = acceso;
	}
	public boolean isAgregar() {
		return agregar;
	}
	public void setAgregar(boolean agregar) {
		this.agregar = agregar;
	}
	public boolean isModificar() {
		return modificar;
	}
	public void setModificar(boolean modificar) {
		this.modificar = modificar;
	}
	public boolean isBorrar() {
		return borrar;
	}
	public void setBorrar(boolean borrar) {
		this.borrar = borrar;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Permiso other = (Permiso) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
