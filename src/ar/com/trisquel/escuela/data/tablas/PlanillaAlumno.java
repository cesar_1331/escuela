package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.data.tablas.identificadores.PlanillaAlumnoId;
import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="PLANILLANOTAS")
public class PlanillaAlumno implements Serializable {

	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id de la planilla y del alumno")
	private PlanillaAlumnoId id;
	@Column(name="nota")
	@FieldAnnotations(Descripcion="Nota del alumno")
	private int nota;
	public PlanillaAlumnoId getId() {
		return id;
	}
	public void setId(PlanillaAlumnoId id) {
		this.id = id;
	}
	public int getNota() {
		return nota;
	}
	public void setNota(int nota) {
		this.nota = nota;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanillaAlumno other = (PlanillaAlumno) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
