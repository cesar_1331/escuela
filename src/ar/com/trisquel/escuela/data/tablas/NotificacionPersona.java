package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.data.tablas.identificadores.NotificacionPersonaId;
import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="NOTIFICACIONPERSONA")
public class NotificacionPersona implements Serializable {
	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id")
	private NotificacionPersonaId id;
	@Column(name="FechaAlta")
	@FieldAnnotations(Descripcion="Fecha de Alta Redundada")
	private Date fechaAlta;
	@Column(name="Prioridad")
	@FieldAnnotations(Descripcion="Prioridad redundada")
	private int prioridad;
	@Column(name="Titulo")
	@FieldAnnotations(Descripcion="Titulo redundado")
	private String titulo;
	@Column(name="Leido")
	@FieldAnnotations(Descripcion="Leido")
	private boolean leido;
	@Column(name="FechaLeido")
	@FieldAnnotations(Descripcion="Fecha en que fue leido")
	private Date fechaLeido;
	@Column(name="cancelado")
	@FieldAnnotations(Descripcion="cancelado")
	private boolean cancelado;
	public NotificacionPersonaId getId() {
		return id;
	}
	public void setId(NotificacionPersonaId id) {
		this.id = id;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public int getPrioridad() {
		return prioridad;
	}
	public void setPrioridad(int prioridad) {
		this.prioridad = prioridad;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public boolean isLeido() {
		return leido;
	}
	public void setLeido(boolean leido) {
		this.leido = leido;
	}
	public Date getFechaLeido() {
		return fechaLeido;
	}
	public void setFechaLeido(Date fechaLeido) {
		this.fechaLeido = fechaLeido;
	}
	public boolean isCancelado() {
		return cancelado;
	}
	public void setCancelado(boolean cancelado) {
		this.cancelado = cancelado;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotificacionPersona other = (NotificacionPersona) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
