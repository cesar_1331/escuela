package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="COMPROBANTE")
public class Personas implements Serializable {
	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id")
	private long id;
	@Column(name="Nombre")
	@FieldAnnotations(Descripcion="Nombre")
	private String nombre;
	@Column(name="CUIT")
	@FieldAnnotations(Descripcion="CUIT/CUIL")
	private String cuit;
	@Column(name="CondicionIva")
	@FieldAnnotations(Descripcion="Condicion de IVA")
	private short condicionIva;
	@Column(name="ingresosBrutosNro")
	@FieldAnnotations(Descripcion="Numero de Ingresos Brutos")
	private String ingresosBrutosNro;
	@Column(name="Domicilio")
	@FieldAnnotations(Descripcion="Domicilio del profesor")
	private String domicilio;
	@Column(name="ciudad")
	@FieldAnnotations(Descripcion="Ciudad")
	private String ciudad;
	@Column(name="provincia")
	@FieldAnnotations(Descripcion="Provincia")
	private String provincia;
	@Column(name="Telefono")
	@FieldAnnotations(Descripcion="Telefono")
	private String telefono;
	@Column(name="Observaciones")
	@FieldAnnotations(Descripcion="Observaciones")
	private String observaciones;
	@Column(name="Es Proveedor")
	@FieldAnnotations(Descripcion="Es Proveedor")
	private boolean esProveedor;
	@Column(name="EsCliente")
	@FieldAnnotations(Descripcion="Es Cliente")
	private boolean esCliente;
	@Column(name="activo")
	@FieldAnnotations(Descripcion="Activo")
	private boolean activo;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre.toUpperCase();
	}
	public void setNombre(String nombre) {
		this.nombre = nombre.toUpperCase();
	}
	public String getCuit() {
		return cuit.toUpperCase();
	}
	public void setCuit(String cuit) {
		this.cuit = cuit.toUpperCase();
	}
	public short getCondicionIva() {
		return condicionIva;
	}
	public void setCondicionIva(short condicionIva) {
		this.condicionIva = condicionIva;
	}
	public String getIngresosBrutosNro() {
		return ingresosBrutosNro;
	}
	public void setIngresosBrutosNro(String ingresosBrutosNro) {
		this.ingresosBrutosNro = ingresosBrutosNro;
	}
	public String getDomicilio() {
		return domicilio;
	}
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public boolean isEsProveedor() {
		return esProveedor;
	}
	public void setEsProveedor(boolean esProveedor) {
		this.esProveedor = esProveedor;
	}
	public boolean isEsCliente() {
		return esCliente;
	}
	public void setEsCliente(boolean esCliente) {
		this.esCliente = esCliente;
	}
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Personas other = (Personas) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return nombre.toUpperCase();
	}
	
	
}
