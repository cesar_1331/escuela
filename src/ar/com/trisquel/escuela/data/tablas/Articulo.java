package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="Articulo")
public class Articulo implements Serializable{

	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id Articulos")
	private String id;
	@Column(name="Titulo")
	@FieldAnnotations(Descripcion="Titulo del Articulo")
	private String titulo;
	@Column(name="Contenido")
	@FieldAnnotations(Descripcion="Contenido del Articulo")
	private String contenido;
	@Column(name="FechaHora")
	@FieldAnnotations(Descripcion="Fecha de Modificacion")
	private Date fechaHora;
	@Column(name="Usuario")
	@FieldAnnotations(Descripcion="Usuario que Modifico")
	private String usuario;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	public Date getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Articulo other = (Articulo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}