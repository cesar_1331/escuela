package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="MATERIA")
public class Materia implements Serializable{
	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id")
	private long id;
	@Column(name="Nombre")
	@FieldAnnotations(Descripcion="Nombre")
	private String nombre;
	@Column(name="Activa")
	@FieldAnnotations(Descripcion="La materia esta activa")
	private boolean activa;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre.toUpperCase();
	}
	public void setNombre(String nombre) {
		this.nombre = nombre.toUpperCase();
	}
	
	public boolean isActiva() {
		return activa;
	}
	public void setActiva(boolean activa) {
		this.activa = activa;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Materia other = (Materia) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return nombre;
	}
	
	
}
