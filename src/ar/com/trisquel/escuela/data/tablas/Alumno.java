package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="ALUMNO")
public class Alumno implements Serializable {
	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id")
	private long id;
	@Column(name="Nombre")
	@FieldAnnotations(Descripcion="Nombre")
	private String nombre;
	@Column(name="Apellido")
	@FieldAnnotations(Descripcion="Apellido")
	private String apellido;
	@Column(name="FechaNacimiento")
	@FieldAnnotations(Descripcion="Fecha de Nacimiento")
	private Date fechaNacimiento;
	@Column(name="Genero")
	@FieldAnnotations(Descripcion="Masculino / Femenino")
	private String genero;
	@Column(name="DNI")
	@FieldAnnotations(Descripcion="DNI")
	private long dni;
	@Column(name="Estado")
	@FieldAnnotations(Descripcion="Estado del alumno (Cursando,Egresado,,Transferido,Abandono)")
	private String estado;
	@Column(name="EstadoObservaciones")
	@FieldAnnotations(Descripcion="Observaciones del estado del alumno")
	private String estadoObservaciones;
	@Column(name="Domicilio")
	@FieldAnnotations(Descripcion="Domicilio del alumno")
	private String domicilio;
	@Column(name="ciudad")
	@FieldAnnotations(Descripcion="Ciudad")
	private String ciudad;
	@Column(name="provincia")
	@FieldAnnotations(Descripcion="Provincia")
	private String provincia;
	@Column(name="Telefono")
	@FieldAnnotations(Descripcion="Telefono")
	private String telefono;
	@Column(name="Observaciones")
	@FieldAnnotations(Descripcion="Observaciones")
	private String observaciones;
	@Column(name="FechaAlta")
	@FieldAnnotations(Descripcion="Fecha de Alta del Alumno")
	private Date fechaAlta;
	@Column(name="ResponsableAlta")
	@FieldAnnotations(Descripcion="Responsable del Alta del Alumno")
	private String responsableAlta;
	@Column(name="FechaModificacion")
	@FieldAnnotations(Descripcion="Fecha de Modificacion de los datos")
	private Date fechaModificacion;
	@Column(name="ResponsableModificacion")
	@FieldAnnotations(Descripcion="Responsable de la Modificacion de datos")
	private String responsableModificacion;
	@Column(name="CursoId")
	@FieldAnnotations(Descripcion="Curso en el que esta inscripto")
	private long cursoId;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre.toUpperCase();
	}
	public void setNombre(String nombre) {
		this.nombre = nombre.toUpperCase();
	}
	public String getApellido() {
		return apellido.toUpperCase();
	}
	public void setApellido(String apellido) {
		this.apellido = apellido.toUpperCase();
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public long getDni() {
		return dni;
	}
	public void setDni(long dni) {
		this.dni = dni;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getEstadoObservaciones() {
		return estadoObservaciones;
	}
	public void setEstadoObservaciones(String estadoObservaciones) {
		this.estadoObservaciones = estadoObservaciones;
	}
	public String getDomicilio() {
		return domicilio;
	}
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public String getResponsableAlta() {
		return responsableAlta;
	}
	public void setResponsableAlta(String responsableAlta) {
		this.responsableAlta = responsableAlta;
	}
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public String getResponsableModificacion() {
		return responsableModificacion;
	}
	public void setResponsableModificacion(String responsableModificacion) {
		this.responsableModificacion = responsableModificacion;
	}
	public long getCursoId() {
		return cursoId;
	}
	public void setCursoId(long cursoId) {
		this.cursoId = cursoId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alumno other = (Alumno) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return apellido.toUpperCase() + ", " + nombre.toUpperCase();
	}

}
