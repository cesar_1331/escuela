package ar.com.trisquel.escuela.data.tablas;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.data.tablas.identificadores.UsuarioLinea;
import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@Entity
@Table(name="UsuarioEstado")
public class UsuarioEstado {
	@Id
	@Column(name="Id" ,unique=true )
	@FieldAnnotations(Descripcion="Identificador compuesto por el Id del usuario y una linea long")
	private UsuarioLinea id;
	@Column(name="FechaIni" )
	@FieldAnnotations(Descripcion="Fecha de inicio del estado")
	private Date fechaIni;
	@Column(name="FechaFin" )
	@FieldAnnotations(Descripcion="Fecha de finalizacion del estado")
	private Date fechaFin;
	@Column(name="Motivo" )
	@FieldAnnotations(Descripcion="Motivo por el cual se asigno el estado")
	private int motivo;
	@Column(name="Observacion")
	@FieldAnnotations(Descripcion="Observaciones del estado")
	private String observacion;
	@Column(name="Responsable")
	@FieldAnnotations(Descripcion="Usuario responsable de la generación del estado")
	private String responsable;
	public UsuarioLinea getId() {
		return id;
	}
	public void setId(UsuarioLinea id) {
		this.id = id;
	}
	public Date getFechaIni() {
		return fechaIni;
	}
	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public int getMotivo() {
		return motivo;
	}
	public void setMotivo(int motivo) {
		this.motivo = motivo;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioEstado other = (UsuarioEstado) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}
