package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="PLANILLA")
public class Planilla implements Serializable {
	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id")
	private long id;
	@Column(name="Anio")
	@FieldAnnotations(Descripcion="A�o")
	private int anio;
	@Column(name="MateriaId")
	@FieldAnnotations(Descripcion="Id la materia")
	private long materiaId;
	@Column(name="ProfesorId")
	@FieldAnnotations(Descripcion="Id del profesor")
	private long profesorId;
	@Column(name="CursoId")
	@FieldAnnotations(Descripcion="Id del Curso")
	private long cursoId;
	@Column(name="Descripci�n")
	@FieldAnnotations(Descripcion="Descripci�n de la nota")
	private String descripcion;
	@Column(name="MostrarAlAlumno")
	@FieldAnnotations(Descripcion="Mostrar al alumno")
	private boolean mostrarAlAlumno;
	@Column(name="Periodo")
	@FieldAnnotations(Descripcion="La nota corresponde al periodo")
	private int periodo;
	@Column(name="esFinal")
	@FieldAnnotations(Descripcion="Es la nota final del periodo")
	private boolean esNotaFinal;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getAnio() {
		return anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
	public long getMateriaId() {
		return materiaId;
	}
	public void setMateriaId(long materiaId) {
		this.materiaId = materiaId;
	}
	public long getProfesorId() {
		return profesorId;
	}
	public void setProfesorId(long profesorId) {
		this.profesorId = profesorId;
	}
	public long getCursoId() {
		return cursoId;
	}
	public void setCursoId(long cursoId) {
		this.cursoId = cursoId;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public boolean isMostrarAlAlumno() {
		return mostrarAlAlumno;
	}
	public void setMostrarAlAlumno(boolean mostrarAlAlumno) {
		this.mostrarAlAlumno = mostrarAlAlumno;
	}
	public int getPeriodo() {
		return periodo;
	}
	public void setPeriodo(int periodo) {
		this.periodo = periodo;
	}
	public boolean isEsNotaFinal() {
		return esNotaFinal;
	}
	public void setEsNotaFinal(boolean esNotaFinal) {
		this.esNotaFinal = esNotaFinal;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Planilla other = (Planilla) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
