package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.data.tablas.identificadores.AsignaturaAlumnoId;
import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="ASIGNATURAALUMNO")
public class AsignaturaAlumno implements Serializable{
	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id del asignatura y alumno")
	private AsignaturaAlumnoId id;
	@Column(name="Orden")
	@FieldAnnotations(Descripcion="Orden en que se mostrara en la lista")
	private int orden;
	@Column(name="Cursando")
	@FieldAnnotations(Descripcion="El alumno cursa la materia")
	private boolean cursando;
	
	public AsignaturaAlumnoId getId() {
		return id;
	}
	public void setId(AsignaturaAlumnoId id) {
		this.id = id;
	}
	public int getOrden() {
		return orden;
	}
	public void setOrden(int orden) {
		this.orden = orden;
	}
	public boolean isCursando() {
		return cursando;
	}
	public void setCursando(boolean cursando) {
		this.cursando = cursando;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AsignaturaAlumno other = (AsignaturaAlumno) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
