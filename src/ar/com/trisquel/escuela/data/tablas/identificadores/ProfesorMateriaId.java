package ar.com.trisquel.escuela.data.tablas.identificadores;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ProfesorMateriaId implements Serializable{

	private long profesorId;
	private long materiaId;
	
	public ProfesorMateriaId(){
	}
	public ProfesorMateriaId(long profesorId ,long materiaId){
		this.profesorId = profesorId;
		this.materiaId = materiaId;
	}
	public long getProfesorId() {
		return profesorId;
	}
	public void setProfesorId(long profesorId) {
		this.profesorId = profesorId;
	}
	public long getMateriaId() {
		return materiaId;
	}
	public void setMateriaId(long materiaId) {
		this.materiaId = materiaId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (materiaId ^ (materiaId >>> 32));
		result = prime * result + (int) (profesorId ^ (profesorId >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfesorMateriaId other = (ProfesorMateriaId) obj;
		if (materiaId != other.materiaId)
			return false;
		if (profesorId != other.profesorId)
			return false;
		return true;
	}
	
}
