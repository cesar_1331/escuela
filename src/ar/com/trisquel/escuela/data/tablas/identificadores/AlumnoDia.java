package ar.com.trisquel.escuela.data.tablas.identificadores;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class AlumnoDia implements Serializable {

	private long alumnoId;
	private Date dia;
	
	public AlumnoDia(){
	}
	public AlumnoDia(long alumnoId ,Date dia){
		this.alumnoId = alumnoId;
		this.dia = dia;
	}
	public long getAlumnoId() {
		return alumnoId;
	}
	public void setAlumnoId(long alumnoId) {
		this.alumnoId = alumnoId;
	}
	public Date getDia() {
		return dia;
	}
	public void setDia(Date dia) {
		this.dia = dia;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (alumnoId ^ (alumnoId >>> 32));
		result = prime * result + ((dia == null) ? 0 : dia.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlumnoDia other = (AlumnoDia) obj;
		if (alumnoId != other.alumnoId)
			return false;
		if (dia == null) {
			if (other.dia != null)
				return false;
		} else if (!dia.equals(other.dia))
			return false;
		return true;
	}
	
}
