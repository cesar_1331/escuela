package ar.com.trisquel.escuela.data.tablas.identificadores;

import java.io.Serializable;

@SuppressWarnings("serial")
public class PlanillaAlumnoId implements Serializable {

	private long planillaId;
	private long alumnoId;
	
	
	public PlanillaAlumnoId(){
	}
	public PlanillaAlumnoId(long planillaId ,long alumnoId){
		this.alumnoId = alumnoId;
		this.planillaId = planillaId;
	}
	public long getPlanillaId() {
		return planillaId;
	}
	public void setPlanillaId(long planillaId) {
		this.planillaId = planillaId;
	}
	public long getAlumnoId() {
		return alumnoId;
	}
	public void setAlumnoId(long alumnoId) {
		this.alumnoId = alumnoId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (alumnoId ^ (alumnoId >>> 32));
		result = prime * result + (int) (planillaId ^ (planillaId >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanillaAlumnoId other = (PlanillaAlumnoId) obj;
		if (alumnoId != other.alumnoId)
			return false;
		if (planillaId != other.planillaId)
			return false;
		return true;
	}
	
	
}
