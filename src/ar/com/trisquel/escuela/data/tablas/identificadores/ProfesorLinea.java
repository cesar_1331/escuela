package ar.com.trisquel.escuela.data.tablas.identificadores;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ProfesorLinea implements Serializable{

	private long profesorId;
	private long linea;
	public long getProfesorId() {
		return profesorId;
	}
	public void setProfesorId(long profesorId) {
		this.profesorId = profesorId;
	}
	public long getLinea() {
		return linea;
	}
	public void setLinea(long linea) {
		this.linea = linea;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (linea ^ (linea >>> 32));
		result = prime * result + (int) (profesorId ^ (profesorId >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfesorLinea other = (ProfesorLinea) obj;
		if (linea != other.linea)
			return false;
		if (profesorId != other.profesorId)
			return false;
		return true;
	}
		
}
