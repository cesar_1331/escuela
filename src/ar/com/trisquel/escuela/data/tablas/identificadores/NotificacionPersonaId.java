package ar.com.trisquel.escuela.data.tablas.identificadores;

import java.io.Serializable;

@SuppressWarnings("serial")
public class NotificacionPersonaId implements Serializable{
	
	private long notificacionId;
	private String tipo;
	private long personaId;
	
	public NotificacionPersonaId(){
	}
	public NotificacionPersonaId(long notificacionId ,String tipo ,long personaId){
		this.notificacionId = notificacionId;
		this.tipo = tipo;
		this.personaId = personaId;
	}
	
	public long getNotificacionId() {
		return notificacionId;
	}
	public void setNotificacionId(long notificacionId) {
		this.notificacionId = notificacionId;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public long getPersonaId() {
		return personaId;
	}
	public void setPersonaId(long personaId) {
		this.personaId = personaId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ (int) (notificacionId ^ (notificacionId >>> 32));
		result = prime * result + (int) (personaId ^ (personaId >>> 32));
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotificacionPersonaId other = (NotificacionPersonaId) obj;
		if (notificacionId != other.notificacionId)
			return false;
		if (personaId != other.personaId)
			return false;
		if (tipo == null) {
			if (other.tipo != null)
				return false;
		} else if (!tipo.equals(other.tipo))
			return false;
		return true;
	}
	
	
}
