package ar.com.trisquel.escuela.data.tablas.identificadores;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ComprobanteLinea implements Serializable{
	private long comprobanteId;
	private long linea;
	
	public ComprobanteLinea(){
	}
	public ComprobanteLinea(long comprobanteId ,long linea){
		this.comprobanteId = comprobanteId;
		this.linea = linea;
	}
	public long getComprobanteId() {
		return comprobanteId;
	}
	public void setComprobanteId(long comprobanteId) {
		this.comprobanteId = comprobanteId;
	}
	public long getLinea() {
		return linea;
	}
	public void setLinea(long linea) {
		this.linea = linea;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ (int) (comprobanteId ^ (comprobanteId >>> 32));
		result = prime * result + (int) (linea ^ (linea >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComprobanteLinea other = (ComprobanteLinea) obj;
		if (comprobanteId != other.comprobanteId)
			return false;
		if (linea != other.linea)
			return false;
		return true;
	}
	
}
