package ar.com.trisquel.escuela.data.tablas.identificadores;

import java.io.Serializable;

@SuppressWarnings("serial")
public class AsignaturaAlumnoId implements Serializable {

	private long asignaturaId;
	private long alumnoId;
	
	public AsignaturaAlumnoId(){
	}
	public AsignaturaAlumnoId(long asignaturaId ,long alumnoId){
		this.asignaturaId = asignaturaId; 
		this.alumnoId = alumnoId;
	}
	
	public long getAsignaturaId() {
		return asignaturaId;
	}
	public void setAsignaturaId(long asignaturaId) {
		this.asignaturaId = asignaturaId;
	}
	public long getAlumnoId() {
		return alumnoId;
	}
	public void setAlumnoId(long alumnoId) {
		this.alumnoId = alumnoId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (alumnoId ^ (alumnoId >>> 32));
		result = prime * result + (int) (asignaturaId ^ (asignaturaId >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AsignaturaAlumnoId other = (AsignaturaAlumnoId) obj;
		if (alumnoId != other.alumnoId)
			return false;
		if (asignaturaId != other.asignaturaId)
			return false;
		return true;
	}
	
	
}
