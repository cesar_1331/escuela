package ar.com.trisquel.escuela.data.tablas.identificadores;

import java.io.Serializable;

@SuppressWarnings("serial")
public class UsuarioLinea implements Serializable{
	
	private String usuarioId;
	private long linea;
	
	public UsuarioLinea(){	
	}
	public UsuarioLinea(String UsuarioId ,long Linea){
		this.usuarioId = UsuarioId; 
		this.linea = Linea;
	}
	public String getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}
	public long getLinea() {
		return linea;
	}
	public void setLinea(long linea) {
		this.linea = linea;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (linea ^ (linea >>> 32));
		result = prime * result
				+ ((usuarioId == null) ? 0 : usuarioId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioLinea other = (UsuarioLinea) obj;
		if (linea != other.linea)
			return false;
		if (usuarioId == null) {
			if (other.usuarioId != null)
				return false;
		} else if (!usuarioId.equals(other.usuarioId))
			return false;
		return true;
	}
	
}
