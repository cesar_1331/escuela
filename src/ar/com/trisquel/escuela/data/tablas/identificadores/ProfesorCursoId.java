package ar.com.trisquel.escuela.data.tablas.identificadores;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ProfesorCursoId implements Serializable{

	private long profesorId;
	private long cursoId;
	
	public ProfesorCursoId(){
	}
	public ProfesorCursoId(long profesorId ,long cursoId){
		this.profesorId = profesorId;
		this.cursoId = cursoId;
	}
	public long getProfesorId() {
		return profesorId;
	}
	public void setProfesorId(long profesorId) {
		this.profesorId = profesorId;
	}
	public long getCursoId() {
		return cursoId;
	}
	public void setCursoId(long cursoId) {
		this.cursoId = cursoId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (cursoId ^ (cursoId >>> 32));
		result = prime * result + (int) (profesorId ^ (profesorId >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfesorCursoId other = (ProfesorCursoId) obj;
		if (cursoId != other.cursoId)
			return false;
		if (profesorId != other.profesorId)
			return false;
		return true;
	}
	
}
