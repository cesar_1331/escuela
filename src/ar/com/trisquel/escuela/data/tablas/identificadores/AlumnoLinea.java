package ar.com.trisquel.escuela.data.tablas.identificadores;

import java.io.Serializable;

@SuppressWarnings("serial")
public class AlumnoLinea implements Serializable {

	private long alumnoId;
	private long linea;
	
	public AlumnoLinea(){
	}
	public AlumnoLinea(long alumnoId ,long linea){
		this.alumnoId = alumnoId;
		this.linea = linea;
	}
	public long getAlumnoId() {
		return alumnoId;
	}
	public void setAlumnoId(long alumnoId) {
		this.alumnoId = alumnoId;
	}
	public long getLinea() {
		return linea;
	}
	public void setLinea(long linea) {
		this.linea = linea;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (alumnoId ^ (alumnoId >>> 32));
		result = prime * result + (int) (linea ^ (linea >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlumnoLinea other = (AlumnoLinea) obj;
		if (alumnoId != other.alumnoId)
			return false;
		if (linea != other.linea)
			return false;
		return true;
	}
	
}
