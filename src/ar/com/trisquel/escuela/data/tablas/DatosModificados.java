package ar.com.trisquel.escuela.data.tablas;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@Entity
@Table(name="DatosModificados")
public class DatosModificados {

	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id")
	private long id;
	@Column(name="Entidad")
	@FieldAnnotations(Descripcion="Entidad. Ver Dominio Entidad")
	private int entidad;
	@Column(name="EntidadId")
	@FieldAnnotations(Descripcion="Id de la entidad")
	private long entidadId;
	@Column(name="Descripcion")
	@FieldAnnotations(Descripcion="Descripcion")
	private String descripcion;
	@Column(name="FechaModificacion")
	@FieldAnnotations(Descripcion="Fecha de la Modificacion")
	private Date fechaModificacion;
	
	
	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public int getEntidad() {
		return entidad;
	}


	public void setEntidad(int entidad) {
		this.entidad = entidad;
	}


	public long getEntidadId() {
		return entidadId;
	}


	public void setEntidadId(long entidadId) {
		this.entidadId = entidadId;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Date getFechaModificacion() {
		return fechaModificacion;
	}


	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DatosModificados other = (DatosModificados) obj;
		if (id != other.id)
			return false;
		return true;
	}


	@Override
	public String toString() {
		return descripcion;
	}
	
}
