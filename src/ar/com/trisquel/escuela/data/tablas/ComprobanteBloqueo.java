package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.data.tablas.identificadores.AnioMesId;
import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="COMPROBANTEBLOQUE")
public class ComprobanteBloqueo implements Serializable {
	
	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id Anio Id")
	private AnioMesId id;
	@Column(name="Bloqueado")
	@FieldAnnotations(Descripcion="Bloqueado")
	private boolean bloqueado;
	@Column(name="fecha")
	@FieldAnnotations(Descripcion="Fecha")
	private Date fecha;
	@Column(name="responsable")
	@FieldAnnotations(Descripcion="Responsable")
	private String responsable;
	
	public AnioMesId getId() {
		return id;
	}
	public void setId(AnioMesId id) {
		this.id = id;
	}
	public boolean isBloqueado() {
		return bloqueado;
	}
	public void setBloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComprobanteBloqueo other = (ComprobanteBloqueo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
