package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="PARAMETRO")
public class Parametro implements Serializable {

	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id")
	private String  id;
	@Column(name="TIPO")
	@FieldAnnotations(Descripcion="Tipo de Dato")
	private String tipoDato;
	@Column(name="VALOR")
	@FieldAnnotations(Descripcion="Valor")
	private String valor;
	
	public Parametro(){
	}
	public Parametro(String  id ,String tipoDato ,String valor){
		this.id = id;
		this.tipoDato = tipoDato;
		this.valor = valor;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTipoDato() {
		return tipoDato;
	}
	public void setTipoDato(String tipoDato) {
		this.tipoDato = tipoDato;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parametro other = (Parametro) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
