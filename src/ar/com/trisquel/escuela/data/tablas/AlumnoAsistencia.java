package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.data.tablas.identificadores.AlumnoDia;
import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="ALUMNOASISTENCIA")
public class AlumnoAsistencia implements Serializable {
	
	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id Articulos")
	private AlumnoDia id;
	@Column(name="asistencia")
	@FieldAnnotations(Descripcion="Asistencia")
	private String asistencia;
	public AlumnoDia getId() {
		return id;
	}
	public void setId(AlumnoDia id) {
		this.id = id;
	}
	public String getAsistencia() {
		return asistencia;
	}
	public void setAsistencia(String asistencia) {
		this.asistencia = asistencia;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlumnoAsistencia other = (AlumnoAsistencia) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
