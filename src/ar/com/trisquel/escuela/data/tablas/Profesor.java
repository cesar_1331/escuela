package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="PROFESOR")
public class Profesor implements Serializable {
	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id")
	private long id;
	@Column(name="Nombre")
	@FieldAnnotations(Descripcion="Nombre")
	private String nombre;
	@Column(name="Apellido")
	@FieldAnnotations(Descripcion="Apellido")
	private String apellido;
	@Column(name="Titulo")
	@FieldAnnotations(Descripcion="Titulo del profesor")
	private String titulo;
	@Column(name="Domicilio")
	@FieldAnnotations(Descripcion="Domicilio del profesor")
	private String domicilio;
	@Column(name="ciudad")
	@FieldAnnotations(Descripcion="Ciudad")
	private String ciudad;
	@Column(name="provincia")
	@FieldAnnotations(Descripcion="Provincia")
	private String provincia;
	@Column(name="Telefono")
	@FieldAnnotations(Descripcion="Telefono")
	private String telefono;
	@Column(name="Celular")
	@FieldAnnotations(Descripcion="Celular")
	private String celular;
	@Column(name="Observaciones")
	@FieldAnnotations(Descripcion="Observaciones")
	private String observaciones;
	@Column(name="activo")
	@FieldAnnotations(Descripcion="Activo")
	private boolean esPreceptor;
	@Column(name="activo")
	@FieldAnnotations(Descripcion="Activo")
	private boolean activo;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre.toUpperCase();
	}
	public void setNombre(String nombre) {
		this.nombre = nombre.toUpperCase();
	}
	public String getApellido() {
		return apellido.toUpperCase();
	}
	public void setApellido(String apellido) {
		this.apellido = apellido.toUpperCase();
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDomicilio() {
		return domicilio;
	}
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	public boolean isEsPreceptor() {
		return esPreceptor;
	}
	public void setEsPreceptor(boolean esPreceptor) {
		this.esPreceptor = esPreceptor;
	}
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Profesor other = (Profesor) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return apellido.trim() + ", " + nombre.trim();
	}
	
}
