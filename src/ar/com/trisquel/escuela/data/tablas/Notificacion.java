package ar.com.trisquel.escuela.data.tablas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.utiles.FieldAnnotations;

@SuppressWarnings("serial")
@Entity
@Table(name="NOTIFICACION")
public class Notificacion implements Serializable{
	@GeneratedValue
	@Id
	@Column(name="Id")
	@FieldAnnotations(Descripcion="Id")
	private long id;
	@Column(name="FechaAlta")
	@FieldAnnotations(Descripcion="Fecha de Alta")
	private Date fechaAlta;
	@Column(name="ResponsableAlta")
	@FieldAnnotations(Descripcion="Responsable del Alta")
	private String responsableAlta;
	@Column(name="Prioridad")
	@FieldAnnotations(Descripcion="Prioridad")
	private int prioridad;
	@Column(name="Titulo")
	@FieldAnnotations(Descripcion="Titulo")
	private String titulo;
	@Column(name="Descripcion")
	@FieldAnnotations(Descripcion="Descripcion")
	private String descripcion;
	@Column(name="Cancelada")
	@FieldAnnotations(Descripcion="Notificacion cancelada")
	private boolean cancelada;
	@Column(name="Leido")
	@FieldAnnotations(Descripcion="Todas las notificaciones leidas")
	private boolean leido;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public String getResponsableAlta() {
		return responsableAlta;
	}
	public void setResponsableAlta(String responsableAlta) {
		this.responsableAlta = responsableAlta;
	}
	public int getPrioridad() {
		return prioridad;
	}
	public void setPrioridad(int prioridad) {
		this.prioridad = prioridad;
	}
	public String getTitulo() {
		return titulo.toUpperCase();
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo.toUpperCase();
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public boolean isCancelada() {
		return cancelada;
	}
	public void setCancelada(boolean cancelada) {
		this.cancelada = cancelada;
	}
	public boolean isLeido() {
		return leido;
	}
	public void setLeido(boolean leido) {
		this.leido = leido;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Notificacion other = (Notificacion) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
