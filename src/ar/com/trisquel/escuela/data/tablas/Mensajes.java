package ar.com.trisquel.escuela.data.tablas;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.trisquel.escuela.utiles.FieldAnnotations;

/**
 * Clase para la definicion de los mensajes que envian los usuarios
 * @Version 1.0
 */

@Entity
@Table(name="Mensajes")
public class Mensajes {
	
	@Id
	@Column(name="Id" ,unique=true)
	@FieldAnnotations(Descripcion="Identificador")
	private long id;
	@Column(name="UsuarioId")
	@FieldAnnotations(Descripcion="Id del Usuario")
	private String usuarioId;
	@Column(name="Nombre")
	@FieldAnnotations(Descripcion="Nombre y Apellido cuando no es de un usuario")
	private String nombre;
	@Column(name="Mail")
	@FieldAnnotations(Descripcion="Mail de la persona cuando no es de un usuario")
	private String mail;
	@Column(name="Asunto")
	@FieldAnnotations(Descripcion="Asunto")
	private String asunto;
	@Column(name="FechaHora" )
	@FieldAnnotations(Descripcion="Fecha hora de alta del mensaje")
	private Date fechaHora;
	@Column(name="Tipo" )
	@FieldAnnotations(Descripcion="Tipo de mensaje, 1:Mensaje, 2:Sugerencia ,3:Reclamo")
	private int tipo;
	@Column(name="Mensaje" ,length=512)
	@FieldAnnotations(Descripcion="Mensaje")
	private String mensaje;
	@Column(name="Leido")
	@FieldAnnotations(Descripcion="Indica si el mensaje fue leido")
	private boolean leido;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	public Date getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public boolean isLeido() {
		return leido;
	}
	public void setLeido(boolean leido) {
		this.leido = leido;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mensajes other = (Mensajes) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
