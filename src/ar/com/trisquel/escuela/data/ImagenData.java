package ar.com.trisquel.escuela.data;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;

import ar.com.trisquel.escuela.utiles.FieldAnnotations;

import com.vaadin.server.StreamResource.StreamSource;

@SuppressWarnings("serial")
@FieldAnnotations(Descripcion="Esta clase se encarga de obtener Stream a partir del dato blob de la base de datos, " +
		                      "que permite que se asigne a una imagen")
public class ImagenData implements StreamSource{
	private Blob blob;
	public ImagenData(Blob blob) {
		this.blob = blob;
	}

    public InputStream getStream () {
    	try {
			return blob.getBinaryStream();
		} catch (SQLException e) {
			return null;
		}
		
    }
 
}
