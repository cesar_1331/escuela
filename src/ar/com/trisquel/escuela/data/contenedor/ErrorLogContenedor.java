package ar.com.trisquel.escuela.data.contenedor;

import java.util.Date;

import ar.com.trisquel.escuela.data.select.ErrorLogSelect;
import ar.com.trisquel.escuela.data.tablas.ErrorLog;

import com.vaadin.data.util.BeanItemContainer;

public class ErrorLogContenedor {
	
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXPeriodoXVisto(boolean mostrarVistos,Date  fechaDesde,Date fechaHasta)
	{
		BeanItemContainer<ErrorLog> container = new BeanItemContainer<ErrorLog>(ErrorLog.class);
		container.addAll(ErrorLogSelect.SelectFiltroVisto(mostrarVistos, fechaDesde, fechaHasta));
		return container;
	}
}
