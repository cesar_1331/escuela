package ar.com.trisquel.escuela.data.contenedor;

import ar.com.trisquel.escuela.data.reglasdenegocio.AsignaturaRN;
import ar.com.trisquel.escuela.data.select.AsignaturaAlumnoSelect;
import ar.com.trisquel.escuela.data.tablas.Asignatura;
import ar.com.trisquel.escuela.data.tablas.AsignaturaAlumno;

import com.vaadin.data.util.BeanItemContainer;

public class AsignaturaAlumnoContenedor {

	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerAsignaturasXAlumno(long alumnaId ,int anio ,boolean soloCursando)
	{
		BeanItemContainer<Asignatura> container = null;
		container = new BeanItemContainer<Asignatura>(Asignatura.class);
		for (AsignaturaAlumno asignaturaAlumno : AsignaturaAlumnoSelect.LeeAsignaturasXAlumno(alumnaId ,soloCursando)){
			Asignatura asignatura = AsignaturaRN.Read(asignaturaAlumno.getId().getAsignaturaId());
			if (asignatura.getAnio() == anio)
				container.addBean(asignatura);
		}
		return container;
	}
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerProfesorMateriaCursandoXAlumno(long alumnaId ,int anio ,boolean soloCursando)
	{
		AsignaturaAlumnoContenedor asignaturaAlumnoContenedor = new AsignaturaAlumnoContenedor();
		BeanItemContainer<ProfesorMateriaCursando> container = new BeanItemContainer<ProfesorMateriaCursando>(ProfesorMateriaCursando.class);
		for (AsignaturaAlumno asignaturaAlumno : AsignaturaAlumnoSelect.LeeAsignaturasXAlumno(alumnaId ,soloCursando)){
			Asignatura asignatura = AsignaturaRN.Read(asignaturaAlumno.getId().getAsignaturaId());
			if (asignatura.getAnio() == anio)
				container.addBean(asignaturaAlumnoContenedor.new ProfesorMateriaCursando(asignatura.getProfesorId() ,asignatura.getMateriaId() ,asignatura.getCursoId() ,asignaturaAlumno));
		}
		return container;
	}
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerProfesorMateriaCursandoXAlumnoXCurso(long alumnaId ,int anio ,long cursoId ,boolean soloCursando)
	{
		AsignaturaAlumnoContenedor asignaturaAlumnoContenedor = new AsignaturaAlumnoContenedor();
		BeanItemContainer<ProfesorMateriaCursando> container = new BeanItemContainer<ProfesorMateriaCursando>(ProfesorMateriaCursando.class);
		for (AsignaturaAlumno asignaturaAlumno : AsignaturaAlumnoSelect.LeeAsignaturasXAlumno(alumnaId ,soloCursando)){
			Asignatura asignatura = AsignaturaRN.Read(asignaturaAlumno.getId().getAsignaturaId());
			if (asignatura.getAnio() == anio && asignatura.getCursoId()== cursoId)
				container.addBean(asignaturaAlumnoContenedor.new ProfesorMateriaCursando(asignatura.getProfesorId() ,asignatura.getMateriaId() ,asignatura.getCursoId() ,asignaturaAlumno));
		}
		return container;
	}
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerAlumnosXAsignatura(long asignaturaId ,boolean soloCursando)
	{
		BeanItemContainer<AsignaturaAlumno> container = null;
		container = new BeanItemContainer<AsignaturaAlumno>(AsignaturaAlumno.class);
		container.addAll(AsignaturaAlumnoSelect.LeeAlumnosXAsignatura(asignaturaId ,soloCursando));
		return container;
	}
	
	public class ProfesorMateriaCursando{
		private long profesorId;
		private long materiaId;
		private long cursoId;
		private AsignaturaAlumno asignaturaAlumno;  
		public ProfesorMateriaCursando(){
		}
		public ProfesorMateriaCursando(long profesorId ,long materiaId ,long cursoId ,AsignaturaAlumno asignaturaAlumno){
			this.profesorId = profesorId; 
			this.materiaId= materiaId;
			this.cursoId = cursoId;
			this.asignaturaAlumno = asignaturaAlumno;
		}
		public long getProfesorId() {
			return profesorId;
		}
		public void setProfesorId(long profesorId) {
			this.profesorId = profesorId;
		}
		public long getMateriaId() {
			return materiaId;
		}
		public void setMateriaId(long materiaId) {
			this.materiaId = materiaId;
		}
		public long getCursoId() {
			return cursoId;
		}
		public void setCursoId(long cursoId) {
			this.cursoId = cursoId;
		}
		public AsignaturaAlumno getAsignaturaAlumno() {
			return asignaturaAlumno;
		}
		public void setAsignaturaAlumno(AsignaturaAlumno asignaturaAlumno) {
			this.asignaturaAlumno = asignaturaAlumno;
		}

		
	}
}
