package ar.com.trisquel.escuela.data.contenedor;

import ar.com.trisquel.escuela.data.select.AlumnoSelect;
import ar.com.trisquel.escuela.data.tablas.Alumno;

import com.vaadin.data.util.BeanItemContainer;

public class AlumnoContenedor {

	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXNombreApellidoXDniXEstadoXCurso(String nombre ,long dni ,String estado,long cursoId)
	{
		BeanItemContainer<Alumno> container = null;
		container = new BeanItemContainer<Alumno>(Alumno.class);
		container.addAll(AlumnoSelect.SelectXNombreApellidoXDniXEstadoXCurso(nombre,dni ,estado ,cursoId));
		return container;
	}
	
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXCurso(long cursoId)
	{
		BeanItemContainer<Alumno> container = null;
		container = new BeanItemContainer<Alumno>(Alumno.class);
		container.addAll(AlumnoSelect.SelectXCursoId(cursoId));
		return container;
	}
}
