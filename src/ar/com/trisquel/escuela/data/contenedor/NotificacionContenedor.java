package ar.com.trisquel.escuela.data.contenedor;

import java.util.Date;

import ar.com.trisquel.escuela.data.select.NotificacionSelect;
import ar.com.trisquel.escuela.data.tablas.Notificacion;

import com.vaadin.data.util.BeanItemContainer;

public class NotificacionContenedor {

	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXPeriodoXTituloXCanceladoXSoloNoLeido(Date fechaDesde,Date fechaHasta,String titulo,boolean cancelada,boolean soloNoLeidas)
	{
		BeanItemContainer<Notificacion> container = new BeanItemContainer<Notificacion>(Notificacion.class);
		container.addAll(NotificacionSelect.SelectXPeriodoXTituloXCanceladoXSoloNoLeido(fechaDesde, fechaHasta, titulo, cancelada, soloNoLeidas));
		return container;
	}
}
