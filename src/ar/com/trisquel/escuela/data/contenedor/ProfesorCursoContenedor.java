package ar.com.trisquel.escuela.data.contenedor;

import ar.com.trisquel.escuela.data.select.ProfesorCursoSelect;
import ar.com.trisquel.escuela.data.tablas.Curso;

import com.vaadin.data.util.BeanItemContainer;


public class ProfesorCursoContenedor {
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerCursoXProfesor(long profesorId)
	{
		BeanItemContainer<Curso> container = new BeanItemContainer<Curso>(Curso.class);
		container.addAll(ProfesorCursoSelect.SelectCursoXProfesor(profesorId));
		return container;
	}
}
