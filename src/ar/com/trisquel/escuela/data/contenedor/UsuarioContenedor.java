package ar.com.trisquel.escuela.data.contenedor;

import ar.com.trisquel.escuela.data.select.UsuarioSelect;
import ar.com.trisquel.escuela.data.tablas.Usuario;

import com.vaadin.data.util.BeanItemContainer;

public class UsuarioContenedor {

	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainer()
	{
		BeanItemContainer<Usuario> container = null;
		container = new BeanItemContainer<Usuario>(Usuario.class);
		container.addAll(UsuarioSelect.Select());
		return container;
	}
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXNombre(String nombre)
	{
		BeanItemContainer<Usuario> container = null;
		container = new BeanItemContainer<Usuario>(Usuario.class);
		container.addAll(UsuarioSelect.SelectXNombre(nombre));
		return container;
	}
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXNombreXEstado(String nombre ,int estado ,int tipo)
	{
		BeanItemContainer<Usuario> container = null;
		container = new BeanItemContainer<Usuario>(Usuario.class);
		container.addAll(UsuarioSelect.SelectXNombreXEstadoXTipo(nombre, estado, tipo));
		return container;
	}
}
