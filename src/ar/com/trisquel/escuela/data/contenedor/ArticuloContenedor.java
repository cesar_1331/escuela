package ar.com.trisquel.escuela.data.contenedor;

import ar.com.trisquel.escuela.data.select.ArticuloSelect;
import ar.com.trisquel.escuela.data.tablas.Articulo;

import com.vaadin.data.util.BeanItemContainer;

public final class ArticuloContenedor {

	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainer()
	{
		BeanItemContainer<Articulo> container = null;
		container = new BeanItemContainer<Articulo>(Articulo.class);
		container.addAll(ArticuloSelect.Select());
		return container;
	}
	
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXTitulo(String title)
	{
		BeanItemContainer<Articulo> container = null;
		container = new BeanItemContainer<Articulo>(Articulo.class);
		container.addAll(ArticuloSelect.SelectXTitulo(title));
		return container;
	}
}
