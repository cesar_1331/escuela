package ar.com.trisquel.escuela.data.contenedor;

import java.util.Date;

import ar.com.trisquel.escuela.data.select.NotificacionPersonaSelect;
import ar.com.trisquel.escuela.data.tablas.NotificacionPersona;

import com.vaadin.data.util.BeanItemContainer;

public class NotificacionPersonaContenedor {

	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXPeriodoXTituloXCanceladoXSoloNoLeido(String tipo,long personaId ,Date fechaDesde,Date fechaHasta,boolean soloNoLeidas)
	{
		BeanItemContainer<NotificacionPersona> container = new BeanItemContainer<NotificacionPersona>(NotificacionPersona.class);
		container.addAll(NotificacionPersonaSelect.SelectXPersonaXPeriodoXSoloNoLeido(tipo, personaId, fechaDesde, fechaHasta, soloNoLeidas));
		return container;
	}
}
