package ar.com.trisquel.escuela.data.contenedor;

import ar.com.trisquel.escuela.data.select.TipoComprobanteSelect;
import ar.com.trisquel.escuela.data.tablas.TipoComprobante;

import com.vaadin.data.util.BeanItemContainer;

public class TipoComprobanteContenedor {

	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXNombreXActivo(String nombre ,boolean activo)
	{
		BeanItemContainer<TipoComprobante> container = new BeanItemContainer<TipoComprobante>(TipoComprobante.class);
		container.addAll(TipoComprobanteSelect.SelectXNombreXActivo(nombre, activo));
		return container;
	}
}
