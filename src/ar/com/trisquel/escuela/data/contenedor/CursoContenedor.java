package ar.com.trisquel.escuela.data.contenedor;

import ar.com.trisquel.escuela.data.select.CursoSelect;
import ar.com.trisquel.escuela.data.tablas.Curso;

import com.vaadin.data.util.BeanItemContainer;

public class CursoContenedor {
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXGradoXTurno(int grado,String turno ,boolean activo)
	{
		BeanItemContainer<Curso> container = null;
		container = new BeanItemContainer<Curso>(Curso.class);
		container.addAll(CursoSelect.SelectXGradoXTurno(grado, turno, activo));
		return container;
	}
}
