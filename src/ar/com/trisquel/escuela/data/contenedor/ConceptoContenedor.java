package ar.com.trisquel.escuela.data.contenedor;

import ar.com.trisquel.escuela.data.select.ConceptoSelect;
import ar.com.trisquel.escuela.data.tablas.Concepto;

import com.vaadin.data.util.BeanItemContainer;

public class ConceptoContenedor {
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXNombreXActivo(String nombre ,boolean activo)
	{
		BeanItemContainer<Concepto> container = new BeanItemContainer<Concepto>(Concepto.class);
		container.addAll(ConceptoSelect.SelectXNombreXActivo(nombre, activo));
		return container;
	}
}
