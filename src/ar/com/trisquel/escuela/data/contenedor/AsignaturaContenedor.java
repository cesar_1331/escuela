package ar.com.trisquel.escuela.data.contenedor;

import ar.com.trisquel.escuela.data.select.AsignaturaSelect;
import ar.com.trisquel.escuela.data.tablas.Asignatura;

import com.vaadin.data.util.BeanItemContainer;

public class AsignaturaContenedor {
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXAnioXGradoXDivisionXProfesorXMateria(int anio, long cursoId, long profesorId,long  materiaId)
	{
		BeanItemContainer<Asignatura> container = null;
		container = new BeanItemContainer<Asignatura>(Asignatura.class);
		container.addAll(AsignaturaSelect.SelectXAnioXCursoXProfesorXMateria(anio, cursoId, profesorId, materiaId));
		return container;
	}
}
