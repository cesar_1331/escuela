package ar.com.trisquel.escuela.data.contenedor;

import ar.com.trisquel.escuela.data.select.MateriaSelect;
import ar.com.trisquel.escuela.data.select.ProfesorMateriaSelect;
import ar.com.trisquel.escuela.data.tablas.Materia;

import com.vaadin.data.util.BeanItemContainer;

public class MateriaContenedor {
	
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainer(boolean activa)
	{
		BeanItemContainer<Materia> container = null;
		container = new BeanItemContainer<Materia>(Materia.class);
		container.addAll(MateriaSelect.Select(activa));
		return container;
	}
	
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXNombre(String nombre ,boolean activo)
	{
		BeanItemContainer<Materia> container = null;
		container = new BeanItemContainer<Materia>(Materia.class);
		container.addAll(MateriaSelect.SelectXNombre(nombre ,activo));
		return container;
	}
	
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXProfesor(long profesorId)
	{
		BeanItemContainer<Materia> container = null;
		container = new BeanItemContainer<Materia>(Materia.class);
		container.addAll(ProfesorMateriaSelect.SelectMateriasXProfesor(profesorId));
		return container;
	}
}
