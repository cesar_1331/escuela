package ar.com.trisquel.escuela.data.contenedor;

import ar.com.trisquel.escuela.data.select.PermisoSelect;
import ar.com.trisquel.escuela.data.tablas.Permiso;

import com.vaadin.data.util.BeanItemContainer;

public class PermisoContenedor {

	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXUsuario(String usuarioId)
	{
		BeanItemContainer<Permiso> container = new BeanItemContainer<Permiso>(Permiso.class);
		container.addAll(PermisoSelect.SelectXUsuarioId(usuarioId));
		return container;
	}
}
