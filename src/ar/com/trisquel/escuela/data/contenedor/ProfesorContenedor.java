package ar.com.trisquel.escuela.data.contenedor;

import ar.com.trisquel.escuela.data.select.ProfesorMateriaSelect;
import ar.com.trisquel.escuela.data.select.ProfesorSelect;
import ar.com.trisquel.escuela.data.tablas.Profesor;

import com.vaadin.data.util.BeanItemContainer;

public class ProfesorContenedor {
	
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXNombreApellidoXActivo(String nombre ,boolean soloPreceptores ,boolean activo)
	{
		BeanItemContainer<Profesor> container = null;
		container = new BeanItemContainer<Profesor>(Profesor.class);
		container.addAll(ProfesorSelect.SelectXNombreApellidoXActivo(nombre ,soloPreceptores ,activo));
		return container;
	}
	
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXMateria(long materiaId)
	{
		BeanItemContainer<Profesor> container = null;
		container = new BeanItemContainer<Profesor>(Profesor.class);
		container.addAll(ProfesorMateriaSelect.SelectProfesorXMateria(materiaId));
		return container;
	}
}
