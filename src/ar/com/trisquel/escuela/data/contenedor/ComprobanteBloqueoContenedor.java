package ar.com.trisquel.escuela.data.contenedor;

import ar.com.trisquel.escuela.data.select.ComprobanteBloqueoSelect;
import ar.com.trisquel.escuela.data.tablas.ComprobanteBloqueo;

import com.vaadin.data.util.BeanItemContainer;

public class ComprobanteBloqueoContenedor {
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXAnio(int anio)
	{
		BeanItemContainer<ComprobanteBloqueo> container = new BeanItemContainer<ComprobanteBloqueo>(ComprobanteBloqueo.class);
		container.addAll(ComprobanteBloqueoSelect.SelectXAnioXMes(anio));
		return container;
	}
}
