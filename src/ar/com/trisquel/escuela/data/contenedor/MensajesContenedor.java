package ar.com.trisquel.escuela.data.contenedor;

import ar.com.trisquel.escuela.data.select.MensajesSelect;
import ar.com.trisquel.escuela.data.tablas.Mensajes;

import com.vaadin.data.util.BeanItemContainer;

public class MensajesContenedor {

	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXNombre(String nombre,boolean leido)
	{
		BeanItemContainer<Mensajes> container = new BeanItemContainer<Mensajes>(Mensajes.class);
		container.addAll(MensajesSelect.SelectXNombre(nombre, leido));
		return container;
	}
}
