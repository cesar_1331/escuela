package ar.com.trisquel.escuela.data.contenedor;

import ar.com.trisquel.escuela.data.select.PersonasSelect;
import ar.com.trisquel.escuela.data.tablas.Personas;

import com.vaadin.data.util.BeanItemContainer;

public class PersonasContenedor {

	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXNombreXActivo(String nombre ,boolean soloCliente,boolean soloProveedor ,boolean activo)
	{
		BeanItemContainer<Personas> container = new BeanItemContainer<Personas>(Personas.class);
		container.addAll(PersonasSelect.SelectXNombreXClienteXProveedorXActivo(nombre, soloCliente, soloProveedor, activo));
		return container;
	}
}
