package ar.com.trisquel.escuela.data.contenedor;

import java.util.Date;

import ar.com.trisquel.escuela.data.select.ComprobanteSelect;
import ar.com.trisquel.escuela.data.tablas.Comprobante;

import com.vaadin.data.util.BeanItemContainer;

public class ComprobanteContenedor {
	
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXPeriodoXTipoXNumeroXPersona(Date fechaDesde,Date fechaHasta,int tipoId,long numero,long personaId,boolean anulado)
	{
		BeanItemContainer<Comprobante> container = new BeanItemContainer<Comprobante>(Comprobante.class);
		container.addAll(ComprobanteSelect.SelectXPeriodoXTipoXNumeroXPersona(fechaDesde, fechaHasta, tipoId, numero, personaId, anulado));
		return container;
	}
	
	@SuppressWarnings("rawtypes")
	public static BeanItemContainer LeeContainerXPeriodo(Date fechaDesde,Date fechaHasta)
	{
		BeanItemContainer<Comprobante> container = new BeanItemContainer<Comprobante>(Comprobante.class);
		container.addAll(ComprobanteSelect.SelectXPeriodo(fechaDesde, fechaHasta));
		return container;
	}
}
