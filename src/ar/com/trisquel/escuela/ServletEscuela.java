package ar.com.trisquel.escuela;

import javax.servlet.ServletException;

import org.jsoup.nodes.Comment;

import com.vaadin.server.BootstrapFragmentResponse;
import com.vaadin.server.BootstrapListener;
import com.vaadin.server.BootstrapPageResponse;
import com.vaadin.server.CustomizedSystemMessages;
import com.vaadin.server.ServiceException;
import com.vaadin.server.SessionInitEvent;
import com.vaadin.server.SessionInitListener;
import com.vaadin.server.SystemMessages;
import com.vaadin.server.SystemMessagesInfo;
import com.vaadin.server.SystemMessagesProvider;
import com.vaadin.server.VaadinServlet;

@SuppressWarnings("serial")
public class ServletEscuela extends VaadinServlet implements SessionInitListener ,BootstrapListener,SystemMessagesProvider {

	public ServletEscuela() {
	}
	@Override
	protected void servletInitialized() throws ServletException {
		super.servletInitialized();
		getService().addSessionInitListener(this);
		getService().setSystemMessagesProvider(this);
	}

	@Override
	public void sessionInit(SessionInitEvent event) throws ServiceException {
		event.getSession().addBootstrapListener(this);
	}

	@Override
	public void modifyBootstrapFragment(BootstrapFragmentResponse response) {		
	}

	@Override
	public void modifyBootstrapPage(BootstrapPageResponse response) {
		response.setHeader("X-Powered-By", "Vaadin");
		response.getDocument().body().appendChild(new Comment("Copyrigth Trisquel Software 2013", ""));
		response.getDocument().head().prependElement("meta").attr("name", "viewport").attr("content", "width=device-width");
		response.getDocument().head().prependElement("meta").attr("name", "description").attr("content", "Administre su escuela con un solo sistema. Administre las notas de los alumnos,notificaciones y los ingresos y egresos de dinero de su institucion");
		response.getDocument().head().prependElement("meta").attr("name", "google").attr("content", "escuela,administracion,cursos,college ,school,alumno,profesor,online,ingresos,egresos");
		response.getDocument().head().prependElement("meta").attr("name", "keywords").attr("content", "escuela,administracion,cursos,college ,school,alumno,profesor,online,ingresos,egresos");
		response.getDocument().head().prependElement("meta").attr("name", "robots").attr("content", "index, follow");
		response.getDocument().head().prependElement("meta").attr("name", "author").attr("content", "Trisquel Software");
		response.getDocument().head().prependElement("meta").attr("name", "Cache-Control").attr("content", "max-age=3600");
	}

	@Override
	public SystemMessages getSystemMessages(SystemMessagesInfo systemMessagesInfo) {
		CustomizedSystemMessages messages = new CustomizedSystemMessages();
		/**
		 * Error de comunicacion
		 */
		messages.setCommunicationErrorCaption("Error de comunicacion<hr>");
		messages.setCommunicationErrorMessage("Se produjo error de comunicacion con el servidor web. Recargue la pagina web");
		messages.setCommunicationErrorNotificationEnabled(true);
		/**
		 * Sesion expiro
		 */
		messages.setSessionExpiredCaption("Ha expirado el tiempo de la sesion<hr>");
		messages.setSessionExpiredMessage("La sesi�n ha caducada por tiempo de inactividad. Recargue la pagina web");
		messages.setCommunicationErrorNotificationEnabled(true);
		/**
		 * Error interno
		 */
		messages.setInternalErrorCaption("Error interno de la aplicacion<hr>");
		messages.setInternalErrorMessage("Se produjo un error interno en la aplicacion. Recargue la pagina web");
		messages.setCommunicationErrorNotificationEnabled(true);
		return messages;
	}


}
